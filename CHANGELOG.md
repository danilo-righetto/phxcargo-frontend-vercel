# Changelog
Documentação das versões do sistema `phcargo-frontend`.

## [Unreleased]

### Changed

## [1.1.0] - 2021-03-04
### Changed
- Ajuste - MASTER
- Ajustes - Roles
- Ajustes - Financeiro
- Ajustes - House
- Cadastro de House
- Cadastro de Tarifário
- Cadastro de Contrato
- feature: Atualizacao -  Ajustes - Produção e Nomenclatura

## [1.0.0] - 2020-05-20
### Added
- Iniciando o projeto