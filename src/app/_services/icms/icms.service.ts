import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { Icms, IcmsListingRequest } from "app/_interfaces/icms/listing-element";

@Injectable()
export class IcmsService extends IndexService {

    protected endpoint: string = '/icms';

    /**
     * Icms[]
     * 
     * @param data 
     */
    protected mapTo(data: any) {

        const response: Array<Icms> = [];

        data.forEach((icms: IcmsListingRequest) => {

            response.push(new Icms(icms));
        });

        return response;
    }
}