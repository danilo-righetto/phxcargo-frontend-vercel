
import { Injectable } from "@angular/core";
import { HttpAuthenticateService } from "./_authentication/http.authenticate.service";

@Injectable()
export class ShowService {

    constructor(
        private http: HttpAuthenticateService
    ) { }

    public fetchData(url) {

        const data = this.http.get(url)
            .subscribe(res => console.log(res));
    }
}