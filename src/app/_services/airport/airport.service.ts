import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { AirportListingElement, AirportListingRequest } from '../../_interfaces/airport/listing-element';

@Injectable()
export class AirportService extends IndexService {
    protected endpoint = "/airports"



    /**
     * Airport[]
     * 
     * @param data
     */
    protected mapTo(data: Array<AirportListingRequest>) {

        const response: Array<AirportListingElement> = [];

        data.forEach((airport: AirportListingRequest) => {

            response.push({
                'ID': airport.id,
                'Nome do Aeroporto': airport.name,
                'Sigla do Aeroporto': airport.code,
            });
        });

        return response;
    }
}