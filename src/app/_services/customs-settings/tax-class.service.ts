import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";

@Injectable()
export class TaxClasseService extends IndexService {
    
    protected endpoint: string = '/customs-tax-classes';
    protected sort: string = 'id';
}