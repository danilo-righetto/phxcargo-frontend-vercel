import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";

@Injectable()
export class CustomsAdministratorsService extends IndexService {

    protected endpoint: string = '/customs-administrators';
    protected sort: string = 'id';
}