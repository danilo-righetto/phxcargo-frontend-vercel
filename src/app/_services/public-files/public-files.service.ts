import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";

@Injectable()
export class PublicFilesService extends IndexService {

    protected sort: string = '-updated_at';

    /**
     * 
     * @param data 
     */
    protected mapTo(data: any) {

        const response: Array<any> = [];

        data.forEach((receipt: any) => {
            response.push({ ...receipt, ...{ url: receipt.file_url } });
        });

        return response;
    }
}