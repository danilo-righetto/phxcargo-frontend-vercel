import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { Role, RoleListingRequest } from 'app/_interfaces/role/listing-element';


@Injectable()
export class RoleService extends IndexService {

    protected endpoint: string = "/roles";

    /**
     * Role[]
     *
     * @param data
     */
    protected mapTo(data: any) {

        const response: Array<Role> = [];

        data.forEach((role: RoleListingRequest) => {
            response.push(new Role(role));
        });

        return response;
    }
}