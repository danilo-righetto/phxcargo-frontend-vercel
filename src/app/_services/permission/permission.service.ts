import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";

@Injectable()
export class PermissionService extends IndexService {

    protected endpoint: string = "/permissions";

    protected sort = 'resource';

    /**
     * Permission[]
     *
     * @param data
     */
    protected mapTo(data: any) {

        const response: Array<any> = [];

        data.forEach(permission => {

            response.push({
                id: permission.id,
                name: permission.name,
                display_name: permission.display_name,
                description: permission.description,
            })
        });

        return response;
    }
}