import { CurrencyListingElement, CurrencyRequest } from 'app/_interfaces/currency/currency.service.interface';
import { Injectable } from '@angular/core';
import { IndexService } from '../index.service';

@Injectable()
export class CurrencyService extends IndexService {

    protected endpoint = '/currencies';

    

    /**
     * Currency[]
     * 
     * @param data 
     */
    protected mapTo(data: Array<CurrencyRequest>) {

        const response: Array<CurrencyListingElement> = [];

        data.forEach((currency: CurrencyRequest) => {

            response.push({
                'ID': currency.id,
                'Símbolo da Moeda': currency.symbol,
                'Código da Moeda': currency.code,
                'Nome da Moeda': currency.name,
            });
        });

        return response;
    }
}