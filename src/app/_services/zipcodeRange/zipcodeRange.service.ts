import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { ZipCodeRangeListingRequest, ZipCodeRangeListingElement, ZipCodeRange } from "app/_interfaces/zipcodeRange/zipcode.interface";

@Injectable()
export class ZipCodeRangeService extends IndexService {

  protected endpoint: string = '/zipcode-ranges';

  /**
   * ZipCodeRange[]
   * 
   * @param data 
   */
  protected mapTo(data: Array<any>) {

    const response: Array<ZipCodeRangeListingElement> = [];

    data.forEach((range: ZipCodeRangeListingRequest) => {

      response.push(new ZipCodeRange(range));

    });

    return response;
  }


}