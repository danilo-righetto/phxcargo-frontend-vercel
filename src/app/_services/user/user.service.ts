import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
//import { User,  } from 'app/_interfaces/user.interface';
import { UserListingRequest, User } from './../../_interfaces/user/listing-element';

@Injectable()
export class UserService extends IndexService {

    public endpoint: string = '/users';

    /**
     * User[]
     * 
     * @param data 
     */
    protected mapTo(data: any) {

        const response: Array<User> = [];

        data.forEach((user: UserListingRequest) => {

            response.push(new User(user));
        });

        return response;
    }
}