import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { AirlineListingRequest, AirlineListingElement } from '../../_interfaces/airline/listing-element';

@Injectable()
export class AirlineService extends IndexService {
    protected endpoint = "/airlines"

    /**
     * Master[]
     * 
     * @param data
     */
    protected mapTo(data: Array<AirlineListingRequest>) {

        const response: Array<AirlineListingElement> = [];
        
        data.forEach((airline: AirlineListingRequest) => {

            response.push({
                'ID': airline.id,
                'Companhia Aérea': airline.name,
            });
        });

        return response;
    }
}