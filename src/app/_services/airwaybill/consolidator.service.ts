import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { ConsolidatorListingElement, ConsolidatorListingRequest } from '../../_interfaces/airwaybill/consolidator.listing';


@Injectable()
export class ConsolidatorService extends IndexService {

    protected endpoint: string = '/airwaybills/consolidators';

    /**
     * Consolidator[]
     * 
     * @param data
     */
    protected mapTo(data: Array<ConsolidatorListingRequest>) {

        const response: Array<ConsolidatorListingElement> = [];

        data.forEach((consolidator: ConsolidatorListingRequest) => {

            response.push({
                'ID': consolidator.id,
                'Código da Bag': consolidator.code,
                'Origem': consolidator.origin,
                'Nome da Empresa': consolidator.company,
                'Destino da Bag': consolidator.destination,
                'Data': consolidator.date,
                "Empresa": consolidator.company,
            });
        });

        return response;
    }
}



