import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { AirwaybillRequest, Airwaybill, HouseListingElement } from "app/_interfaces/airwaybill/house.airwaybill";

@Injectable()
export class AirwaybillService extends IndexService {

    protected endpoint: string = '/airwaybills/house';

    /**
     * House[]
     * 
     * @param data 
     */
    protected mapTo(data: Array<AirwaybillRequest>) {

        const response: Array<HouseListingElement> = [];

        data.forEach((house: AirwaybillRequest) => {

            response.push(new Airwaybill(house));
        });

        return response;
    }
}