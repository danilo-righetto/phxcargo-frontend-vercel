import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { MasterAirwaybillRequest, MasterAirwaybill } from "app/_interfaces/airwaybill/master.interface";

@Injectable()
export class MasterAirwaybillService extends IndexService {

    protected endpoint: string = '/airwaybills/master';

    /**
     * Master[]
     * 
     * @param data 
     */
    protected mapTo(data: any) {

        const response: Array<MasterAirwaybill> = [];

        data.forEach((master: MasterAirwaybillRequest) => {

            response.push(new MasterAirwaybill(master));
        });

        return response;
    }
}