import { IndexService } from './../index.service';
import { Injectable } from '@angular/core';


@Injectable()
export class DebitNoteService extends IndexService {

  protected endpoint: string = '/debit-note';

  /**
   * Debits[]
   * 
   * @param data 
   */
  protected mapTo(data: any) {
    const response: Array<any> = [];

    data.forEach(debit => {

      response.push({
        id: debit.id,
        code: debit.code,
        paid_at: debit.paid_at,
        fine_code: debit.fine_code,
        amount: debit.amount,

      });
    });

    return response;
  }
}


