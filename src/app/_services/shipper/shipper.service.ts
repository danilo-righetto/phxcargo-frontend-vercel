import { ShipperListingRequest, ShipperListingElement } from 'app/_interfaces/shipper/listing-element';
import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";

@Injectable()
export class ShipperService extends IndexService {

    protected endpoint: string = '/shippers';

    /**
     * Shipper[]
     * 
     * @param data 
     */
    protected mapTo(data: Array<ShipperListingRequest>) {

        const response: Array<ShipperListingElement> = [];

        data.forEach((shipper: ShipperListingRequest) => {

            response.push({
                'ID': shipper.id,
                'Nome do Embarcador': shipper.name,
                'Documento do Embarcador': shipper.documents ? shipper.documents[0].number : 's/n',
            });
        });

        return response;
    }
}