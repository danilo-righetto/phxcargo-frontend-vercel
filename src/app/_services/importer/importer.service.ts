import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { ImporterListingElement, ImporterListingRequest } from "app/_interfaces/importer/importer.interface";

@Injectable()
export class ImporterService extends IndexService {

    protected endpoint: string = '/importers';

    /**
     * Importer[]
     * 
     * @param data 
     */
    protected mapTo(data: Array<ImporterListingRequest>) {

        const response: Array<ImporterListingElement> = [];

        data.forEach((importer: ImporterListingRequest) => {

            response.push({
                'ID': importer.id,
                'Nome do Importador': importer.name,
                'Documento do Importador': importer.documents ? importer.documents[0].number : 's/n',
            });
        });

        return response;
    }
}