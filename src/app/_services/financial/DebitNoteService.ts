import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { DebitNoteListingElement, DebitNoteListingRequest } from '../../_interfaces/financial/debit-note-listing';

@Injectable()
export class DebitNoteService extends IndexService {

    protected endpoint: string = '/financial/debit-note';

    /**
     * House[]
     *
     * @param data
     */
    protected mapTo(data: Array<DebitNoteListingRequest>) {

        const response: Array<DebitNoteListingElement> = [];

        data.forEach((fine: DebitNoteListingRequest) => {

            response.push({
                'ID': fine.id,
                'Código': fine.code,
                'Valor Total': fine.total_amount,
                'Empresa': fine.company,
                'Pago': fine.paid_at,
                'Linha Digital': fine.digitable_line,
            });
        });

        return response;
    }

}