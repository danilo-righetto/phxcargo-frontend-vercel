import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { Invoice, InvoiceRequest, InvoiceListingElement } from "app/_interfaces/financial/invoice.interface";

@Injectable()
export class InvoiceService extends IndexService {

    protected endpoint: string = '/financial/invoices';

    /**
     * House[]
     *
     * @param data
     */
    protected mapTo(data: any) {

        const response: Array<any> = [];

        data.forEach((invoice: InvoiceRequest) => {

            response.push(new Invoice(invoice));
        });

        return response;
    }

}