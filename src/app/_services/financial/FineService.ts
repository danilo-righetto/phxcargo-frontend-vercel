import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import {FineListingElement, FineListingRequest} from '../../_interfaces/financial/fine-listing';

@Injectable()
export class FineService extends IndexService {

    protected endpoint: string = '/financial/fines';

    /**
     * House[]
     *
     * @param data
     */
    protected mapTo(data: Array<FineListingRequest>) {

        const response: Array<FineListingElement> = [];

        data.forEach((fine: FineListingRequest) => {

            response.push({
                'ID': fine.id,
                'Código': fine.code,
                'Descrição da Multa': fine.fine_description,
                'Código da Multa': fine.fine_code,
                'Quantidade': fine.amount,
                'Data de Criação': fine.created_at,
            });
        });

        return response;
    }

}