import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";

@Injectable()
export class ManifestFinesService extends IndexService {

    /**
     * @param data 
     */
    protected mapTo(data: any) {

        const response: Array<any> = [];

        data.forEach((fine: any) => {
            response.push({
                'Codigo do House': fine.air_waybill.external_id,
                'Categoria': fine.category,
                'Tributável - Real': fine.dutiable_brl,
                'Tributável - Dolar': fine.dutiable_usd,
                'Tributável Total - Real': fine.dutiable_total_brl,
                'Tributável Total - Dólar': fine.dutiable_total_usd,
                'Frete - Real': fine.dutiable_freight_brl,
                'Frete - Dólar': fine.dutiable_freight_usd,
                'Multas': fine.dutiable_fine_brl,
                'Total': fine.amount,
                'Taxa do Dólar': fine.dutiable_exchange_rate,
                'Taxa do Dólar (Real)': fine.dutiable_exchange_rate_brl,
                code: fine.code,
            });
        });

        return response;
    }
}