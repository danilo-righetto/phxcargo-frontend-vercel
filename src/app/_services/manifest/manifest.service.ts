import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { ManifestRequest, Manifest } from "app/_interfaces/airwaybill/manifest";

@Injectable()
export class ManifestService extends IndexService {

    protected endpoint: string = '/manifests';

    /**
     * 
     * @param ids 
     */
    public create(selectedMaster: Array<any>) {

        return this.http.post(
            this.endpoint, {
                code: '001',
                description: 'Generated',
                masters: selectedMaster
            });
    }

    /**
     * Manifest[]
     * 
     * @param data 
     */
    protected mapTo(data: any) {

        const response: Array<any> = [];

        data.forEach((manifest: ManifestRequest) => {
            response.push(new Manifest(manifest));
        });

        return response;
    }
}