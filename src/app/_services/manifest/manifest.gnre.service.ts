import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";

@Injectable()
export class ManifestGnreService extends IndexService {

    /**
     * @param data 
     */
    protected mapTo(data: any) {

        const response: Array<any> = [];

        data.forEach((fine: any) => {
            response.push({
                awb_code: fine.air_waybill.code,
                awb_number: fine.air_waybill.external_id,
                category: fine.category,
                code: fine.code,
                amount: fine.amount,
                dutiable_brl: fine.dutiable_brl,
                dutiable_usd: fine.dutiable_usd,
                dutiable_exchange_rate: fine.dutiable_exchange_rate,
                reference_tax: fine.reference_tax,
                receipt_code: fine.receipt_code,
            });
        });

        return response;
    }
}