import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";

@Injectable()
export class ManifestEventsService extends IndexService {

    /**
     * @param data 
     */
    protected mapTo(data: any) {

        const response: Array<any> = [];

        data.forEach((event: any) => {
            response.push({
                ...event,
                ...{
                    awb_code: event.air_waybill.code,
                    awb_number: event.air_waybill.external_id
                }
            });
        });

        return response;
    }
}