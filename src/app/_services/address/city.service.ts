import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { CityRequest } from "app/_interfaces/address/city.interface";

@Injectable()
export class CityService extends IndexService {

    protected endpoint: string = '/cities';

    protected sort: string = 'id';

    /**
     * Cities[]
     * 
     * @param data 
     */
    protected mapTo(data: any) {
        const response: Array<any> = [];

        data.forEach(cities => {

            response.push({
                id: cities.id,
                uf: cities.uf,
                code: cities.code,
                name: cities.name,
                ibge_code: cities.ibge_code
            });
        });

        return response;
        }
    }
