import { Injectable } from '@angular/core'
import {
    MatSnackBar,
    MatSnackBarHorizontalPosition,
    MatSnackBarVerticalPosition,
} from '@angular/material';

/**
 * @title Snack-bar with configurable position
 */
@Injectable()
export class NotificationService {

    private horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    private verticalPosition: MatSnackBarVerticalPosition = 'top';

    private buttonClose: string = 'x';

    /**
     * Constructor
     * 
     * @param snackBar 
     */
    constructor(public snackBar: MatSnackBar) { }

    /**
     * 
     * @param message 
     * @param typeAlert 
     */
    public notify(message: string, typeAlert: string = 'success'): void {

        this.snackBar.open(message, this.buttonClose, {
            duration: 9000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            panelClass: [typeAlert + '-snackbar']
        });
    }
}