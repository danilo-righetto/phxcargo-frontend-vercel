import { throwError as observableThrowError, Observable } from 'rxjs';

import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core'

import { environment } from '../../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { NotificationService } from '../notification/notification.service';

import { ResponseInterface } from 'app/_interfaces/Http/response.interface';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class HttpAuthenticateService {

    private contentType: string = 'application/json';

    constructor(
        private http: HttpClient,
        private authenticationService: AuthenticationService,
        private notification: NotificationService
    ) { }

    /**
     * 
     * @param url 
     * @param body 
     */
    public post(url: string, body = {}): Observable<HttpResponse<any>> {
        return this.parseRequest(
            this.http.post<any>(this.endpoint(url), body, this.options(body))
        );
    }

    /**
     * 
     * @param url 
     */
    public get(url: string): Observable<HttpResponse<any>> {
        return this.parseRequest(
            this.http.get<any>(this.endpoint(url), this.options())
        );
    }

    /**
     * 
     * @param url 
     * @param body 
     */
    public put(url: string, body = {}): Observable<HttpResponse<any>> {

        return this.parseRequest(
            this.http.put<any>(this.endpoint(url), body, this.options(body))
        );
    }

    /**
     * 
     * @param url 
     */
    public delete(url: string, body = {}): Observable<HttpResponse<any>> {

        return this.parseRequest(
            this.http.delete<any>(this.endpoint(url), this.options(body))
        );
    }

    /**
     * 
     * @param url 
     */
    public endpoint(url: string): string {

        return environment.apiUrl + environment.apiPrefix + url;
    }

    /**
     * Build request options
     * 
     * @param body 
     */
    private options(body: any = {}) {

        return {
            headers: this.header(),
            body: body
        };
    }

    /**
     * Generate authenticated request headers
     */
    public header(): HttpHeaders {

        return new HttpHeaders({
            // 'Access-Control-Allow-Origin': '*',
            // 'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
            // 'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret',
            'Content-Type': this.contentType,
            'Authorization': this.authenticationService.getAuthentication()
        });
    }

    /**
     * Parse result 
     * 
     * @param request 
     */
    private parseRequest(request: Observable<any>): Observable<any> {

        return request.pipe(
            map(res => {
                let status = null;
                if (res && res.status == ResponseInterface.HTTP_CREATED) {
                    status = ResponseInterface.HTTP_CREATED
                }
                if (res && res.status == ResponseInterface.HTTP_NO_CONTENT) {
                    status = ResponseInterface.HTTP_NO_CONTENT
                }

                if (status)
                    this.notification.notify(ResponseInterface.statusTexts[status]);

                return res;
            }),
            catchError((err: HttpErrorResponse) => {
                let message = 'Something happened';

                if (err.error) message = err.error.message;

                if (ResponseInterface.statusTexts[err.status]) {
                    message = ResponseInterface.statusTexts[err.status];
                }

                this.notification.notify(message, 'error');

                if (err.status == ResponseInterface.HTTP_UNAUTHORIZED) {

                    this.authenticationService.logout();
                }

                return observableThrowError(err.statusText);
            }));
    }

    /**
     * 
     * @param type 
     */
    public setContentType(type: string) {
        this.contentType = type
    }

    public getToken() {
        return this.authenticationService.getAuthentication();
    }
}