import { Injectable } from '@angular/core'
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ResetPasswordForgotService {

    constructor(private http: HttpClient) { }
    
    public requestRecoveryLink(email: string, token: string, password: string) {

        return this.http.post(
            `${environment.apiUrl}/public/users/reset-password-forgot/`+`${token}`,
            {   
                email: email,
                token: token,
                password: password,
            }
        );
    }

    public requestToken(token: string) {

        return this.http.get(
            `${environment.apiUrl}/users/password-recover/`+`${token}`,
            {

            }
        );

   
    };

}