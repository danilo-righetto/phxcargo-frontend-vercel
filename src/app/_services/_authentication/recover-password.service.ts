import { Injectable } from '@angular/core'
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RecoverPasswordService {

    constructor(private http: HttpClient) { }

    public requestRecoveryLink(email: string) {
        return this.request(email);
    }

    private request(email) {
        return this.http.post(
            `${environment.apiUrl}/public/users/password-recover`,
            {
                email: email, 
            }
        );
    }
}