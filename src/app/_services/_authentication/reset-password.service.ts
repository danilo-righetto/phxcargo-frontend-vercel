import { Injectable } from '@angular/core'
import { environment } from '../../../environments/environment'; 
import { HttpClient, HttpHeaders } from '@angular/common/http'; 


@Injectable()
export class ResetPasswordService {

    constructor(private http: HttpClient ) { }k

    public requestRecoveryLink(email: string, password: string) {
        
        let headers = new HttpHeaders()
 
        headers = this.header(headers);

        return this.http.post(
            `${environment.apiUrl}/users/reset-password`,
            {
                email: email, 
                password: password
            },
            {
                headers
            }
        );
    }

    private header(headers) {

        let token = this.token();
        
        headers=headers.append('content-type','application/json')
        headers=headers.append('Access-Control-Allow-Origin', '*')
        headers=headers.append('content-type','application/x-www-form-urlencoded')
        headers=headers.append('Authorization', 'Bearer ' + token) 

        return headers;
    }

    private token(){
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        const token = currentUser.auth['access_token'];

        return token;
    }
}