import { Injectable } from '@angular/core'

import { environment } from '../../../environments/environment';
import { User } from 'app/_interfaces/user.interface';
import { Router } from '@angular/router';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {

    private auth: any;
    private user: any;

    constructor(
        private http: HttpClient,
        private router: Router,
        private navgatiionService: FuseNavigationService
    ) { }

    public setAuth(auth) {
        this.auth = auth;
    }

    public setUser(user) {
        this.user = user;
        this.storeUserDetails()
    }

    public getUser() {
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));

        if (currentUser.user) {
            return currentUser.user;
        }
        return false;
    }

    /**
     * @param username
     * @param password
     */
    public login(username: string, password: string) {

        return this.http.post<User>(
            `${environment.apiUrl}/oauth/token`,
            {
                grant_type: 'password',
                client_id: environment.apiOauthClientId,
                client_secret: environment.apiOauthClientSecret,
                username: username,
                password: password,
                scope: '*'
            }
        )
            .map((user: User) => {
                if (user && user.access_token) {
                    this.auth = user;

                    // TODO calculate token expiration date
                    this.storeUserDetails();
                }

                return user;
            });
    }

    /**
     * Remove user from local storage to log user out
     */
    public logout(): void {

        localStorage.removeItem('currentUser');
        this.router.navigate(['login']);
        this.navgatiionService.unregister('main')
    }

    /**
     * Alter user key  
     */  
    public resetPassword() {
        if (!this.isAuthenticated()) {
            this.logout();
        } else {
            return this.router.navigate(['reset-password']);
        }
    }

    /**
     * Check if user exists
     */
    public isAuthenticated(): boolean {

        if (localStorage.getItem('currentUser')) {
            return true;
        }
        return false;
    }

    /**
     * @return string with current token_type and access_token
     */
    public getAuthentication() {

        if (!this.isAuthenticated()) {
            this.logout();
        }

        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        return `${currentUser.auth.token_type} ${currentUser.auth.access_token}`;
    }

    /**
     * Store user details and jwt token in local storage
     * @param user 
     */
    private storeUserDetails(): void {

        localStorage.setItem('currentUser', JSON.stringify({
            auth: this.auth,
            user: this.user
        }));
    }

    /**
     * Get Authenticated user information
     * 
     */
    public currentUserInformation() {

        //return this.http.get(
        //    `${environment.apiUrl}/users/authenticated-user`, this.options()
        //).map((user: any) => user.data)

         return this.http.get(
             `${environment.apiUrl}/users/authenticated-user`, this.options()
         ).map((user: any) => user.data)
    }

    /**
     * Build request options
     * 
     * @param body 
     */
    private options(body: any = {}): any {

        return {
            headers: this.header(),
            body: body
        };
    }

    /**
     * Generate authenticated request headers
     * 
     */
    private header(): HttpHeaders {

        return new HttpHeaders({
            'Content-Type': 'application/json',
            // 'Access-Control-Allow-Origin': '*',
            // 'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
            // 'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret',
            'Authorization': this.getAuthentication()
        });
    }

    public hasPermission(permission: string) {

        if (!this.isAuthenticated()) {
            return false;
        }

        const currentUser = JSON.parse(localStorage.getItem('currentUser'));

        if (!currentUser.user) {
            return false;
        }

        const role = currentUser.user.role;

        if (!role) {
            return false
        }

        if (role.admin) {
            return true;
        }

        let result = false;
        role.permissions.forEach(item => {
            if (item.name == permission) {
                result = true;
            }
            return;
        });

        return result;
    }
}