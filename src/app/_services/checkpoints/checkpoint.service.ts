import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { CheckpointListingRequest, CheckpointListingElement } from "app/_interfaces/checkpoint/checkpoint.interface";

@Injectable()
export class CheckpointService extends IndexService {

    protected endpoint = "/checkpoints"

    /**
     * Checkpoint[]
     * 
     * @param data
     */
    protected mapTo(data: Array<CheckpointListingRequest>) {

        const response: Array<CheckpointListingElement> = [];

        data.forEach((checkpoint: CheckpointListingRequest) => {

            response.push(
                {
                    id: checkpoint.id,
                    code: checkpoint.name,
                    name: checkpoint.code,
                    'ID': checkpoint.id,
                    'Nome do Checkpoint': checkpoint.name,
                    'Código do Checkpoint': checkpoint.code
                }
            );
        });

        return response;
    }
}