import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { FlightListingElement, FlightListingRequest } from "app/_interfaces/flight/listing-element";


@Injectable()
export class FlightService extends IndexService {

    protected endpoint: string = '/flights'

    /**
     * Flight[]
     * 
     * @param data 
     */
    protected mapTo(data: Array<FlightListingRequest>) {

        const response: Array<FlightListingElement> = [];

        data.forEach((flight: FlightListingRequest) => {



            if (flight.destination !== undefined) {
                if (flight.destination.airportCode !== undefined) {
                    var flightDestinationCode = flight.destination.airportCode;
                }
            }


            if (flight.origin !== undefined) {
                if (flight.origin.airportCode !== undefined) {
                    var flightOriginCode = flight.origin.airportCode;
                }
            }

            if (flight.airline !== undefined) {
                var nameFlight = flight.airline.name;
            }

            response.push({
                'ID': flight.id,
                'Número do Voo': flight.number,
                'Número Alternativo': flight.alternative_number ? flight.alternative_number : 's/n',
                'Destino e Origem': (flightDestinationCode ? flightDestinationCode : '') + " - " + (flightOriginCode ? flightOriginCode : ''),
                'Companhia Aérea': nameFlight ? nameFlight : 's/n',
            });

        });

        return response;

    }
}



