import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";

@Injectable()
export class CountryService extends IndexService {

    protected endpoint = "/countries";

    public sort = "id";

    /**
     * Country[]
     * 
     * @param data 
     */
    protected mapTo(data: any) {

        const response: Array<any> = [];

        data.forEach(country => {

            response.push({
                id: country.id,
                name: country.name,
                code: country.code,
                initials: country.initials,
            });
        });

        return response;
    }
}