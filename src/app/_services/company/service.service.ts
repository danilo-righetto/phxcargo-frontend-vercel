import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { ServiceListingRequest, ServiceListingElement } from "app/_interfaces/contract/contract_service.interface";

@Injectable()
export class ServiceService extends IndexService {

    protected endpoint: string = '/services';

    /**
     * Service[]
     * 
     * @param data 
     */
    protected mapTo(data: Array<ServiceListingRequest>) {

        const response: Array<ServiceListingElement> = [];

        data.forEach((service: ServiceListingRequest) => {

            response.push({
                'ID': service.id,
                'Descrição': service.description,
                'Checkpoint': service.checkpoint.code,
                'Nível de Serviço': service.service_level_agreement,
                'Tipo de Carga': service.charge_type,
                'Aproximação': service.approach,
                'Quantidade': service.amount,
                checkpoint: service.checkpoint.id,
                description: service.description,
                service_level_agreement: service.service_level_agreement,
                charge_type: service.charge_type,
                approach: service.approach,
                amount: service.amount
            })
        });

        return response;
    }
}