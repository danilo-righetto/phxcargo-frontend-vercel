import { CompanyListingRequest, CompanyListingElement } from 'app/_interfaces/company/listing-element';
import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";


@Injectable()
export class CompanyService extends IndexService {

    protected endpoint: string = '/companies';

    /**
     * Company[]
     * 
     * @param data 
     */
    protected mapTo(data: Array<CompanyListingRequest>) {

        const response: Array<CompanyListingElement> = [];

        data.forEach((company: CompanyListingRequest) => {
            if (company.status == "ACTIVE"){
                company.status = "Ativa";
            } else if(company.status == "DISABLED"){
                company.status = "Bloqueada";
            } else (
                company.status = "s/n"
            )
            response.push({
                'ID': company.id,
                'Código': company.code,
                'Nome da Empresa': company.name,
                'Status': company.status,
                type_of_customer_relationship: company.type_of_customer_relationship,
                type_of_supplier_relationship: company.type_of_supplier_relationship,
                type_of_carrier_relationship: company.type_of_carrier_relationship,
                fantasy_name: company.fantasy_name,
                document: company.documents,
                addresses: company.addresses,
                billing_addresses: company.addresses,
            });
            
        });

        return response;
    }
}