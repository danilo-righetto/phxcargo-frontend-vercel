import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";
import { Contract, ContractRequest } from "app/_interfaces/contract/contract.interface";

@Injectable()
export class ContractService extends IndexService {

    protected endpoint: string = '/contracts';

    /**
     * Contract[]
     * 
     * @param data 
     */
    protected mapTo(data: any) {

        const response: Array<any> = [];

        data.forEach((contract: ContractRequest) => {

            response.push(new Contract(contract));
        });

        return response;
    }
}