import { Injectable } from "@angular/core";
import { IndexService } from "../index.service";

@Injectable()
export class ChargeTypeService extends IndexService {

    protected endpoint: string = '/service_charge_types';

    public sort = 'id';

    /**
     * service_charge_type[]
     * 
     * @param data
     */
    protected mapTo(data: any) {

        const response: Array<any> = [];

        data.forEach(service_charge_type => {

            response.push({
                id: service_charge_type.id,
                type: service_charge_type.type,
                description: service_charge_type.description,
            });
        });

        return response;
    }
}
