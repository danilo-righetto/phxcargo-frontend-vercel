
import { map } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpAuthenticateService } from "./_authentication/http.authenticate.service";

@Injectable()
export class IndexService {

    protected endpoint: string = null;

    protected query: string = '';

    protected sort: string = '-id';

    protected limit: number = 10;

    /**
     * 
     * @param http 
     */
    constructor(
        protected http: HttpAuthenticateService,
    ) { }


    /**
     * Fetch received data
     * use mapTo to properly parse given data
     * 
     * @param limit 
     * @param offset 
     */
    public fetchData(filter?: string, limit: number = 10, offset: number = 0) {

        if (!this.endpoint) {
            throw 'Invalid argument: Empty endpoint';
        }

        if (filter && filter.includes('undefined')) {
            filter = null;
        }

        return this.http.get(
            this.endpoint + this.getParameters(filter, limit, offset)
        ).pipe(
            map((res: any) => {
                return {
                    data: this.mapTo(res.data),
                    meta: res.meta
                }
            }));
    }

    /**
     * 
     * @param url 
     */
    public setEndpoint(url: string) {

        this.endpoint = url;
        return this;
    }

    public getEndpoint(): string {
        return this.endpoint;
    }

    public setSort(sort: string) {
        this.sort = sort;
        return this;
    }

    public setLimit(limit: number) {
        this.limit = limit;
        return this;
    }

    /**
     * 
     * @param query 
     */
    public setQuery(query: string) {
        this.query = query;
        return this;
    }

    /**
     * 
     * @param query 
     */
    public appendQueryparameters(query: string) {

        this.query += query;
        return this;
    }

    /**
     * 
     * @param filter 
     * @param limit 
     * @param offset 
     */
    private getParameters(filter: string, limit: number, offset: number): string {

        let parameters = `?_sort=${this.sort}&_limit=${limit}&_offset=${offset || 0}${this.query}`
        if (filter) {
            parameters += `&${filter}`;
        }

        return `${parameters}&_config=meta-total-count,meta-filter-count`;
    }

    /**
     * 
     * @param data 
     */
    protected mapTo(data: any) {

        return data;
    }
}