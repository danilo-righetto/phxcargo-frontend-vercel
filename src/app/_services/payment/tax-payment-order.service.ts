import { Injectable } from '@angular/core';
import { IndexService } from '../index.service';

@Injectable()
export class TaxPaymentOrderService extends IndexService {

    protected endpoint = '/tax-payment-orders';

    public sort = 'id';

    /**
     * Currency[]
     * 
     * @param data 
     */
    protected mapTo(data: any) {

        const response: Array<any> = [];

        data.forEach(paymentOrder => {

            response.push({
                order_type: paymentOrder.order_type,
                identification: paymentOrder.identification,
                description: paymentOrder.description,
                amount: paymentOrder.amount,
                authentication: paymentOrder.authentication_code,
                due_date: paymentOrder.due_date,
                created_at: paymentOrder.created_at,
            });
        });

        return response;
    }
}