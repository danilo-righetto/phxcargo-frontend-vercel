import { Injectable } from '@angular/core';
import { IndexService } from '../index.service';
import { Payment, PaymentListingRequest } from "app/_interfaces/payment/payment.interface";

@Injectable()
export class PaymentService extends IndexService {

    protected endpoint = '/payments';

    public sort = 'id';

    /**
     * Currency[]
     * 
     * @param data 
     */
    protected mapTo(data: any) {

        const response: Array<Payment> = [];

        data.forEach((payment: PaymentListingRequest) => {

            response.push(new Payment(payment));
        });

        return response;
    }
}