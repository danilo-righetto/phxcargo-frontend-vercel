export const locale = {
    lang: 'pt',
    data: {
        'NAV': {
            'NAVIGATION': 'Navegação',
            'DASHBOARD': {
                'TITLE': 'Dashboard',
                'BADGE': '15'
            },
            'IMPORTS': {
                'TITLE': 'Importação',
                'MANIFESTS': 'Processos Aduaneiros',
                'CONSOLIDATORS': 'Bags',
                'BADGE': '15'
            },
            'EXPORTS': {
                'TITLE': 'Exportação',
                'BADGE': '15'
            },
            'FINANCIAL': {
                'TITLE': 'Financeiro',
                'BADGE': '15',
                'INVOICES': {
                    'TITLE': 'Faturas'
                },
                'DEBIT_NOTE': {
                    'TITLE': 'Notas de Débito'
                },
                'FINES': {
                    'TITLE': 'Multas e Encargos'
                }
            },
            'COMPANIES': {
                'TITLE': 'Clientes',
                'CONTRACTS': 'Contratos',
                'SERVICES': 'Serviços',
                'CURRENCIES': 'Moedas',
                'CUSTOMERS': 'Cadastros',
                'BADGE': '15',
                'COMPANIE': 'Cliente',
                'TARIFFS': 'Tarifários',
            },
            'USERS': {
                'TITLE': 'Utilizadores',
                'BADGE': '15'
            },
            'IMPORTERS': {
                'TITLE': 'Importadores'
            },
            'SHIPPERS': {
                'TITLE': 'Embarcadores'
            },
            'STATUS': {
                'TITLE': 'Status'
            },
            'ICMS': {
                'TITLE': 'Icms'
            },
            'ROLES': {
                'TITLE': 'Hierarquias'
            },
            'FLIGHTS': {
                'TITLE': 'Voos'
            },
            'AIRLINE': {
                'TITLE': 'Companhias aéreas'
            },
            'AIRPORT': {
                'TITLE': 'Aeroportos'
            },
            'SETTINGS': {
                'TITLE': 'Configurações',
                'CHECKPOINTS': 'Checkpoints',
            },
        }
    }
};
