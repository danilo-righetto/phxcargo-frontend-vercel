import { FuseNavigation } from '@fuse/types';
import { AuthenticationService } from 'app/_services/_authentication/authentication.service';
import { Injectable } from '@angular/core';

@Injectable()
export class Navigation {

    constructor(private authService: AuthenticationService) {
    }

    /**
     * generate
     */
    public generate(): FuseNavigation[] {
        return [
            {
                id: 'navigation',
                title: 'Navegação',
                translate: 'NAV.NAVIGATION',
                type: 'group',
                children: [
                    {
                        id: 'dashboard',
                        title: 'Dashboard',
                        translate: 'NAV.DASHBOARD.TITLE',
                        type: 'item',
                        icon: 'computer',
                        url: '/dashboard',
                    },
                    {
                        id: 'imports',
                        title: 'Importação',
                        translate: 'NAV.IMPORTS.TITLE',
                        type: 'collapsable',
                        icon: 'flight_land',
                        children: [
                            {
                                id: 'hawb',
                                title: 'Houses (HAWB)',
                                hidden: !this.authService.hasPermission('airwaybill-house-index'),
                                type: 'item',
                                url: '/airwaybills/house',
                            },
                            {
                                id: 'mawb',
                                title: 'Masters (MAWB)',
                                hidden: !this.authService.hasPermission('airwaybill-master-index'),
                                type: 'item',
                                url: '/airwaybills/master'
                            },
                            {
                                id: 'manifest',
                                title: 'Manifestos',
                                translate: 'NAV.IMPORTS.MANIFESTS',
                                hidden: !this.authService.hasPermission('manifest-index'),
                                type: 'item',
                                url: '/manifests'
                            },
                            {
                                id: 'consolidator',
                                title: 'Bags',
                                translate: 'NAV.IMPORTS.CONSOLIDATORS',
                                hidden: !this.authService.hasPermission('airwaybill-house-index'),
                                type: 'item',
                                url: '/airwaybills/consolidators'
                            },
                        ]
                    },
                    {
                        id: 'customers',
                        title: 'Clientes e Empresas',
                        translate: 'NAV.COMPANIES.CUSTOMERS',
                        type: 'collapsable',
                        icon: 'business',
                        children: [
                            {
                                id: 'companies',
                                title: 'Empresas',
                                translate: 'NAV.COMPANIES.TITLE',
                                type: 'item',
                                hidden: !this.authService.hasPermission('company-index'),
                                url: '/companies'
                            },
                            {
                                id: 'contracts',
                                title: 'Contratos',
                                translate: 'NAV.COMPANIES.CONTRACTS',
                                hidden: !this.authService.hasPermission('company-contract-index'),
                                type: 'item',
                                url: '/contracts'
                            },
                            {
                                id: 'currencies',
                                title: 'Moedas',
                                translate: 'NAV.COMPANIES.CURRENCIES',
                                hidden: !this.authService.hasPermission('currencies-index'),
                                type: 'item',
                                url: '/currencies'
                            },
                            {
                                id: 'services',
                                title: 'Serviços',
                                translate: 'NAV.COMPANIES.SERVICES',
                                hidden: !this.authService.hasPermission('company-contract-index'),
                                type: 'item',
                                url: '/services'
                            },
                            {
                            id:"zipcode",
                            title:"Tarifários",
                            translate:"NAV.COMPANIES.TARIFFS",
                            hidden:!this.authService.hasPermission("zipcode-range-index"),
                            type:"item",
                            url:"/zipcode-ranges"
                            },
                        ]
                    },
                    {
                        id: 'financial',
                        title: 'Financeiro',
                        translate: 'NAV.FINANCIAL.TITLE',
                        type: 'collapsable',
                        icon: 'money',
                        hidden: !this.authService.hasPermission('flight-index'),
                        children: [
                            {
                                id: 'invoices',
                                title: 'Fatura',
                                translate: 'NAV.FINANCIAL.INVOICES.TITLE',
                                type: 'item',
                                url: '/financial/invoices',
                            },
                            {
                                id: 'debit_note',
                                title: 'Nota de Débito',
                                translate: 'NAV.FINANCIAL.DEBIT_NOTE.TITLE',
                                type: 'item',
                                url: '/financial/debit-note',
                            },
                            {
                                id: 'fines',
                                title: 'Multas',
                                translate: 'NAV.FINANCIAL.FINES.TITLE',
                                type: 'item',
                                url: '/financial/fines',
                            },
                        ]
                    },
                    {
                        id: 'flights',
                        title: 'Voos',
                        translate: 'NAV.FLIGHTS.TITLE',
                        type: 'collapsable',
                        icon: 'flight',
                        hidden: !this.authService.hasPermission('flight-index'),
                        children: [
                            {
                                id: 'airports',
                                title: 'Aeroportos',
                                translate: 'NAV.AIRPORT.TITLE',
                                type: 'item',
                                icon: 'location_on',
                                url: '/airports',
                            },
                            {
                                id: 'airlines',
                                title: 'Linhas Aéreas',
                                translate: 'NAV.AIRLINE.TITLE',
                                type: 'item',
                                icon: 'linear_scale',
                                url: '/airlines',
                            },
                            {
                                id: 'flights',
                                title: 'Voos',
                                translate: 'NAV.FLIGHTS.TITLE',
                                type: 'item',
                                icon: 'flight',
                                url: '/flights',
                            },
                        ]
                    },
                    {
                        id: 'importers',
                        title: 'Importadores',
                        translate: 'NAV.IMPORTERS.TITLE',
                        type: 'item',
                        hidden: !this.authService.hasPermission('importer-index'),
                        icon: 'people',
                        url: '/importers',
                    },
                    {
                        id: 'shippers',
                        title: 'Embarcadores',
                        translate: 'NAV.SHIPPERS.TITLE',
                        type: 'item',
                        hidden: !this.authService.hasPermission('shipper-index'),
                        icon: 'people',
                        url: '/shippers',
                    },
                    {
                        id: 'bankfiles',
                        title: 'Retornos do banco',
                        type: 'item',
                        icon: 'account_balance',
                        url: '/bank-files',
                    },
                    {
                        id: 'settings',
                        title: 'Configurações',
                        translate: 'NAV.SETTINGS.TITLE',
                        type: 'collapsable',
                        icon: 'money',
                        children: [
                            {
                                id: 'users',
                                title: 'Usuários',
                                translate: 'NAV.USERS.TITLE',
                                type: 'item',
                                hidden: !this.authService.hasPermission('user-index'),
                                icon: 'people',
                                url: '/users'
                            },
                            {
                                id: 'roles',
                                title: 'Permissões',
                                translate: 'NAV.ROLES.TITLE',
                                type: 'item',
                                hidden: !this.authService.hasPermission('role-index'),
                                icon: 'control_camera',
                                url: '/roles'
                            },
                            {
                                id: 'checkpoints',
                                title: 'Checkpoints',
                                translate: 'NAV.SETTINGS.CHECKPOINTS',
                                type: 'item',
                                hidden: !this.authService.hasPermission('checkpoint-index'),
                                icon: 'check_box',
                                url: '/checkpoints',
                            },
                            {
                                id: 'icms',
                                title: 'Icms',
                                translate: 'NAV.ICMS.TITLE',
                                type: 'item',
                                hidden: !this.authService.hasPermission('icms-index'),
                                icon: 'money',
                                url: '/icms',
                            }
                        ]
                    },
                ]
            }
        ]
    }
}