import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './authentication/login/login.component';
import { AuthGuardService as AuthGuard } from './_services/_authentication/auth-guard.service';
import { DashobardComponent } from './main/dashboard/dashboard.component';
import { ForgotPasswordComponent } from './authentication/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './authentication/reset-password/reset-password.component';
import { ResetPasswordForgotComponent } from './authentication/reset-password-forgot/reset-password-forgot.component';
import { AirwaybillHouseComponent } from './main/airwaybill/house/airwaybill.house.component';
import { AirwaybillMasterComponent } from './main/airwaybill/master/airwaybill.master.component';
import { HouseShowComponent } from './main/airwaybill/house/show/house.show.component';
import { HouseIntegrationComponent } from './main/airwaybill/house/integration/house.integration.component';
import { HouseUpdateComponent } from './main/airwaybill/house/update/house.update.component';
import { MasterShowComponent } from './main/airwaybill/master/show/master.show.component';
import { UserComponent } from './main/user/user.component';
import { UserShowComponent } from './main/user/show/user.show.component';
import { UserUpdateComponent } from './main/user/update/user.update.component';
import { ImporterComponent } from './main/importer/importer.component';
import { ShipperComponent } from './main/shipper/shipper.component';
import { ShipperShowComponent } from './main/shipper/show/shipper.show.component';
import { ShipperUpdateComponent } from './main/shipper/update/shipper.update.component';
import { FlightComponent } from './main/flight/flight.component';
import { FlightShowComponent } from './main/flight/show/flight.show.component';
import { FlightUpdateComponent } from './main/flight/update/flight.update.component';
import { AirlineComponent } from './main/airline/airline.component';
import { AirlineShowComponent } from './main/airline/show/airline.show.component';
import { AirlineUpdateComponent } from './main/airline/update/airline.update.component';
import { AirportComponent } from './main/airport/airport.component';
import { AirportShowComponent } from './main/airport/show/airport.show.component';
import { AirportUpdateComponent } from './main/airport/update/airport.update.component';
import { IcmsComponent } from './main/icms/icms.component';
import { IcmsShowComponent } from './main/icms/show/icms.show.component';
import { IcmsUpdateComponent } from './main/icms/update/icms.update.component';
import { RoleComponent } from './main/role/role.component';
import { RoleShowComponent } from './main/role/show/role.show.component';
import { RoleUpdateComponent } from './main/role/update/role.update.component';
import { CompanyComponent } from './main/company/company.component';
import { CompanyShowComponent } from './main/company/show/company.show.component';
import { CompanyUpdateComponent } from './main/company/update/company.update.component';
import { CurrencyComponent } from './main/currency/currency.component';
import { CurrencyShowComponent } from './main/currency/show/currency.show.component';
import { CurrencyUpdateComponent } from './main/currency/update/currency.update.component';
import { MasterImportComponent } from './main/airwaybill/master/import/master.import.component';
import { ManifestComponent } from './main/airwaybill/manifest/manifest.component';
import { ManifestShowComponent } from './main/airwaybill/manifest/show/manifest.show.component';
import { ContractComponent } from './main/contract/contract.component';
import { ContractUpdateComponent } from './main/contract/update/contract.update.component';
import { ContractShowComponent } from './main/contract/show/contract.show.component';
import { CheckpointComponent } from './main/checkpoint/checkpoint.component';
import { CheckpointShowComponent } from './main/checkpoint/show/checkpoint.show.component';
import { CheckpointUpdateComponent } from './main/checkpoint/update/checkpoint.update.component';
import { ImporterShowComponent } from './main/importer/show/importer.show.component';
import { ImporterUpdateComponent } from './main/importer/update/importer.update.component';
import { MasterUpdateComponent } from './main/airwaybill/master/update/master.update.component';
import { ServiceComponent } from './main/service/service.component';
import { ServiceUpdateComponent } from './main/service/update/service.update.component';
import { ServiceShowComponent } from './main/service/show/service.show.component';
import { InvoiceComponent } from './main/financial/invoice/invoice.component';
import { InvoiceUpdateComponent } from './main/financial/invoice/update/invoice.update.component';
import { InvoiceShowComponent } from './main/financial/invoice/show/invoice.show.component';
import { FineComponent } from './main/financial/fine/fine.component';
import { FineShowComponent } from './main/financial/fine/show/fine.show.component';
import { FineUpdateComponent } from './main/financial/fine/update/fine.update.component';
import { BankReturnFilesComponent } from './main/payments/bank.return.file.component';
import { DebitNoteComponent } from './main/financial/debit-note/debit-note.component';
import { DebitNoteShowComponent } from './main/financial/debit-note/show/debit-note-show.component';
import { DebitNoteUpdateComponent } from './main/financial/debit-note/update/debit-note-update.component';
import { ConsolidatorComponent } from './main/airwaybill/consolidator/consolidator.component';
import { ConsolidatorUpdateComponent } from './main/airwaybill/consolidator/update/consolidator.update.component';
import { ConsolidatorShowComponent } from './main/airwaybill/consolidator/show/consolidator.show.component';
import { ZipCodeRangeComponent } from './main/zipcodeRange/zipcodeRange.component';
import { ZipCodeRangeShowComponent } from './main/zipcodeRange/show/zipcodeRange.show.component';
import { ZipCodeRangeUpdateComponent } from './main/zipcodeRange/update/zipcodeRange.update.component';
import { ZipCodeRangeImportComponent } from './main/zipcodeRange/import/zipcodeRange.import.component';

const routes: Routes = [

    // Authentication
    { path: 'login', component: LoginComponent },
    { path: 'forgot-password', component: ForgotPasswordComponent },
    { path: 'reset-password-forgot/:token', component: ResetPasswordForgotComponent },
    { path: 'reset-password', component: ResetPasswordComponent, canActivate: [AuthGuard] },

    // Company
    { path: 'companies/show/:id', component: CompanyShowComponent, canActivate: [AuthGuard] },
    { path: 'companies/edit/:id', component: CompanyUpdateComponent, canActivate: [AuthGuard] },
    { path: 'companies/create', component: CompanyUpdateComponent, canActivate: [AuthGuard] },
    { path: 'companies', component: CompanyComponent, canActivate: [AuthGuard] },

    // Contract
    { path: 'contracts/create', component: ContractUpdateComponent, canActivate: [AuthGuard] },
    { path: 'contracts/edit/:id', component: ContractUpdateComponent, canActivate: [AuthGuard] },
    { path: 'contracts/show/:id', component: ContractShowComponent, canActivate: [AuthGuard] },
    { path: 'contracts', component: ContractComponent, canActivate: [AuthGuard] },

    // Services
    { path: 'services/create', component: ServiceUpdateComponent, canActivate: [AuthGuard] },
    { path: 'services/edit/:id', component: ServiceUpdateComponent, canActivate: [AuthGuard] },
    { path: 'services/show/:id', component: ServiceShowComponent, canActivate: [AuthGuard] },
    { path: 'services', component: ServiceComponent, canActivate: [AuthGuard] },

    //Currency
    { path: 'currencies/create', component: CurrencyUpdateComponent, canActivate: [AuthGuard] },
    { path: 'currencies/edit/:id', component: CurrencyUpdateComponent, canActivate: [AuthGuard] },
    { path: 'currencies/show/:id', component: CurrencyShowComponent, canActivate: [AuthGuard] },
    { path: 'currencies', component: CurrencyComponent, canActivate: [AuthGuard] },

    // Airwaybill
    { path: 'airwaybills/house', component: AirwaybillHouseComponent, canActivate: [AuthGuard] },
    { path: 'airwaybills/house/create', component: HouseUpdateComponent, canActivate: [AuthGuard] },
    { path: 'airwaybills/house/show/:id', component: HouseShowComponent, canActivate: [AuthGuard] },
    { path: 'airwaybills/house/integration', component: HouseIntegrationComponent, canActivate: [AuthGuard] },
    { path: 'airwaybills/house/edit/:id', component: HouseUpdateComponent, canActivate: [AuthGuard] },

    { path: 'airwaybills/master/show/:id', component: MasterShowComponent, canActivate: [AuthGuard] },
    { path: 'airwaybills/master/edit/:id', component: MasterUpdateComponent, canActivate: [AuthGuard] },
    { path: 'airwaybills/master/create', component: MasterUpdateComponent, canActivate: [AuthGuard] },
    { path: 'airwaybills/master/import', component: MasterImportComponent, canActivate: [AuthGuard] },
    { path: 'airwaybills/master', component: AirwaybillMasterComponent, canActivate: [AuthGuard] },

    // Checkpoints
    { path: 'checkpoints', component: CheckpointComponent, canActivate: [AuthGuard] },
    { path: 'checkpoints/show/:id', component: CheckpointShowComponent, canActivate: [AuthGuard] },
    { path: 'checkpoints/edit/:id', component: CheckpointUpdateComponent, canActivate: [AuthGuard] },
    { path: 'checkpoints/create', component: CheckpointUpdateComponent, canActivate: [AuthGuard] },

    // Consolidator
    { path: 'airwaybills/consolidators', component: ConsolidatorComponent, canActivate: [AuthGuard] },
    { path: 'airwaybills/consolidators/show/:id', component: ConsolidatorShowComponent, canActivate: [AuthGuard] },
    { path: 'airwaybills/consolidators/edit/:id', component: ConsolidatorUpdateComponent, canActivate: [AuthGuard] },
    { path: 'airwaybills/consolidators/create', component: ConsolidatorUpdateComponent, canActivate: [AuthGuard] },

    // Manifest
    { path: 'manifests', component: ManifestComponent, canActivate: [AuthGuard] },
    { path: 'manifests/show/:id', component: ManifestShowComponent, canActivate: [AuthGuard] },
    { path: 'manifests/edit/:id', component: ManifestShowComponent, canActivate: [AuthGuard] },
    { path: 'manifests/create', component: ManifestComponent, canActivate: [AuthGuard] },

    // User
    { path: 'users/show/:id', component: UserShowComponent, canActivate: [AuthGuard] },
    { path: 'users/edit/:id', component: UserUpdateComponent, canActivate: [AuthGuard] },
    { path: 'users/create', component: UserUpdateComponent, canActivate: [AuthGuard] },
    { path: 'users', component: UserComponent, canActivate: [AuthGuard] },

    // Importer
    { path: 'importers/show/:id', component: ImporterShowComponent, canActivate: [AuthGuard] },
    { path: 'importers/edit/:id', component: ImporterUpdateComponent, canActivate: [AuthGuard] },
    { path: 'importers/create', component: ImporterUpdateComponent, canActivate: [AuthGuard] },
    { path: 'importers', component: ImporterComponent, canActivate: [AuthGuard] },

    // Shipper
    { path: 'shippers/show/:id', component: ShipperShowComponent, canActivate: [AuthGuard] },
    { path: 'shippers/edit/:id', component: ShipperUpdateComponent, canActivate: [AuthGuard] },
    { path: 'shippers/create', component: ShipperUpdateComponent, canActivate: [AuthGuard] },
    { path: 'shippers', component: ShipperComponent, canActivate: [AuthGuard] },

    // Flight
    { path: 'flights/show/:id', component: FlightShowComponent, canActivate: [AuthGuard] },
    { path: 'flights/edit/:id', component: FlightUpdateComponent, canActivate: [AuthGuard] },
    { path: 'flights/create', component: FlightUpdateComponent, canActivate: [AuthGuard] },
    { path: 'flights', component: FlightComponent, canActivate: [AuthGuard] },

    // Airline
    { path: 'airlines/show/:id', component: AirlineShowComponent, canActivate: [AuthGuard] },
    { path: 'airlines/edit/:id', component: AirlineUpdateComponent, canActivate: [AuthGuard] },
    { path: 'airlines/create', component: AirlineUpdateComponent, canActivate: [AuthGuard] },
    { path: 'airlines', component: AirlineComponent, canActivate: [AuthGuard] },

    // Airport
    { path: 'airports/show/:id', component: AirportShowComponent, canActivate: [AuthGuard] },
    { path: 'airports/edit/:id', component: AirportUpdateComponent, canActivate: [AuthGuard] },
    { path: 'airports/create', component: AirportUpdateComponent, canActivate: [AuthGuard] },
    { path: 'airports', component: AirportComponent, canActivate: [AuthGuard] },

    // Icms
    { path: 'icms/show/:id', component: IcmsShowComponent, canActivate: [AuthGuard] },
    { path: 'icms/edit/:id', component: IcmsUpdateComponent, canActivate: [AuthGuard] },
    { path: 'icms/create', component: IcmsUpdateComponent, canActivate: [AuthGuard] },
    { path: 'icms', component: IcmsComponent, canActivate: [AuthGuard] },

    // Role
    { path: 'roles/show/:id', component: RoleShowComponent, canActivate: [AuthGuard] },
    { path: 'roles/edit/:id', component: RoleUpdateComponent, canActivate: [AuthGuard] },
    { path: 'roles/create', component: RoleUpdateComponent, canActivate: [AuthGuard] },
    { path: 'roles', component: RoleComponent, canActivate: [AuthGuard] },

    //ZipCodeRange
    { path: 'zipcode-ranges/show/:id', component: ZipCodeRangeShowComponent, canActivate: [AuthGuard] },
    { path: 'zipcode-ranges/edit/:id', component: ZipCodeRangeUpdateComponent, canActivate: [AuthGuard] },
    { path: 'zipcode-ranges/create', component: ZipCodeRangeUpdateComponent, canActivate: [AuthGuard] },
    { path: 'zipcode-ranges', component: ZipCodeRangeComponent, canActivate: [AuthGuard] },

    // Bank files
    { path: 'bank-files', component: BankReturnFilesComponent, canActivate: [AuthGuard] },

    // Dashboard
    { path: 'dashboard', component: DashobardComponent, canActivate: [AuthGuard] },
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },

    //ZipCodeRange
    { path: 'zipcode-ranges/show/:id', component: ZipCodeRangeShowComponent, canActivate: [AuthGuard] },
    { path: 'zipcode-ranges/edit/:id', component: ZipCodeRangeUpdateComponent, canActivate: [AuthGuard] },
    { path: 'zipcode-ranges/create', component: ZipCodeRangeUpdateComponent, canActivate: [AuthGuard] },
    { path: 'zipcode-ranges', component: ZipCodeRangeComponent, canActivate: [AuthGuard] },
    { path: 'zipcode-ranges/import', component: ZipCodeRangeImportComponent, canActivate: [AuthGuard] },


    //Invoice
    { path: 'financial/invoices', component: InvoiceComponent, canActivate: [AuthGuard] },
    { path: 'financial/invoices/show/:id', component: InvoiceShowComponent, canActivate: [AuthGuard] },
    { path: 'financial/invoices/edit/:id', component: InvoiceUpdateComponent, canActivate: [AuthGuard] },
    { path: 'financial/invoices/create', component: InvoiceUpdateComponent, canActivate: [AuthGuard] },
    { path: 'financial/fines', component: FineComponent, canActivate: [AuthGuard] },
    { path: 'financial/fines/create', component: FineUpdateComponent, canActivate: [AuthGuard] },
    { path: 'financial/fines/edit/:id', component: FineUpdateComponent, canActivate: [AuthGuard] },
    { path: 'financial/fines/show/:id', component: FineShowComponent, canActivate: [AuthGuard] },
    { path: 'financial/debit-note', component: DebitNoteComponent, canActivate: [AuthGuard] },
    { path: 'financial/debit-note/show/:id', component: DebitNoteShowComponent, canActivate: [AuthGuard] },
    { path: 'financial/debit-note/edit/:id', component: DebitNoteUpdateComponent, canActivate: [AuthGuard] },
    { path: 'financial/debit-note/create', component: DebitNoteUpdateComponent, canActivate: [AuthGuard] },
    // generic 404
    { path: '**', redirectTo: 'dashboard' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }