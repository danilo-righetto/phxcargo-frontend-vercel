

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule, MatIconModule, MatSnackBar, MatCheckboxModule, MatInputModule, MatGridListModule } from '@angular/material';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { TranslateModule } from '@ngx-translate/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import 'hammerjs';


import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { AppRoutingModule } from './app-routing.module';
import { AuthenticationService } from './_services/_authentication/authentication.service';
import { NotificationService } from './_services/notification/notification.service';
import { LoginModule } from './authentication/login/login.module';
import { AuthGuardService } from './_services/_authentication/auth-guard.service';
import { DashboardModule } from './main/dashboard/dashboard.module';
import { ForgotPasswordModule } from './authentication/forgot-password/forgot-password.module';
import { RecoverPasswordService } from './_services/_authentication/recover-password.service';
import { ResetPasswordModule } from './authentication/reset-password/reset-password.module';
import { ResetPasswordForgotModule } from './authentication/reset-password-forgot/reset-password-forgot.module';
import { AirwaybillHouseModule } from './main/airwaybill/house/airwaybill.house.module';
import { AirwaybillMasterModule } from './main/airwaybill/master/airwaybill.master.module';
import { HouseShowModule } from './main/airwaybill/house/show/house.show.module';
import { HouseIntegrationModule } from './main/airwaybill/house/integration/house.integration.module';
import { HouseUpdateModule } from './main/airwaybill/house/update/house.update.module';
import { HttpAuthenticateService } from './_services/_authentication/http.authenticate.service';
import { MasterShowModule } from './main/airwaybill/master/show/master.show.module';
import { ShowService } from './_services/show.service';
import { ImporterModule } from './main/importer/importer.module';
import { ShipperModule } from './main/shipper/shipper.module';
import { ShipperShowModule } from './main/shipper/show/shipper.show.module';
import { ShipperUpdateModule } from './main/shipper/update/shipper.update.module';
import { FlightModule } from './main/flight/flight.module';
import { FlightShowModule } from './main/flight/show/flight.show.module';
import { FlightUpdateModule } from './main/flight/update/flight.update.module';
import { IcmsModule } from './main/icms/icms.module';
import { IcmsShowModule } from './main/icms/show/icms.show.module';
import { IcmsUpdateModule } from './main/icms/update/icms.update.module';
import { AirlineModule } from './main/airline/airline.module';
import { AirlineShowModule } from './main/airline/show/airline.show.module';
import { AirlineUpdateModule } from './main/airline/update/airline.update.module';
import { AirportModule } from './main/airport/airport.module';
import { AirportShowModule } from './main/airport/show/airport.show.module';
import { AirportUpdateModule } from './main/airport/update/airport.update.module';
import { RoleModule } from './main/role/role.module';
import { RoleShowModule } from './main/role/show/role.show.module';
import { RoleUpdateModule } from './main/role/update/role.update.module';

import { IndexService } from './_services/index.service';
import { CompanyModule } from './main/company/company.module';
import { CompanyShowModule } from './main/company/show/company.show.module';
import { CompanyUpdateModule } from './main/company/update/company.update.module';
import { CurrencyModule } from './main/currency/currency.module';
import { CurrencyShowModule } from './main/currency/show/currency.show.module';
import { CurrencyUpdateModule } from './main/currency/update/currency.update.module';
import { ServiceModule } from './main/service/service.module';
import { ServiceShowModule } from './main/service/show/service.show.module';
import { ServiceUpdateModule } from './main/service/update/service.update.module';
import { ContractModule } from './main/contract/contract.module';
import { ContractUpdateModule } from './main/contract/update/contract.update.module';
import { ContractShowModule } from './main/contract/show/contract.show.module';

import { UserModule } from './main/user/user.module';
import { UserShowModule } from './main/user/show/user.show.module';
import { UserUpdateModule } from './main/user/update/user.update.module';
import { HouseCheckpointModule } from './main/airwaybill/house/checkpoint/checkpoint.module';
import { CheckpointService } from './_services/checkpoints/checkpoint.service';
import { MasterImportModule } from './main/airwaybill/master/import/master.import.module';
import { MasterUpdateModule } from './main/airwaybill/master/update/master.update.module';
import { ManifestModule } from './main/airwaybill/manifest/manifest.module';
import { ManifestShowModule } from './main/airwaybill/manifest/show/manifest.show.module';
import { ManifestUpdateModule } from './main/airwaybill/manifest/update/manifest.update.module';
import { CheckpointUpdateModule } from './main/checkpoint/update/checkpoint.update.module';
import { CheckpointShowModule } from './main/checkpoint/show/checkpoint.show.module';
import { CheckpointModule } from './main/checkpoint/checkpoint.module';
import { CountryService } from './_services/country/country.service';
import { CityService } from './_services/address/city.service';
import { Navigation } from './navigation/navigation';
import { ImporterShowModule } from './main/importer/show/importer.show.module';
import { ImporterUpdateModule } from './main/importer/update/importer.update.module';
import { InvoiceModule } from './main/financial/invoice/invoice.module';
import { FineModule } from './main/financial/fine/fine.module';
import { FineShowModule } from './main/financial/fine/show/fine.show.module';
import { FineUpdateModule } from './main/financial/fine/update/fine.update.module';
import { BankReturnFilesModule } from './main/payments/bank.return.file.module';
import { DebitNoteModule} from './main/financial/debit-note/debit-note.module';
import { DebitNoteUpdateModule } from './main/financial/debit-note/update/debit-note-update.module';
import { ConsolidatorModule } from './main/airwaybill/consolidator/consolidator.module';
import { ConsolidatorUpdateModule } from './main/airwaybill/consolidator/update/consolidator.update.module';
import { ConsolidatorShowModule } from './main/airwaybill/consolidator/show/consolidator.show.module';
import { InvoiceShowModule } from './main/financial/invoice/show/invoice.show.module';
import { InvoiceUpdateModule } from './main/financial/invoice/update/invoice.update.module';
import { ZipCodeRangeModule } from './main/zipcodeRange/zipcodeRange.module';
import { ZipCodeRangeShowModule } from './main/zipcodeRange/show/zipcodeRange.show.module';
import { ZipCodeRangeUpdateModule } from './main/zipcodeRange/update/zipcodeRange.update.module';
import { ZipCodeRangeImportModule } from './main/zipcodeRange/import/zipcodeRange.import.module';
import { DialogCompanyModule } from 'app/_default/dialogs/company/dialog-company-create.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,
        MatCheckboxModule,
        MatInputModule,
        MatGridListModule,
        MatSlideToggleModule, 
         

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,
        MatSnackBarModule,

        /* App modules */
        LayoutModule,
        DashboardModule,
        
        // Authentication
        LoginModule,
        ForgotPasswordModule,
        ResetPasswordModule,
        ResetPasswordForgotModule,

        // Company
        CompanyModule,
        CompanyShowModule,
        CompanyUpdateModule,

        //Currency
        CurrencyModule,
        CurrencyShowModule,
        CurrencyUpdateModule,

        // Contract
        ContractModule,
        ContractShowModule,
        ContractUpdateModule,
        
        // Company
        ServiceModule,
        ServiceShowModule,
        ServiceUpdateModule,

        // Airwaybill
        AirwaybillHouseModule,
        HouseUpdateModule,
        HouseShowModule,
        HouseIntegrationModule,
        
        AirwaybillMasterModule,
        MasterShowModule,
        MasterUpdateModule,
        MasterImportModule,

        ManifestModule,
        ManifestShowModule,
        ManifestUpdateModule,
        HouseCheckpointModule,

        CheckpointModule,
        CheckpointShowModule,
        CheckpointUpdateModule,

        ConsolidatorModule,
        ConsolidatorShowModule,
        ConsolidatorUpdateModule,

        //Importer
        ImporterModule,
        ImporterShowModule,
        ImporterUpdateModule,

        //Shipper
        ShipperModule,
        ShipperShowModule,
        ShipperUpdateModule,

        //User
        UserModule,
        UserShowModule,
        UserUpdateModule,

        //Airport
        AirportModule,
        AirportShowModule,
        AirportUpdateModule,

        //Airline
        AirlineModule,
        AirlineShowModule,
        AirlineUpdateModule,

        //Flight
        FlightModule,
        FlightShowModule,
        FlightUpdateModule,

        //Icms
        IcmsModule,
        IcmsShowModule,
        IcmsUpdateModule,

        //Role
        RoleModule,
        RoleShowModule,
        RoleUpdateModule,

        //Financial
        InvoiceModule,
        InvoiceUpdateModule,
        InvoiceShowModule,
        FineModule,
        FineUpdateModule,
        FineShowModule,
        DebitNoteModule,
        
        //DebitNoteUpdateModule,
        RoleUpdateModule,

        // Files
        BankReturnFilesModule,

        //ZipCodeRange
        ZipCodeRangeModule,
        ZipCodeRangeShowModule,
        ZipCodeRangeUpdateModule,
        ZipCodeRangeImportModule,

        DialogCompanyModule,
    ],
    providers: [
        AuthenticationService,
        AuthGuardService,
        HttpAuthenticateService,
        RecoverPasswordService,
        ShowService,
        IndexService,
        CheckpointService,
        CountryService,
        CityService,
        NotificationService,
        MatSnackBar,
        Navigation,
    ],
    bootstrap: [
        AppComponent
    ],
   

})
export class AppModule {

}