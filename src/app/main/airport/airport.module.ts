import { NgModule } from "@angular/core";
import { AirportComponent } from "./airport.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { AirportService } from "app/_services/airport/airport.service";

@NgModule({
    declarations: [
        AirportComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        AirportComponent,
    ],
    providers: [
        AirportService,
    ]
})
export class AirportModule {

}