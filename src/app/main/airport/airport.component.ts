import { Component, ViewChild } from "@angular/core";
import { MatTableDataSource } from "@angular/material";
import { AirportListingElement } from 'app/_interfaces/airport/listing-element';
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { locale as portuguese } from './_i18n/pt';
import { AirportService } from "app/_services/airport/airport.service";
import { DefaultTableComponent } from 'app/_default/data-list/table.component';

@Component({
    selector: 'airport',
    templateUrl: './airport.component.html'
})
export class AirportComponent {

    @ViewChild(DefaultTableComponent, { static: true })
    public table: DefaultTableComponent;

    public endpoint: string = '/airports';
    
    public displayedColumns: string[] = [
        'Nome do Aeroporto',
        'Sigla do Aeroporto',
    ];

    public filterColumns = [
        {
            column: 'name',
            display_name: 'Nome do Aeroporto',
            type: 'string'
        },
        {
            column: 'code',
            display_name: 'Sigla do Aeroporto',
            type: 'string'
        }
    ]

    private data: AirportListingElement[] = [];
    public dataSource: MatTableDataSource<AirportListingElement>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        public airportService: AirportService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

}