import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { AirportUpdateComponent } from './airport.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'


@NgModule({
    declarations: [
        AirportUpdateComponent
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
    ],
    exports: [
        AirportUpdateComponent
    ]
})

export class AirportUpdateModule {
}
