
import {map} from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { AirportListingElement } from 'app/_interfaces/airport/listing-element';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'airport-update',
    templateUrl: './airport.update.component.html',
    styleUrls: ['./airport.update.component.css'],
})
export class AirportUpdateComponent {

    private data: AirportListingElement[] = [];
    public loading: boolean = true;
    public _update: boolean = false;
    private endpoint: string = '/airports';
    private currentId = null;

    public formGroupAirport: FormGroup;

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private router: Router
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.buildFormGroupAirport();

        this.route.params.subscribe(params => {

            if (params.id) {

                this._update = true;
                this.currentId = params.id;
                this.fetchData(`/airports/${params.id}`);
            }
        });

    }

    /**
     * Build reactive form
     *
     */
    private buildFormGroupAirport() {
        this.formGroupAirport = this._formBuilder.group({
            name: ['', Validators.required],
            code: ['', Validators.required]
        });
    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(airport => {
                this.data = airport;
                this.formGroupAirport.patchValue({
                    name: airport.name,
                    code: airport.code
                });
                this.loading = false;
            });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {

        const controls = this.parseControls(this.formGroupAirport.controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {
        const parsed = {
            "name": controls.name.value,
            "code": controls.code.value,
            "country_id": 1
        }

        return parsed;
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }
}