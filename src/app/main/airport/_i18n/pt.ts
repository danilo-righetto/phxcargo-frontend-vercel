export const locale = {
    lang: 'pt',
    data: {
        'AIRPORT': {
            'AIRPORT': 'Aeroporto',
            'AIRPORTS': 'Aeroportos',
            'INFORMATIONS': 'Informações',
            'REGISTRATION_INFORMATIONS': 'Informações cadastrais',
            'SHOW': 'Visualizar',
            'EDIT': 'Editar',
            'CREATE': 'Adicionar',
            'BACK': 'Voltar',
            'SAVE': 'Salvar',
            'LISTING': {
                'ID': 'ID',
                'CODE': 'Código',
                'NAME': 'Nome',
            }
        }
    }
};