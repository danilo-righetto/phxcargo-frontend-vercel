
import {map} from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { AirportListingElement } from 'app/_interfaces/airport/listing-element';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';


@Component({
    selector: 'airport-show',
    templateUrl: './airport.show.component.html',
    styleUrls: ['./airport.show.component.css'],
})
export class AirportShowComponent {

    private data: AirportListingElement[] = [];
    public loading: boolean = true;

    /**
     * Constructor
     * 
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    public constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        public spinner: MatProgressSpinnerModule
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.route.params.subscribe(params => {

            this.fetchDataAirports(`/airports/${params.id}`);
        })

    }

    /**
     * 
     * @param url 
     */
    private fetchDataAirports(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(airport => {
                this.data = airport;
                this.loading = false;
            });
    }
}
