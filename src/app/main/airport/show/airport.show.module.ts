import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { AirportShowComponent } from './airport.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'


@NgModule({
    declarations: [
        AirportShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
    ],
    exports: [
        AirportShowComponent,
    ]
})

export class AirportShowModule {
}
