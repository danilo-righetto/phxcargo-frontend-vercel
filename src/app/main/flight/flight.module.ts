import { NgModule } from "@angular/core";
import { FlightComponent } from "./flight.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { FlightService } from "app/_services/flight/flight.service";

@NgModule({
    declarations: [
        FlightComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        FlightComponent,
    ],
    providers: [
        FlightService,
    ]
})
export class FlightModule {

}