import { Component, ViewChild} from "@angular/core";
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { locale as portuguese } from './_i18n/pt';
import { FlightService } from "app/_services/flight/flight.service";
import { FlightListingElement } from 'app/_interfaces/flight/listing-element';
import { MatTableDataSource } from '@angular/material';
import { DefaultTableComponent } from 'app/_default/data-list/table.component';

@Component({
    selector: 'flight',
    templateUrl: './flight.component.html'
})
export class FlightComponent {
    
    @ViewChild(DefaultTableComponent, { static: true })
    public table: DefaultTableComponent;

    public endpoint: string = '/flights';

    public displayedColumns: string[] = [
        'Número do Voo',
        'Número Alternativo',
        'Destino e Origem',
        'Companhia Aérea',
    ];

    private data: FlightListingElement[] = [];

    public dataSource: MatTableDataSource<FlightListingElement>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        public flightService: FlightService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    ngOnInit() {
        this.flightService.setQuery('');
    }
}