export const locale = {
    lang: 'pt',
    data: {
        'FLIGHT': {
            'INFORMATIONS': 'Informações',
            'REGISTRATION_INFORMATIONS': 'Informações cadastrais',
            'SHOW': 'Visualizar',
            'CREATE': 'Adicionar',
            'UPDATE': 'Editar',
            'FLIGHT': 'Voo',
            'FLIGHTS': 'Voos',
            'DESCRIPTION': 'Descrição',
            'EDIT': 'Editar',
            'BACK': 'Voltar',
            'SAVE': 'Salvar',
            'LISTING': {
                'ID': 'ID',
                'NUMBER': 'Número',
                'ALTERNATIVE_NUMBER': 'Número alternativo',
                'AIRLINE_NAME': 'Companhia aérea',
                'AIRPORT_NAME': 'Nome do aeroporto',
                'AIRPORT_CODE': 'Código do aeroporto',
                'DATE': 'Nova',
                'NEW_DATE': 'Nova data',
                'CURRENCY_DATE': 'Data atual',
                'ORIGIN': 'Origem',
                'DESTINATION': 'Destino',
            },  
        }
    }
};