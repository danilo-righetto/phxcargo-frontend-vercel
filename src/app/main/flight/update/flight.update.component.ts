
import {map} from 'rxjs/operators';
import { Component } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FlightListingElement } from 'app/_interfaces/flight/listing-element';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {AirlineListingElement as Airline} from "../../../_interfaces/airline/listing-element";
import { AirlineService } from 'app/_services/airline/airline.service';
import {AirportListingElement as Airport} from "../../../_interfaces/airport/listing-element";
import { AirportService } from 'app/_services/airport/airport.service';

@Component({
    selector: 'flight-update',
    templateUrl: './flight.update.component.html',
    styleUrls: ['./flight.update.component.css'],
})
export class FlightUpdateComponent {

    private data: FlightListingElement[] = [];
    public loading: boolean = true;
    public loadingUpdate: boolean = true;
    public _update: boolean = false;
    private endpoint: string = '/flights';
    private currentId = null;

    public airlines: Airline[];
    public loadingAirline: boolean = true;

    public airports: Airport[];
    public loadingAirport: boolean = true;

    public formGroup: FormGroup;

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private airlineService: AirlineService,
        private airportService: AirportService,
        private router: Router
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.buildFormGroup();

        this.route.params.subscribe(params => {

            if (params.id) {
                this._update = true;
                this.currentId = params.id;
                this.fetchDataFlight(`/flights/${params.id}`);
            }
        });

        this.airlineService.fetchData().subscribe(airlines => {
            this.airlines = airlines.data;
            this.loadingAirline = false;
        });

        this.airportService.fetchData().subscribe(airports => {
            this.airports = airports.data;
            this.loadingAirport = false;
            this.loadingUpdate = false;
        });
        this.loading = true;

    }

    /**
     * Build reactive form
     *
     */
    private buildFormGroup() {
        this.formGroup = this._formBuilder.group({
            flight_number: ['', Validators.required],
            flight_alternative_number: ['', Validators.required],
            airline: ['', Validators.required],
            origin_airport: ['', Validators.required],
            origin_date: ['', Validators.required],
            destination_airport: ['', Validators.required],
            destination_date: ['', Validators.required],
            origin_new_date: ['',],
            destination_new_date: ['',],
        });
        this.loadingUpdate = false;
    }

    /**
     * 
     * @param url 
     */
    private fetchDataFlight(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(flight => {
                this.data = flight;

                if (flight.origin == undefined){
                    if((flight.origin.airportId) === undefined){
                        flight.origin.airportId = 's/n';
                    }
                }


                this.formGroup.patchValue({
                    flight_number: (typeof flight.number !== undefined || flight.number !== null) && (flight.number !== undefined) ? flight.number : '',
                    flight_alternative_number: (typeof flight.alternative_number !== undefined || flight.alternative_number !== null) && (flight.alternative_number !== undefined) ? flight.alternative_number : 's/n',
                    airline: (typeof flight.airline.id !== undefined || flight.airline.id !== null) && (flight.airline.id !== undefined) ? flight.airline.id : '',
                    origin_airport: (typeof flight.origin.airportId !== undefined || flight.origin.airportId !== null) && (flight.origin.airportId !== undefined) ? flight.origin.airportId : '',
                    origin_date: (typeof flight.origin.date !== undefined || flight.origin.date !== null) && (flight.origin.date !== undefined) ? flight.origin.date : '',
                    destination_airport: (typeof flight.destination.airportId !== undefined || flight.destination.airportId !== null) && (flight.destination.airportId !== undefined) ? flight.destination.airportId : '',
                    destination_date: (typeof flight.destination.date !== undefined || flight.destination.date !== null) && (flight.destination.date !== undefined) ? flight.destination.date : '',

                });
                this.loading = false;
                this._update = true;
            });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {

        

        if (this._update) {
            const controls = this.parseControlsUpdate(this.formGroup.controls);
            
            return this.update(controls);
        } else {
            const controls = this.parseControls(this.formGroup.controls);
            return this.create(controls);
        }

        
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {

        const parsed = {
            "number": controls.flight_number.value,
            "alternative_number": controls.flight_alternative_number.value,
            "airline_id": controls.airline.value,
            "origin": {
                "airport_id": controls.origin_airport.value,
                "date": controls.origin_date.value.format("YYYY-MM-DDTHH:mm")
            },
            "destination": {
                "airport_id": controls.destination_airport.value,
                "date": controls.destination_date.value.format("YYYY-MM-DDTHH:mm")
            }
        }

        return parsed;
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControlsUpdate(controls: any) {

        const parsed = {
            "number": controls.flight_number.value,
            "alternative_number": controls.flight_alternative_number.value,
            "airline_id": controls.airline.value,
            "origin": {
                "airport_id": controls.origin_airport.value,
                "date": controls.origin_new_date.value.format("YYYY-MM-DDTHH:mm")
            },
            "destination": {
                "airport_id": controls.destination_airport.value,
                "date": controls.origin_new_date.value.format("YYYY-MM-DDTHH:mm")
            }
        }

        return parsed;
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }
}