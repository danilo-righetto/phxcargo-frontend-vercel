import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { FlightShowComponent } from './flight.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'


@NgModule({
    declarations: [
        FlightShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
    ],
    exports: [
        FlightShowComponent,
    ]
})

export class FlightShowModule {
}
