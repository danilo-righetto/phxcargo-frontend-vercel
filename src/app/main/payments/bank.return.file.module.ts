import { NgModule } from "@angular/core";
import { BankReturnFilesComponent } from "./bank.return.file.component";
import { MatTableModule } from "@angular/material";
import { FuseSharedModule } from "@fuse/shared.module";
import { DeclarationsModule } from "app/declarations.module";
import { PublicFilesService } from "app/_services/public-files/public-files.service";

@NgModule({
    declarations:[
        BankReturnFilesComponent
    ],
    imports:[
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        BankReturnFilesComponent
    ],
    providers: [
        PublicFilesService
    ]
})
export class BankReturnFilesModule {

}