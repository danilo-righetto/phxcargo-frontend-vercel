import { Component, OnInit } from "@angular/core";
import { PublicFilesService } from "app/_services/public-files/public-files.service";

@Component({
    templateUrl: 'bank.return.file.component.html'
})
export class BankReturnFilesComponent implements OnInit {

    public endpoint: string = '/files/return-bank';

    public displayedColumns: string[] = [
        'Tipo de Ordem',
        'Indentificação',
        'Código de Antenticação',
        'Data de Vencimento',
        'Código de Barras',
        'url',
    ];

    public filterColumns = [
        {
            column: 'order_type',
            display_name: 'Tipo Arquivo',
            type: 'string'
        },
        {
            column: 'identification',
            display_name: 'Código (remessa/manifesto)',
            type: 'string'
        },
    ];

    constructor(public fileService: PublicFilesService) { }

    public ngOnInit(): void {
        this.fileService.setEndpoint(this.endpoint);
    }
}