import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CheckpointService } from 'app/_services/checkpoints/checkpoint.service';
import { ChargeTypeService } from 'app/_services/company/charge_type.service';
 
@Component({
    selector: 'service-update',
    templateUrl: './service.update.component.html',
    styleUrls: ['./service.update.component.css'],
})


export class ServiceUpdateComponent {

    public data: any;
    public loading: boolean = true;
    public _update: boolean = false;
    private endpoint: string = '/services';
    private currentId = null;

    // public countries: any;
    // public loadingcountry: boolean = true;

    public checkpoints: any;
    public loadingCheckpoint: boolean = true;

    public service_charge_types: any;
    public loadingChargeType: boolean = true;



    public formGroup: FormGroup;

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        // private countryService: CountryService,
        private checkpointService: CheckpointService,
        private chargeTypeService: ChargeTypeService,
        private router: Router
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.buildFormGroup();

        this.route.params.subscribe(params => {

            if (params.id) {

                this._update = true;
                this.currentId = params.id;
                this.fetchData(`/services/${params.id}`);
                return
            }
        });
        this.checkpointService.fetchData('', 255).subscribe(checkpoints => {
            this.checkpoints = checkpoints.data
            this.loadingCheckpoint = false;
        });
        this.chargeTypeService.fetchData('', 255).subscribe(service_charge_types => {
            this.service_charge_types = service_charge_types.data
                this.loadingChargeType = false;
                this.loading = false;
        });
        // this.countryService.fetchData('', 255).subscribe(countries => {
        //     this.countries = countries.data
        //     this.loadingcountry = false;
        // });
    }

    /**
     * Build reactive form
     *
     */
    private buildFormGroup() {
        this.formGroup = this._formBuilder.group({
            type: ['', Validators.required],
            applicable: ['', Validators.required],
            checkpoint: [''],
            description: [''],
            price: [''],
        });
    }

    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;

    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(service => {
                this.data = service;
                this.data;
                this.formGroup.patchValue({
                    type: service.service_level_agreement,
                    name: service.name,
                    applicable: service.charge_type_id,
                    checkpoint: service.checkpoints_id,
                    description: service.description,
                    price: service.min_price
                });
                this.loading = false;
            });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {

        const controls = this.parseControls(this.formGroup.controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {
        const parsed = {
            "service_level_agreement": controls.type.value,
            "charge_type_id": controls.applicable.value,
            "description": controls.description.value,
            "checkpoints_id": controls.checkpoint.value,
            "min_price": controls.price.value, 
        
        }

        return parsed;
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }
}
