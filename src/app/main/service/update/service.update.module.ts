import { ChargeTypeService } from 'app/_services/company/charge_type.service';
import { CheckpointService } from 'app/_services/checkpoints/checkpoint.service';
import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { ServiceUpdateComponent } from './service.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatRadioModule } from '@angular/material';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'


@NgModule({
    declarations: [
        ServiceUpdateComponent
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatRadioModule,
        NgxMatSelectSearchModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
    ],
    exports: [
        ServiceUpdateComponent
    ],
    providers: [
        CheckpointService,
        ChargeTypeService,
    ]
})

export class ServiceUpdateModule {
}
