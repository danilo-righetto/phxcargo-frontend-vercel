import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { ServiceShowComponent } from './service.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'


@NgModule({
    declarations: [
        ServiceShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
    ],
    exports: [
        ServiceShowComponent,
    ]
})

export class ServiceShowModule {
}
