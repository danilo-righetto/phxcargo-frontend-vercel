
import {map} from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { ServiceListingRequest, Service, ServiceListingElement } from 'app/_interfaces/contract/contract_service.interface';


@Component({
    selector: 'service-show',
    templateUrl: './service.show.component.html',
    styleUrls: ['./service.show.component.css'],
})
export class ServiceShowComponent {

    private data: ServiceListingElement;
    public loading: boolean = true;

    /**
     * Constructor
     * 
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    public constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.route.params.subscribe(params => {

            this.fetchData(`/services/${params.id}`);
        })

    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe((service: ServiceListingRequest) => {
                this.data = new Service(service);
                this.loading = false;
            });
    }
}
