export const locale = {
    lang: 'pt',
    data: {
        'SERVICE': {
            'SERVICES': 'Serviço',
            'INFORMATIONS': 'Informações',
            'REGISTRATION_INFORMATIONS': 'Informações cadastrais',
            'SHOW': 'Visualizar',
            'EDIT': 'Editar',
            'CREATE': 'Adicionar',
            'BACK': 'Voltar',
            'SAVE': 'Salvar',
            'WEIGHT': 'Peso',
            'PARCEL': 'Remessa',
            'DUTIES': 'Impostos',
            'DESCRIPTION': 'Descrição',
            'PRICE': 'Preço mínimo',
            'STANDARD': 'Standard',
            'EXPRESS': 'Express',
            'ECONOMIC': 'Economic',
            'LISTING': {
                'ID': 'ID',
                'DESCRIPTION': 'Descrição',
                'SERVICE': 'Serviço',
                'SERVICE_LEVEL_AGREEMENT': 'Seletor Standard ou Expresso',
                'CHECKPOINT': 'Checkpoint',
                'CHARGE_TYPE': 'Tipo de cobrança',
                'APPROACH': 'Metodo de cobrança',
                'TYPE': 'Tipo',
                'APPLICABLE': 'Tipo de cobrança',
            }
        }
    }
};