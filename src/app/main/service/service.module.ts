import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { ServiceComponent } from './service.component';
import { MatTableModule } from '@angular/material';
import { DeclarationsModule } from 'app/declarations.module';
import { ServiceService } from 'app/_services/company/service.service';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'


@NgModule({
    declarations: [
        ServiceComponent
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
    ],
    exports: [
        ServiceComponent,
    ],
    providers: [
        ServiceService,
    ]
})

export class ServiceModule {
}
