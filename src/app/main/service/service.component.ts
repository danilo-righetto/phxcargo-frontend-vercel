import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from './_i18n/pt';
import { ServiceService } from 'app/_services/company/service.service';

@Component({
    selector: 'service',
    templateUrl: './service.component.html'
})
export class ServiceComponent {

    public endpoint: string = '/services';

    public displayedColumns: string[] = [
        'Descrição',
        'Nível de Serviço',
        'Checkpoint',
        'Tipo de Carga',
        'Aproximação',
    ];

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        public service: ServiceService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }
}
