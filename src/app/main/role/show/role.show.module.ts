import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { RoleShowComponent } from './role.show.component';
import { DeclarationsModule } from 'app/declarations.module';

@NgModule({
    declarations: [
        RoleShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
    ],
    exports: [
        RoleShowComponent,
    ]
})

export class RoleShowModule {
}
