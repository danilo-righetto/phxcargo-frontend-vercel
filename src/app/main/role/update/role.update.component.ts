
import {map} from 'rxjs/operators';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { RoleListingElement } from 'app/_interfaces/role/listing-element';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PermissionService } from "../../../_services/permission/permission.service";
import { PermissionListingElement as Permission } from "../../../_interfaces/permission/listing-element";
import {SelectionModel} from "@angular/cdk/collections";

@Component({
    selector: 'role-update',
    templateUrl: './role.update.component.html',
    styleUrls: ['role.update.component.css']
})
export class RoleUpdateComponent {

    private data: RoleListingElement;
    public loading: boolean = true;
    public _update: boolean = false;
    private endpoint: string = '/roles';
    private currentId = null;

    public permissions: Permission[];
    public loadingPermission: boolean = true;

    public formGroup: FormGroup;

    public selection = new SelectionModel<any>(true, []);

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private permissionService: PermissionService,
        private router: Router
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.buildFormGroup();

        this.route.params.subscribe(params => {

            if (params.id) {

                this._update = true;
                this.currentId = params.id;
                this.fetchData(`/roles/${params.id}`);
            }
        });

        this.permissionService.fetchData(null, 1000).subscribe(permissions => {
            this.permissions = permissions.data;
            this.loadingPermission = false;
        });
    }

    /**
     * Build reactive form
     *
     */
    private buildFormGroup() {
        this.formGroup = this._formBuilder.group({
            display_name: ['', Validators.required],
            description: ['', Validators.required],
            admin: ['', Validators.required]
        });
    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(role => {
                this.data = role;
                this.formGroup.patchValue({
                    display_name: role.display_name,
                    description: role.description,
                    admin: role.admin
                });
                role.permissions.forEach(row => this.selection.select(row.id));
                this.loading = false;
            });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {
        const controls = this.parseControls(this.formGroup.controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {
        const parsed = {
            "display_name": controls.display_name.value,
            "description": controls.description.value,
            "admin": controls.admin.value ? true : false,
            "permissions": this.selection.selected.map(num => {
                return {id: num};
            })
        }

        return parsed;
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * Whether the number of selected elements matches the total number of rows.
     *
     */
    public isAllSelected() {
        if(typeof this.permissions === 'undefined') {
            return false;
        }

        const numSelected = this.selection.selected.length;
        const numRows = this.permissions.length;
        return numSelected === numRows;
    }

    /**
     * Selects all rows if they are not all selected; otherwise clear selection.
     *
     */
    public masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.permissions.forEach(row => this.selection.select(row.id));
    }

    /**
     *
     */
    public getSelected() {

        return this.selection.selected[0];
    }
}