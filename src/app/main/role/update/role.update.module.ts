import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { RoleUpdateComponent } from './role.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { PermissionService } from "../../../_services/permission/permission.service";

@NgModule({
    declarations: [
        RoleUpdateComponent
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule
    ],
    exports: [
        RoleUpdateComponent
    ],
    providers: [
        PermissionService
    ]
})

export class RoleUpdateModule {
}
