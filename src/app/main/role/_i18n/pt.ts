export const locale = {
    lang: 'pt',
    data: {
        'ROLE': {
            'ROLE': 'Hierarquia',
            'ROLES': 'Hierarquias',
            'INFORMATIONS': 'Informações',
            'SHOW': 'Visualizar',
            'EDIT': 'Editar',
            'CREATE': 'Adicionar',
            'BACK': 'Voltar',
            'SAVE': 'Salvar',
            'LISTING': {
                'ID': 'ID',
                'DESCRIPTION': 'Descrição',
                'NAME': 'Nome',
                'ADMIN': 'Admin',
                'YES': 'Sim',
                'NO': 'Não',
                'PERMISSIONS': {
                    'PERMISSIONS': 'Permissões',
                    'PERMISSION': 'Permissão',
                    'DESCRIPTION': 'Descrição',
                    'SELECT_ALL': 'Selecionar tudo',
                    'ROLE_NOT_HAVE_PERMISSIONS': 'Essa hierarquia não possui permissões.',
                    'ADMINISTRATOR_RULES': 'Administradores possuem acesso à todas as areas do sistema.',
                }
            }
        }
    }
};