import { NgModule } from "@angular/core";
import { RoleComponent } from "./role.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { RoleService } from "app/_services/role/role.service";

@NgModule({
    declarations: [
        RoleComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        RoleComponent,
    ],
    providers: [
        RoleService
    ]
})
export class RoleModule {

}