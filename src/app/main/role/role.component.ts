import { Component, ViewChild } from "@angular/core";
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { locale as portuguese } from './_i18n/pt';
import { RoleService } from "app/_services/role/role.service";
import { DefaultTableComponent } from "../../_default/data-list/table.component";

@Component({
    selector: 'role',
    templateUrl: './role.component.html'
})
export class RoleComponent {

    @ViewChild(DefaultTableComponent, { static: false }) public table: DefaultTableComponent;

    public endpoint: string = '/roles';

    public displayedColumns: string[] = [
        'Nome da Permissão',
        'Descrição da Permissão',
        'Administrador'
    ];

    /**
     *
     * @param _fuseTranslationLoaderService
     * @param roleService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        public roleService: RoleService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }
}