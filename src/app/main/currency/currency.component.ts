import { Component, ViewChild } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from './_i18n/pt';
import { CurrencyListingElement } from 'app/_interfaces/currency/currency.service.interface';
import { MatTableDataSource } from '@angular/material';
import { CurrencyService } from 'app/_services/currency/currency.service';
import { DefaultTableComponent } from 'app/_default/data-list/table.component';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
})
export class CurrencyComponent {

  @ViewChild(DefaultTableComponent, { static: true })
  public table: DefaultTableComponent;

  public endpoint: string = '/currencies';

  public displayedColumns: string[] = [
    'Nome da Moeda',
    'Código da Moeda',
    'Símbolo da Moeda',
  ];

  public filterColumns = [
    {
      column: 'name',
      display_name: 'Nome da Moeda',
      type: 'string'
    },
    {
      column: 'code',
      display_name: 'Código da Moeda',
      type: 'string'
    }
  ];

  private data: CurrencyListingElement[] = [];
  public dataSource: MatTableDataSource<CurrencyListingElement>;

  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */
  constructor(
    public service: CurrencyService,
    private _fuseTranslationLoaderService: FuseTranslationLoaderService
  ) {
    this._fuseTranslationLoaderService.loadTranslations(portuguese);
  }
}