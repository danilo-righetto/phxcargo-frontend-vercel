
import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.update.component.html',
  styleUrls: ['./currency.update.component.css'],
})
export class CurrencyUpdateComponent {

  public data: any;
  public loading: string = 'true';
  public loadingCurrency: boolean = false;
  public _update: boolean = false;
  public endpoint: string = '/currencies';
  public currentId = null;
  public hidden: boolean = false;

  public currencyFormGroup: FormGroup;

  /**
   * 
   * Constructor
   * @param _fuseTranslationLoaderService 
   * @param route 
   * @param http 
   */
  constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private http: HttpAuthenticateService,
    private router: Router,
    public spinner: MatProgressSpinnerModule
  ) {
    this._fuseTranslationLoaderService.loadTranslations(portuguese);
  }

  public ngOnInit(): void {


    this.buildFormGroup();

    this.route.params.subscribe(params => {

      if (params.id) {

        this._update = true;
        this.currentId = params.id;
        this.fetchData(`/currencies/${params.id}`);
        this.loadingCurrency = true;
      }

      
      
    });

  }

  /**
   * Build reactive form
   *
   */
  private buildFormGroup() {
    this.currencyFormGroup = this._formBuilder.group({
      id: ['',],
      name: ['', Validators.required],
      symbol: ['', Validators.required],
      code: ['', Validators.required],
    });
  }

  /**
   * 
   * @param url 
   */
  private fetchData(url: string) {

    this.http.get(url).pipe(
      map((res: any) => res.data))
      .subscribe(currency => {
        this.data = currency;
        this.loadingCurrency = true;
        this.currencyFormGroup.patchValue({
          id: currency.id,
          name: currency.code,
          symbol: currency.symbol,
          code: currency.name,
        });
      });
      
  }

  /**
   * Submit form group (update|create)
   *
   */
  public submit(): void {

    if (this._update) {
      const controls = this.parseControlsCurrency(this.currencyFormGroup.controls);
      return this.update(controls);
    }

    const controls = this.parseControlsCurrencyCreate(this.currencyFormGroup.controls);
    return this.create(controls);
  }

  /**
   * Parse controls to expected data structure for request
   * 
   * @param controls 
   */
  public parseControlsCurrency(controls: any) {
    const parsed = {
      "id": controls.id.value,
      "name": controls.name.value,
      "symbol": controls.symbol.value,
      "code": controls.code.value,
    }

    return parsed;
  }

  /**
   * Parse controls to expected data structure for request
   * 
   * @param controls 
   */
  public parseControlsCurrencyCreate(controls: any) {
    const parsed = {
      "name": controls.name.value,
      "symbol": controls.symbol.value,
      "code": controls.code.value,
    }

    return parsed;
  }

  /**
   * 
   * @param controls 
   */
  private create(controls: any): void {
    this.http.post(this.endpoint, controls)
      .subscribe(res => this.router.navigate([this.endpoint]))
  }

  /**
   * 
   * @param controls 
   */
  private update(controls: any): void {
    this.http.put(`${this.endpoint}/${this.currentId}`, controls)
      .subscribe(res => this.router.navigate([this.endpoint]))
  }

}
