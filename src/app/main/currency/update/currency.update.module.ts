import { CurrencyUpdateComponent } from './currency.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatRadioModule , MAT_SLIDE_TOGGLE_DEFAULT_OPTIONS, MatProgressSpinnerModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { FormsModule } from '@angular/forms';

const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    CurrencyUpdateComponent
  ],
  imports: [
    FuseSharedModule,
    DeclarationsModule,
    MatProgressSpinnerModule,
    NgxMaskModule.forRoot(maskConfig),
    FuseSharedModule,
    DeclarationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatStepperModule,
    MatSlideToggleModule,
    BrowserAnimationsModule,
    FormsModule,
    MatProgressSpinnerModule,
  ],
  exports: [
    CurrencyUpdateComponent,
  ]
})
export class CurrencyUpdateModule {
}
