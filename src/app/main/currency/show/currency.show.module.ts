import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { CurrencyShowComponent } from './currency.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

@NgModule({
    declarations: [
        CurrencyShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
    ],
    exports: [
        CurrencyShowComponent,
    ]
})

export class CurrencyShowModule {
}