
import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { CurrencyListingElement } from 'app/_interfaces/currency/currency.service.interface';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.show.component.html',
  styleUrls: ['./currency.show.component.css'],
})
export class CurrencyShowComponent {

  public data: CurrencyListingElement[] = [];
  public loadingCurrency: boolean = true;

  /**
   * Constructor
   * 
   * @param _fuseTranslationLoaderService 
   * @param route 
   * @param http 
   */
  public constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private route: ActivatedRoute,
    private http: HttpAuthenticateService,
    public spinner: MatProgressSpinnerModule
  ) {
    this._fuseTranslationLoaderService.loadTranslations(portuguese);
  }

  public ngOnInit(): void {

    this.loadingCurrency = true;

    this.route.params.subscribe(params => {

      this.fetchDataCurrency(`/currencies/${params.id}`);
    })

  }

  /**
   * 
   * @param url 
   */
  private fetchDataCurrency(url: string) {

    this.http.get(url).pipe(
      map((res: any) => res.data))
      .subscribe(currency => {
        this.data = currency;
        this.loadingCurrency = false;
      });
  }
}