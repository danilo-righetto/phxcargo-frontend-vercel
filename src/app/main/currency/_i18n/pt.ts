export const locale = {
    lang: 'pt',
    data: {
        'CURRENCY': {
            'CURRENCIES': 'Moedas',
            'CURRENCY': 'Moeda',
            'INFORMATIONS': 'Informações',
            'REGISTRATION_INFORMATIONS': 'Informações cadastrais',
            'BACK': 'Voltar',
            'EDIT': 'Editar',
            'SAVE': 'Salvar',
            'TITLE': 'Moedas',
            'CREATE': 'Adicionar',
            'LISTING': {
                'ID': 'ID',
                'CODE': 'Código',
                'SYMBOL': 'Símbolo',
                'NAME': 'Nome',
            }
        }
    }
};