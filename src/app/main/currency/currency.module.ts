import { NgModule } from "@angular/core";
import { FuseSharedModule } from '@fuse/shared.module';
import { CurrencyComponent } from "./currency.component";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { CurrencyService } from 'app/_services/currency/currency.service';

@NgModule({
  declarations: [
    CurrencyComponent
  ],
  imports: [
    FuseSharedModule,
    MatTableModule,
    DeclarationsModule
  ],
  exports: [
    CurrencyComponent,
  ],
  providers: [
    CurrencyService,
  ]
})
export class CurrencyModule { }
