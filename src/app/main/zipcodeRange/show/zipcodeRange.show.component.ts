import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, FormGroupDirective, NgForm } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material';
import { ZipCodeRangeListingElement, ZipCodeRange } from 'app/_interfaces/zipcodeRange/zipcode.interface';
import { CompanyService } from 'app/_services/company/company.service';
import { ContractService } from 'app/_services/company/contract.service';
import { TaxClasseService as TaxClassService } from 'app/_services/customs-settings/tax-class.service';
import { ContractListingElement, ContractRequest, Contract } from 'app/_interfaces/contract/contract.interface';
import * as XLSX from 'xlsx';
import { ReplaySubject } from 'rxjs';

interface Tax_Class {
    value: string;
    viewValue: string;
}

export interface Company {
    'ID': number;
    'Código': string;
    'Nome da Empresa': string;
}

export interface TaxClasses {
    id: number;
    code: number;
    description: string;
}

@Component({
    selector: 'zipcodeRange-show',
    templateUrl: './zipcodeRange.show.component.html',
    styleUrls: ['./zipcodeRange.show.component.css'],
})

export class ZipCodeRangeShowComponent {
    public fileUrl: any;
    public fileName: string;
    // public data: ZipCodeRangeListingElement[] = [];
    public data: ZipCodeRange;
    private endpoint: string = '/zipcode-ranges';
    private currentId = null;
    public formGroupZipCodeRange: FormGroup;
    public loadingZipCodeRanges: boolean = true;
    public loadingZipCodeRangesDataValues: boolean = true;
    public loadingcontract: boolean = true;
    public _update: boolean = false;
    public companies: any;
    public contracts: any;
    public taxClass: any;
    public taxClasses: Array<{ code: number, description: string }>;
    public loadingTax: boolean = true;
    public formGroup: FormGroup;
    public companyLastFilter: FormGroup;
    public formCompany: FormGroup;
    public formContract: FormGroup;
    public formTaxClass: FormGroup;
    public dataSpreadsheet: any;
    public spreadSheetJson: any;
    public value: string;
    public viewValue: string;
    public zip_code_ranges: any;
    public spreadSheetOn: boolean = false;

    public storeData: any;
    public csvData: any;
    public jsonData: any;
    public textData: any;
    public htmlData: any;
    public fileUploaded: File;
    public worksheet: any;
    public spreadSheet: any;
    public workbook: any;
    public id: any;
    public resultsLength = 0;
    public pageSize = 10;
    public isLoadingResults = true;
    public file: File
    public title = 'XlsRead';
    public arrayBuffer: any;
    public fileList: any;
    public items = [];
    public loading: boolean = false;
    public loadingContract: boolean = false;
    public filteredCompany: ReplaySubject<Company[]> = new ReplaySubject<Company[]>(1);
    public filteredContract: ReplaySubject<Contract[]> = new ReplaySubject<Contract[]>(1);
    public filteredTaxClasses: ReplaySubject<TaxClasses[]> = new ReplaySubject<TaxClasses[]>(1);
    public classes: any;
    public company_id: string;
    public loadingCompany: boolean = true;
    public contract_id: any;
    public zipCodeRangeItems: any;

    public tax_class: Tax_Class[] = [
        { value: 'INT', viewValue: 'INT' },
        { value: 'CAP', viewValue: 'CAP' },
    ];

    /**
     * 
     * @param zip_code_ranges
     */
    public addNewZipCodeRangesGroup(zip_code_ranges = null) {
        this.newZipCodeRangesGroup.push(this.initZipCodeRangesRow(zip_code_ranges));
    }

    /**
     * 
     * @param zip_code_ranges
     */
    public addNewZipCodeRangesGroupSpreadsheet(zip_code_ranges = null) {
        this.zipCodeRangesGroup.push(this.initZipCodeRangesRowSpreadsheet(zip_code_ranges));
    }

    /**
     * 
     * @param zip_code_ranges
     */
    public addNewZipCodeRangesGroupSpreadsheetManual(zip_code_ranges = null) {
        this.zipCodeRangesGroup.push(this.initZipCodeRangesRow(zip_code_ranges));
    }

    removeZipCodeRangesGroup(item) {
        this.newZipCodeRangesGroup.controls.splice(this.items.indexOf(item), 1);
    }

    removeZipCodeRangesGroupSpread(item) {
        this.zipCodeRangesGroup.controls.splice(this.items.indexOf(item), 1);
    }

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private router: Router,
        public spinner: MatProgressSpinnerModule,
        private companyService: CompanyService,
        private contractService: ContractService,
        private taxClassService: TaxClassService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }


    public verifyFields() {
        if (
            !this.formGroupZipCodeRange.get('name').valid ||
            !this.formGroupZipCodeRange.get('active').valid ||
            !this.formGroupZipCodeRange.get('companies_id').valid ||
            !this.formGroupZipCodeRange.get('contract_id').valid ||
            !this.formGroupZipCodeRange.get('tax_class_id').valid ||
            !this.formGroupZipCodeRange.get('date_to').valid ||
            !this.formGroupZipCodeRange.get('date_from').valid
        ) {
            return true;
        }
    }

    public ngOnInit(): void {
        this.buildFormGroupZipCodeRanges();

        this.formCompany = this._formBuilder.group({
            companyLastFilter: [''],
        });

        this.formContract = this._formBuilder.group({
            contractLastFilter: [''],
        });

        this.formTaxClass = this._formBuilder.group({
            taxClassLastFilter: [''],
        });

        this.spreadSheetOn = false;
        this.route.params.subscribe(params => {

            if (params.id) {
                this._update = true;
                this.currentId = params.id;
                this.fetchDataZipCodeRanges(`/zipcode-ranges/${params.id}`);
            } else {
                // this.addNewZipCodeRangesGroup();
            }
        });

        this.companyService.fetchData(this.company_id, 255, 1).subscribe(companies => {
            this.companies = companies.data;
            this.filteredCompany.next(this.companies.slice());
            this.loadingCompany = false;
        });

        this.contractService.fetchData(this.contract_id, 255, 1).subscribe(contracts => {
            this.contracts = contracts.data;
            this.filteredContract.next(this.contracts.slice());
            this.loadingContract = false;
        });

        this.taxClassService.fetchData().subscribe(classes => {
            this.taxClasses = classes.data;
            this.classes = classes.data;
            this.filteredTaxClasses.next(this.classes.slice());
            this.loadingTax = false;
        });

        this.initCompanyFilter();
        this.initContractFilter();
        this.initTaxClassFilter();

        this.loading = false;
    }

    get zipCodeRangesGroup(): FormArray {
        return <FormArray>this.formGroupZipCodeRange.get('dataSpreadsheet');
    }

    get newZipCodeRangesGroup(): FormArray {
        return <FormArray>this.formGroupZipCodeRange.get('dataZipCodeRange');
    }

    /**
     * Build reactive form
     *
     */
    private buildFormGroupZipCodeRanges() {
        this.loadingZipCodeRanges = true;
        this.formGroupZipCodeRange = this._formBuilder.group({
            name: [{ value: '', disabled: true }, Validators.required],
            active: [{ value: '', disabled: true }, Validators.required],
            costs_id: [{ value: '', disabled: true }, Validators.required],
            companies_id: [{ value: '', disabled: true }, Validators.required],
            contract_id: [{ value: '', disabled: true }, Validators.required],
            tax_class_id: [{ value: '', disabled: true }, Validators.required],
            date_to: [{ value: '', disabled: true }, Validators.required],
            date_from: [{ value: '', disabled: true }, Validators.required],

            dataSpreadsheet: this._formBuilder.array([]),
            dataZipCodeRange: this._formBuilder.array([]),
            companyLastFilter: [{ value: '', disabled: false }],
            contractLastFilter: [{ value: '', disabled: false }],
            taxClassLastFilter: [{ value: '', disabled: false }],
        });
    }

    addfile(event) {
        this.file = event.target.files[0];
        let fileReader = new FileReader();
        fileReader.readAsArrayBuffer(this.file);
        fileReader.onload = (e) => {
            this.arrayBuffer = fileReader.result;
            var data = new Uint8Array(this.arrayBuffer);
            var arr = new Array();
            for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");
            var workbook = XLSX.read(bstr, { type: "binary" });
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];
            var arraylist = XLSX.utils.sheet_to_json(worksheet, { raw: true });
            this.spreadSheetJson = XLSX.utils.sheet_to_json(worksheet, { raw: true });
            this.fileList = [];
            this.resultsLength = arraylist.length;

            this.removeZipCodeRangesGroup(1);

            arraylist.forEach(zip_code_ranges => {
                this.addNewZipCodeRangesGroupSpreadsheet(zip_code_ranges);
            });

            this.spreadSheetOn = true;
        }
    }

    /**
     * 
     * @param zip_code_ranges
     */
    private initZipCodeRangesRow(zip_code_ranges: any = null) {

        return this._formBuilder.group({
            uf: [zip_code_ranges ? zip_code_ranges.uf : ''],
            cep_from: [zip_code_ranges ? zip_code_ranges.cep_from : ''],
            cep_to: [zip_code_ranges ? zip_code_ranges.cep_to : ''],
            type_zone: [zip_code_ranges ? zip_code_ranges.type_zone : ''],
            weight_from: [zip_code_ranges ? zip_code_ranges.weight_from : ''],
            weight_to: [zip_code_ranges ? zip_code_ranges.weight_to : ''],
            rate: [zip_code_ranges ? zip_code_ranges.rate : ''],
            tax_class_id: [zip_code_ranges ? zip_code_ranges.tax_class_id : ''],
        });
    }

    /**
     * 
     * @param zip_code_ranges
     */
    private initZipCodeRangesRowSpreadsheet(zip_code_ranges: any = null) {

        return this._formBuilder.group({
            cep_from: [zip_code_ranges['CepFrom'] ? zip_code_ranges['CepFrom'] : ''],
            cep_to: [zip_code_ranges['CepTo'] ? zip_code_ranges['CepTo'] : ''],
            uf: [zip_code_ranges['UFs'] ? zip_code_ranges['UFs'] : ''],
            type_zone: [zip_code_ranges['TypeZone'] ? zip_code_ranges['TypeZone'] : ''],
            weight_from: [zip_code_ranges['FromWeigth'] ? zip_code_ranges['FromWeigth'] : ''],
            weight_to: [zip_code_ranges['ToWeigth'] ? zip_code_ranges['ToWeigth'] : ''],
            rate: [zip_code_ranges['Rate'] ? zip_code_ranges['Rate'] : ''],
            tax_class_id: [zip_code_ranges['Modal'] ? zip_code_ranges['Modal'] : ''],
        });
    }


    /**
     * 
     * @param url 
     */
    private fetchDataZipCodeRanges(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(zipcodeRanges => {
                this.data = new ZipCodeRange(zipcodeRanges);
                this.id = zipcodeRanges.id;
                this.zipCodeRangeItems = zipcodeRanges.items ? zipcodeRanges.items : null;
                this.formGroupZipCodeRange.patchValue({
                    name: zipcodeRanges.name ? zipcodeRanges.name : '',
                    active: zipcodeRanges.active ? zipcodeRanges.active : 0,
                    companies_id: zipcodeRanges.companies_id ? zipcodeRanges.companies_id.id : null,
                    contract_id: zipcodeRanges.contract_id ? zipcodeRanges.contract_id.id : null,
                    tax_class_id: zipcodeRanges.tax_class_id ? zipcodeRanges.tax_class_id.id : null,
                    date_to: zipcodeRanges.date_to ? zipcodeRanges.date_to : '',
                    date_from: zipcodeRanges.date_from ? zipcodeRanges.date_from : '',
                });
                // if (zipcodeRanges.cep_ranges) {
                //     zipcodeRanges.cep_ranges.forEach(cep_ranges => {
                //         this.addNewZipCodeRangesGroup(cep_ranges);
                //     });
                // } else {
                //     this.addNewZipCodeRangesGroup();
                // }

                this.loadingZipCodeRangesDataValues = false;
                this.loading = false;
            });
    }

    public initCompanyFilter() {
        const companyLastFilter = this.formCompany.controls.companyLastFilter;

        companyLastFilter.valueChanges.subscribe(() => {
            let search = companyLastFilter.value.toLowerCase();

            this.filteredCompany.next(
                this.companies.filter(company => company.code.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initContractFilter() {
        const contractLastFilter = this.formContract.controls.contractLastFilter;

        contractLastFilter.valueChanges.subscribe(() => {
            let search = contractLastFilter.value.toLowerCase();

            this.filteredContract.next(
                this.contracts.filter(contract => contract.description.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initTaxClassFilter() {
        const taxClassastFilter = this.formTaxClass.controls.taxClassLastFilter;

        taxClassastFilter.valueChanges.subscribe(() => {
            let search = taxClassastFilter.value.toLowerCase();

            this.filteredContract.next(
                this.taxClass.filter(taxClass => taxClass.description.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {
        if (this.verifyFields() == false) {
            return;
        }

        const controls = this.parseControls(this.formGroupZipCodeRange.controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControlsCreate(controls: any) {

        // if (typeof this.spreadSheetJson == undefined) {
        //     this.spreadSheetJson.forEach(zip_code_ranges => {
        //         this.initZipCodeRangesRowSpreadsheet(zip_code_ranges);
        //     });
        // }
        const parsed = {
        //     "contract_id": controls.contract_id.value,
        //     "company_id": controls.company_id.value,
        //     "date_to": controls.date_to.value.format("YYYY-MM-DDTHH:mm"),
        //     "date_from": controls.date_from.value.format("YYYY-MM-DDTHH:mm"),
        //     "zip_code_ranges": this.spreadSheetJson ? this.spreadSheetJson.map((zip_code_ranges: FormGroup) => {
        //         return {
        //             "uf": zip_code_ranges['UFs'],
        //             "cep_from": zip_code_ranges['CepFrom'],
        //             "cep_to": zip_code_ranges['CepTo'],
        //             "type_zone": zip_code_ranges['TypeZone'],
        //             "weight_from": zip_code_ranges['FromWeigth'],
        //             "weight_to": zip_code_ranges['ToWeigth'],
        //             "rate": zip_code_ranges['Rate'],
        //             "tax_class_id": zip_code_ranges['Modal'],
        //         }
        //     }) : false,
        //     "manual_zip_code_ranges": this.newZipCodeRangesGroup ? this.newZipCodeRangesGroup.value.map((zip_code_ranges: FormGroup) => {
        //         return {
        //             "uf": zip_code_ranges['uf'],
        //             "cep_from": zip_code_ranges['cep_from'],
        //             "cep_to": zip_code_ranges['cep_to'],
        //             "type_zone": zip_code_ranges['type_zone'],
        //             "weight_from": zip_code_ranges['weight_from'],
        //             "weight_to": zip_code_ranges['weight_to'],
        //             "rate": zip_code_ranges['rate'],
        //             "tax_class_id": zip_code_ranges['tax_class_id'],
        //         }
        //     }) : false,
        }
        return parsed;
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {

        let date_to = new Date(controls.date_to.value);
        let date_to_formated = '';
        date_to_formated = date_to.getFullYear() + "-" + ("0" + (date_to.getMonth() + 1)).slice(-2) + "-" + ("0" + (date_to.getDate())).slice(-2);

        let date_from = new Date(controls.date_from.value);
        let date_from_formated = '';
        date_from_formated = date_from.getFullYear() + "-" + ("0" + (date_from.getMonth() + 1)).slice(-2) + "-" + ("0" + (date_from.getDate())).slice(-2);

        const parsed = {
            'name': controls.name.value,
            'active': controls.active.value ? controls.active.value : 0,
            'costs_id': 1,
            'companies_id': controls.companies_id.value,
            'contract_id': controls.contract_id.value,
            'tax_class_id': controls.tax_class_id.value,
            'date_to': date_to_formated,
            'date_from': date_from_formated	
        }
        return parsed;

        // if (typeof this.spreadSheetJson == undefined) {
        //     const parsed = {
        //         "id": this.id,
        //         "contract_id": controls.contract_id.value,
        //         "company_id": controls.company_id.value,
        //         "date_to": controls.new_date_to.value.format("YYYY-MM-DDTHH:mm"),
        //         "date_from": controls.new_date_from.value.format("YYYY-MM-DDTHH:mm"),
        //         "zip_code_ranges": this.spreadSheetJson ? this.spreadSheetJson.map((zip_code_ranges: FormGroup) => {
        //             return {
        //                 "uf": zip_code_ranges['UFs'] ? zip_code_ranges['UFs'] : '',
        //                 "cep_from": zip_code_ranges['CepFrom'] ? zip_code_ranges['CepFrom'] : '',
        //                 "cep_to": zip_code_ranges['CepTo'] ? zip_code_ranges['CepTo'] : '',
        //                 "type_zone": zip_code_ranges['TypeZone'] ? zip_code_ranges['TypeZone'] : '',
        //                 "weight_from": zip_code_ranges['FromWeigth'] ? zip_code_ranges['FromWeigth'] : '',
        //                 "weight_to": zip_code_ranges['ToWeigth'] ? zip_code_ranges['ToWeigth'] : '',
        //                 "rate": zip_code_ranges['Rate'] ? zip_code_ranges['Rate'] : '',
        //                 "tax_class_id": zip_code_ranges['Modal'] ? zip_code_ranges['Modal'] : '',
        //             }
        //         }) : '',
        //         "manual_zip_code_ranges": this.newZipCodeRangesGroup ? this.newZipCodeRangesGroup.value.map((zip_code_ranges: FormGroup) => {
        //             return {
        //                 "uf": zip_code_ranges['uf'],
        //                 "cep_from": zip_code_ranges['cep_from'],
        //                 "cep_to": zip_code_ranges['cep_to'],
        //                 "type_zone": zip_code_ranges['type_zone'],
        //                 "weight_from": zip_code_ranges['weight_from'],
        //                 "weight_to": zip_code_ranges['weight_to'],
        //                 "rate": zip_code_ranges['rate'],
        //                 "tax_class_id": zip_code_ranges['tax_class_id'],
        //             }
        //         }) : '',
        //     }
        //     return parsed;
        // } else {
        //     const parsed = {
        //         "id": this.id,
        //         "contract_id": controls.contract_id.value,
        //         "company_id": controls.company_id.value,
        //         "date_to": controls.new_date_to.value.format("YYYY-MM-DDTHH:mm"),
        //         "date_from": controls.new_date_from.value.format("YYYY-MM-DDTHH:mm"),
        //         "zip_code_ranges": this.newZipCodeRangesGroup.value.map((zip_code_ranges: FormGroup) => {
        //             return {
        //                 "uf": zip_code_ranges['uf'],
        //                 "cep_from": zip_code_ranges['cep_from'],
        //                 "cep_to": zip_code_ranges['cep_to'],
        //                 "type_zone": zip_code_ranges['type_zone'],
        //                 "weight_from": zip_code_ranges['weight_from'],
        //                 "weight_to": zip_code_ranges['weight_to'],
        //                 "rate": zip_code_ranges['rate'],
        //                 "tax_class_id": zip_code_ranges['tax_class_id'],
        //             }
        //         }),
        //     }
        //     return parsed;
        // }
    }


    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }
}
