import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { ZipCodeRangeShowComponent } from './zipcodeRange.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { MatTableModule } from '@angular/material';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { FormsModule } from '@angular/forms';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { NgxPaginationModule } from 'ngx-pagination';
import { OwlDateTimeModule, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE, OwlDateTimeIntl } from 'ng-pick-datetime';

export class DefaultIntl extends OwlDateTimeIntl {
    setBtnLabel = 'Selecionar';
    cancelBtnLabel = 'Cancelar';
};

export const MY_MOMENT_FORMATS = {
    parseInput: 'l LT',
    fullPickerInput: 'DD/MM/YYYY HH:mm',
    datePickerInput: 'l',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
};

const maskConfig: Partial<IConfig> = {
    validation: false,
};

@NgModule({
    declarations: [
        ZipCodeRangeShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        BrowserAnimationsModule,
        MatProgressSpinnerModule,
        BrowserModule, 
        MatTableModule,
        NgxMatSelectSearchModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatSortModule,
        FormsModule,
        NgxMaskModule.forRoot(maskConfig),
        NgxPaginationModule,
        OwlDateTimeModule,
    ],
    exports: [
        ZipCodeRangeShowComponent,
        MatPaginatorModule,
    ],
    providers: [
        { provide: OwlDateTimeIntl, useClass: DefaultIntl },
        { provide: OWL_DATE_TIME_LOCALE, useValue: 'pt-BR' },
        { provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS },

    ]
})

export class ZipCodeRangeShowModule {
}
