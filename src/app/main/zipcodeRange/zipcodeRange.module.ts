import { NgModule } from "@angular/core";
import { ZipCodeRangeService } from "app/_services/zipcodeRange/zipcodeRange.service";
import { ZipCodeRangeComponent } from "./zipcodeRange.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";

@NgModule({
    declarations: [
        ZipCodeRangeComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        ZipCodeRangeComponent,
    ],
    providers: [
        ZipCodeRangeService,
    ],
})
export class ZipCodeRangeModule { }