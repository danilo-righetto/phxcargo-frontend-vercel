import { ZipCodeRangeListingElement, ZipCodeRange } from 'app/_interfaces/zipcodeRange/zipcode.interface';
import { Component, ViewChild } from "@angular/core";
import { ZipCodeRangeService } from "app/_services/zipcodeRange/zipcodeRange.service";
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { MatTableDataSource } from '@angular/material';
import { locale as portuguese } from './_i18n/pt';
import { DefaultTableComponent } from 'app/_default/data-list/table.component';

@Component({
    selector: 'zipcode-range',
    templateUrl: 'zipcodeRange.component.html',
})
export class ZipCodeRangeComponent {

    public endpoint: string = '/zipcode-ranges';

    public displayedColumns: string[] = [
        'Nome',
        'Empresa',
        'Data De',
        'Data Para',
        'Ativo'
    ];

    public data: ZipCodeRangeListingElement[] = [];

    public dataSource: MatTableDataSource<ZipCodeRangeListingElement>;

    /**
     * 
     * @param service 
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        public service: ZipCodeRangeService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }
}
