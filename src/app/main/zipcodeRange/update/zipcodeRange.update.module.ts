import { NgModule } from "@angular/core";
import { ZipCodeRangeService } from "app/_services/zipcodeRange/zipcodeRange.service";
import { ZipCodeRangeUpdateComponent } from "./zipcodeRange.update.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { DeclarationsModule } from "app/declarations.module";
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatPaginatorModule, MatSortModule, MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { NgxPaginationModule } from 'ngx-pagination';
import { BrowserModule } from '@angular/platform-browser';
import { DateTimeModule } from 'app/_default/datetime/datetime.default.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';


const maskConfig: Partial<IConfig> = {
    validation: false,
};

@NgModule({
    declarations: [
        ZipCodeRangeUpdateComponent,
    ],
    imports: [
        NgxMaskModule.forRoot(maskConfig),
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
        FormsModule,
        NgxPaginationModule,
        BrowserModule,
        MatSortModule,
        DateTimeModule,
        NgxMatSelectSearchModule
    ],
    exports: [
        ZipCodeRangeUpdateComponent,
        MatPaginatorModule,
    ],
    providers: [
        ZipCodeRangeService,
    ],
})
export class ZipCodeRangeUpdateModule { }