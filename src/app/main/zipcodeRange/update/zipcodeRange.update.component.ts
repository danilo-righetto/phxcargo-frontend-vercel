import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, FormGroupDirective, NgForm } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material';
import { ZipCodeRangeListingElement, ZipCodeRange } from 'app/_interfaces/zipcodeRange/zipcode.interface';
import { CompanyService } from 'app/_services/company/company.service';
import { ContractService } from 'app/_services/company/contract.service';
import { TaxClasseService as TaxClassService } from 'app/_services/customs-settings/tax-class.service';
import { ContractListingElement, ContractRequest, Contract } from 'app/_interfaces/contract/contract.interface';
import { NotificationService } from 'app/_services/notification/notification.service';
import * as XLSX from 'xlsx';
import { ReplaySubject } from 'rxjs';
import {saveAs as importedSaveAs} from "file-saver";

interface Tax_Class {
    value: string;
    viewValue: string;
}

export interface Company {
    'ID': number;
    'Código': string;
    'Nome da Empresa': string;
}

export interface TaxClasses {
    id: number;
    code: number;
    description: string;
}

@Component({
    selector: 'zipcodeRange-update',
    templateUrl: './zipcodeRange.update.component.html',
    styleUrls: ['./zipcodeRange.update.component.css'],
})
export class ZipCodeRangeUpdateComponent {
    public fileUrl: any;
    public fileName: string;
    public data: ZipCodeRange;
    // public data: ZipCodeRangeListingElement[] = [];
    private endpoint: string = '/zipcode-ranges';
    private currentId = null;
    public formGroupZipCodeRange: FormGroup;
    public loadingZipCodeRanges: boolean = true;
    public loadingZipCodeRangesDataValues: boolean = true;
    public loadingcontract: boolean = true;
    public _update: boolean = false;
    public companies: any;
    public contracts: any;
    public taxClass: any;
    public taxClasses: Array<{ code: number, description: string }>;
    public loadingTax: boolean = true;
    public formGroup: FormGroup;
    public companyLastFilter: FormGroup;
    public formCompany: FormGroup;
    public formContract: FormGroup;
    public formTaxClass: FormGroup;
    public dataSpreadsheet: any;
    public spreadSheetJson: any;
    public value: string;
    public viewValue: string;
    public zip_code_ranges: any;
    public spreadSheetOn: boolean = false;

    public storeData: any;
    public csvData: any;
    public jsonData: any;
    public textData: any;
    public htmlData: any;
    public fileUploaded: File;
    public worksheet: any;
    public spreadSheet: any;
    public workbook: any;
    public id: any;
    public resultsLength = 0;
    public pageSize = 10;
    public isLoadingResults = true;
    public file: File
    public title = 'XlsRead';
    public arrayBuffer: any;
    public fileList: any;
    public items = [];
    public loading: boolean = false;
    public loadingContract: boolean = false;
    public filteredCompany: ReplaySubject<Company[]> = new ReplaySubject<Company[]>(1);
    public filteredContract: ReplaySubject<Contract[]> = new ReplaySubject<Contract[]>(1);
    public filteredTaxClasses: ReplaySubject<TaxClasses[]> = new ReplaySubject<TaxClasses[]>(1);
    public classes: any;
    public company_id: string;
    public loadingCompany: boolean = true;
    public contract_id: any;
    public arrayListSize: number = 0;

    public tax_class: Tax_Class[] = [
        { value: 'INT', viewValue: 'INT' },
        { value: 'CAP', viewValue: 'CAP' },
    ];

    /**
     * 
     * @param zip_code_ranges
     */
    public addNewZipCodeRangesGroup(zip_code_ranges = null) {
        this.notificationService.notify('Item adicionado com sucesso !');
        this.newZipCodeRangesGroup.push(this.initZipCodeRangesRow(zip_code_ranges));
    }

    /**
     * 
     * @param zip_code_ranges
     */
    public addNewZipCodeRangesGroupSpreadsheet(zip_code_ranges = null, spreadsheet: boolean = true) {
        this.notificationService.notify('Item adicionado com sucesso !');
        this.zipCodeRangesGroup.push(this.initZipCodeRangesRowSpreadsheet(zip_code_ranges, spreadsheet));
    }

    /**
     * 
     * @param zip_code_ranges
     */
    public addNewZipCodeRangesGroupSpreadsheetManual(zip_code_ranges = null) {
        this.arrayListSize++;
        this.notificationService.notify('Item adicionado com sucesso !');
        this.zipCodeRangesGroup.push(this.initZipCodeRangesRow(zip_code_ranges));
    }

    removeZipCodeRangesGroup(item) {
        this.arrayListSize > 0 ? this.arrayListSize-- : 0;
        this.notificationService.notify('Item excluído com sucesso ! ', 'error');
        this.newZipCodeRangesGroup.controls.splice(this.items.indexOf(item), 1);
    }

    removeZipCodeRangesGroupSpread(item) {
        this.arrayListSize > 0 ? this.arrayListSize-- : 0;
        this.notificationService.notify('Item excluído com sucesso ! ', 'error');
        this.zipCodeRangesGroup.controls.splice(this.items.indexOf(item), 1);
    }

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private router: Router,
        public spinner: MatProgressSpinnerModule,
        private companyService: CompanyService,
        private contractService: ContractService,
        private taxClassService: TaxClassService,
        private notificationService: NotificationService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }


    public verifyFields() {
        if (
            !this.formGroupZipCodeRange.get('name').valid ||
            !this.formGroupZipCodeRange.get('active').valid ||
            !this.formGroupZipCodeRange.get('companies_id').valid ||
            !this.formGroupZipCodeRange.get('contract_id').valid ||
            !this.formGroupZipCodeRange.get('tax_class_id').valid ||
            !this.formGroupZipCodeRange.get('date_to').valid ||
            !this.formGroupZipCodeRange.get('date_from').valid
        ) {
            return true;
        }
    }

    public ngOnInit(): void {
        this.buildFormGroupZipCodeRanges();

        this.formCompany = this._formBuilder.group({
            companyLastFilter: [''],
        });

        this.formContract = this._formBuilder.group({
            contractLastFilter: [''],
        });

        this.formTaxClass = this._formBuilder.group({
            taxClassLastFilter: [''],
        });

        this.spreadSheetOn = false;
        this.route.params.subscribe(params => {
            if (params.id) {
                this._update = true;
                this.currentId = params.id;
                this.fetchDataZipCodeRanges(`/zipcode-ranges/${params.id}`);
            } else {
                this.addNewZipCodeRangesGroupSpreadsheet(null, false);
            }
        });

        this.companyService.fetchData(this.company_id, 255, 1).subscribe(companies => {
            this.companies = companies.data;
            this.filteredCompany.next(this.companies.slice());
            this.loadingCompany = false;
        });

        this.contractService.fetchData(this.contract_id, 255, 1).subscribe(contracts => {
            this.contracts = contracts.data;
            this.filteredContract.next(this.contracts.slice());
            this.loadingContract = false;
        });

        this.taxClassService.fetchData().subscribe(classes => {
            this.taxClasses = classes.data;
            this.classes = classes.data;
            this.filteredTaxClasses.next(this.classes.slice());
            this.loadingTax = false;
        });

        this.initCompanyFilter();
        this.initContractFilter();
        this.initTaxClassFilter();

        this.loading = false;
    }

    get zipCodeRangesGroup(): FormArray {
        return <FormArray>this.formGroupZipCodeRange.get('dataSpreadsheet');
    }

    get newZipCodeRangesGroup(): FormArray {
        return <FormArray>this.formGroupZipCodeRange.get('dataZipCodeRange');
    }

    /**
     * Build reactive form
     *
     */
    private buildFormGroupZipCodeRanges() {
        this.loadingZipCodeRanges = true;
        this.formGroupZipCodeRange = this._formBuilder.group({
            name: [{ value: '', disabled: false }, Validators.required],
            active: [{ value: '', disabled: false }, Validators.required],
            costs_id: [{ value: '', disabled: false }, Validators.required],
            companies_id: [{ value: '', disabled: false }, Validators.required],
            contract_id: [{ value: '', disabled: false }, Validators.required],
            tax_class_id: [{ value: '', disabled: false }, Validators.required],
            date_to: [{ value: '', disabled: false }, Validators.required],
            date_from: [{ value: '', disabled: false }, Validators.required],

            items_list: this._formBuilder.array([]),
            dataSpreadsheet: this._formBuilder.array([]),
            dataZipCodeRange: this._formBuilder.array([]),
            companyLastFilter: [{ value: '', disabled: false }],
            contractLastFilter: [{ value: '', disabled: false }],
            taxClassLastFilter: [{ value: '', disabled: false }],
        });
    }

    get itemsGroup(): FormArray {
        return <FormArray>this.formGroupZipCodeRange.get('items_list');
    }

     /**
     * 
     * @param item
     */
    public addNewItemsGroups(item = null) {
        this.itemsGroup.push(this.initItemsRow(item));
    }

    public removeItemsGroup(item)
    {
        var listOfServices = this.itemsGroup.value;
        let lenghtArray = listOfServices.length - 1;
        listOfServices.splice(lenghtArray, 1);
        this.itemsGroup.controls.splice(this.items.indexOf(item), 1);
        this.itemsGroup.updateValueAndValidity()
        this.notificationService.notify('Item excluído com sucesso ! ', 'error');
        if (listOfServices.length == 0) {
            this.addNewItemsGroup();
        }
    }

    public addNewItemsGroup(item = null)
    {
        var listOfServices = this.itemsGroup.value;
        let check = 0;
        let checkServices = listOfServices.map(function (item, index, array) {
            let count = listOfServices.length - 1;
            if (count == index) {
                if (item.id == '') {
                    return check = 1;
                } else {
                    return check = 2;
                }
            }
        });

        if (check == 1) {
            this.notificationService.notify('Os campos descrição e valor são obrigatórios ! ', 'error');
            return null;
        } else {
            if (item == null || item == ''){
                this.notificationService.notify('Item adicionado com sucesso ! ');
            }
            this.itemsGroup.push(this.initItemsRow(item));
        }
    }

    /**
     * 
     * @param item
     */
    private initItemsRow(item: any = null) {

        return this._formBuilder.group({
            type_of_service: [item ? item['type_service'] : ''],
            ufs: [item ? item['uf'] : ''],
            from_weigth: [item ? item['from_weigth'] : 0],
            to_weigth: [item ? item['to_weigth'] : 0],
            rate: [item ? item['rate'] : 0],
            add_rate: [item ? item['add_rate'] : 0],
            modal: [item ? item['modal'] : 0],
            type_zone: [item ? item['type_zone'] : 0],
            cep_from: [item ? item['cep_from'] : ''],
            cep_to: [item ? item['cep_to'] : ''],
        });
    }

    addfile(event) {
        this.file = event.target.files[0];
        let fileReader = new FileReader();
        fileReader.readAsArrayBuffer(this.file);
        fileReader.onload = (e) => {
            this.arrayBuffer = fileReader.result;
            var data = new Uint8Array(this.arrayBuffer);
            var arr = new Array();
            for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");
            var workbook = XLSX.read(bstr, { type: "binary" });
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];
            var arraylist = XLSX.utils.sheet_to_json(worksheet, { raw: true });
            this.spreadSheetJson = XLSX.utils.sheet_to_json(worksheet, { raw: true });
            this.fileList = [];
            this.resultsLength = arraylist.length;

            if (this.checkSpreadSheet(arraylist).length > 0) {
                let error = this.checkSpreadSheet(arraylist);
                this.notificationService.notify(error, 'error');
                return '';
            }

            arraylist.forEach(zip_code_ranges => {
                this.arrayListSize++;
                this.addNewZipCodeRangesGroupSpreadsheet(zip_code_ranges, true);
            });

            this.spreadSheetOn = true;
        }
    }

    public checkSpreadSheet(arraylist) {
        var errors = '';
        for (let index = 0; index < arraylist.length; index++) {
            if (Object.keys(arraylist[index]).length < 8) {
                errors = 'A planilha não possui os campos necessários';
                break;
            }

            /* TypeOfService */
            if (Object.values(arraylist[index]).indexOf('TypeOfService') < -1) {
                errors = 'O campo TypeOfService não existe. Linha: ' + (index + 2);
                break;
            }

            if (typeof arraylist[index]['TypeOfService'] != 'string') {
                errors = 'O campo TypeOfService não é texto. Linha: ' + (index + 2);
                break;
            } else if (arraylist[index]['TypeOfService'].length <= 0) {
                errors = 'O campo TypeOfService está vazio. Linha: ' + (index + 2);
                break;
            }

            /* UFs */
            if (Object.values(arraylist[index]).indexOf('UFs') < -1) {
                errors = 'O campo UFs não existe. Linha: ' + (index + 2);
                break;
            }

            if (typeof arraylist[index]['UFs'] != 'string') {
                errors = 'O campo UFs não é texto. Linha: ' + (index + 2);
                break;
            } else if (arraylist[index]['UFs'].length <= 0) {
                errors = 'O campo UFs está vazio. Linha: ' + (index + 2);
                break;
            }

            /* FromWeigth */
            if (Object.values(arraylist[index]).indexOf('FromWeigth') < -1) {
                errors = 'O campo FromWeigth não existe. Linha: ' + (index + 2);
                break;
            }

            if (!parseFloat(arraylist[index]['FromWeigth']) && arraylist[index]['FromWeigth'] != '0') {
                errors = 'O campo FromWeigth não é um número. Linha: ' + (index + 2);
                break;
            }

            /* ToWeigth */
            if (Object.values(arraylist[index]).indexOf('ToWeigth') < -1) {
                errors = 'O campo ToWeigth não existe. Linha: ' + (index + 2);
                break;
            }

            if (!parseFloat(arraylist[index]['ToWeigth']) && arraylist[index]['ToWeigth'] != '0') {
                errors = 'O campo ToWeigth não é um número. Linha: ' + (index + 2);
                break;
            }

            /* Rate */
            if (Object.values(arraylist[index]).indexOf('Rate') < -1) {
                errors = 'O campo Rate não existe. Linha: ' + (index + 2);
                break;
            }

            if (!parseFloat(arraylist[index]['Rate']) && arraylist[index]['Rate'] != '0') {
                errors = 'O campo Rate não é um número. Linha: ' + (index + 2);
                break;
            }

            /* AddRate */
            if (Object.values(arraylist[index]).indexOf('AddRate') < -1) {
                errors = 'O campo AddRate não existe. Linha: ' + (index + 2);
                break;
            }

            if (!parseFloat(arraylist[index]['AddRate']) && arraylist[index]['AddRate'] != '0') {
                errors = 'O campo AddRate não é um número. Linha: ' + (index + 2);
                break;
            }

            /* Modal */
            if (Object.values(arraylist[index]).indexOf('Modal') < -1) {
                errors = 'O campo Modal não existe. Linha: ' + (index + 2);
                break;
            }

            if (!parseFloat(arraylist[index]['Modal']) && arraylist[index]['Modal'] != '0') {
                errors = 'O campo Modal não é um número. Linha: ' + (index + 2);
                break;
            }

            /* TypeZone */
            if (Object.values(arraylist[index]).indexOf('TypeZone') < -1) {
                errors = 'O campo TypeZone não existe. Linha: ' + (index + 2);
                break;
            }

            if (!parseFloat(arraylist[index]['TypeZone']) && arraylist[index]['TypeZone'] != '0') {
                errors = 'O campo TypeZone não é um número. Linha: ' + (index + 2);
                break;
            }

            /* CepFrom */
            if (Object.values(arraylist[index]).indexOf('CepFrom') > -1) {
                if (typeof arraylist[index]['CepFrom'] != 'string') {
                    errors = 'O campo CepFrom não é um texto. Linha: ' + (index + 2);
                    break;
                } else if (arraylist[index]['CepFrom'].length <= 0) {
                    errors = 'O campo CepFrom está vazio. Linha: ' + (index + 2);
                    break;
                }
            }

            /* CepTo */
            if (Object.values(arraylist[index]).indexOf('CepTo') > -1) {
                if (typeof arraylist[index]['CepTo'] != 'string') {
                    errors = 'O campo CepTo não é um texto. Linha: ' + (index + 2);
                    break;
                } else if (arraylist[index]['CepTo'].length <= 0) {
                    errors = 'O campo CepTo está vazio. Linha: ' + (index + 2);
                    break;
                }
            }
        }
        return errors;
    }

    /**
     * 
     * @param zip_code_ranges
     */
    private initZipCodeRangesRow(zip_code_ranges: any = null) {

        if (zip_code_ranges != null) {
            return this._formBuilder.group({
                type_of_service: [{ value: zip_code_ranges ? zip_code_ranges['TypeOfService'] : '', disabled: false }, Validators.required],
                ufs: [{ value: zip_code_ranges ? zip_code_ranges['UFs'] : '', disabled: false }, Validators.required],
                from_weigth: [{ value: zip_code_ranges ? zip_code_ranges['FromWeigth'].toString().replace(",", ".") : '', disabled: false }, Validators.required],
                to_weigth: [{ value: zip_code_ranges ? zip_code_ranges['ToWeigth'].toString().replace(",", ".") : '', disabled: false }, Validators.required],
                rate: [{ value: zip_code_ranges ? zip_code_ranges['Rate'].toString().replace(",", ".") : '', disabled: false }, Validators.required],
                add_rate: [{ value: zip_code_ranges ? zip_code_ranges['AddRate'].toString().replace(",", ".") : '', disabled: false }, Validators.required], 
                modal: [{ value: zip_code_ranges ? zip_code_ranges['Modal'].toString().replace(",", ".") : '', disabled: false }, Validators.required],
                type_zone: [{ value: zip_code_ranges ? zip_code_ranges['TypeZone'] : '', disabled: false }, Validators.required],
                cep_from: [{ value: zip_code_ranges ? zip_code_ranges['CepFrom'] : '', disabled: false }, Validators.required],
                cep_to: [{ value: zip_code_ranges ? zip_code_ranges['CepTo'] : '', disabled: false }, Validators.required],
            });
        }

        return this._formBuilder.group({
            type_of_service: [{ value: '', disabled: false }, Validators.required],
            ufs: [{ value: '', disabled: false }, Validators.required],
            from_weigth: [{ value: '', disabled: false }, Validators.required],
            to_weigth: [{ value: '', disabled: false }, Validators.required],
            rate: [{ value: '', disabled: false }, Validators.required],
            add_rate: [{ value: '', disabled: false }, Validators.required],
            modal:  [{ value: '', disabled: false }, Validators.required],
            type_zone: [{ value: '', disabled: false }, Validators.required],
            cep_from: [{ value: '', disabled: false }, Validators.required],
            cep_to: [{ value: '', disabled: false }, Validators.required],
        });
    }

    /**
     * 
     * @param zip_code_ranges
     */
    private initZipCodeRangesRowSpreadsheet(zip_code_ranges: any = null, spreadsheet: boolean = true) {

        if (spreadsheet) {
            return this._formBuilder.group({
                type_of_service: [{ value: zip_code_ranges ? zip_code_ranges['TypeOfService'] : '', disabled: false }, Validators.required],
                ufs: [{ value: zip_code_ranges ? zip_code_ranges['UFs'] : '', disabled: false }, Validators.required],
                from_weigth: [{ value: zip_code_ranges ? zip_code_ranges['FromWeigth'] : '', disabled: false }, Validators.required],
                to_weigth: [{ value: zip_code_ranges ? zip_code_ranges['ToWeigth'] : '', disabled: false }, Validators.required],
                rate: [{ value: zip_code_ranges ? zip_code_ranges['Rate'] : '', disabled: false }, Validators.required],
                add_rate: [{ value: zip_code_ranges ? zip_code_ranges['AddRate'] : '', disabled: false }, Validators.required], 
                modal: [{ value: zip_code_ranges ? zip_code_ranges['Modal'] : '', disabled: false }, Validators.required],
                type_zone: [{ value: zip_code_ranges ? zip_code_ranges['TypeZone'] : '', disabled: false }, Validators.required],
                cep_from: [{ value: zip_code_ranges ? zip_code_ranges['CepFrom'] : '', disabled: false }, Validators.required],
                cep_to: [{ value: zip_code_ranges ? zip_code_ranges['CepTo'] : '', disabled: false }, Validators.required],
            });
        }

        return this._formBuilder.group({
            type_of_service: [{ value: zip_code_ranges ? zip_code_ranges['type_service'] : '', disabled: false }, Validators.required],
            ufs: [{ value: zip_code_ranges ? zip_code_ranges['uf'] : '', disabled: false }, Validators.required],
            from_weigth: [{ value: zip_code_ranges ? zip_code_ranges['from_weigth'] : '', disabled: false }, Validators.required],
            to_weigth: [{ value: zip_code_ranges ? zip_code_ranges['to_weigth'] : '', disabled: false }, Validators.required],
            rate: [{ value: zip_code_ranges ? zip_code_ranges['rate'] : '', disabled: false }, Validators.required],
            add_rate: [{ value: zip_code_ranges ? zip_code_ranges['add_rate'] : '', disabled: false }, Validators.required], 
            modal: [{ value: zip_code_ranges ? zip_code_ranges['modal'] : '', disabled: false }, Validators.required],
            type_zone: [{ value: zip_code_ranges ? zip_code_ranges['type_zone'] : '', disabled: false }, Validators.required],
            cep_from: [{ value: zip_code_ranges ? zip_code_ranges['cep_from'] : '', disabled: false }, Validators.required],
            cep_to: [{ value: zip_code_ranges ? zip_code_ranges['cep_to'] : '', disabled: false }, Validators.required],
        });
        
    }

    public uploadReturn(fileInputEvent: any) {
        const form = new FormData();
        form.append(
            "import",
            fileInputEvent.target.files[0]
        );
    }


    /**
     * 
     * @param url 
     */
    private fetchDataZipCodeRanges(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(zipcodeRanges => {
                this.data = new ZipCodeRange(zipcodeRanges);
                // this.items = zipcodeRanges.items;
                this.items = this.data.items;
                this.id = zipcodeRanges.id;
                this.formGroupZipCodeRange.patchValue({
                    name: zipcodeRanges.name ? zipcodeRanges.name : '',
                    active: zipcodeRanges.active ? zipcodeRanges.active : 0,
                    companies_id: zipcodeRanges.companies_id ? zipcodeRanges.companies_id.id : null,
                    contract_id: zipcodeRanges.contract_id ? zipcodeRanges.contract_id.id : null,
                    tax_class_id: zipcodeRanges.tax_class_id ? zipcodeRanges.tax_class_id.id : null,
                    date_to: zipcodeRanges.date_to ? zipcodeRanges.date_to : '',
                    date_from: zipcodeRanges.date_from ? zipcodeRanges.date_from : '',
                });
                if (zipcodeRanges.items) {
                    this.arrayListSize = zipcodeRanges.items.length - 1;
                    zipcodeRanges.items.forEach(item => {
                        this.addNewZipCodeRangesGroupSpreadsheet(item, false);
                    });
                } else {
                    this.arrayListSize++;
                    this.addNewZipCodeRangesGroupSpreadsheet(null, false);
                }
                // if (zipcodeRanges.cep_ranges) {
                //     zipcodeRanges.cep_ranges.forEach(cep_ranges => {
                //         this.addNewZipCodeRangesGroup(cep_ranges);
                //     });
                // } else {
                //     this.addNewZipCodeRangesGroup();
                // }

                this.loadingZipCodeRangesDataValues = false;
                this.loading = false;
            });
    }

    public initCompanyFilter() {
        const companyLastFilter = this.formCompany.controls.companyLastFilter;

        companyLastFilter.valueChanges.subscribe(() => {
            let search = companyLastFilter.value.toLowerCase();

            this.filteredCompany.next(
                this.companies.filter(company => company.code.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initContractFilter() {
        const contractLastFilter = this.formContract.controls.contractLastFilter;

        contractLastFilter.valueChanges.subscribe(() => {
            let search = contractLastFilter.value.toLowerCase();

            this.filteredContract.next(
                this.contracts.filter(contract => contract.description.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initTaxClassFilter() {
        const taxClassastFilter = this.formTaxClass.controls.taxClassLastFilter;

        taxClassastFilter.valueChanges.subscribe(() => {
            let search = taxClassastFilter.value.toLowerCase();

            this.filteredContract.next(
                this.taxClass.filter(taxClass => taxClass.description.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {
        if (this.verifyFields() == false) {
            return;
        }

        if (this.checkCEP(this.formGroupZipCodeRange.controls) == true) {
            return;
        }

        const controls = this.parseControls(this.formGroupZipCodeRange.controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControlsCreate(controls: any) {

        const parsed = {
        
        }
        return parsed;
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {
        let items = this.zipCodeRangesGroup.controls.map((item: FormGroup) => {
            if (
                item.controls.type_of_service.value != '' &&
                item.controls.ufs.value != ''
            ) {
                return {
                    "type_service": item.controls.type_of_service.value,
					"uf": item.controls.ufs.value,
					"from_weigth": parseFloat(item.controls.from_weigth.value),
					"to_weigth": parseFloat(item.controls.to_weigth.value),
					"rate": parseFloat(item.controls.rate.value),
					"add_rate": parseFloat(item.controls.add_rate.value),
					"type_zone": parseFloat(item.controls.type_zone.value),
					"modal": parseFloat(item.controls.modal.value),
					"description": '',
					"cep_from": item.controls.cep_from.value,
					"cep_to": item.controls.cep_to.value,
                }
            }
        });

        let items2 = items.filter(item => item != undefined || item != null);

        let date_to = new Date(controls.date_to.value);
        let date_to_formated = '';
        date_to_formated = date_to.getFullYear() + "-" + ("0" + (date_to.getMonth() + 1)).slice(-2) + "-" + ("0" + (date_to.getDate())).slice(-2);

        let date_from = new Date(controls.date_from.value);
        let date_from_formated = '';
        date_from_formated = date_from.getFullYear() + "-" + ("0" + (date_from.getMonth() + 1)).slice(-2) + "-" + ("0" + (date_from.getDate())).slice(-2);

        const parsed = {
            'name': controls.name.value,
            'active': controls.active.value ? controls.active.value : 0,
            'costs_id': 1,
            'companies_id': controls.companies_id.value,
            'contract_id': controls.contract_id.value,
            'tax_class_id': controls.tax_class_id.value,
            'date_to': date_to_formated,
            'date_from': date_from_formated	
        }

        if (items2.length > 0 && items2 != null) {
            return { ...parsed, 'items': items2 };
        }

        return { ...parsed, 'items': [] };
    }

    public checkCEP(controls: any)
    {
        let cepCheck = /[0-9]{5}[\d]{3}/g;
        let errorCEP = false;
        let items = this.zipCodeRangesGroup.controls.map((item: FormGroup) => {
            if(item.controls.cep_from.value != '' && item.controls.cep_from.value.match(cepCheck) == null)
            {
                this.notificationService.notify('O CEP: ' + item.controls.cep_from.value + ' é invalido !', 'error');
                errorCEP = true;
            }

            if(item.controls.cep_to.value != '' && item.controls.cep_to.value.match(cepCheck) == null)
            {
                this.notificationService.notify('O CEP: ' + item.controls.cep_to.value + ' é invalido !', 'error');
                errorCEP = true;
            }
        });

        return errorCEP;
    }


    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }
}