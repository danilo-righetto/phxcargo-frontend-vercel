import { Component } from "@angular/core";
import { locale as portuguese } from '../_i18n/pt';
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HttpAuthenticateService } from "app/_services/_authentication/http.authenticate.service";
import { Router } from "@angular/router";
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';

@Component({
    selector: 'zipcodeRange-import',
    templateUrl: './zipcodeRange.import.component.html'
})
export class ZipCodeRangeImportComponent {

    public formGroup: FormGroup;
    public storeData: any;
    public csvData: any;
    public jsonData: any;
    public textData: any;
    public htmlData: any;
    public fileUploaded: File;
    public worksheet: any;
    public spreadSheet: any;
    public workbook: any;
    public spreadSheetName: any;

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private http: HttpAuthenticateService,
        private router: Router
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.formGroup = this._formBuilder.group({
            file: [null, Validators.required],
            fileName: ['', Validators.required]
        });
    }

    /**
     * Read file content and patch value to form control
     * 
     * @param event 
     */
    public onFileChange(event) {

        if (!event.target.files || !event.target.files.length) {
            throw "Invalid file content"
        }

        let reader = new FileReader;
        const [file] = event.target.files;

        reader.readAsDataURL(file);
        reader.onload = () => {

            this.formGroup.patchValue({
                file: reader.result,
                fileName: file.name
            });
        };
    }

    /**
     * Submit form group
     * 
     */
    public submit(): void {

        const controls = this.formGroup.controls;
        this.http.post(
            '/zipcode-ranges/import',
            {
                file: controls.file.value,
                fileName: controls.fileName.value
            }
        ).subscribe(res => this.router.navigate(['/zipcode-ranges']));
    }

    uploadedFile(event) {
        this.fileUploaded = event.target.files[0];
        this.readExcel();
    }

    readExcel() {
        let readFile = new FileReader();
        readFile.onload = (e) => {
            this.storeData = readFile.result;
            var data = new Uint8Array(this.storeData);
            var arr = new Array();
            for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");
            var workbook = XLSX.read(bstr, { type: "binary" });
            var first_sheet_name = workbook.SheetNames[0];
            this.worksheet = workbook.Sheets[first_sheet_name];

            this.workbook = workbook;
            this.workbook['Strings'][0]['r'] = "<t>Tipo de Serviço</t>";
            this.workbook['Strings'][0]['t'] = "<t>Tipo de Serviço</t>";
            this.workbook['Strings'][0]['h'] = "<t>Tipo de Serviço</t>";

            var stopHere = 0;
        }
        readFile.readAsArrayBuffer(this.fileUploaded);
    }

    readAsCSV() {
        this.csvData = XLSX.utils.sheet_to_csv(this.worksheet);
        const data: Blob = new Blob([this.csvData], { type: 'text/csv;charset=utf-8;' });
        FileSaver.saveAs(data, "CSVFile" + new Date().getTime() + '.csv');
    }

    readAsJson() {
        this.jsonData = XLSX.utils.sheet_to_json(this.worksheet, { raw: false });
        this.jsonData = JSON.stringify(this.jsonData);
        const data: Blob = new Blob([this.jsonData], { type: "application/json" });
        FileSaver.saveAs(data, "JsonFile" + new Date().getTime() + '.json');
    }

    readAsHTML() {
        var teste = this.worksheet
        var teste1 = this.worksheet.A1['h']
        var teste2 = this.worksheet.A1['c']
        var teste3 = this.worksheet.A1['t']
        var teste4 = this.worksheet.A1['v']
        var teste5 = this.worksheet.A1['w']
        var teste6 = this.spreadSheet = this.htmlData = XLSX.utils.sheet_to_html(this.worksheet);
        const data: Blob = new Blob([this.htmlData], { type: "text/html;charset=utf-8;" });
        var stopHere = 0;
    }

    readAsText() {
        this.textData = XLSX.utils.sheet_to_txt(this.worksheet);
        const data: Blob = new Blob([this.textData], { type: 'text/plain;charset=utf-8;' });
        FileSaver.saveAs(data, "TextFile" + new Date().getTime() + '.txt');
    }
}
