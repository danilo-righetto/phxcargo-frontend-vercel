import { NgModule } from "@angular/core";
import { DeclarationsModule } from "app/declarations.module";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatInputModule } from "@angular/material";
import { ZipCodeRangeImportComponent } from "./zipcodeRange.import.component";

@NgModule({
    declarations: [
        ZipCodeRangeImportComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatInputModule,
    ],
    exports: [
        ZipCodeRangeImportComponent,
    ]
})
export class ZipCodeRangeImportModule {

}