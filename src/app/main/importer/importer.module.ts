import { NgModule } from "@angular/core";
import { ImporterComponent } from "./importer.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { ImporterService } from "app/_services/importer/importer.service";
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    declarations: [
        ImporterComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
        BrowserAnimationsModule,
        MatProgressSpinnerModule,
    ],
    exports: [
        ImporterComponent,
    ],
    providers: [
        ImporterService,
    ]
})
export class ImporterModule {

}