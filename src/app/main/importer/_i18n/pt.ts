export const locale = {
    lang: 'pt',
    data: {
        'IMPORTER': {
            'HOUSES': 'Houses',
            'INFORMATIONS': 'Informações cadastrais',
            'EDIT': 'Editar',
            'BACK': 'Voltar',
            'SAVE': 'Salvar',
            'SHOW': 'Visualizar',
            'CREATE': 'Adicionar',
            'UPDATE': 'Editar',
            'IMPORTER': 'Importador',
            'IMPORTERS': 'Importadores',
            'DESCRIPTION': 'Descrição',
            'EMAIL': 'Email',
            'PERSON': {
                'NAME': 'Nome',
                'DOCUMENT': 'Documento'
            },
            'LISTING': {
                'ID': 'ID',
                'DOCUMENT': 'Documento',
                'DOCUMENT_CPF': 'CPF',
                'DOCUMENT_CNPJ': 'CNPJ',
                'DOCUMENT_TAX_ID': 'TAX ID',
                'DOCUMENT_RG': 'RG',
                'DOCUMENT_PASSAPORT': 'Passaporte',
                'NAME': 'Nome',
                'DESCRIPTION': 'Descrição',
                'NUMBER': 'Número',
                'ADDRESS_STREET': 'Endereço',
                'ADDRESS_COUNTRY': 'País',
                'ADDRESS_CITY': 'Cidade',
                'ADDRESS_ZIP_CODE': 'CEP',
                'ADDRESS_STATE': 'Estado',
                'ADDRESS_NEIGHBORHOOD': 'Bairro',
                'ADDRESS_ADDITIONAL_INFO': 'Estado',
                'PHONE': 'Telefone',
                'EMAILS': 'Emails'
            },
        },
        'ADDRESS': {
            'ADDRESS': 'Endereço',
            'NUMBER': 'Número',
            'ADDITIONAL_INFO': 'Complemento',
            'NEIGHBORHOOD': 'Bairro',
            'ZIP_CODE': 'CEP',
            'CITY': 'Cidade',
            'STATE': 'Estado',
            'COUNTRY': 'País',
        },
    }
};