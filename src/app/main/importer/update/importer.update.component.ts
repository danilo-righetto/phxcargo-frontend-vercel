
import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CountryService } from 'app/_services/country/country.service';
import { MatProgressSpinnerModule } from '@angular/material';
import { Subject, ReplaySubject } from 'rxjs';
import { CityService } from 'app/_services/address/city.service';

export interface City {
    id: number
    uf: string
    code: string
    name: string
    ibge_code: string
}

@Component({
    selector: 'importer-update',
    templateUrl: './importer.update.component.html',
    styleUrls: ['./importer.update.component.css'],
})
export class ImporterUpdateComponent {

    public data: any;
    public loadingImporter: boolean = true;
    public _update: boolean = false;
    public endpoint: string = '/importers';
    public currentId = null;
    public hidden = false;

    public countries: any;
    public loadingcountry: boolean = true;

    public formGroupImporter: FormGroup;

    public kind_of_document: number;
    public type_of_documento: string;
    public changeLayoutModelKindDocumentCPF: boolean = false;
    public changeLayoutModelKindDocumentRG: boolean = false;
    public changeLayoutModelKindDocumentCNPJ: boolean = false;
    public changeLayoutModelKindDocumentPassaport: boolean = false;
    public changeLayoutModelKindDocumentTAXID: boolean = false;
    public cityFilter: FormGroup;
    public filteredCity: ReplaySubject<City[]> = new ReplaySubject<City[]>(1);
    public cities: any;
    public loadingCity: boolean = true;

    onClickKindOfDocumentCPF() {
        this.kind_of_document = 1;
        this.type_of_documento = "CPF";
        this.changeLayoutModelKindDocumentCPF = true;
        this.changeLayoutModelKindDocumentRG = false;
        this.changeLayoutModelKindDocumentTAXID = false;
        this.changeLayoutModelKindDocumentCNPJ = false;
        this.changeLayoutModelKindDocumentPassaport = false;
        this.formGroupImporter.patchValue({ importer_document: '' })
        this.formGroupImporter.controls.importer_document.clearValidators();
    }

    onClickKindOfDocumentRG() {
        this.kind_of_document = 2;
        this.type_of_documento = "RG";
        this.changeLayoutModelKindDocumentRG = true;
        this.changeLayoutModelKindDocumentCPF = false;
        this.changeLayoutModelKindDocumentTAXID = false;
        this.changeLayoutModelKindDocumentCNPJ = false;
        this.changeLayoutModelKindDocumentPassaport = false;
        this.formGroupImporter.patchValue({ importer_document: '' })
        this.formGroupImporter.controls.importer_document.clearValidators();
    }

    onClickKindOfDocumentCPNJ() {
        this.kind_of_document = 3;
        this.type_of_documento = "CNPJ";
        this.changeLayoutModelKindDocumentCNPJ = true;
        this.changeLayoutModelKindDocumentCPF = false;
        this.changeLayoutModelKindDocumentRG = false;
        this.changeLayoutModelKindDocumentTAXID = false;
        this.changeLayoutModelKindDocumentPassaport = false;
        this.formGroupImporter.patchValue({ importer_document: '' })
        this.formGroupImporter.controls.importer_document.clearValidators();
    }

    onClickKindOfDocumentTAXID() {
        this.kind_of_document = 6;
        this.type_of_documento = "TAX_ID";
        this.changeLayoutModelKindDocumentTAXID = true;
        this.changeLayoutModelKindDocumentCPF = false;
        this.changeLayoutModelKindDocumentRG = false;
        this.changeLayoutModelKindDocumentCNPJ = false;
        this.changeLayoutModelKindDocumentPassaport = false;
        this.formGroupImporter.patchValue({ importer_document: '' })
        this.formGroupImporter.controls.importer_document.clearValidators();
    }

    onClickKindOfDocumentPassaport() {
        this.kind_of_document = 5;
        this.type_of_documento = "PASSAPORT";
        this.changeLayoutModelKindDocumentPassaport = true;
        this.changeLayoutModelKindDocumentTAXID = false;
        this.changeLayoutModelKindDocumentCPF = false;
        this.changeLayoutModelKindDocumentRG = false;
        this.changeLayoutModelKindDocumentCNPJ = false;
        this.formGroupImporter.patchValue({ importer_document: '' })
        this.formGroupImporter.controls.importer_document.clearValidators();
    }

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private router: Router,
        private countryService: CountryService,
        public spinner: MatProgressSpinnerModule,
        private citiesService: CityService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.buildFormGroupImporters();

        this.route.params.subscribe(params => {
            if (params.id) {
                this._update = true;
                this.currentId = params.id;
                this.fetchDataImporters(`/importers/${params.id}`);
            } else {
                this.loadingImporter = false;
            }
        });

        this.countryService.fetchData('', 255).subscribe(countries => {
            this.countries = countries.data
            this.loadingcountry = false;
        });

        this.citiesService.fetchData('', 5000).subscribe(cities => {
            this.cities = cities.data;
            this.filteredCity.next(this.cities.slice());
            this.loadingCity = false;
        });
    }

    /**
     * Build reactive form
     *
     */
    private buildFormGroupImporters() {
        this.formGroupImporter = this._formBuilder.group({
            id: ['',],
            importer_document: ['', Validators.required],
            importer_name: ['', Validators.required],
            importer_address: ['', Validators.required],
            importer_number: ['', Validators.required],
            importer_additional_info: [''],
            importer_neighborhood: ['', Validators.required],
            importer_zip_code: ['', Validators.required],
            importer_city: ['', Validators.required],
            importer_state: ['', Validators.required],
            importer_phone: ['', Validators.required],
            importer_country_id: ['', Validators.required],
            importer_email: ['',],
            cityFilter: [{ value: '', disabled: false }],
        });
    }

    public initCityFilter() {
        const cityFilter = this.formGroupImporter.controls.cityFilter;

        cityFilter.valueChanges.subscribe(() => {
            let search = cityFilter.value.toLowerCase();

            this.filteredCity.next(
                this.cities.filter(city => city.name.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    /**
     * 
     * @param url 
     */
    private fetchDataImporters(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(importer => {
                this.data = importer;

                if (importer.documents[0].type == "CPF") {
                    this.kind_of_document = 1;
                    this.type_of_documento = "CPF";
                    this.changeLayoutModelKindDocumentCPF = true;
                    this.changeLayoutModelKindDocumentRG = false;
                    this.changeLayoutModelKindDocumentTAXID = false;
                    this.changeLayoutModelKindDocumentCNPJ = false;
                    this.changeLayoutModelKindDocumentPassaport = false;
                }

                if (importer.documents[0].type == "CNPJ") {
                    this.kind_of_document = 3;
                    this.type_of_documento = "CNPJ";
                    this.changeLayoutModelKindDocumentCNPJ = true;
                    this.changeLayoutModelKindDocumentCPF = false;
                    this.changeLayoutModelKindDocumentRG = false;
                    this.changeLayoutModelKindDocumentTAXID = false;
                    this.changeLayoutModelKindDocumentPassaport = false;
                }

                if (importer.documents[0].type == "RG") {
                    this.kind_of_document = 2;
                    this.type_of_documento = "RG";
                    this.changeLayoutModelKindDocumentRG = true;
                    this.changeLayoutModelKindDocumentCPF = false;
                    this.changeLayoutModelKindDocumentTAXID = false;
                    this.changeLayoutModelKindDocumentCNPJ = false;
                    this.changeLayoutModelKindDocumentPassaport = false;
                }

                if (importer.documents[0].type == "PASSPORT") {
                    this.kind_of_document = 5;
                    this.type_of_documento = "Passaport";
                    this.changeLayoutModelKindDocumentPassaport = true;
                    this.changeLayoutModelKindDocumentTAXID = false;
                    this.changeLayoutModelKindDocumentCPF = false;
                    this.changeLayoutModelKindDocumentRG = false;
                    this.changeLayoutModelKindDocumentCNPJ = false;
                }

                if (importer.documents[0].type == "TAX_ID") {
                    this.kind_of_document = 6;
                    this.type_of_documento = "TAX_ID";
                    this.changeLayoutModelKindDocumentTAXID = true;
                    this.changeLayoutModelKindDocumentCPF = false;
                    this.changeLayoutModelKindDocumentRG = false;
                    this.changeLayoutModelKindDocumentCNPJ = false;
                    this.changeLayoutModelKindDocumentPassaport = false;
                }

                this.formGroupImporter.patchValue({
                    id: (typeof importer.email !== undefined || importer.email !== null) && (importer.email[0] !== undefined) ? importer.email[0].id : '',
                    importer_document: (typeof importer.documents !== undefined || importer.documents !== null) && (importer.documents[0] !== undefined) ? importer.documents[0].number : '',
                    importer_name: (typeof importer.name !== undefined || importer.name !== null) ? importer.name : '',
                    importer_address: (typeof importer.addresses !== undefined || importer.addresses !== null) && (importer.addresses[0] !== undefined) ? importer.addresses[0].address : '',
                    importer_number: (typeof importer.addresses !== undefined || importer.addresses !== null) && (importer.addresses[0] !== undefined) ? importer.addresses[0].number : '',
                    importer_additional_info: (typeof importer.addresses !== undefined || importer.addresses !== null) && (importer.addresses[0] !== undefined) ? importer.addresses[0].additional_info : '',
                    importer_neighborhood: (typeof importer.addresses !== undefined || importer.addresses !== null) && (importer.addresses[0] !== undefined) ? importer.addresses[0].neighborhood : '',
                    importer_zip_code: (typeof importer.addresses !== undefined || importer.addresses !== null) && (importer.addresses[0] !== undefined) ? importer.addresses[0].zip_code : '',
                    importer_city: (typeof importer.addresses !== undefined || importer.addresses !== null) && (importer.addresses[0] !== undefined) ? importer.addresses[0].city : '',
                    importer_state: (typeof importer.addresses !== undefined || importer.addresses !== null) && (importer.addresses[0] !== undefined) ? importer.addresses[0].state : '',
                    importer_country_id: (typeof importer.addresses !== undefined || importer.addresses !== null) && (importer.addresses[0] !== undefined) ? importer.addresses[0].country_id : '',
                    importer_phone: (typeof importer.phones !== undefined || importer.phones !== null) && (importer.phones !== undefined) ? importer.phones[0].number : '',
                    importer_email: (typeof importer.email !== undefined || importer.email !== null) && (importer.email[0] !== undefined) ? importer.email[0].address : '',
                });
                this.loadingImporter = false;
            });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {

        const controls = this.parseControls(this.formGroupImporter.controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {
        const parsed = {
            "name": controls.importer_name.value,
            "addresses": [
                {
                    "number": controls.importer_number.value,
                    "additional_info": controls.importer_additional_info.value,
                    "country_id": controls.importer_country_id.value,
                    "zip_code": controls.importer_zip_code.value,
                    "address": controls.importer_zip_code.value,
                    "cities_id": controls.importer_city.value,
                    "neighborhood": controls.importer_neighborhood.value,
                    "state": controls.importer_state.value,
                }
            ],
            "documents": [
                {
                    "type_id": this.kind_of_document,
                    "type": this.type_of_documento,
                    "number": controls.importer_document.value,
                }
            ],
            "phones": [
                {
                    "number": controls.importer_phone.value,
                    "name": controls.importer_name.value,
                    "type": "phone"
                }
            ],
            "emails": [
                {
                    "id": controls.id.value,
                    "name": controls.importer_name.value,
                    "address": controls.importer_email.value,
                    "type": "DELIVERY"
                }
            ],
        }

        return parsed;
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }
}