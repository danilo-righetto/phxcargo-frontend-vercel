import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { ImporterUpdateComponent } from './importer.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

const maskConfig: Partial<IConfig> = {
    validation: false,
};

@NgModule({
    declarations: [
        ImporterUpdateComponent
    ],
    imports: [
        NgxMaskModule.forRoot(maskConfig),
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
        NgxMatSelectSearchModule,
    ],
    exports: [
        ImporterUpdateComponent
    ]
})

export class ImporterUpdateModule {
}
