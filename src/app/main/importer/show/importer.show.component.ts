
import {map} from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { MatProgressSpinnerModule } from '@angular/material';

@Component({
    selector: 'importer-show',
    templateUrl: './importer.show.component.html',
    styleUrls: ['./importer.show.component.css'],
})
export class ImporterShowComponent {

    public data: any;
    public loadingImporter: boolean = true;
    public loading: boolean = false;

    /**
     * Constructor
     * 
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    public constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        public spinner: MatProgressSpinnerModule
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.route.params.subscribe(params => {

            this.fetchDataImporters(`/importers/${params.id}`);
        })

    }

    /**
     * 
     * @param url 
     */
    private fetchDataImporters(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(importer => {
                this.data = importer;
                this.loadingImporter = false;
            });
    }
}
