import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { ImporterShowComponent } from './importer.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    declarations: [
        ImporterShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        BrowserAnimationsModule,
        MatProgressSpinnerModule
    ],
    exports: [
        ImporterShowComponent,
    ]
})

export class ImporterShowModule {
}
