import { Component } from "@angular/core";
import { MatTableDataSource } from "@angular/material";
import { HttpAuthenticateService } from "app/_services/_authentication/http.authenticate.service";
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { locale as portuguese } from './_i18n/pt';
import { ImporterService } from "app/_services/importer/importer.service";

@Component({
    selector: 'importer',
    templateUrl: './importer.component.html'
})
export class ImporterComponent {

    public endpoint: string = '/importers';

    public displayedColumns: string[] = [
        'Nome do Importador',
        'Documento do Importador',
    ];

        public filterColumns = [
        {
            column: 'name',
            display_name: 'Nome do Importador',
            type: 'string'
        }
    ];

    public dataSource: MatTableDataSource<any>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private http: HttpAuthenticateService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        public importerService: ImporterService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.importerService.fetchData().subscribe(importer => {

            this.dataSource = new MatTableDataSource(importer.data);
        
        });
    }
}