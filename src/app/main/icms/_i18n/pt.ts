export const locale = {
    lang: 'pt',
    data: {
        'ICMS': {
            'ICMS': 'Icms',
            'INFORMATIONS': 'Informações',
            'SHOW': 'Visualizar',
            'EDIT': 'Editar',
            'CREATE': 'Adicionar',
            'BACK': 'Voltar',
            'SAVE': 'Salvar',
            'LISTING': {
                'ID': 'ID',
                'STATE': 'Estado',
                'ALIQUOT': 'Alíquota',
            }
        }
    }
};