import { NgModule } from "@angular/core";
import { FuseSharedModule } from '@fuse/shared.module';
import { IcmsComponent } from "./icms.component";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { IcmsService } from 'app/_services/icms/icms.service';

@NgModule({
    declarations: [
        IcmsComponent
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        IcmsComponent,
    ],
    providers: [
        IcmsService,
    ]
})

export class IcmsModule {
}
