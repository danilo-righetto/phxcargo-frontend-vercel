import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from './_i18n/pt';
import { IcmsListingElement } from 'app/_interfaces/icms/listing-element';
import { MatTableDataSource } from '@angular/material';
import { IcmsService } from 'app/_services/icms/icms.service';

@Component({
    selector: 'icms',
    templateUrl: './icms.component.html'
})
export class IcmsComponent {

    public endpoint: string = '/icms';

    public displayedColumns: string[] = [
        'Estado do ICMS',
        'Alíquota do ICMS',
    ];

    private data: IcmsListingElement[] = [];
    public dataSource: MatTableDataSource<IcmsListingElement>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        public icmsService: IcmsService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.icmsService.fetchData()
            .subscribe(res => {

                this.dataSource = new MatTableDataSource(res.data);
            });
    }
}