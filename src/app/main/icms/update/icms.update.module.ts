import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { IcmsUpdateComponent } from './icms.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule } from '@angular/material';

@NgModule({
    declarations: [
        IcmsUpdateComponent
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule
    ],
    exports: [
        IcmsUpdateComponent
    ]
})

export class IcmsUpdateModule {
}
