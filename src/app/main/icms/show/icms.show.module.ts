import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { IcmsShowComponent } from './icms.show.component';
import { DeclarationsModule } from 'app/declarations.module';

@NgModule({
    declarations: [
        IcmsShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
    ],
    exports: [
        IcmsShowComponent,
    ]
})

export class IcmsShowModule {
}
