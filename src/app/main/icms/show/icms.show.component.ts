
import {map} from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { IcmsListingElement } from 'app/_interfaces/icms/listing-element';

@Component({
    selector: 'icms-show',
    templateUrl: './icms.show.component.html',
})
export class IcmsShowComponent {

    private data: IcmsListingElement;
    public loading: boolean = true;

    /**
     * Constructor
     * 
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    public constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.route.params.subscribe(params => {

            this.fetchData(`/icms/${params.id}`);
        })

    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(icms => {
                this.data = icms;
                this.loading = false;
            });
    }
}
