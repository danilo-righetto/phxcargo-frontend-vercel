import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { CompanyUpdateComponent } from './company.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatCheckboxModule, MatInputModule, MatSelectModule, MatStepperModule, MatRadioModule , MAT_SLIDE_TOGGLE_DEFAULT_OPTIONS, MatProgressSpinnerModule } from '@angular/material';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { DialogCompany } from "app/_default/dialogs/company/dialog-company-create.component";
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';


export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

const maskConfig: Partial<IConfig> = {
    validation: false,
};

@NgModule({
    declarations: [
        CompanyUpdateComponent,
    ],
    imports: [
        NgxMaskModule.forRoot(maskConfig),
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        MatSelectModule,
        MatStepperModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        BrowserAnimationsModule,
        FormsModule,
        MatProgressSpinnerModule,
        NgxMatSelectSearchModule,
    ],
    exports: [
        CompanyUpdateComponent
    ],
    providers: [   
        {provide: MAT_SLIDE_TOGGLE_DEFAULT_OPTIONS , useValue: {disableToggleValue: false, disableDragValue: true}},
    ],
    entryComponents: [
        DialogCompany
    ]
})

export class CompanyUpdateModule {
}
