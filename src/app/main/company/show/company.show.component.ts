
import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { CompanyListingElement } from 'app/_interfaces/company/listing-element';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, FormGroupDirective, NgForm } from '@angular/forms';
import { CountryService } from 'app/_services/country/country.service';
import { ContractService } from 'app/_services/company/contract.service';
import { CityService } from 'app/_services/address/city.service';
import { NotificationService } from 'app/_services/notification/notification.service';
import { DialogCompany } from "app/_default/dialogs/company/dialog-company-create.component";
import { MatDialog, MatProgressSpinnerModule } from '@angular/material';
import { Subject, ReplaySubject } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';
import { ContractListingElement, ContractRequest, Contract } from 'app/_interfaces/contract/contract.interface';

export interface Country {
    id: number
    name: string
    code: number
    initials: string
}

export interface City {
    id: number
    uf: string
    code: string
    name: string
    ibge_code: string
}

@Component({
    selector: 'company-show',
    templateUrl: './company.show.component.html',
    styleUrls: ['./company.show.component.css'],
})
export class CompanyShowComponent {
    public loading: boolean = false;
    public _update: boolean = false;
    public formGroup: FormGroup;
    public isCreate: boolean = false;
    public currentId = null;
    private endpoint: string = '/companies'
    public contracts: any;
    public contractLastFilter: FormGroup;
    public cityFilter: FormGroup;
    public loadingContract: boolean = true;
    public filteredContract: ReplaySubject<Contract[]> = new ReplaySubject<Contract[]>(1);
    public addressForm: any;
    public formGroupAddress: FormGroup;
    public items = [];
    public addresses: number = 0;
    public countryFilter: FormGroup;
    public countries: any;
    public cities: any;
    public loadingCountry: boolean = true;
    public loadingCity: boolean = true;
    public addressCharge: boolean = false;
    public filteredCountry: ReplaySubject<Country[]> = new ReplaySubject<Country[]>(1);
    public filteredCity: ReplaySubject<City[]> = new ReplaySubject<City[]>(1);
    public company: any;
    public data: any;

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private countryService: CountryService,
        private contractService: ContractService,
        private citiesService: CityService,
        private router: Router,
        private notificationService: NotificationService,
        public dialog: MatDialog,
        public spinner: MatProgressSpinnerModule

    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {
        this.buildFormGroup();

        this.route.params.subscribe(params => {
            if (params.id) {
                this._update = true;
                this.currentId = params.id;
                this.fetchCompany(`${this.endpoint}/${this.currentId}`);
            }
        });

        this.contractService.fetchData('', 300).subscribe(contracts => {
            this.contracts = contracts.data;
            this.filteredContract.next(this.contracts.slice());
            this.loadingContract = false;
        });

        this.countryService.fetchData('', 300).subscribe(countries => {
            this.countries = countries.data;
            this.filteredCountry.next(this.countries.slice());
            this.loadingCountry = false;
        });

        this.citiesService.fetchData('', 5000).subscribe(cities => {
            this.cities = cities.data;
            this.filteredCity.next(this.cities.slice());
            this.loadingCity = false;
        });

        this.initContractFilter();
        this.initCountryFilter()
        this.initCityFilter();
    }

    private buildFormGroup() {
        this.formGroup = this._formBuilder.group({
            nationality: [{ value: '', disabled: true }, Validators.required],
            type_of_person: [{ value: '', disabled: true }, Validators.required],
            kind_of_person_client: [{ value: '', disabled: true }, Validators.required],
            kind_of_person_supplier: [{ value: '', disabled: true }, Validators.required],
            kind_of_person_conveyor: [{ value: '', disabled: true }, Validators.required],
            status: [{ value: '', disabled: true }, Validators.required],
            name: [{ value: '', disabled: true }, Validators.required],
            fantasy_name: [{ value: '', disabled: true }, Validators.required],
            contract: [{ value: '', disabled: true }, Validators.required],
            document: [{ value: '', disabled: true }, Validators.required],

            contractLastFilter: [{ value: '', disabled: true }],
            addressForm: this._formBuilder.array([]),
            countryFilter: [{ value: '', disabled: true }],

            number: [{ value: '', disabled: true }, Validators.required],
            additional_info: [{ value: '', disabled: true }, Validators.required],
            type: [{ value: '', disabled: true }, Validators.required],
            zip_code: [{ value: '', disabled: true }, Validators.required],
            country_id: [{ value: '', disabled: true }, Validators.required],
            cities_id: [{ value: '', disabled: true }, Validators.required],
            address: [{ value: '', disabled: true }, Validators.required],
            state: [{ value: '', disabled: true }, Validators.required],
            neighborhood: [{ value: '', disabled: true }, Validators.required],
            phone: [{ value: '', disabled: true }, Validators.required],
            email: [{ value: '', disabled: true }, Validators.required],
            commercial_contact: [{ value: '', disabled: true }, Validators.required],

            number_second: [{ value: '', disabled: true }, Validators.required],
            additional_info_second: [{ value: '', disabled: true }, Validators.required],
            type_second: [{ value: '', disabled: true }, Validators.required],
            zip_code_second: [{ value: '', disabled: true }, Validators.required],
            country_id_second: [{ value: '', disabled: true }, Validators.required],
            cities_id_second: [{ value: '', disabled: true }, Validators.required],
            address_second: [{ value: '', disabled: true }, Validators.required],
            state_second: [{ value: '', disabled: true }, Validators.required],
            neighborhood_second: [{ value: '', disabled: true }, Validators.required],
            phone_second: [{ value: '', disabled: true }, Validators.required],
            email_second: [{ value: '', disabled: true }, Validators.required],
            financial_contact: [{ value: '', disabled: true }, Validators.required],

            cityFilter: [{ value: '', disabled: true }],
        });
    }

    private fetchCompany(url: string) {
        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(company => {
                this.company = company;
                this.data = company;
                this.formGroup.patchValue({
                    nationality: company.nationality ? company.nationality : '',
                    type_of_person: company.type_of_person ? company.type_of_person :  '',
                    kind_of_person_client: company.kind_of_person && company.kind_of_person.indexOf('C') > -1 ? true : false,
                    kind_of_person_supplier: company.kind_of_person && company.kind_of_person.indexOf('F') > -1 ? true : false,
                    kind_of_person_conveyor: company.kind_of_person && company.kind_of_person.indexOf('T') > -1 ? true : false,
                    status: company.status ? company.status : '',
                    name: company.name ? company.name : '',
                    fantasy_name: company.fantasy_name ? company.fantasy_name : '',
                    contract: company.contract_id ? company.contract_id.id : '',
                    document: company.documents ? company.documents[0].number : ''
                });
                if (company.addresses) {
                    if (company.addresses.length >= 1) {
                        this.formGroup.patchValue({
                            number: company.addresses[0].number,
                            additional_info: company.addresses[0].additional_info,
                            type: company.addresses[0].type,
                            zip_code: company.addresses[0].zip_code,
                            country_id: company.addresses[0].country_id,
                            cities_id: company.addresses[0].cities_id,
                            address: company.addresses[0].address,
                            state: company.addresses[0].state,
                            neighborhood: company.addresses[0].neighborhood,
                            phone: company.phones[0].number,
                            email: company.email[0].address,
                            commercial_contact: company.email[0].name,
                        });
                    }

                    if (company.addresses.length >= 2) {
                        this.addressCharge = true;
                        this.formGroup.patchValue({
                            number_second: company.addresses[1].number,
                            additional_info_second: company.addresses[1].additional_info,
                            type_second: company.addresses[1].type,
                            zip_code_second: company.addresses[1].zip_code,
                            country_id_second: company.addresses[1].country_id,
                            cities_id_second: company.addresses[1].cities_id,
                            address_second: company.addresses[1].address,
                            state_second: company.addresses[1].state,
                            neighborhood_second: company.addresses[1].neighborhood,
                            phone_second: company.phones[1].number,
                            email_second: company.email[1].address,
                            financial_contact: company.email[1].name
                        });
                    }
                }
                this.loading = false;
            });
    }

    public initCountryFilter() {
        const countryFilter = this.formGroup.controls.countryFilter;

        countryFilter.valueChanges.subscribe(() => {
            let search = countryFilter.value.toLowerCase();

            this.filteredCountry.next(
                this.countries.filter(country => country.name.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initCityFilter() {
        const cityFilter = this.formGroup.controls.cityFilter;

        cityFilter.valueChanges.subscribe(() => {
            let search = cityFilter.value.toLowerCase();

            this.filteredCity.next(
                this.cities.filter(city => city.name.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public verifyFields() {
        let verify = false;
        if (
            !this.formGroup.get('nationality').valid ||
            !this.formGroup.get('type_of_person').valid ||
            !this.formGroup.get('name').valid ||
            !this.formGroup.get('status').valid ||
            !(
                this.formGroup.get('kind_of_person_client').valid ||
                this.formGroup.get('kind_of_person_supplier').valid ||
                this.formGroup.get('kind_of_person_conveyor').valid
            ) ||
            !this.formGroup.get('fantasy_name').valid ||
            !this.formGroup.get('contract').valid ||
            !this.formGroup.get('document').valid ||
            !this.formGroup.get('number').valid ||
            !this.formGroup.get('additional_info').valid ||
            !this.formGroup.get('type').valid ||
            !this.formGroup.get('zip_code').valid ||
            !this.formGroup.get('country_id').valid ||
            !this.formGroup.get('cities_id').valid ||
            !this.formGroup.get('address').valid ||
            !this.formGroup.get('state').valid ||
            !this.formGroup.get('neighborhood').valid ||
            !this.formGroup.get('phone').valid ||
            !this.formGroup.get('email').valid
        ) {
            verify = true;
        }

        let verifySecond = false;

        if (this.addressCharge) {
            if (
                !this.formGroup.get('number_second').valid ||
                !this.formGroup.get('additional_info_second').valid ||
                !this.formGroup.get('type_second').valid ||
                !this.formGroup.get('zip_code_second').valid ||
                !this.formGroup.get('country_id_second').valid ||
                !this.formGroup.get('cities_id_second').valid ||
                !this.formGroup.get('address_second').valid ||
                !this.formGroup.get('state_second').valid ||
                !this.formGroup.get('neighborhood_second').valid ||
                !this.formGroup.get('phone_second').valid ||
                !this.formGroup.get('email_second').valid
            ) {
                verifySecond = true
            }
        }

        return verify == true || verifySecond == true;
    }

    public submit(): void {
        if (this.verifyFields() == true) {
            this.formGroup.updateValueAndValidity();
            return;
        }

        const controls = this.parseControls();

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    public kindOfPerson() {

    }

    public parseControls() {
        let bodyControls = this.formGroup.controls;

        let kind_of_person = "";

        kind_of_person += bodyControls.kind_of_person_client.value == true ?
            'C' : '';
        
        kind_of_person += bodyControls.kind_of_person_supplier.value == true ?
            'F' : '';
        
        kind_of_person += bodyControls.kind_of_person_conveyor.value == true ?
            'T' : '';

        let body = {
            "name": bodyControls.name.value,
            "nationality": bodyControls.nationality.value,
            "fantasy_name": bodyControls.fantasy_name.value,
            "kind_of_person": kind_of_person,
            "contract_id": bodyControls.contract.value,
            "type_of_person": bodyControls.type_of_person.value,
        }

        let addresses = [
            {
                "number": bodyControls.number.value,
                "additional_info": bodyControls.additional_info.value,
                "type": bodyControls.type.value,
                "zip_code": bodyControls.zip_code.value,
                "country_id": bodyControls.country_id.value,
                "cities_id": bodyControls.cities_id.value,
                "address": bodyControls.address.value,
                "state": bodyControls.state.value,
                "neighborhood": bodyControls.neighborhood.value,
            }
        ];

        let phones = [
            {
                "number": bodyControls.phone.value,
                "type": "phone"
            }
        ];

        let emails = [
            {
                "name": bodyControls.commercial_contact.value,
                "address": bodyControls.email.value
            }
        ];

        if (this.addressCharge) {
            addresses.push(
                {
                    "number": bodyControls.number_second.value,
                    "additional_info": bodyControls.additional_info_second.value,
                    "type": bodyControls.type_second.value,
                    "zip_code": bodyControls.zip_code_second.value,
                    "country_id": bodyControls.country_id_second.value,
                    "cities_id": bodyControls.cities_id_second.value,
                    "address": bodyControls.address_second.value,
                    "state": bodyControls.state_second.value,
                    "neighborhood": bodyControls.neighborhood_second.value,
                }
            );

            phones.push(
                {
                    "number": bodyControls.phone_second.value,
                    "type": "phone"
                }
            );

            emails.push(
                {
                    "name": bodyControls.financial_contact.value,
                    "address": bodyControls.email_second.value
                }
            );
        }

        let typePerson = bodyControls.type_of_person.value;
        
        let documents = [{
            "type": typePerson == 'LEGAL' ? "CNPJ" : "CPF",
			"number": bodyControls.document.value
        }];
        

        return { ...body, addresses, documents, phones, emails };
    }

    public initContractFilter() {
        const contractLastFilter = this.formGroup.controls.countryFilter;

        contractLastFilter.valueChanges.subscribe(() => {
            let search = contractLastFilter.value.toLowerCase();

            this.filteredContract.next(
                this.contracts.filter(contract => contract.description.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public addNewFormGroupAddress(addresses: any = null) {
        this.addresses++;
    }

    private initFormGroupAddress(addresses: any = null) {
        return this._formBuilder.group({
            number: [{ value: addresses ? addresses.number : '', disabled: true }, Validators.required],
            additional_info: [{ value: addresses ? addresses.additional_info : '', disabled: true }, Validators.required],
            type: [{ value: addresses ? addresses.type : '', disabled: true }, Validators.required],
            zip_code: [{ value: addresses ? addresses.zip_code : '', disabled: true }, Validators.required],
            country_id: [{ value: addresses ? addresses.country_id : '', disabled: true }, Validators.required],
            cities_id: [{ value: addresses ? addresses.cities_id : '', disabled: true }, Validators.required],
            address: [{ value: addresses ? addresses.address : '', disabled: true }, Validators.required],
            state: [{ value: addresses ? addresses.state : '', disabled: true }, Validators.required],
            neighborhood: [{ value: addresses ? addresses.neighborhood : '', disabled: true }, Validators.required],
            phone: [{ value: addresses ? addresses.phone : '', disabled: true }, Validators.required],
            email: [{ value: addresses ? addresses.email : '', disabled: true }, Validators.required],
            countryFilter: [{ value: '', disabled: true }],
        });
    }

    public addNewAddress(addresses: any = null) {
        this.addresses++;
        this.addressCharge = true;
    }

    removeAddress() {
        this.addresses--;
        this.addressCharge = false;
    }
}
