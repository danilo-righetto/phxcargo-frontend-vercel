import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { CompanyShowComponent } from './company.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatProgressSpinnerModule, MatFormFieldModule, MatCheckboxModule, MatInputModule, MatSelectModule, MatStepperModule, MatRadioModule , MAT_SLIDE_TOGGLE_DEFAULT_OPTIONS } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule } from '@angular/forms';
import { DialogCompany } from "app/_default/dialogs/company/dialog-company-create.component";
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

const maskConfig: Partial<IConfig> = {
    validation: false,
};

@NgModule({
    declarations: [
        CompanyShowComponent,
    ],
    imports: [
        NgxMaskModule.forRoot(maskConfig),
        FuseSharedModule,
        DeclarationsModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatRadioModule,
        MatSlideToggleModule,
        FormsModule,
        NgxMatSelectSearchModule,
    ],
    exports: [
        CompanyShowComponent,
    ],
    providers: [   
        {provide: MAT_SLIDE_TOGGLE_DEFAULT_OPTIONS , useValue: {disableToggleValue: false, disableDragValue: true}},
    ],
    entryComponents: [
        DialogCompany
    ]
})

export class CompanyShowModule {
}
