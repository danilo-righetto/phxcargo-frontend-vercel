import { Component, ViewChild } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from './_i18n/pt';
import { CompanyListingElement } from 'app/_interfaces/company/listing-element';
import { MatTableDataSource } from '@angular/material';
import { CompanyService } from 'app/_services/company/company.service';
import { DefaultTableComponent } from 'app/_default/data-list/table.component';


@Component({
    selector: 'company',
    templateUrl: './company.component.html'
})
export class CompanyComponent {

    @ViewChild(DefaultTableComponent, { static: true })
    public table: DefaultTableComponent;

    public endpoint: string = '/companies';

    public displayedColumns: string[] = [
        'Código',
        'Nome da Empresa',
        'Status',
    ];

    public filterColumns = [
        {
            column: 'code',
            display_name: 'Código',
            type: 'string'
        },
        {
            column: 'name',
            display_name: 'Nome',
            type: 'string'
        }
    ];

    private data: CompanyListingElement[] = [];
    public dataSource: MatTableDataSource<CompanyListingElement>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        public service: CompanyService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

}