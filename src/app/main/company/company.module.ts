import { NgModule } from "@angular/core";
import { FuseSharedModule } from '@fuse/shared.module';
import { CompanyComponent } from "./company.component";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { CompanyService } from 'app/_services/company/company.service';

@NgModule({
    declarations: [
        CompanyComponent
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        CompanyComponent,
    ],
    providers: [
        CompanyService,
    ]
})

export class CompanyModule {
}
