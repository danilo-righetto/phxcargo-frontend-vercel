export const locale = {
    lang: 'pt',
    data: {
        'COMPANY': {
            'COMPANIES': 'Empresas',
            'COMPANY': 'Empresa',
            'INFORMATIONS': 'Informações',
            'REGISTRATION_INFORMATIONS': 'Informações cadastrais',
            'PERSON': 'TIPO DE PESSOA',
            'COLLECTION': 'Preencha os campos abaixo.',
            'ADD_COLLETION': 'Adicionar endereço de cobrança',
            'REMOVE_COLLECTION': 'Remover endereço de cobrança',
            'SHOW': 'Visualizar',
            'EDIT': 'Editar',
            'CREATE': 'Adicionar',
            'BACK': 'Voltar',
            'SAVE': 'Salvar',
            'REGISTER': 'Cadastrar',
            'NATIONAL': 'Nacional',
            'NATIONALITY': 'Nacionalidade',
            'TYPE': 'Tipo de endereço',
            'TYPE_ADDRESS': {
                'DELIVERY': 'Entrega',
                'CHARGE': 'Cobrança'
            },
            'NATIONALITIES': {
                'NATIONAL': 'Nacional',
                'INTERNATIONAL': 'Internacional'
            },
            'TYPE_OF_PERSON': 'Tipo de Pessoa',
            'TYPES_OF_PERSON': {
                'PHYSICAL': 'Fisica',
                'LEGAL': 'Jurídica'
            },
            'KIND_OF_PERSON': {
                'CLIENT': 'Cliente',
                'SUPPLIER': 'Fornecedor',
                'CONVEYOR': 'Transportador',
                'GENERAL': 'Tipo de Cadastro'
            },
            'STATUS': {
                'STATUS': 'Status',
                'ACTIVE': 'Ativo',
                'INACTIVE': 'Inativo'
            },
            'LISTING': {
                'ID': 'ID',
                'CODE': 'Código da conta',
                'NAME': 'Nome',
                'EMAIL': 'Email',
                'EMAILS': 'Emails',
                'PHONES': 'Telefones',
                'ADDRESSES': 'Endereços',
                'MAIN_ADDRESSES': 'Endereço Principal',
                'SECOND_ADDRESSES': 'Endereço de cobrança',
                'ADDRESS': 'Endereço',
                'BILLING_ADDRESSES': 'Endereços de cobrança',
                'BILLING_ADDRESS': 'Endereço de cobrança',
                'DOCUMENT': 'Documento',
                'DOCUMENT_TYPE': 'CPF / CNPJ / TAX ID',
                'FINANCIAL_CONTACT': 'Contato Financeiro',
                'COMMERCIAL_CONTACT': 'Contato Comercial',
                'DESCRIPTION': 'Descrição',
                'NUMBER': 'Número',
                'ADDRESS_STREET': 'Endereço',
                'ADDRESS_CITY': 'Cidade',
                'ADDRESS_ZIP_CODE': 'CEP',
                'ADDRESS_STATE': 'Estado',
                'ADDRESS_NEIGHBORHOOD': 'Bairro',
                'ADDRESS_COUNTRY': 'País',
                'ADDRESS_ADDITIONAL_INFO': 'Complemento',
                'PHONE': 'Telefone',
                'CONTRACT': 'Contrato',
                'CNPJ': 'Cnpj',
                'TAX_ID': 'Tax ID',
                'IE': 'Inscrição Estadual',
                'IM': 'Incrição Municipal',
                'FANTASY_NAME': 'Nome Fantasia',
                'KIND_OF_PERSON_PHYSICAL': 'CPF',
                'KIND_OF_PERSON_LEGAL': 'CNPJ',
                'CONTACT_NAME': 'Nome de contato',
                'NATIONALITY': 'Nacionalidade',
                'STATUS': 'Status da empresa',
            }
        },
        'ERRORS': {
            'MANDATORY': 'Obrigatório'
        }
    }
};