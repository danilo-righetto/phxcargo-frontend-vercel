import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { UserShowComponent } from './user.show.component';
import { DeclarationsModule } from 'app/declarations.module';

@NgModule({
    declarations: [
        UserShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
    ],
    exports: [
        UserShowComponent,
    ]
})

export class UserShowModule {
}
