import { Component } from "@angular/core";
import { MatTableDataSource } from "@angular/material";
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { locale as portuguese } from './_i18n/pt';
import { UserService } from "app/_services/user/user.service";

@Component({
    selector: 'user',
    templateUrl: './user.component.html'
})
export class UserComponent {
    
    public endpoint: string = '/users';

    public displayedColumns: string[] = [
        'Nome do Usuario',
        'Email',
        'Empresa Vinculada',
        'Permissão'
    ];

    public dataSource: MatTableDataSource<any>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        public userService: UserService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.userService.fetchData()
            .subscribe(res => {
                this.dataSource = new MatTableDataSource(res.data);
            });
    }
}