import { NgModule } from "@angular/core";
import { FuseSharedModule } from '@fuse/shared.module';
import { UserComponent } from "./user.component";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { UserService } from 'app/_services/user/user.service';

@NgModule({
    declarations: [
        UserComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        UserComponent,
    ],
    providers: [
        UserService,
    ]
})
export class UserModule {

}