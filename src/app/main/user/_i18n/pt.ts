export const locale = {
    lang: 'pt',
    data: {
        'USER': {
            'USER': 'Utilizador',
            'USERS': 'Utilizadores',
            'INFORMATIONS': 'Informações',
            'SHOW': 'Visualizar',
            'EDIT': 'Editar',
            'CREATE': 'Adicionar',
            'BACK': 'Voltar',
            'SAVE': 'Salvar',
            'COMPANY': 'Empresa',
            'NAME': 'Nome',
            'LISTING': {
                'ID': 'ID',
                'NAME': 'Nome',
                'EMAIL': 'E-mail',
                'PASSWORD': 'Senha',
                'ROLE': 'Hierarquia',
                'COMPANY': 'Empresa',
            }
        },
        'ERRORS': {
            'MANDATORY': 'Obrigatório'
        }
    }
};