
import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { UserListingElement } from 'app/_interfaces/user/listing-element';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CompanyService } from 'app/_services/company/company.service';
import { RoleService } from 'app/_services/role/role.service';
import { CompanyListingElement as Company } from 'app/_interfaces/company/listing-element';
import { RoleListingElement as Role } from 'app/_interfaces/role/listing-element';
import { ReplaySubject } from 'rxjs';

@Component({
    selector: 'user-update',
    templateUrl: './user.update.component.html',
    styleUrls: ['user.update.component.css']
})
export class UserUpdateComponent {

    private data: UserListingElement;
    public loading: boolean = true;
    public _update: boolean = false;
    private endpoint: string = '/users';
    private currentId = null;
    public companies: Company[];
    public company_id = null;
    public loadingCompany: boolean = true;
    public roles: Role[];
    public rolesAll: any;
    public role_id = null;
    public loadingRole: boolean = true;
    public formGroup: FormGroup;
    public companyLastFilter: FormGroup;
    public roleFilter: FormGroup;
    public filteredCompany: ReplaySubject<Company[]> = new ReplaySubject<Company[]>(1);
    public filteredRole: ReplaySubject<Company[]> = new ReplaySubject<Company[]>(1);

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private router: Router,
        public companyService: CompanyService,
        public roleService: RoleService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.buildFormGroup();

        this.route.params.subscribe(params => {

            if (params.id) {
                this._update = true;
                this.currentId = params.id;
                this.fetchData(`/users/${params.id}`);
            } else {
                this.loading = false;
            }
        });

        this.companyService.fetchData(null, 300).subscribe(companies => {
            this.companies = companies.data;
            this.filteredCompany.next(this.companies.slice());
            this.loadingCompany = false;
        });

        this.roleService.fetchData(null, 500).subscribe(roles => {
            this.roles = roles.data;
            this.rolesAll = roles.data;
            this.filteredRole.next(this.rolesAll.slice());
            this.loadingRole = false;
        });

        this.initCompanyFilter();
        this.initRoleFilter();
    }

    public verifyFields() {
        return false;
    }

    /**
     * Easy access to form fields
     * 
     */
    get controls() { return this.formGroup.controls; }

    /**
     * Build reactive form
     *
     */
    private buildFormGroup() {
        this.formGroup = this._formBuilder.group({
            company: ['', Validators.required],
            name: ['', Validators.required],
            email: ['', Validators.required],
            password: [''],
            role: ['', Validators.required],
            companyLastFilter: [{ value: '', disabled: false }],
            roleFilter: [{ value: '', disabled: false }],
        });
    }

    public initCompanyFilter() {
        const companyLastFilter = this.formGroup.controls.companyLastFilter;

        companyLastFilter.valueChanges.subscribe(() => {
            let search = companyLastFilter.value.toLowerCase();

            this.filteredCompany.next(
                this.companies.filter(company => company['Nome da Empresa'].toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initRoleFilter() {
        const roleFilter = this.formGroup.controls.roleFilter;

        roleFilter.valueChanges.subscribe(() => {
            let search = roleFilter.value.toLowerCase();

            this.filteredRole.next(
                this.rolesAll.filter(role => role['Nome da Empresa'].toLowerCase().indexOf(search) > 1)
            );
        });
    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(user => {
                this.data = user;
                this.formGroup.patchValue({
                    company: user.company.id,
                    name: user.name,
                    email: user.email,
                    role: user.role ? user.role.id : null
                });
                this.loading = false;
            });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {

        const controls = this.parseControls();

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls() {

        const parsed = {
            "company_id": this.controls.company.value || null,
            "role_id": this.controls.role.value || null,
            "name": this.controls.name.value,
            "email": this.controls.email.value,
            "password": this.controls.password.value,
        }

        return parsed;
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {

        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {

        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }
}