import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { UserUpdateComponent } from './user.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { UserService } from 'app/_services/user/user.service';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { DateTimeModule } from 'app/_default/datetime/datetime.default.module';

@NgModule({
    declarations: [
        UserUpdateComponent
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatDividerModule,
        MatTooltipModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule,
        NgxMatSelectSearchModule,
        DateTimeModule,
    ],
    exports: [
        UserUpdateComponent
    ],
    providers: [
        UserService,
    ]
})

export class UserUpdateModule {
}
