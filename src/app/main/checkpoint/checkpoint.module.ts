import { NgModule } from "@angular/core";
import { FuseSharedModule } from '@fuse/shared.module';
import { CheckpointComponent } from "./checkpoint.component";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { CheckpointService } from 'app/_services/checkpoints/checkpoint.service';

@NgModule({
    declarations: [
        CheckpointComponent
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        CheckpointComponent,
    ],
    providers: [
        CheckpointService,
    ]
})

export class CheckpointModule {
}
