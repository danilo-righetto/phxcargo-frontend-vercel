import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { CheckpointShowComponent } from './checkpoint.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'


@NgModule({
    declarations: [
        CheckpointShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
    ],
    exports: [
        CheckpointShowComponent,
    ]
})

export class CheckpointShowModule {
}
