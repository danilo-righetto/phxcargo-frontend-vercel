import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { CheckpointUpdateComponent } from './checkpoint.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'


@NgModule({
    declarations: [
        CheckpointUpdateComponent
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
    ],
    exports: [
        CheckpointUpdateComponent
    ]
})

export class CheckpointUpdateModule {
}
