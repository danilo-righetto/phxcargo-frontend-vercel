
import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'checkpoint-update',
    templateUrl: './checkpoint.update.component.html',
    styleUrls: ['./checkpoint.update.component.css'],
})
export class CheckpointUpdateComponent {

    private data: any;
    public loading: boolean = true;
    public _update: boolean = false;
    private endpoint: string = '/checkpoints';
    private currentId = null;

    public formGroup: FormGroup;

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private router: Router
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.buildFormGroup();

        this.route.params.subscribe(params => {

            if (params.id) {

                this._update = true;
                this.currentId = params.id;
                this.fetchData(`/checkpoints/${params.id}`);
            }
        });

    }

    /**
     * Build reactive form
     *
     */
    private buildFormGroup() {
        this.formGroup = this._formBuilder.group({
            name: ['', Validators.required],
            code: ['', Validators.required],
        });
    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(checkpoint => {
                this.data = checkpoint;
                this.formGroup.patchValue({
                    code: checkpoint.code,
                    name: checkpoint.name,
                });
                this.loading = false;
            });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {

        const controls = this.parseControls(this.formGroup.controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {
        const parsed = {
            "name": controls.name.value,
            "code": controls.code.value,
        }

        return parsed;
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }
}
