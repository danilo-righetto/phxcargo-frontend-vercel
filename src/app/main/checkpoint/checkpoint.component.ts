import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from './_i18n/pt';
import { MatTableDataSource } from '@angular/material';
import { CheckpointService } from 'app/_services/checkpoints/checkpoint.service';

@Component({
    selector: 'checkpoint',
    templateUrl: './checkpoint.component.html'
})
export class CheckpointComponent {

    public endpoint: string = '/checkpoints';

    public displayedColumns: string[] = [
        'Código do Checkpoint',
        'Nome do Checkpoint',
    ];

    private data: any[] = [];
    public dataSource: MatTableDataSource<any>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        public service: CheckpointService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }
}