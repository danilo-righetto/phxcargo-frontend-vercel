export const locale = {
    lang: 'pt',
    data: {
        'CHECKPOINT': {
            'CHECKPOINT': 'Checkpoint',
            'CREATE': 'Adicionar',
            'CHECKPOINTS': 'Checkpoints',
            'EDIT': 'Editar',
            'SHOW': 'Visualizar',
            'BACK': 'Voltar',
            'SAVE': 'Salvar',
            'INFORMATIONS': 'Informações',
            'REGISTRATION_INFORMATIONS': 'Informações cadastrais',
            'LISTING': {
                'ID': 'ID',
                'CODE': 'Código',
                'NAME': 'Nome',
            }
        }
    }
};