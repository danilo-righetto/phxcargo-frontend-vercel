import { Component } from "@angular/core";
import { MatTableDataSource } from "@angular/material";
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { locale as portuguese } from './_i18n/pt';
import { ShipperService } from "app/_services/shipper/shipper.service";
import { ShipperListingElement } from 'app/_interfaces/shipper/listing-element';

@Component({
    selector: 'shipper',
    templateUrl: './shipper.component.html'
})
export class ShipperComponent {

    public endpoint: string = '/shippers';

    public displayedColumns: string[] = [
        'Nome do Embarcador',
        'Documento do Embarcador'
    ];

    public filterColumns = [
        {
            column: 'name',
            display_name: 'Nome do Embarcador',
            type: 'string',
        },
    ]

    private data: ShipperListingElement[] = [];
    public dataSource: MatTableDataSource<ShipperListingElement>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        public service: ShipperService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }
}