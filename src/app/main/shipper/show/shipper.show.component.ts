
import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { ShipperListingElement } from 'app/_interfaces/shipper/listing-element';
import { MatProgressSpinnerModule } from '@angular/material';

@Component({
    selector: 'shipper-show',
    templateUrl: './shipper.show.component.html',
    styleUrls: ['./shipper.show.component.css'],
})
export class ShipperShowComponent {

    public data: ShipperListingElement[] = [];
    public loadingShipper: boolean = true;
    public loading: boolean = true;

    /**
     * Constructor
     * 
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    public constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        public spinner: MatProgressSpinnerModule
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.route.params.subscribe(params => {

            this.fetchDataShippers(`/shippers/${params.id}`);
        })

    }

    /**
     * 
     * @param url 
     */
    private fetchDataShippers(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(shipper => {
                this.data = shipper;
                this.loadingShipper = false;
                this.loading = false;
            });
            this.loading = false;
    }
}
