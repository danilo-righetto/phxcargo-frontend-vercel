import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { ShipperShowComponent } from './shipper.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    declarations: [
        ShipperShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        BrowserAnimationsModule,
        MatProgressSpinnerModule,
        BrowserModule,   
    ],
    exports: [
        ShipperShowComponent,
    ]
})

export class ShipperShowModule {
}
