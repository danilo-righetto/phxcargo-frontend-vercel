import { NgModule } from "@angular/core";
import { ShipperComponent } from "./shipper.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { ShipperService } from "app/_services/shipper/shipper.service";
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IConfig, NgxMaskModule } from 'ngx-mask';

const maskConfig: Partial<IConfig> = {
    validation: false,
};

@NgModule({
    declarations: [
        ShipperComponent,
    ],
    imports: [
        NgxMaskModule.forRoot(maskConfig),
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
        BrowserAnimationsModule,
        MatProgressSpinnerModule,  
    ],
    exports: [
        ShipperComponent,
    ],
    providers: [
        ShipperService,
    ]
})
export class ShipperModule {

}