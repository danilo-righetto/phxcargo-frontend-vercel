import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { ShipperUpdateComponent } from './shipper.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

const maskConfig: Partial<IConfig> = {
    validation: false,
};

@NgModule({
    declarations: [
        ShipperUpdateComponent
    ],
    imports: [
        NgxMaskModule.forRoot(maskConfig),
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
        FormsModule,
        NgxMatSelectSearchModule,
    ],
    exports: [
        ShipperUpdateComponent
    ]
})

export class ShipperUpdateModule {
}
