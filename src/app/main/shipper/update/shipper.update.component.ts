
import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { ShipperListingElement } from 'app/_interfaces/shipper/listing-element';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CountryService } from 'app/_services/country/country.service';
import { MatProgressSpinnerModule } from '@angular/material';
import { Subject, ReplaySubject } from 'rxjs';
import { CityService } from 'app/_services/address/city.service';

export interface City {
    id: number
    uf: string
    code: string
    name: string
    ibge_code: string
}

@Component({
    selector: 'shipper-update',
    templateUrl: './shipper.update.component.html',
    styleUrls: ['./shipper.update.component.css'],
})
export class ShipperUpdateComponent {

    public data: ShipperListingElement;
    public _update: boolean = false;
    public endpoint: string = '/shippers';
    public currentId = null;

    public countries: any;
    public loadingcountry: boolean = true;
    public loadingShipper: boolean = true;
    public formGroupShipper: FormGroup;

    public kind_of_document: number;
    public type_of_documento: string;

    public changeLayoutModelKindDocumentCPF: boolean = false;
    public changeLayoutModelKindDocumentRG: boolean = false;
    public changeLayoutModelKindDocumentCNPJ: boolean = false;
    public changeLayoutModelKindDocumentPassaport: boolean = false;
    public changeLayoutModelKindDocumentTAXID: boolean = false;
    public cityFilter: FormGroup;
    public filteredCity: ReplaySubject<City[]> = new ReplaySubject<City[]>(1);
    public cities: any;
    public loadingCity: boolean = true;

    onClickKindOfDocumentCPF() {
        this.kind_of_document = 1;
        this.type_of_documento = "CPF";
        this.changeLayoutModelKindDocumentCPF = true;
        this.changeLayoutModelKindDocumentRG = false;
        this.changeLayoutModelKindDocumentTAXID = false;
        this.changeLayoutModelKindDocumentCNPJ = false;
        this.changeLayoutModelKindDocumentPassaport = false;
        this.formGroupShipper.patchValue({ shipper_document: '' })
        this.formGroupShipper.controls.shipper_document.clearValidators();
    }

    onClickKindOfDocumentRG() {
        this.kind_of_document = 2;
        this.type_of_documento = "RG";
        this.changeLayoutModelKindDocumentRG = true;
        this.changeLayoutModelKindDocumentCPF = false;
        this.changeLayoutModelKindDocumentTAXID = false;
        this.changeLayoutModelKindDocumentCNPJ = false;
        this.changeLayoutModelKindDocumentPassaport = false;
        this.formGroupShipper.patchValue({ shipper_document: '' })
        this.formGroupShipper.controls.shipper_document.clearValidators();
    }

    onClickKindOfDocumentCPNJ() {
        this.kind_of_document = 3;
        this.type_of_documento = "CNPJ";
        this.changeLayoutModelKindDocumentCNPJ = true;
        this.changeLayoutModelKindDocumentCPF = false;
        this.changeLayoutModelKindDocumentRG = false;
        this.changeLayoutModelKindDocumentTAXID = false;
        this.changeLayoutModelKindDocumentPassaport = false;
        this.formGroupShipper.patchValue({ shipper_document: '' })
        this.formGroupShipper.controls.shipper_document.clearValidators();
    }

    onClickKindOfDocumentTAXID() {
        this.kind_of_document = 6;
        this.type_of_documento = "TAX_ID";
        this.changeLayoutModelKindDocumentTAXID = true;
        this.changeLayoutModelKindDocumentCPF = false;
        this.changeLayoutModelKindDocumentRG = false;
        this.changeLayoutModelKindDocumentCNPJ = false;
        this.changeLayoutModelKindDocumentPassaport = false;
        this.formGroupShipper.patchValue({ shipper_document: '' })
        this.formGroupShipper.controls.shipper_document.clearValidators();
    }

    onClickKindOfDocumentPassaport() {
        this.kind_of_document = 5;
        this.type_of_documento = "PASSPORT";
        this.changeLayoutModelKindDocumentPassaport = true;
        this.changeLayoutModelKindDocumentTAXID = false;
        this.changeLayoutModelKindDocumentCPF = false;
        this.changeLayoutModelKindDocumentRG = false;
        this.changeLayoutModelKindDocumentCNPJ = false;
        this.formGroupShipper.patchValue({ shipper_document: '' })
        this.formGroupShipper.controls.shipper_document.clearValidators();
    }

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private router: Router,
        private countryService: CountryService,
        public spinner: MatProgressSpinnerModule,
        private citiesService: CityService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.buildFormGroupShipper();

        this.route.params.subscribe(params => {
            if (params.id) {
                this._update = true;
                this.currentId = params.id;
                this.fetchDataShipper(`/shippers/${params.id}`);
            } else {
                this.loadingShipper = false;
            }
        });

        this.countryService.fetchData('', 255).subscribe(countries => {
            this.countries = countries.data
            this.loadingcountry = false;
        });

        this.citiesService.fetchData('', 5000).subscribe(cities => {
            this.cities = cities.data;
            this.filteredCity.next(this.cities.slice());
            this.loadingCity = false;
        });
    }

    /**
     * Build reactive form
     *
     */
    private buildFormGroupShipper() {
        this.loadingShipper = true;
        this.formGroupShipper = this._formBuilder.group({
            id: ['',],
            shipper_document: [''],
            shipper_name: [''],
            shipper_address: [''],
            shipper_number: [''],
            shipper_additional_info: [''],
            shipper_neighborhood: [''],
            shipper_zip_code: [''],
            shipper_city: [''],
            shipper_state: [''],
            shipper_phone: [''],
            shipper_country_id: [''],
            shipper_email: [''],
            cityFilter: [{ value: '', disabled: false }],
        });
    }

    public initCityFilter() {
        const cityFilter = this.formGroupShipper.controls.cityFilter;

        cityFilter.valueChanges.subscribe(() => {
            let search = cityFilter.value.toLowerCase();

            this.filteredCity.next(
                this.cities.filter(city => city.name.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    /**
     * 
     * @param url 
     */
    private fetchDataShipper(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(shipper => {
                this.data = shipper;

                if (shipper.documents[0].type == "CPF") {
                    this.kind_of_document = 1;
                    this.type_of_documento = "CPF";
                    this.changeLayoutModelKindDocumentCPF = true;
                    this.changeLayoutModelKindDocumentRG = false;
                    this.changeLayoutModelKindDocumentTAXID = false;
                    this.changeLayoutModelKindDocumentCNPJ = false;
                    this.changeLayoutModelKindDocumentPassaport = false;
                }

                if (shipper.documents[0].type == "CNPJ") {
                    this.kind_of_document = 1;
                    this.type_of_documento = "CNPJ";
                    this.changeLayoutModelKindDocumentCNPJ = true;
                    this.changeLayoutModelKindDocumentCPF = false;
                    this.changeLayoutModelKindDocumentRG = false;
                    this.changeLayoutModelKindDocumentTAXID = false;
                    this.changeLayoutModelKindDocumentPassaport = false;
                }

                if (shipper.documents[0].type == "RG") {
                    this.kind_of_document = 2;
                    this.type_of_documento = "RG";
                    this.changeLayoutModelKindDocumentRG = true;
                    this.changeLayoutModelKindDocumentCPF = false;
                    this.changeLayoutModelKindDocumentTAXID = false;
                    this.changeLayoutModelKindDocumentCNPJ = false;
                    this.changeLayoutModelKindDocumentPassaport = false;
                }

                if (shipper.documents[0].type == "PASSPORT") {
                    this.kind_of_document = 5;
                    this.type_of_documento = "Passaport";
                    this.changeLayoutModelKindDocumentPassaport = true;
                    this.changeLayoutModelKindDocumentTAXID = false;
                    this.changeLayoutModelKindDocumentCPF = false;
                    this.changeLayoutModelKindDocumentRG = false;
                    this.changeLayoutModelKindDocumentCNPJ = false;
                }

                if (shipper.documents[0].type == "TAX_ID") {
                    this.kind_of_document = 6;
                    this.type_of_documento = "TAX_ID";
                    this.changeLayoutModelKindDocumentTAXID = true;
                    this.changeLayoutModelKindDocumentCPF = false;
                    this.changeLayoutModelKindDocumentRG = false;
                    this.changeLayoutModelKindDocumentCNPJ = false;
                    this.changeLayoutModelKindDocumentPassaport = false;
                }

                this.formGroupShipper.patchValue({
                    id: (typeof shipper.email !== undefined || shipper.email !== null) && (shipper.email[0] !== undefined) ? shipper.email[0].id : '',
                    shipper_document: (typeof shipper.documents !== undefined || shipper.documents !== null) && (shipper.documents[0] !== undefined) ? shipper.documents[0].number : '',
                    shipper_name: (typeof shipper.name !== undefined || shipper.name !== null) ? shipper.name : '',
                    shipper_address: (typeof shipper.addresses !== undefined || shipper.addresses !== null) && (shipper.addresses[0] !== undefined) ? shipper.addresses[0].address : '',
                    shipper_number: (typeof shipper.addresses !== undefined || shipper.addresses !== null) && (shipper.addresses[0] !== undefined) ? shipper.addresses[0].number : '',
                    shipper_additional_info: (typeof shipper.addresses !== undefined || shipper.addresses !== null) && (shipper.addresses[0] !== undefined) ? shipper.addresses[0].additional_info : '',
                    shipper_neighborhood: (typeof shipper.addresses !== undefined || shipper.addresses !== null) && (shipper.addresses[0] !== undefined) ? shipper.addresses[0].neighborhood : '',
                    shipper_zip_code: (typeof shipper.addresses !== undefined || shipper.addresses !== null) && (shipper.addresses[0] !== undefined) ? shipper.addresses[0].zip_code : '',
                    shipper_city: (typeof shipper.addresses !== undefined || shipper.addresses !== null) && (shipper.addresses[0] !== undefined) ? shipper.addresses[0].city : '',
                    shipper_state: (typeof shipper.addresses !== undefined || shipper.addresses !== null) && (shipper.addresses[0] !== undefined) ? shipper.addresses[0].state : '',
                    shipper_country_id: (typeof shipper.addresses !== undefined || shipper.addresses !== null) && (shipper.addresses[0] !== undefined) ? shipper.addresses[0].country_id : '',
                    shipper_phone: (typeof shipper.phones !== undefined || shipper.phones !== null) && (shipper.phones[0] !== undefined) ? shipper.phones[0].number : '',
                    shipper_email: (typeof shipper.email !== undefined || shipper.email !== null) && (shipper.email[0] !== undefined) ? shipper.email[0].address : '',
                });
                this.loadingShipper = false;


            });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {


        if (this._update) {
            const controls = this.parseControlsUpdate(this.formGroupShipper.controls);
            return this.update(controls);
        } else {
            const controls = this.parseControls(this.formGroupShipper.controls);
            return this.create(controls);
        }

        
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControlsUpdate(controls: any) {
        const parsed = {
            "name": controls.shipper_name.value,
            "addresses": [
                {
                    "number": controls.shipper_number.value,
                    "additional_info": controls.shipper_additional_info.value,
                    "country_id": controls.shipper_country_id.value,
                    "zip_code": controls.shipper_zip_code.value,
                    "address": controls.shipper_address.value,
                    "cities_id": controls.shipper_city.value,
                    "state": controls.shipper_state.value,
                    "neighborhood": controls.shipper_neighborhood.value,
                }
            ],
            "documents": [
                {
                    "type_id": this.kind_of_document,
                    "type": this.type_of_documento,
                    "number": controls.shipper_document.value,
                }
            ],
            "phones": [
                {
                    "number": controls.shipper_phone.value,
                    "type": "phone",
                    "name": controls.shipper_name.value
                }
            ],
            "emails": [
                {
                    "id": controls.id.value,
                    "name": controls.shipper_name.value,
                    "address": controls.shipper_email.value,
                    "type": "DELIVERY"
                }
            ],

        }

        return parsed;
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {
        const parsed = {
            "name": controls.shipper_name.value,
            "addresses": [
                {
                    "number": controls.shipper_number.value,
                    "additional_info": controls.shipper_additional_info.value,
                    "country_id": controls.shipper_country_id.value,
                    "zip_code": controls.shipper_zip_code.value,
                    "address": controls.shipper_address.value,
                    "cities_id": controls.shipper_city.value,
                    "state": controls.shipper_state.value,
                    "neighborhood": controls.shipper_neighborhood.value,
                }
            ],
            "documents": [
                {
                    "type_id": this.kind_of_document,
                    "type": this.type_of_documento,
                    "number": controls.shipper_document.value,
                }
            ],
            "phones": [
                {
                    "number": controls.shipper_phone.value,
                    "type": "phone",
                    "name": controls.shipper_name.value
                }
            ],
            "emails": [
                {
                    "name": controls.shipper_name.value,
                    "address": controls.shipper_email.value,
                    "type": "DELIVERY"
                }
            ],

        }

        return parsed;
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }
}