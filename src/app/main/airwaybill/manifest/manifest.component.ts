import { Component, ViewChild } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as portuguese } from '../_i18n/pt';
import { DefaultTableComponent } from 'app/_default/data-list/table.component';
import { ManifestService } from 'app/_services/manifest/manifest.service';

@Component({
    selector: 'airwaybill-master',
    templateUrl: './manifest.component.html',
})
export class ManifestComponent {

    @ViewChild(DefaultTableComponent, {static: false}) public table: DefaultTableComponent;

    public endpoint: string = '/manifests';

    public displayedColumns: string[] = [
        'Número',
        'ID Externo',
        'Descrição',
    ];

    public filterColumns = [
        {
            column: 'company_code',
            display_name: 'Empresa',
            type: 'string'
        },
        {
            column: 'air_waybill_code',
            display_name: 'Número House',
            type: 'string'
        },
        {
            column: 'master_airwaybill_code',
            display_name: 'Número Master',
            type: 'string'
        },
        {
            column: 'created_at',
            type: 'date'
        }
    ];

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        public service: ManifestService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }
}