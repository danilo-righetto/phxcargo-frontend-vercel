import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatTableModule } from '@angular/material';
import { DeclarationsModule } from 'app/declarations.module';
import { ManifestService } from 'app/_services/manifest/manifest.service';
import { ManifestComponent } from './manifest.component';

@NgModule({
    declarations: [
        ManifestComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        ManifestComponent,
    ],
    providers: [
        ManifestService,
    ]
})
export class ManifestModule { }