import { NgModule } from "@angular/core";
import { ManifestUpdateComponent } from "./manifest.update.component";

@NgModule({
    declarations: [
        ManifestUpdateComponent,
    ],
    imports: [

    ],
    exports: [
        ManifestUpdateComponent,
    ]
})
export class ManifestUpdateModule {

}