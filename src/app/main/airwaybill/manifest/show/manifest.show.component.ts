import { map } from 'rxjs/operators';
import { Component, ViewChild } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../_i18n/pt';
import { ActivatedRoute } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { DefaultTableComponent } from 'app/_default/data-list/table.component';
import { AirwaybillService } from 'app/_services/airwaybill/house.service';
import { ManifestRequest, Manifest } from 'app/_interfaces/airwaybill/manifest';
import { ManifestEventsService } from 'app/_services/manifest/manifest.events.service';
import { ManifestFinesService } from 'app/_services/manifest/manifest.fines.service';
import { TaxPaymentOrderService } from 'app/_services/payment/tax-payment-order.service';
import { ManifestGnreService } from 'app/_services/manifest/manifest.gnre.service';
import { MatTableDataSource } from '@angular/material';
import { HouseListingElement } from 'app/_interfaces/airwaybill/house.airwaybill';
import { DomSanitizer } from '@angular/platform-browser';
import {saveAs as importedSaveAs} from "file-saver";

@Component({
    selector: 'airwaybill-manifest-show',
    templateUrl: './manifest.show.component.html',
})
export class ManifestShowComponent {
    public fileUrl: any;
    public fileName: string;
    public headers: any;
    public data: any;
    public loading: boolean = true;
    private currentId: number = null;

    public housesDataSource: any;

    public selectedTab = 0;

    public displayedColumns: string[] = [
        'Número',
        'ID Externo',
        'Cliente',
        'Master',
        'Peso',
        'Quantidade',
        'Estado'
    ];

    public endpoint: string = '/airwaybills/house';

    @ViewChild(DefaultTableComponent, { static: false })
    housesTable: DefaultTableComponent;

    @ViewChild('manifestFiles', { static: false })
    manifestFiles: DefaultTableComponent;

    @ViewChild('paymentOrders', { static: false })
    paymentOrders: DefaultTableComponent;

    public houseFilterColumns: Array<any> = [
        {
            column: 'code',
            display_name: 'Número House',
            type: 'string'
        },
        {
            column: 'checkpoint_code',
            display_name: 'Código Checkpoint',
            type: 'string'
        }
    ];

    public dataSource: MatTableDataSource<HouseListingElement>;

    /**
     * Constructor
     * 
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    public constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        public airwaybillService: AirwaybillService,
        public eventService: ManifestEventsService,
        public fineService: ManifestFinesService,
        public gnreService: ManifestGnreService,
        public paymentOrderService: TaxPaymentOrderService,
        private sanitizer: DomSanitizer
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.currentId = params.id;
            this.fetchData(`/manifests/${params.id}`);

            this.fineService.setQuery(`&fines_category_id=1|2`);
            this.gnreService.setQuery(`&fines_category_id=3`);
            this.airwaybillService.setQuery(`&manifest_id=${params.id}`);
            this.paymentOrderService.setQuery(`&manifest_id=${params.id}`);
            this.eventService.setEndpoint(`/manifests/events/${params.id}`);
            this.fineService.setEndpoint(`/manifests/fines/${params.id}`);
            this.gnreService.setEndpoint(`/manifests/fines/${params.id}`);
        });
    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe((manifest: ManifestRequest) => {

                this.data = new Manifest(manifest);
                this.loading = false;
            });
    }

    /**
     * Start RBF Manifest process
     * 
     */
    public startProcess() {
        this.manifestFiles.isLoadingResults = true;
        this.selectedTab = 1;

        this.http.post(`/manifests/generate-xml/${this.currentId}`).pipe(
            map((res: any) => res.data))
            .subscribe(
                manifest => {
                    if (!manifest.files) {
                        this.manifestFiles.isLoadingResults = false;
                        return;
                    }

                    this.manifestFiles.fetchRawSource(manifest.files);
                },
                err => {
                    this.manifestFiles.isLoadingResults = false;
                    return err;
                }
            )
    }

    /**
     * Create the rfb presence file
     */
    public generatePresence() {
        this.manifestFiles.isLoadingResults = true;
        this.selectedTab = 1;

        this.http.post(`/manifests/generate-presence/${this.currentId}`).pipe(
            map((res: any) => res.data))
            .subscribe(
                manifest => {
                    if (!manifest.files) {
                        this.manifestFiles.isLoadingResults = false;
                        return;
                    }

                    this.manifestFiles.fetchRawSource(manifest.files);
                },
                err => {
                    this.manifestFiles.isLoadingResults = false;
                    return err;
                }
            )
    }

    /**
     * Generate and register payment order file
     * 
     */
    public generatePaymentOrder() {
        this.paymentOrders.isLoadingResults = true;
        this.selectedTab = 4;

        this.http.post(`/manifests/tax-payment-order/${this.currentId}`).pipe(
            map((res: any) => res.data))
            .subscribe(
                paymentOrder => {
                    if (!paymentOrder.order_type) {
                        this.paymentOrders.isLoadingResults = false;
                        return;
                    }

                    this.paymentOrders.fetchDataSource();
                },
                err => {
                    this.paymentOrders.isLoadingResults = false;
                    return err;
                }
            )
    }

    /**
     * Generate and register payment order file
     */
    public sendSefazBatch() {

        this.paymentOrders.isLoadingResults = true;

        this.http.post(`/manifests/gnre-order/${this.currentId}`).pipe(
            map((res: any) => res.data))
            .subscribe(
                paymentOrder => {
                    this.selectedTab = 4;
                    this.paymentOrders.isLoadingResults = false;
                    this.paymentOrders.fetchDataSource();
                },
                err => {
                    this.paymentOrders.isLoadingResults = false;
                    return err;
                }
            )
        return;
    }

    public downloadManifest() {
        this.downloadFile(
            this.http.endpoint(`/manifests/xml-download/${this.currentId}`),
            ''
        );
    }

    public downloadDeclaration() {
        this.downloadFile(
            this.http.endpoint(`/manifests/download-declaration-xml-house/${this.currentId}`),
            ''
        );
    }

    public uploadReturn(fileInputEvent: any) {
        const form = new FormData();
        form.append(
            "import",
            fileInputEvent.target.files[0]
        );

        fetch(this.http.endpoint(`/import/file/${this.currentId}`), { 
            "method": "POST",
            "headers": {
                "Content-Type": "multipart/form-data",
                "Authorization": this.http.getToken(),
            },
            "body": fileInputEvent.target.files[0],
        })
        .then(response => {
            if (response.status == 403) {
                alert('Erro de Importação');
            } else {
                alert('Importação realizada com sucesso');
            }
        }).then(send => location.reload())
        .catch(err => {
            alert('Erro de Importação');
        });
    }

    public downloadFile(url: any, body: any) {
        fetch(url, {
        "method": "POST",
        "headers": {
            "Content-Type": "application/json",
            "Authorization": this.http.getToken()
        },
        "body": body
        })
            .then(response => {
                this.headers = response.headers;
                console.log(response);
                response.blob().then(function (myBlob) {
                    console.log(myBlob);
                    const blob = new Blob([myBlob], { type: 'application/xml' });
                    importedSaveAs(blob, response.headers.get('content-disposition'));
                });
            })
        .catch(err => {
            console.error(err);
        });
    }
}