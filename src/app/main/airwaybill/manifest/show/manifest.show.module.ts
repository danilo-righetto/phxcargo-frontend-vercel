import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { ManifestShowComponent } from './manifest.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatTabsModule } from '@angular/material';
import { ManifestEventsService } from 'app/_services/manifest/manifest.events.service';
import { ManifestFinesService } from 'app/_services/manifest/manifest.fines.service';
import { TaxPaymentOrderService } from 'app/_services/payment/tax-payment-order.service';
import { ManifestGnreService } from 'app/_services/manifest/manifest.gnre.service';
import { HouseCheckpointModule } from '../../house/checkpoint/checkpoint.module';

@NgModule({
    declarations: [
        ManifestShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatTabsModule,
        HouseCheckpointModule,
    ],
    exports: [
        ManifestShowComponent,
    ],
    providers: [
        ManifestFinesService,
        ManifestEventsService,
        TaxPaymentOrderService,
        ManifestGnreService,
    ]
})

export class ManifestShowModule {
}
