import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { ImporterService } from 'app/_services/importer/importer.service';
import { ShipperService } from 'app/_services/shipper/shipper.service';
import { CompanyService } from 'app/_services/company/company.service';
import { TaxClasseService } from 'app/_services/customs-settings/tax-class.service';
import { CustomsAdministratorsService } from 'app/_services/customs-settings/customs-administrators.service';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { ConsolidatorUpdateComponent } from './consolidator.update.component';
import { HouseCheckpointModule } from '../../house/checkpoint/checkpoint.module';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { DateTimeModule } from 'app/_default/datetime/datetime.default.module';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
    declarations: [
        ConsolidatorUpdateComponent
    ],
    imports: [
        NgxMaskModule.forRoot(options),
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        HouseCheckpointModule,
        MatDividerModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
        NgxMatSelectSearchModule,
        DateTimeModule,
    ],
    exports: [
        ConsolidatorUpdateComponent,
        HouseCheckpointModule,
    ],
    providers: [
        ImporterService,
        ShipperService,
        CompanyService,
        TaxClasseService,
        CustomsAdministratorsService,
    ]
})

export class ConsolidatorUpdateModule {

}
