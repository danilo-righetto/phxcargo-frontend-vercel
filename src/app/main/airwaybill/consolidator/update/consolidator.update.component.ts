
import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CompanyListingElement as Company } from 'app/_interfaces/company/listing-element';
import { HouseListingElement } from 'app/_interfaces/airwaybill/house.airwaybill';
import { CompanyService } from 'app/_services/company/company.service';
import { ReplaySubject } from 'rxjs';
import { AirportService } from 'app/_services/airport/airport.service'
import { Airport } from 'app/_interfaces/airport/listing-element';

@Component({
    selector: 'consolidator-update',
    templateUrl: './consolidator.update.component.html',
    styleUrls: ['consolidator.update.component.css']
})
export class ConsolidatorUpdateComponent implements OnInit {

    public data: HouseListingElement;
    public loading: boolean = true;
    public loadingAirport: boolean = true;
    public _update: boolean = false;
    public currentId = null;

    public companies: Company[];
    public company_id = null;
    public loadingCompany: boolean = true;
    public formGroup: FormGroup;
    private endpoint: string = '/airwaybills/consolidators';
    public companyLastFilter: FormGroup;
    public filteredCompany: ReplaySubject<Company[]> = new ReplaySubject<Company[]>(1);
    public airportOriginLastFilter: FormGroup;
    public airportDestinationLastFilter: FormGroup;
    public filteredOriginAirport: ReplaySubject<Airport[]> = new ReplaySubject<Airport[]>(1);
    public filteredDestinationAirport: ReplaySubject<Airport[]> = new ReplaySubject<Airport[]>(1);
    public airportsOrigin: any;
    public airportsDestination: any;

    /**
     * 
     * @param _fuseTranslationLoaderService 
     * @param _formBuilder 
     * @param route 
     * @param http 
     * @param router 
     * @param companyService 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private router: Router,
        private companyService: CompanyService,
        private airportService: AirportService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    /**
     * Lifecycle hook that is called after data-bound properties of a directive are
     * initialized.
     */
    public ngOnInit(): void {
        this.buildFormGroup();

        this.route.params.subscribe(params => {
            if (params.id) {
                this._update = true;
                this.fetchHouseInformation(`${this.endpoint}/${params.id}`);
            } else {
                this.loading = false;
            }
        });

        this.companyService.fetchData(null, 300).subscribe(companies => {
            this.companies = companies.data;
            this.filteredCompany.next(this.companies.slice());
            this.loadingCompany = false;
        });

        this.airportService.fetchData('', 300).subscribe(airports => {
            this.airportsOrigin = airports.data;
            this.airportsDestination = airports.data;
            this.filteredOriginAirport.next(this.airportsOrigin.slice());
            this.filteredDestinationAirport.next(this.airportsDestination.slice());
            this.loadingAirport = false;
        });

        this.initCompanyFilter();
        this.initOriginFilter();
        this.initDestinationFilter();
    }

    public verifyFields(): boolean {
        if (
            !this.formGroup.get('origin').valid ||
            !this.formGroup.get('destination').valid ||
            !this.formGroup.get('date').valid ||
            !this.formGroup.get('code').valid ||
            !this.formGroup.get('company').valid
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Build reactive form
     * 
     */
    private buildFormGroup() {

        this.formGroup = this._formBuilder.group({
            origin: ['', Validators.required],
            destination: ['', Validators.required],
            date: ['', Validators.required],
            code: ['', Validators.required],
            company: ['', Validators.required],
            companyLastFilter: [{ value: '', disabled: false }],
            airportOriginLastFilter: [{ value: '', disabled: false }],
            airportDestinationLastFilter: [{ value: '', disabled: false }],
        });
    }

    public initCompanyFilter() {
        const companyLastFilter = this.formGroup.controls.companyLastFilter;

        companyLastFilter.valueChanges.subscribe(() => {
            let search = companyLastFilter.value.toLowerCase();

            this.filteredCompany.next(
                this.companies.filter(company => company['Nome da Empresa'].toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initOriginFilter() {
        const airportOriginLastFilter = this.formGroup.controls.airportOriginLastFilter;

        airportOriginLastFilter.valueChanges.subscribe(() => {
            let search = airportOriginLastFilter.value.toLowerCase();

            this.filteredOriginAirport.next(
                this.airportsOrigin.filter(airport => airport['Nome do Aeroporto'].toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initDestinationFilter() {
        const airportDestinationLastFilter = this.formGroup.controls.airportDestinationLastFilter;

        airportDestinationLastFilter.valueChanges.subscribe(() => {
            let search = airportDestinationLastFilter.value.toLowerCase();

            this.filteredDestinationAirport.next(
                this.airportsDestination.filter(airport => airport['Nome do Aeroporto'].toLowerCase().indexOf(search) > 1)
            );
        });
    }

    /**
     * Fetch data to current form
     * 
     * @param url 
     */
    private fetchHouseInformation(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(consolidator => {
                this.data = consolidator;
                this.formGroup.patchValue({
                    origin: consolidator.origin,
                    company: consolidator.company ? consolidator.company.id : null,
                    destination: consolidator.destination,
                    date: consolidator.date,
                    code: consolidator.code,
                });
                this.loading = false;
            });
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {

        console.log(controls.date.value);
        let bag_date = new Date(controls.date.value);
        let date_to_formated = '';
        date_to_formated = bag_date.getFullYear() + "-" + ("0" + (bag_date.getMonth() + 1)).slice(-2) + "-" + ("0" + (bag_date.getDate())).slice(-2);

        const parsed = {
            "origin": controls.origin.value,
            "destination": controls.destination.value,
            "company_id": controls.company.value,
            "date": date_to_formated,
            "code": controls.code.value,
        }
        return parsed;
    }

    /**
     * Submit form group (update|create)
     * 
     */
    public submit(): void {

        const controls = this.parseControls(this.formGroup.controls);
        console.log(controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {

        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {

        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }
}