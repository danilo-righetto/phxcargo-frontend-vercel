import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { DeclarationsModule } from 'app/declarations.module';
import { ImporterService } from 'app/_services/importer/importer.service';
import { ShipperService } from 'app/_services/shipper/shipper.service';
import { CompanyService } from 'app/_services/company/company.service';
import { TaxClasseService } from 'app/_services/customs-settings/tax-class.service';
import { CustomsAdministratorsService } from 'app/_services/customs-settings/customs-administrators.service';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { ConsolidatorShowComponent } from './consolidator.show.component';
import { HouseCheckpointModule } from '../../house/checkpoint/checkpoint.module';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { DateTimeModule } from 'app/_default/datetime/datetime.default.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule } from '@angular/material';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
    declarations: [
        ConsolidatorShowComponent
    ],
    imports: [
        NgxMaskModule.forRoot(options),
        FuseSharedModule,
        DeclarationsModule,

        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,

        HouseCheckpointModule,
        MatDividerModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
        NgxMatSelectSearchModule,
        DateTimeModule,
    ],
    exports: [
        ConsolidatorShowComponent,
        HouseCheckpointModule,
    ],
    providers: [
        ImporterService,
        ShipperService,
        CompanyService,
        TaxClasseService,
        CustomsAdministratorsService,
    ]
})

export class ConsolidatorShowModule {

}
