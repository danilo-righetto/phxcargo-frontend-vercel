
import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HouseListingElement } from 'app/_interfaces/airwaybill/house.airwaybill';
import { CompanyListingElement as Company } from 'app/_interfaces/company/listing-element';
import { CompanyService } from 'app/_services/company/company.service';
import { ReplaySubject } from 'rxjs';
import { AirportService } from 'app/_services/airport/airport.service'
import { Airport } from 'app/_interfaces/airport/listing-element';

@Component({
    selector: 'consolidator-update',
    templateUrl: './consolidator.show.component.html',
    styleUrls: ['consolidator.show.component.css']
})
export class ConsolidatorShowComponent implements OnInit {

    public data: any;
    public formGroup: FormGroup;
    public loading: boolean = true;
    public loadingAirport: boolean = true;
    public loadingCompany: boolean = true;
    private endpoint: string = '/airwaybills/consolidators';
    public companyLastFilter: FormGroup;
    public filteredCompany: ReplaySubject<Company[]> = new ReplaySubject<Company[]>(1);
    public airportOriginLastFilter: FormGroup;
    public airportDestinationLastFilter: FormGroup;
    public filteredOriginAirport: ReplaySubject<Airport[]> = new ReplaySubject<Airport[]>(1);
    public filteredDestinationAirport: ReplaySubject<Airport[]> = new ReplaySubject<Airport[]>(1);
    public airportsOrigin: any;
    public airportsDestination: any;
    public companies: Company[];

    /**
     * 
     * @param _fuseTranslationLoaderService 
     * @param _formBuilder 
     * @param route 
     * @param http 
     * @param router 
     * @param companyService 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private router: Router,
        private companyService: CompanyService,
        private airportService: AirportService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    /**
     * Lifecycle hook that is called after data-bound properties of a directive are
     * initialized.
     */
    public ngOnInit(): void {
        this.buildFormGroup();

        this.route.params.subscribe(params => {

            this.fetchHouseInformation(`${this.endpoint}/${params.id}`);
        });

        this.companyService.fetchData(null, 300).subscribe(companies => {
            this.companies = companies.data;
            this.filteredCompany.next(this.companies.slice());
            this.loadingCompany = false;
        });

        this.airportService.fetchData('', 300).subscribe(airports => {
            this.airportsOrigin = airports.data;
            this.airportsDestination = airports.data;
            this.filteredOriginAirport.next(this.airportsOrigin.slice());
            this.filteredDestinationAirport.next(this.airportsDestination.slice());
            this.loadingAirport = false;
        });

        this.initCompanyFilter();
        this.initOriginFilter();
        this.initDestinationFilter();
    }

    /**
     * Fetch data to current form
     * 
     * @param url 
     */
    private fetchHouseInformation(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(consolidator => {
                this.data = consolidator;
                this.formGroup.patchValue({
                    origin: consolidator.origin,
                    company: consolidator.company ? consolidator.company.id : null,
                    destination: consolidator.destination,
                    date: consolidator.date,
                    code: consolidator.code,
                });
                this.loading = false;
            });
    }

    private buildFormGroup() {

        this.formGroup = this._formBuilder.group({
            origin: [{ value: '', disabled: true }],
            destination: [{ value: '', disabled: true }],
            date: [{ value: '', disabled: true }],
            code: [{ value: '', disabled: true }],
            company: [{ value: '', disabled: true }],
            companyLastFilter: [{ value: '', disabled: true }],
            airportOriginLastFilter: [{ value: '', disabled: true }],
            airportDestinationLastFilter: [{ value: '', disabled: true }],
        });
    }

    public initCompanyFilter() {
        const companyLastFilter = this.formGroup.controls.companyLastFilter;

        companyLastFilter.valueChanges.subscribe(() => {
            let search = companyLastFilter.value.toLowerCase();

            this.filteredCompany.next(
                this.companies.filter(company => company['Nome da Empresa'].toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initOriginFilter() {
        const airportOriginLastFilter = this.formGroup.controls.airportOriginLastFilter;

        airportOriginLastFilter.valueChanges.subscribe(() => {
            let search = airportOriginLastFilter.value.toLowerCase();

            this.filteredOriginAirport.next(
                this.airportsOrigin.filter(airport => airport['Nome do Aeroporto'].toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initDestinationFilter() {
        const airportDestinationLastFilter = this.formGroup.controls.airportDestinationLastFilter;

        airportDestinationLastFilter.valueChanges.subscribe(() => {
            let search = airportDestinationLastFilter.value.toLowerCase();

            this.filteredDestinationAirport.next(
                this.airportsDestination.filter(airport => airport['Nome do Aeroporto'].toLowerCase().indexOf(search) > 1)
            );
        });
    }
}