import { NgModule } from "@angular/core";
import { ConsolidatorComponent } from "./consolidator.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";

@NgModule({
    declarations: [
        ConsolidatorComponent
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        ConsolidatorComponent
    ]
})
export class ConsolidatorModule {

}