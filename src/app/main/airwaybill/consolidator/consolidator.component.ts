import { ConsolidatorService } from 'app/_services/airwaybill/consolidator.service';
import { Component, OnInit } from "@angular/core";
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';

@Component({
    selector: 'list-component',
    templateUrl: 'consolidator.component.html'
})
export class ConsolidatorComponent implements OnInit {

    public endpoint: string = '/airwaybills/consolidators';

    public displayedColumns: string[] = [
        'Código da Bag',
        'Origem',
        'Destino da Bag',
        'Data',
    ];

    private data: any = [];
    public dataSource: any;

    constructor(
        public service: ConsolidatorService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }


    public ngOnInit() {
        this.service.setEndpoint(this.endpoint);
    }
}