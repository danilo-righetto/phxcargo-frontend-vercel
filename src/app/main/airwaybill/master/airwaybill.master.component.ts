
import { map } from 'rxjs/operators';
import { Component, ViewChild } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as portuguese } from '../_i18n/pt';
import { MasterLitingElement } from 'app/_interfaces/airwaybill/master.interface';
import { MatTableDataSource } from '@angular/material';
import { MasterAirwaybillService } from 'app/_services/airwaybill/master.service';
import { DefaultTableComponent } from 'app/_default/data-list/table.component';
import { NotificationService } from 'app/_services/notification/notification.service';
import { ManifestService } from 'app/_services/manifest/manifest.service';
import { Router } from '@angular/router';

@Component({
    selector: 'airwaybill-master',
    templateUrl: './airwaybill.master.component.html',
})
export class AirwaybillMasterComponent {

    @ViewChild(DefaultTableComponent, { static: true })
    public table: DefaultTableComponent;

    public endpoint: string = '/airwaybills/master';

    public displayedColumns: string[] = [
        'Número',
        'ID Externo',
        'Data de Saída',
        'Origem do Aeroporto',
        'Destino do Aeroporto',
    ];

    public filterColumns = [
        {
            column: 'company_code',
            display_name: 'Empresa',
            type: 'string'
        },
        {
            column: 'air_waybill_code',
            display_name: 'Número House',
            type: 'string'
        },
        {
            column: 'code',
            display_name: 'Número Master',
            type: 'string'
        },
        {
            column: 'created_at',
            type: 'date'
        }
    ];

    public dataSource: MatTableDataSource<MasterLitingElement>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        public service: MasterAirwaybillService,
        private notification: NotificationService,
        private manifestService: ManifestService,
        private router: Router
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public manifest(): void {

        this.table.isLoadingResults = true;
        const selectedRows = this.table.selection.selected;

        if (selectedRows.length == 0) {
            this.notification.notify('Nenhuma Master foi selecionada', 'error');
            this.table.isLoadingResults = false;
            return;
        }

        this.manifestService.create(
            selectedRows.map(master => master.id)
        )
            .subscribe((res: any) => {
                this.notification.notify('Manifesto gerado com sucesso');
                this.table.isLoadingResults = false;
                this.router.navigate([`manifests/show/${res.data.id}`])
            });
    }
}