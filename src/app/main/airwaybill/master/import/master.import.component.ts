import { Component } from "@angular/core";
import { locale as portuguese } from '../../_i18n/pt';
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HttpAuthenticateService } from "app/_services/_authentication/http.authenticate.service";
import { Router } from "@angular/router";

@Component({
    selector: 'master-import',
    templateUrl: './master.import.component.html'
})
export class MasterImportComponent {

    public formGroup: FormGroup;

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private http: HttpAuthenticateService,
        private router: Router
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.formGroup = this._formBuilder.group({
            file: [null, Validators.required],
            fileName: ['', Validators.required]
        });
    }

    /**
     * Read file content and patch value to form control
     * 
     * @param event 
     */
    public onFileChange(event) {

        if (!event.target.files || !event.target.files.length) {
            throw "Invalid file content"
        }

        let reader = new FileReader;
        const [file] = event.target.files;

        reader.readAsDataURL(file);
        reader.onload = () => {

            this.formGroup.patchValue({
                file: reader.result,
                fileName: file.name
            });
        };
    }

    /**
     * Submit form group
     * 
     */
    public submit(): void {

        const controls = this.formGroup.controls;
        this.http.post(
            '/airwaybills/master/import',
            {
                file: controls.file.value,
                fileName: controls.fileName.value
            }
        ).subscribe(res => this.router.navigate(['/airwaybills/master']));
    }
}