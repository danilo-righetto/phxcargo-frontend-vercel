import { NgModule } from "@angular/core";
import { MasterImportComponent } from "./master.import.component";
import { DeclarationsModule } from "app/declarations.module";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatInputModule } from "@angular/material";

@NgModule({
    declarations: [
        MasterImportComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatInputModule,
    ],
    exports: [
        MasterImportComponent,
    ]
})
export class MasterImportModule {

}