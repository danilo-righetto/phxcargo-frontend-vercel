import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { AirwaybillMasterComponent } from './airwaybill.master.component';
import { MatTableModule } from '@angular/material';
import { DeclarationsModule } from 'app/declarations.module';
import { MasterAirwaybillService } from 'app/_services/airwaybill/master.service';
import { ManifestService } from 'app/_services/manifest/manifest.service';

@NgModule({
    declarations: [
        AirwaybillMasterComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        AirwaybillMasterComponent,
    ],
    providers: [
        MasterAirwaybillService,
        ManifestService,
    ]
})

export class AirwaybillMasterModule {
}
