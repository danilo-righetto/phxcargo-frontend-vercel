import { map } from "rxjs/operators";
import { Component, OnInit } from "@angular/core";
import { CompanyListingElement as Company } from "app/_interfaces/company/listing-element";
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpAuthenticateService } from "app/_services/_authentication/http.authenticate.service";
import { CompanyService } from "app/_services/company/company.service";
import { locale as portuguese } from "../../_i18n/pt";
import { AirlineListingElement as Airline } from "app/_interfaces/airline/listing-element";
import { AirlineService } from "app/_services/airline/airline.service";
import { AirportListingElement as Airport } from "app/_interfaces/airport/listing-element";
import { AirportService } from "app/_services/airport/airport.service";
import { MasterAirwaybillRequest } from "app/_interfaces/airwaybill/master.interface";
import { FlightService } from "app/_services/flight/flight.service";
import { FlightListingElement } from "app/_interfaces/flight/listing-element";
import { ReplaySubject } from "rxjs";
import { ContractService } from "app/_services/company/contract.service";
import {
  ContractListingElement,
  ContractRequest,
  Contract,
} from "app/_interfaces/contract/contract.interface";

@Component({
  selector: "master-update",
  templateUrl: "./master.update.component.html",
  styleUrls: ["master.update.component.css"],
})
export class MasterUpdateComponent implements OnInit {
  public flights: FlightListingElement;
  public data: any;
  public airportsOrigin: any;
  public airportsDestination: any;
  public contracts: any;
  public loadingContract: any;
  public loading: boolean = true;
  public loadingCompany: boolean = true;
  public loadingFlights: boolean = true;
  public loadingAirline: boolean = true;
  public loadingAirport: boolean = true;
  public _update: boolean = false;
  public isCreate: boolean = false;
  public currentId = null;
  public companies: Company[];
  public company_id = null;
  public airlines: Airline[];
  public airports: Airport[];
  public formGroup: FormGroup;
  public companyLastFilter: FormGroup;
  public bagOriginLastFilter: FormGroup;
  public bagDestinationLastFilter: FormGroup;
  public airportOriginLastFilter: FormGroup;
  public airportDestinationLastFilter: FormGroup;
  public airlineLastFilter: FormGroup;
  public contractLastFilter: FormGroup;
  public filteredContract: ReplaySubject<Contract[]> = new ReplaySubject<
    Contract[]
  >(1);
  public filteredCompany: ReplaySubject<Company[]> = new ReplaySubject<
    Company[]
  >(1);
  public filteredOriginBag: ReplaySubject<Airport[]> = new ReplaySubject<
    Airport[]
  >(1);
  public filteredDestinationBag: ReplaySubject<Airport[]> = new ReplaySubject<
    Airport[]
  >(1);
  public filteredOriginAirport: ReplaySubject<Airport[]> = new ReplaySubject<
    Airport[]
  >(1);
  public filteredDestinationAirport: ReplaySubject<
    Airport[]
  > = new ReplaySubject<Airport[]>(1);
  public filteredAirline: ReplaySubject<Airline[]> = new ReplaySubject<
    Airline[]
  >(1);
  private endpoint: string = "/airwaybills/master";

  /**
   *
   * @param _fuseTranslationLoaderService
   * @param _formBuilder
   * @param route
   * @param http
   * @param importerService
   * @param shipperService
   * @param companyService
   * @param countryService
   * @param router
   */
  constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private http: HttpAuthenticateService,
    private companyService: CompanyService,
    private flightService: FlightService,
    private airlineService: AirlineService,
    private airportService: AirportService,
    private router: Router,
    private contractService: ContractService
  ) {
    this._fuseTranslationLoaderService.loadTranslations(portuguese);
  }

  public verifyFields() {
    if (
      !this.formGroup.get("company_branch").valid ||
      !this.formGroup.get("company").valid ||
      !this.formGroup.get("contract").valid ||
      !this.formGroup.get("description").valid ||
      !this.formGroup.get("number").valid ||
      !this.formGroup.get("airline").valid ||
      !this.formGroup.get("flight_number").valid ||
      !this.formGroup.get("flight_alternative_number").valid ||
      !this.formGroup.get("origin_airport").valid ||
      !this.formGroup.get("destination_airport").valid ||
      !this.formGroup.get("bag_code").valid ||
      !this.formGroup.get("bag_origin").valid ||
      !this.formGroup.get("bag_destination").valid ||
      !this.formGroup.get("bag_date").valid
    ) {
      return true;
    }
    return false;
  }

  public ngOnInit(): void {
    this.isCreate = this.router.url.includes("create");
    this.buildFormGroup();

    this.route.params.subscribe((params) => {
      if (params.id) {
        this._update = true;
        this.currentId = params.id;
        this.fetchData(`${this.endpoint}/${this.currentId}`);
      } else {
        this.loading = false;
      }
    });

    this.companyService.fetchData(null, 300).subscribe((companies) => {
      this.companies = companies.data;
      this.filteredCompany.next(this.companies.slice());
      this.loadingCompany = false;
    });
    this.flightService.fetchData(null, 300).subscribe((flights) => {
      this.flights = flights.data;
      this.loadingFlights = false;
    });
    this.airlineService.fetchData(null, 300).subscribe((airlines) => {
      this.airlines = airlines.data;
      this.filteredAirline.next(this.airlines.slice());
      this.loadingAirline = false;
    });
    this.airportService.fetchData(null, 300).subscribe((airports) => {
      this.airports = airports.data;
      this.airportsOrigin = airports.data;
      this.airportsDestination = airports.data;
      this.filteredOriginBag.next(this.airportsOrigin.slice());
      this.filteredDestinationBag.next(this.airportsDestination.slice());
      this.filteredOriginAirport.next(this.airportsOrigin.slice());
      this.filteredDestinationAirport.next(this.airportsDestination.slice());
      this.loadingAirport = false;
    });
    this.contractService.fetchData("", 300).subscribe((contracts) => {
      this.contracts = contracts.data;
      this.filteredContract.next(this.contracts.slice());
      this.loadingContract = false;
    });

    this.initCompanyFilter();
    this.initOriginFilter();
    this.initDestinationFilter();
    this.initAirlineFilter();
    this.initContractFilter();
  }

  /**
   * Build reactive form
   *
   */
  private buildFormGroup() {
    this.formGroup = this._formBuilder.group({
      company: ["", Validators.required],
      company_branch: ["", Validators.required],
      number: ["", Validators.required],
      description: ["", Validators.required],
      contract: ["", Validators.required],

      // Flight
      flight_id: [""],
      flight_number: ["", Validators.required],
      flight_alternative_number: ["", Validators.required],
      airline: [""],
      origin_airport: ["", Validators.required],
      destination_airport: ["", Validators.required],
      origin_date: [""],
      destination_date: [""],

      // BAGS
      bag_code: [{ value: "", disabled: false }, Validators.required],
      bag_origin: [
        { value: "", disabled: false },
        Validators.required,
      ],
      bag_destination: [
        { value: "", disabled: false },
        Validators.required,
      ],
      bag_date: [{ value: "", disabled: false }, Validators.required],

      // Search
      companyLastFilter: [{ value: "", disabled: false }],
      bagOriginLastFilter: [{ value: "", disabled: false }],
      bagDestinationLastFilter: [{ value: "", disabled: false }],
      airportOriginLastFilter: [{ value: "", disabled: false }],
      airportDestinationLastFilter: [{ value: "", disabled: false }],
      airlineLastFilter: [{ value: "", disabled: false }],
      contractLastFilter: [{ value: '', disabled: false }],
    });
  }

  public initCompanyFilter() {
    const companyLastFilter = this.formGroup.controls.companyLastFilter;

    companyLastFilter.valueChanges.subscribe(() => {
      let search = companyLastFilter.value.toLowerCase();

      this.filteredCompany.next(
        this.companies.filter(
          (company) =>
            company["Nome da Empresa"].toLowerCase().indexOf(search) > 1
        )
      );
    });
  }

  public initOriginFilter() {
    const bagOriginLastFilter = this.formGroup.controls.bagOriginLastFilter;

    bagOriginLastFilter.valueChanges.subscribe(() => {
      let searchBagOrigin = bagOriginLastFilter.value.toLowerCase();

      this.filteredOriginBag.next(
        this.airportsOrigin.filter(
          (airport) =>
            airport["Nome do Aeroporto"]
              .toLowerCase()
              .indexOf(searchBagOrigin) > 1
        )
      );
    });
  }

  public initDestinationFilter() {
    const bagDestinationLastFilter = this.formGroup.controls
      .bagDestinationLastFilter;

    bagDestinationLastFilter.valueChanges.subscribe(() => {
      let searchBagDestination = bagDestinationLastFilter.value.toLowerCase();

      this.filteredDestinationBag.next(
        this.airportsDestination.filter(
          (airport) =>
            airport["Nome do Aeroporto"]
              .toLowerCase()
              .indexOf(searchBagDestination) > 1
        )
      );
    });

    const airportOriginLastFilter = this.formGroup.controls
      .airportOriginLastFilter;

    airportOriginLastFilter.valueChanges.subscribe(() => {
      let searchBagDestination = airportOriginLastFilter.value.toLowerCase();

      this.filteredOriginAirport.next(
        this.airportsOrigin.filter(
          (airport) =>
            airport["Nome do Aeroporto"]
              .toLowerCase()
              .indexOf(searchBagDestination) > 1
        )
      );
    });

    const airportDestinationLastFilter = this.formGroup.controls
      .airportDestinationLastFilter;

    airportDestinationLastFilter.valueChanges.subscribe(() => {
      let searchBagDestination = airportDestinationLastFilter.value.toLowerCase();

      this.filteredDestinationAirport.next(
        this.airportsDestination.filter(
          (airport) =>
            airport["Nome do Aeroporto"]
              .toLowerCase()
              .indexOf(searchBagDestination) > 1
        )
      );
    });
  }

  public initAirlineFilter() {
    const airlineLastFilter = this.formGroup.controls.airlineLastFilter;

    airlineLastFilter.valueChanges.subscribe(() => {
      let searchAirlineDestination = airlineLastFilter.value.toLowerCase();

      this.filteredAirline.next(
        this.airlines.filter(
          (airline) =>
            airline["Companhia Aérea"]
              .toLowerCase()
              .indexOf(searchAirlineDestination) > 1
        )
      );
    });
  }

  public initContractFilter() {
    const contractLastFilter = this.formGroup.controls.contractLastFilter;

    contractLastFilter.valueChanges.subscribe(() => {
      let search = contractLastFilter.value.toLowerCase();

      this.filteredContract.next(
        this.contracts.filter(
          (contract) => contract.description.toLowerCase().indexOf(search) > 1
        )
      );
    });
  }

  /**
   * Fetch data to current form
   *
   * @param url
   */
  private fetchData(url: string) {
    this.http
      .get(url)
      .pipe(map((res: any) => res.data))
      .subscribe((master: MasterAirwaybillRequest) => {
        this.data = master;
        let airline: any;
        if (master.flight) {
          if (master.flight.airline) {
            airline = master.flight.airline;
          }
        }

        let controls = {
          number: master.code,
          description: master.description,
          company_branch: master.company_branch ? master.company_branch.id : "",
          contract: master.contract ? master.contract.id : "",
          company: master.company ? master.company.id : "",
          airline: airline ? airline.id : "",
          flight_number: master.flight ? master.flight.number : "",
          flight_alternative_number: master.flight
            ? master.flight.alternative_number
            : "",
          origin_airport: master.flight ? master.flight.origin.airportId : "",
          destination_airport: master.flight
            ? master.flight.destination.airportId
            : "",
          bag_code: master.bag ? master.bag.code : "",
          bag_origin: master.bag ? master.bag.origin : "",
          bag_destination: master.bag ? master.bag.destination : "",
          bag_date: master.bag ? master.bag.date : "",
        };

        this.formGroup.patchValue(controls);
        this.loading = false;
      });
  }

  /**
   * Parse controls to expected data structure for request
   *
   * @param controls
   */
  public parseControls(controls: any) {

    let newParsed = {
      code: controls.number.value,
      external_identifiers: [controls.number.value],
      company_branch: parseInt(controls.company_branch.value),
      company_id: controls.company.value,
      description: controls.description.value,

      bag_code: {
        code: controls.bag_code.value,
        origin: controls.bag_origin.value,
        destination: controls.bag_destination.value,
        date: controls.bag_date.value.format("YYYY-MM-DD HH:mm:ss"),
      },

      flight: {
        airline_id: controls.airline.value,
        number: controls.flight_number.value,
        alternative_number: controls.flight_alternative_number.value,
        origin: {
          airport_id: controls.origin_airport.value,
          date: controls.bag_date.value.format("YYYY-MM-DD HH:mm:ss"),
        },
        destination: {
          airport_id: controls.destination_airport.value,
          date: controls.bag_date.value.format("YYYY-MM-DD HH:mm:ss"),
        },
      },
    };

    // return parsed;
    return newParsed;
  }

  /**
   * Submit form group (update|create)
   *
   */
  public submit(): void {
    const controls = this.parseControls(this.formGroup.controls);

    if (this._update) {
        return this.update(controls);
    }

    return this.create(controls);
  }
  /**
   *
   * @param controls
   */
  private create(controls: any): void {
    this.http
      .post(this.endpoint, controls)
      .subscribe((res) => this.router.navigate([this.endpoint]));
  }

  /**
   *
   * @param controls
   */
  private update(controls: any): void {
    this.http
      .put(`${this.endpoint}/${this.currentId}`, controls)
      .subscribe((res) => this.router.navigate([this.endpoint]));
  }
}
