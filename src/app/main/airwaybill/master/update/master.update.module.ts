import { MasterUpdateComponent } from "./master.update.component";
import { NgModule } from "@angular/core";
import { FuseSharedModule } from "@fuse/shared.module";
import { DeclarationsModule } from "app/declarations.module";
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule } from "@angular/material";
import { DateTimeModule } from "app/_default/datetime/datetime.default.module";
import { ImporterService } from 'app/_services/importer/importer.service';
import { ShipperService } from 'app/_services/shipper/shipper.service';
import { CompanyService } from 'app/_services/company/company.service';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
    declarations: [
        MasterUpdateComponent,
    ],
  imports: [
      NgxMaskModule.forRoot(options),
      FuseSharedModule,
      DeclarationsModule,
      MatInputModule,
      MatSelectModule,
      DateTimeModule,
      MatFormFieldModule,
      MatStepperModule,
      MatDividerModule,
      MatTooltipModule,
      ReactiveFormsModule,
      MatProgressSpinnerModule,
      NgxMatSelectSearchModule,
    ],
    exports: [
        MasterUpdateComponent,
  ],
  providers: [
    CompanyService,
    ImporterService,
    ShipperService,
  ]
})
export class MasterUpdateModule {

}