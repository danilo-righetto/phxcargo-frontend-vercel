import { MasterShowComponent } from './master.show.component';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { DeclarationsModule } from 'app/declarations.module';
import { HouseCheckpointModule } from '../../house/checkpoint/checkpoint.module';
import {
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatStepperModule,
} from "@angular/material";
import { DateTimeModule } from "app/_default/datetime/datetime.default.module";
import { ImporterService } from "app/_services/importer/importer.service";
import { ShipperService } from "app/_services/shipper/shipper.service";
import { CompanyService } from "app/_services/company/company.service";
import { NgxMaskModule, IConfig } from "ngx-mask";
import { MatDividerModule } from "@angular/material/divider";
import { MatTooltipModule } from "@angular/material/tooltip";
import { ReactiveFormsModule } from "@angular/forms";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { NgxMatSelectSearchModule } from "ngx-mat-select-search";

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [MasterShowComponent],
  imports: [
    FuseSharedModule,
    DeclarationsModule,
    HouseCheckpointModule,
    NgxMaskModule.forRoot(options),
    MatInputModule,
    MatSelectModule,
    DateTimeModule,
    MatFormFieldModule,
    MatStepperModule,
    MatDividerModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    NgxMatSelectSearchModule,
  ],
  exports: [MasterShowComponent],
  providers: [CompanyService, ImporterService, ShipperService],
})
export class MasterShowModule {}
