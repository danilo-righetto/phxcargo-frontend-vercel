
import { map, catchError } from 'rxjs/operators';
import { Component, OnInit } from "@angular/core";
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../_i18n/pt';
import { ActivatedRoute, Router } from "@angular/router";
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { MasterLitingElement } from 'app/_interfaces/airwaybill/master.interface';
import { AirwaybillService } from 'app/_services/airwaybill/house.service';
import { NotificationService } from "app/_services/notification/notification.service";
import { CompanyListingElement as Company } from "app/_interfaces/company/listing-element";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { CompanyService } from "app/_services/company/company.service";
import { AirlineListingElement as Airline } from "app/_interfaces/airline/listing-element";
import { AirlineService } from "app/_services/airline/airline.service";
import { AirportListingElement as Airport } from "app/_interfaces/airport/listing-element";
import { AirportService } from "app/_services/airport/airport.service";
import { MasterAirwaybillRequest } from "app/_interfaces/airwaybill/master.interface";
import { FlightService } from "app/_services/flight/flight.service";
import { FlightListingElement } from "app/_interfaces/flight/listing-element";
import { ReplaySubject } from "rxjs";
import { ContractService } from "app/_services/company/contract.service";
import {
  ContractListingElement,
  ContractRequest,
  Contract,
} from "app/_interfaces/contract/contract.interface";

@Component({
  selector: "airwaybill-master-show",
  templateUrl: "./master.show.component.html",
  styleUrls: ["master.show.component.css"],
})
export class MasterShowComponent {
  public flights: FlightListingElement;
  public data: any;
  public airportsOrigin: any;
  public airportsDestination: any;
  public contracts: any;
  public loadingContract: any;
  public loading: boolean = true;
  public loadingCompany: boolean = true;
  public loadingFlights: boolean = true;
  public loadingAirline: boolean = true;
  public loadingAirport: boolean = true;
  public _update: boolean = false;
  public isCreate: boolean = false;
  public currentId = null;
  public companies: Company[];
  public company_id = null;
  public airlines: Airline[];
  public airports: Airport[];
  public formGroup: FormGroup;
  public companyLastFilter: FormGroup;
  public bagOriginLastFilter: FormGroup;
  public bagDestinationLastFilter: FormGroup;
  public airportOriginLastFilter: FormGroup;
  public airportDestinationLastFilter: FormGroup;
  public airlineLastFilter: FormGroup;
  public contractLastFilter: FormGroup;
  public filteredContract: ReplaySubject<Contract[]> = new ReplaySubject<
    Contract[]
  >(1);
  public filteredCompany: ReplaySubject<Company[]> = new ReplaySubject<
    Company[]
  >(1);
  public filteredOriginBag: ReplaySubject<Airport[]> = new ReplaySubject<
    Airport[]
  >(1);
  public filteredDestinationBag: ReplaySubject<Airport[]> = new ReplaySubject<
    Airport[]
  >(1);
  public filteredOriginAirport: ReplaySubject<Airport[]> = new ReplaySubject<
    Airport[]
  >(1);
  public filteredDestinationAirport: ReplaySubject<
    Airport[]
  > = new ReplaySubject<Airport[]>(1);
  public filteredAirline: ReplaySubject<Airline[]> = new ReplaySubject<
    Airline[]
  >(1);
  private endpoint: string = "/airwaybills/master";

  /**
   * Constructor
   *
   * @param _fuseTranslationLoaderService
   * @param route
   * @param http
   */
  public constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private route: ActivatedRoute,
    private http: HttpAuthenticateService,
    public airwaybillService: AirwaybillService,
    public notification: NotificationService,
    private router: Router,
    private _formBuilder: FormBuilder,
    private companyService: CompanyService,
    private flightService: FlightService,
    private airlineService: AirlineService,
    private airportService: AirportService,
    private contractService: ContractService
  ) {
    this._fuseTranslationLoaderService.loadTranslations(portuguese);
  }

  public ngOnInit(): void {
    this.buildFormGroup();

    this.route.params.subscribe((params) => {
      this.fetchData(`/airwaybills/master/${params.id}`);
      // this.airwaybillService.setQuery(`&master_airwaybill_id=${params.id}`);
    });

    this.companyService.fetchData(null, 300).subscribe((companies) => {
      this.companies = companies.data;
      this.filteredCompany.next(this.companies.slice());
      this.loadingCompany = false;
    });
    this.flightService.fetchData(null, 300).subscribe((flights) => {
      this.flights = flights.data;
      this.loadingFlights = false;
    });
    this.airlineService.fetchData(null, 300).subscribe((airlines) => {
      this.airlines = airlines.data;
      this.filteredAirline.next(this.airlines.slice());
      this.loadingAirline = false;
    });
    this.airportService.fetchData(null, 300).subscribe((airports) => {
      this.airports = airports.data;
      this.airportsOrigin = airports.data;
      this.airportsDestination = airports.data;
      this.filteredOriginBag.next(this.airportsOrigin.slice());
      this.filteredDestinationBag.next(this.airportsDestination.slice());
      this.filteredOriginAirport.next(this.airportsOrigin.slice());
      this.filteredDestinationAirport.next(this.airportsDestination.slice());
      this.loadingAirport = false;
    });
    this.contractService.fetchData("", 400).subscribe((contracts) => {
      this.contracts = contracts.data;
      this.filteredContract.next(this.contracts.slice());
      this.loadingContract = false;
    });

    this.initCompanyFilter();
    this.initOriginFilter();
    this.initDestinationFilter();
    this.initAirlineFilter();
    this.initContractFilter();
  }

  private buildFormGroup() {
    this.formGroup = this._formBuilder.group({
      company: [{ value: "", disabled: true }, Validators.required],
      company_branch: [{ value: "", disabled: true }, Validators.required],
      number: [{ value: "", disabled: true }, Validators.required],
      description: [{ value: "", disabled: true }, Validators.required],
      contract: [{ value: "", disabled: true }, Validators.required],

      // Flight
      flight_id: [{ value: "", disabled: true }],
      flight_number: [{ value: "", disabled: true }, Validators.required],
      flight_alternative_number: [
        { value: "", disabled: true },
        Validators.required,
      ],
      airline: [{ value: "", disabled: true }],
      origin_airport: [{ value: "", disabled: true }, Validators.required],
      destination_airport: [{ value: "", disabled: true }, Validators.required],
      origin_date: [{ value: "", disabled: true }],
      destination_date: [{ value: "", disabled: true }],

      // BAGS
      bag_code: [{ value: "", disabled: true }, Validators.required],
      bag_origin: [{ value: "", disabled: true }, Validators.required],
      bag_destination: [{ value: "", disabled: true }, Validators.required],
      bag_date: [{ value: "", disabled: true }, Validators.required],

      // Search
      companyLastFilter: [{ value: "", disabled: true }],
      bagOriginLastFilter: [{ value: "", disabled: true }],
      bagDestinationLastFilter: [{ value: "", disabled: true }],
      airportOriginLastFilter: [{ value: "", disabled: true }],
      airportDestinationLastFilter: [{ value: "", disabled: true }],
      airlineLastFilter: [{ value: "", disabled: true }],
      contractLastFilter: [{ value: "", disabled: true }],
    });
  }

  /**
   *
   * @param url
   */
  private fetchData(url: string) {
    this.http
      .get(url)
      .pipe(map((res: any) => res.data))
      .subscribe((master) => {
        this.data = master;
        let airline: any;
        if (master.flight) {
          if (master.flight.airline) {
            airline = master.flight.airline;
          }
        }

        let controls = {
          number: master.code,
          description: master.description,
          company_branch: master.company_branch ? master.company_branch.id : "",
          contract: master.contract ? master.contract.id : "",
          company: master.company ? master.company.id : "",
          airline: airline ? airline.id : "",
          flight_number: master.flight ? master.flight.number : "",
          flight_alternative_number: master.flight
            ? master.flight.alternative_number
            : "",
          origin_airport: master.flight ? master.flight.origin.airportId : "",
          destination_airport: master.flight
            ? master.flight.destination.airportId
            : "",
          bag_code: master.bag ? master.bag.code : "",
          bag_origin: master.bag ? master.bag.origin : "",
          bag_destination: master.bag ? master.bag.destination : "",
          bag_date: master.bag ? master.bag.date : "",
        };

        this.formGroup.patchValue(controls);
        this.loading = false;
      });
  }

  public delete(id: number) {
    this.loading = true;

    this.http
      .delete(`/airwaybills/master`, { ids: [id] })
      .pipe(catchError((err) => (this.loading = false || err)))
      .subscribe((res) => {
        this.notification.notify("Registro deletado com sucesso !");
        this.router.navigate(["/airwaybills/master"]);
      });
  }

  public initCompanyFilter() {
    const companyLastFilter = this.formGroup.controls.companyLastFilter;

    companyLastFilter.valueChanges.subscribe(() => {
      let search = companyLastFilter.value.toLowerCase();

      this.filteredCompany.next(
        this.companies.filter(
          (company) =>
            company["Nome da Empresa"].toLowerCase().indexOf(search) > 1
        )
      );
    });
  }

  public initOriginFilter() {
    const bagOriginLastFilter = this.formGroup.controls.bagOriginLastFilter;

    bagOriginLastFilter.valueChanges.subscribe(() => {
      let searchBagOrigin = bagOriginLastFilter.value.toLowerCase();

      this.filteredOriginBag.next(
        this.airportsOrigin.filter(
          (airport) =>
            airport["Nome do Aeroporto"]
              .toLowerCase()
              .indexOf(searchBagOrigin) > 1
        )
      );
    });
  }

  public initDestinationFilter() {
    const bagDestinationLastFilter = this.formGroup.controls
      .bagDestinationLastFilter;

    bagDestinationLastFilter.valueChanges.subscribe(() => {
      let searchBagDestination = bagDestinationLastFilter.value.toLowerCase();

      this.filteredDestinationBag.next(
        this.airportsDestination.filter(
          (airport) =>
            airport["Nome do Aeroporto"]
              .toLowerCase()
              .indexOf(searchBagDestination) > 1
        )
      );
    });

    const airportOriginLastFilter = this.formGroup.controls
      .airportOriginLastFilter;

    airportOriginLastFilter.valueChanges.subscribe(() => {
      let searchBagDestination = airportOriginLastFilter.value.toLowerCase();

      this.filteredOriginAirport.next(
        this.airportsOrigin.filter(
          (airport) =>
            airport["Nome do Aeroporto"]
              .toLowerCase()
              .indexOf(searchBagDestination) > 1
        )
      );
    });

    const airportDestinationLastFilter = this.formGroup.controls
      .airportDestinationLastFilter;

    airportDestinationLastFilter.valueChanges.subscribe(() => {
      let searchBagDestination = airportDestinationLastFilter.value.toLowerCase();

      this.filteredDestinationAirport.next(
        this.airportsDestination.filter(
          (airport) =>
            airport["Nome do Aeroporto"]
              .toLowerCase()
              .indexOf(searchBagDestination) > 1
        )
      );
    });
  }

  public initAirlineFilter() {
    const airlineLastFilter = this.formGroup.controls.airlineLastFilter;

    airlineLastFilter.valueChanges.subscribe(() => {
      let searchAirlineDestination = airlineLastFilter.value.toLowerCase();

      this.filteredAirline.next(
        this.airlines.filter(
          (airline) =>
            airline["Companhia Aérea"]
              .toLowerCase()
              .indexOf(searchAirlineDestination) > 1
        )
      );
    });
  }

  public initContractFilter() {
    const contractLastFilter = this.formGroup.controls.contractLastFilter;

    contractLastFilter.valueChanges.subscribe(() => {
      let search = contractLastFilter.value.toLowerCase();

      this.filteredContract.next(
        this.contracts.filter(
          (contract) => contract.description.toLowerCase().indexOf(search) > 1
        )
      );
    });
  }
}