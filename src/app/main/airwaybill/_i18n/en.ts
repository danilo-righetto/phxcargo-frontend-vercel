export const locale = {
    lang: 'en',
    data: {
        'HOUSE': {
            'HOUSES': 'Houses',
            'HOUSE': 'House',
            'CODE': 'Código',
            'HOUSE_CODE': 'Número House',
            'SHOW': 'Visualizar e Editar',
            'EDIT': 'Editar',
            'CREATE': 'Adicionar',
            'BACK': 'Voltar',
            'SAVE': 'Salvar',
            'PRINTHAWB': 'Imprimir House',
            'PRINTLABEL': 'Imprimir Etiqueta',
            'CONSOLIDATOR': 'Bag',
            'CONSOLIDATORS': 'Bags',
            'NO_CONSOLIDATORS': 'NO INFORMATION FOR BAGS',
            'HOUSE_DESCRIPTION': 'House - Description',
            'COMPANY': 'Company',
            'CUSTOMS_SETTINGS': {
                'SETTINGS': 'Configurações alfandegárias',
                'TAX_CLASS': 'Regime tributário',
                'CUSTOMS_ADM': 'Tratamento administrativo',
                'PAYMENT_TYPE': 'Type Payment',
                'INCOTERM': 'Incoterm'
            },
            'UNIT_TYPE': 'Unit Type (KG)',
            'SERVICE_TYPE': 'Service Type',
            'TYPE_TRANSPORT': 'Type Transport',
            'PACKAGES': 'Packages',
            'CURRENCY': 'Currency',
            'COUNTRY': {
                'COUNTRY': 'País'
            },
            'ITEMS': {
                'ITEMS': 'Itens',
                'ITEMS_TOTAL': 'Itens - Total',
                'ITEMS_DETAIL': 'Itens - Detail',
                'COMERCIAL_DUTIABLE_VALUE': 'Comercial Dutiable Value',
                'COMPLETE_COMMERCIAL_VALUE': 'Complete Comercial Value',
                'TOTAL_WEIGHT': 'Total Weight',
                'TOTAL_VOLUME_WEIGHT': 'Total Volume Weight',
                'QUANTITY': 'Quantidade',
                'HEIGHT': 'Altura (cm)',
                'LENGTH': 'Comprimento (cm)',
                'WIDTH': 'Largura (cm)',
                'WEIGHT': 'Peso (kg)',
                'COMMERCIAL_VALUE': 'Valor comercial (USD)',
                'DESCRIPTION': 'Descrição',
                'NEW_ITEM': 'Adicionar novo item',
                'MODAL': 'Modal',
                'CURRENCY': 'Currency',
                'VOLUME_WEIGHT': 'Volume Weight',
                'UNIT_TYPE': 'Unit Type'
            },
            'LISTING': {
                'ID': 'ID',
                'NUMBER': 'Número',
                'ADD_NUMBER': 'Adicionar número',
                'EXTERNAL_ID': 'Número',
                'WEIGHT': 'Peso (kg)',
                'AMOUNT': 'Valor',
                'AMOUNT_FREIGHT': 'Frete Total',
                'ADDRESS_STREET': 'Endereço',
                'ADDRESS_COUNTRY': 'País',
                'ADDRESS_CITY': 'Cidade',
                'ADDRESS_ZIP_CODE': 'CEP',
                'ADDRESS_STATE': 'Estado',
                'ADDRESS_NEIGHBORHOOD': 'Bairro ',
                'ADDRESS_ADDITIONAL_INFO': 'Complemento',
            },
            'IMPORTER': {
                'IMPORTER': 'Importador',
            },
            'SHIPPER': {
                'SHIPPER': 'Embarcador',
                'USE_COMPANY_INFORMATION': 'Utilizar mesmas informações da empresa selecionada',
            },
            'PERSON': {
                'NAME': 'Nome',
                'DOCUMENT': 'Documento'
            },
            'PHONES': {
                'NUMBER': 'Number'
            },
            'EMAIL': {
                'ADDRESS': 'E-MAIL'
            }
        },
        'MASTER': {
            'ID': 'ID',
            'MASTERS': 'Masters',
            'MASTER': 'Master',
            'MASTER_CODE': 'Número Master',
            'GROSS_WEIGHT': 'Peso total (kg)',
            'IMPORT': {
                'IMPORT': 'Importar',
                'FILE': 'Arquivo',
                'IMPORTATION': 'Importação',
            },
            'LISTING': {
                'NUMBER': 'Código',
                'DESCRIPTION': 'Descrição',
                'DEPARTURE_DATE': 'Data de partida',
                'AIRPORT_ORIGIN': 'Origem',
                'AIRPORT_DESTINATION': 'Destino',
                'AMOUNT_FREIGHT': 'Frete Total',
                'GROSS_WEIGHT': 'Peso total (kg)',
            },
            'FLIGHT': {
                'FLIGHT': 'Voo',
                'ORIGIN': 'Origem',
                'DESTINATION': 'Destino',
                'DEPARTURE': 'Partida',
                'ARRIVAL': 'Chegada',
                'AIRLINE': 'Companhia aérea',
                'DATE': 'Data'
            },
        },
        'ADDRESS': {
            'ADDRESS': 'Endereço',
            'NUMBER': 'Número',
            'ADDITIONAL_INFO': 'Complemento',
            'NEIGHBORHOOD': 'Bairro',
            'ZIP_CODE': 'CEP',
            'CITY': 'Cidade',
            'STATE': 'Estado',
            'COUNTRY_ID': 'País',
        },
        'INFORMATIONS': 'Informações',
        'COMPANY': 'Empresa',
        'MANIFEST': {
            'START_PROCESS': 'Iniciar Processo',
            'MANIFEST': 'Processo Aduaneiro',
            'MANIFESTS': 'Processos Aduaneiros',
            'NUMBER': 'Número processo Aduaneiro',
        },
        'FLIGHT': {
            'SELECT_FLIGHT': 'Selecione um voo',
            'INFORMATIONS': 'Informações do Voo',
            'SHOW': 'Visualizar',
            'CREATE': 'Adicionar',
            'UPDATE': 'Editar',
            'FLIGHT': 'Voo',
            'FLIGHTS': 'Voos',
            'DESCRIPTION': 'Descrição',
            'EDIT': 'Editar',
            'BACK': 'Voltar',
            'SAVE': 'Salvar',
            'LISTING': {
                'ID': 'ID',
                'NUMBER': 'Número',
                'ALTERNATIVE_NUMBER': 'Número alternativo',
                'AIRLINE_NAME': 'Companhia aérea',
                'AIRPORT_NAME': 'Nome do aeroporto',
                'AIRPORT_CODE': 'Código do aeroporto',
                'DATE': 'Data',
                'ORIGIN': 'Origem',
                'DESTINATION': 'Destino',
            },  
        },
        'CONTRACT': {
            'CONTRACT': 'Contract',
            'NOCONTRACT': 'No Contract'
        },
        'ERRORS': {
            'EMAIL': 'Invalid email',
            'NAME': 'Name required',
            'DOCUMENT': 'Mandatory document',
            'MANDATORY': 'Mandatory'
        }
    }
};