import { map } from 'rxjs/operators';
import { Component, Inject, AfterViewInit, ViewChild } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../_i18n/pt';
import { ActivatedRoute } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { HouseListingElement } from 'app/_interfaces/airwaybill/house.airwaybill';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

export interface Item {
    quantity?: number;
    length: string;
    width: string;
    height: string;
    description: string;
    commercial_value: string;
    weight: string;
    currency: any;
    unitType: any;
    volu_weight: string;
}

const ITEM_DATA: Item[] = [];

@Component({
    selector: 'airwaybill-house-show',
    templateUrl: './house.show.component.html',
    styleUrls: ['house.show.component.css']
})
export class HouseShowComponent {
    public loading: boolean = false;
    public houses: any;
    public data: any;
    public itemsSum: any;

    public displayedColumns: string[] = [
        'quantity',
        'length',
        'width',
        'height',
        'description',
        'commercial_value',
        'weight',
        'currency',
        'unitType',
        'volu_weight',
        'modal'
    ];
    itens = new MatTableDataSource(ITEM_DATA);

    /**
     * Constructor
     * 
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    public constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        public dialog: MatDialog,
        public spinner: MatProgressSpinnerModule
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {
        this.loading = true;
        this.route.params.subscribe(params => {
            this.fetchData(`/airwaybills/house/${params.id}`);
        });
    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {
        this.loading = true;
        this.itemsSum = {
            commercial_value: 0,
            weight: 0,
            volu_weight: 0
        };

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(house => {
                this.houses = house.items;
                this.data = house;
                let item: Item[] = this.houses;
                this.itens = new MatTableDataSource(item);
                this.houses.forEach(item => {
                    this.itemsSum.commercial_value += parseFloat(item.commercial_value);
                    this.itemsSum.weight += parseFloat(item.weight);
                    this.itemsSum.volu_weight += parseFloat(item.volu_weight);
                });
                this.loading = false;
            });
        this.loading = false;
    }
}