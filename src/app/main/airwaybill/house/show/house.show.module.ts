import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { HouseShowComponent } from './house.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { HouseCheckpointModule } from '../checkpoint/checkpoint.module';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
    declarations: [
        HouseShowComponent,
    ],
    imports: [
        NgxMaskModule.forRoot(options),
        FuseSharedModule,
        DeclarationsModule,
        HouseCheckpointModule,
        MatDividerModule,
        MatTooltipModule,
        MatTableModule,
        MatProgressSpinnerModule
    ],
    exports: [
        HouseShowComponent,
        HouseCheckpointModule,
    ]
})

export class HouseShowModule {
}
