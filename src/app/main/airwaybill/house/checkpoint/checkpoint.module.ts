import { NgModule } from "@angular/core";
import { CheckpointComponent } from "./checkpoint.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatButtonModule, MatGridListModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { CommonModule } from "@angular/common";
import { DateTimeModule } from "app/_default/datetime/datetime.default.module";
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

@NgModule({
    declarations: [
        CheckpointComponent,
    ],
    imports: [
        CommonModule,
        DeclarationsModule,
        FuseSharedModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatGridListModule,
        DateTimeModule,
        NgxMatSelectSearchModule,
    ],
    exports: [
        CheckpointComponent,
    ],
})
export class HouseCheckpointModule {

}