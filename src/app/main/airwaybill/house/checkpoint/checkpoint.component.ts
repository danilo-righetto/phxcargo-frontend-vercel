import { Component, Input, ViewChild } from "@angular/core";
import { HttpAuthenticateService } from "app/_services/_authentication/http.authenticate.service";
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { locale as portuguese } from '../../_i18n/pt';
import { Moment } from "moment";
import { CheckpointService } from "app/_services/checkpoints/checkpoint.service";
import { MatTableDataSource } from "@angular/material";
import { DefaultTableComponent } from "app/_default/data-list/table.component";
import { ReplaySubject } from "rxjs";
import { NotificationService } from "app/_services/notification/notification.service";

export interface Checkpoint {
    code: string;
    name: string;
}

@Component({
    selector: 'checkpoint',
    templateUrl: 'checkpoint.component.html',
    styleUrls: ['checkpoint.component.css']
})
export class CheckpointComponent {

    /** @var houseId */
    @Input() public houseId: number;

    /** @var masterId */
    @Input() public masterId: number;

    /** @var manifestId */
    @Input() public manifestId: number;

    /** @var checkpoints */
    @Input() public checkpoints: any;

    /** @var tableComponent */
    @ViewChild(DefaultTableComponent, { static: true })
    tableComponent: DefaultTableComponent;

    /** @var FormGroup formCheckpoint */
    public formCheckpoint: FormGroup;

    public loading: boolean = true;
    public all: Array<any>;
    public filteredCheckpoints: ReplaySubject<Checkpoint[]> = new ReplaySubject<Checkpoint[]>(1);

    public displayedColumns: string[] = [
        'station_code',
        'code',
        'date',
        'description',
        'comments',
        'user',
        'created_at',
    ];

    /**
     * Constructor
     * 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private http: HttpAuthenticateService,
        private notification: NotificationService,
        private checkpointService: CheckpointService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit() {

        if (this.checkpoints) {
            this.checkpoints = new MatTableDataSource(this.checkpoints)
        }

        this.formCheckpoint = this._formBuilder.group({
            checkpoint: ['', Validators.required],
            checkpointFilter: [''],
            comments: [''],
            date: ['', Validators.required],
        });

        this.checkpointService.fetchData('', 200).subscribe(all => {
            this.loading = false;
            this.all = all.data;
            this.filteredCheckpoints.next(this.all.slice());
        });

        this.initCheckpointFilter();
    }

    /**
     * Add new checkpoint to airwaybill
     * 
     * @param event 
     */
    public addCheckpoint(event) {

        event.preventDefault();

        if (!this.houseId && !this.masterId && !this.manifestId) {
            this.notification.notify('Invalid argument: Empty identifier', 'error');
            return;
        }
        const controls = this.formCheckpoint.controls;

        if (!controls.checkpoint.value) {
            this.notification.notify('É necessario selecionar um checkpoint');
            return;
        }
        if (!controls.date.value) {
            this.notification.notify('É necessario selecionar uma data');
            return;
        }

        let moment: Moment = controls.date.value;

        let postParams = {
            "checkpoint": {
                'id': controls.checkpoint.value,
                'comments': controls.comments.value,
                'date': moment.format('YYYY-MM-DDTHH:mm')
            }
        }

        let identifier = {};
        if (this.houseId) {
            identifier = { "airwaybill_id": this.houseId };
        }
        if (this.masterId) {
            identifier = { "master_id": this.masterId };
        }
        if (this.manifestId) {
            identifier = { "manifest_id": this.manifestId };
        }

        postParams = { ...postParams, ...identifier }

        this.http.post(`/airwaybills/checkpoints`, postParams)
            .subscribe((res: any) => {
                this.formCheckpoint.reset();
                this.initCheckpointFilter();
                // this.tableComponent.fetchRawSource(res.data.checkpoints);
            })
    }

    public initCheckpointFilter() {
        const checkpointFilter = this.formCheckpoint.controls.checkpointFilter;

        checkpointFilter.valueChanges.subscribe(() => {
            let search = checkpointFilter.value.toLowerCase();

            this.filteredCheckpoints.next(
                this.all.filter(checkpoint => checkpoint.code.toLowerCase().indexOf(search) > -1)
            );
        })
    }
}