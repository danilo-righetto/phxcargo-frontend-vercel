import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { MatTableDataSource } from '@angular/material';
import { AirwaybillService } from 'app/_services/airwaybill/house.service';
import { HouseListingElement } from 'app/_interfaces/airwaybill/house.airwaybill';

@Component({
    selector: 'airwaybill-house',
    templateUrl: './airwaybill.house.component.html',
})
export class AirwaybillHouseComponent {

    public endpoint: string = '/airwaybills/house';

    public displayedColumns: string[] = [
        'Número',
        'ID Externo',
        'Peso',
        'Último Checkpoint',
        'Quantidade',
        'Endereço',
        'Cidade',
        'Código Postal',
        'Estado',
    ];

    public filterColumns = [
        {
            column: 'company_code',
            display_name: 'Empresa',
            type: 'string'
        },
        {
            column: 'code',
            display_name: 'Número House',
            type: 'string'
        },
        {
            column: 'master_airwaybill_code',
            display_name: 'Número Master',
            type: 'string'
        },
        {
            column: 'created_at',
            type: 'date'
        }
    ];

    public dataSource: MatTableDataSource<HouseListingElement>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        public service: AirwaybillService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {
        this.service.setQuery('');
    }
}