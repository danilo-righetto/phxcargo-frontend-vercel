import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { AirwaybillHouseComponent } from './airwaybill.house.component';
import { MatTableModule } from '@angular/material';
import { DeclarationsModule } from 'app/declarations.module';
import { AirwaybillService } from 'app/_services/airwaybill/house.service';

@NgModule({
    declarations: [
        AirwaybillHouseComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        AirwaybillHouseComponent,
    ],
    providers: [
        AirwaybillService,
    ]
})

export class AirwaybillHouseModule {
}
