
import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, FormGroupDirective, NgForm} from '@angular/forms';
import { ImporterService } from 'app/_services/importer/importer.service';
import { ShipperService } from 'app/_services/shipper/shipper.service';
import { Importer } from 'app/_interfaces/importer/importer.interface';
import { ShipperListingElement as Shipper } from 'app/_interfaces/shipper/listing-element';
import { CompanyListingElement as Company } from 'app/_interfaces/company/listing-element';
import { CompanyService } from 'app/_services/company/company.service';
import { AirportService } from 'app/_services/airport/airport.service'
import { Airport } from 'app/_interfaces/airport/listing-element';
import { CountryService } from 'app/_services/country/country.service';
import { HouseListingElement } from 'app/_interfaces/airwaybill/house.airwaybill';
import { TaxClasseService as TaxClassService } from 'app/_services/customs-settings/tax-class.service';
import { CustomsAdministratorsService } from 'app/_services/customs-settings/customs-administrators.service';
import { ConsolidatorService } from 'app/_services/airwaybill/consolidator.service';
import { MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material/dialog';
import { ErrorStateMatcher } from '@angular/material/core';
import { NotificationService } from 'app/_services/notification/notification.service';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ReplaySubject } from 'rxjs';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ContractService } from 'app/_services/company/contract.service';
import { ContractListingElement, ContractRequest, Contract } from 'app/_interfaces/contract/contract.interface';
import { CityService } from 'app/_services/address/city.service';

export interface Item {
    quantity?: number;
    length: string;
    width: string;
    height: string;
    description: string;
    commercial_value: string;
    weight: string;
    currency: any;
    unitType: any;
    volu_weight: string;
}

export interface Country {
    id: number
    name: string
    code: number
    initials: string
}

export interface City {
    id: number
    uf: string
    code: string
    name: string
    ibge_code: string
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

const ITEM_DATA: Item[] = [];

@Component({
    selector: 'airwaybill-house-update',
    templateUrl: './house.update.component.html',
    styleUrls: ['house.update.component.css']
})
export class HouseUpdateComponent implements OnInit {

    public data: HouseListingElement;
    public loading: boolean = true;
    public _update: boolean = false;
    public loadingContract: boolean = true;
    public loadingCity: boolean = true;
    public currentId = null;

    public companies: any;
    public airportsOrigin: any;
    public airportsDestination: any;
    public company_id = null;
    public airport_id = null;
    public loadingAirport: boolean = true;
    public loadingCompany: boolean = true;

    public countries: any;
    public loadingCountry: boolean = true;

    public importers: Importer[];
    public loadingImporter: boolean = true;

    public shippers: Shipper[];
    public companyShipper: boolean = false;
    public currentShipper: any;
    public loadingShipper: boolean = true;

    public taxClasses: Array<{ code: number, description: string }>;
    public loadingTax: boolean = true;
    public customsAdm: Array<{ code: number, name: string }>;
    public loadingCustomsAdm: boolean = true;

    public consolidators: Array<any>;
    public loadingConsolidators: boolean = true;

    public formGroup: FormGroup;
    public companyLastFilter: FormGroup;
    public contractLastFilter: FormGroup;
    public countryShipperFilter: FormGroup;
    public countryImporterFilter: FormGroup;
    public cityShipperFilter: FormGroup;
    public cityImporterFilter: FormGroup;
    public airportOriginLastFilter: FormGroup;
    public airportDestinationLastFilter: FormGroup;
    public bagOriginLastFilter: FormGroup;
    public bagDestinationLastFilter: FormGroup;
    public filteredCompany: ReplaySubject<Company[]> = new ReplaySubject<Company[]>(1);
    public filteredOriginAirport: ReplaySubject<Airport[]> = new ReplaySubject<Airport[]>(1);
    public filteredDestinationAirport: ReplaySubject<Airport[]> = new ReplaySubject<Airport[]>(1);
    public filteredOriginBag:ReplaySubject<Airport[]> = new ReplaySubject<Airport[]>(1);
    public filteredDestinationBag:ReplaySubject<Airport[]> = new ReplaySubject<Airport[]>(1);
    public filteredContract: ReplaySubject<Contract[]> = new ReplaySubject<Contract[]>(1);
    public filteredShipperCountry: ReplaySubject<Country[]> = new ReplaySubject<Country[]>(1);
    public filteredImporterCountry: ReplaySubject<Country[]> = new ReplaySubject<Country[]>(1);
    public filteredImporterCity: ReplaySubject<City[]> = new ReplaySubject<City[]>(1);
    public filteredShipperCity: ReplaySubject<City[]> = new ReplaySubject<City[]>(1);
    public formCompany: FormGroup;

    private endpoint: string = '/airwaybills/house'
    public houses: any;
    public importer: any;
    public itemsSum: any;
    public isCreate: boolean = false;
    public contracts: any;
    public cities: any;
    public arrayListSize: number = 0;
    public items = [];

    matcher = new MyErrorStateMatcher();

    public displayedColumns: string[] = [
        'quantity',
        'length',
        'width',
        'height',
        'description',
        'commercial_value',
        'weight',
        'currency',
        'unitType',
        'volu_weight',
        'modal'
    ];
    itens = new MatTableDataSource(ITEM_DATA);

    public email = new FormControl('', [Validators.required, Validators.email]);

    /**
     * 
     * @param _fuseTranslationLoaderService 
     * @param _formBuilder 
     * @param route 
     * @param http 
     * @param importerService 
     * @param shipperService 
     * @param companyService 
     * @param countryService 
     * @param router 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private importerService: ImporterService,
        private shipperService: ShipperService,
        private companyService: CompanyService,
        private airportService: AirportService,
        private countryService: CountryService,
        private taxClassService: TaxClassService,
        private customsAdmService: CustomsAdministratorsService,
        private consolidatorService: ConsolidatorService,
        private router: Router,
        public dialog: MatDialog,
        private notification: NotificationService,
        public spinner: MatProgressSpinnerModule,
        private contractService: ContractService,
        public cityService: CityService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public verifyFields() {
        let updateValid = false;
        let createValid = false;
        
        if (
            !this.formGroup.get('importer_name').valid ||
            !this.formGroup.get('importer_document_number').valid ||
            !this.formGroup.get('importer_addresses_country_name').valid ||
            !this.formGroup.get('importer_addresses_address').valid ||
            !this.formGroup.get('importer_addresses_number').valid ||
            !this.formGroup.get('importer_addresses_additional_info').valid ||
            !this.formGroup.get('importer_addresses_neighborhood').valid ||
            !this.formGroup.get('importer_addresses_city').valid ||
            !this.formGroup.get('importer_addresses_zip_code').valid ||
            !this.formGroup.get('importer_addresses_state').valid ||
            !this.formGroup.get('importer_phones_number').valid ||
            !this.formGroup.get('importer_email_address').valid ||
            !this.formGroup.get('description').valid ||
            !this.formGroup.get('amount_freight').valid ||
            !this.formGroup.get('gross_weight').valid ||
            !this.formGroup.get('packages').valid ||
            !this.itemsGroup.valid
        ) {
            updateValid = true;
        }

        if (
            !this.formGroup.get('company').valid ||
            !this.formGroup.get('external_id').valid ||
            !this.formGroup.get('origin').valid ||
            !this.formGroup.get('destination').valid ||
            !this.formGroup.get('contract').valid ||
            !this.formGroup.get('shipper_name').valid ||
            !this.formGroup.get('shipper_document_type').valid ||
            !this.formGroup.get('shipper_document_number').valid ||
            !this.formGroup.get('shipper_addresses_country_name').valid ||
            !this.formGroup.get('shipper_addresses_address').valid ||
            !this.formGroup.get('shipper_addresses_number').valid ||
            !this.formGroup.get('shipper_addresses_additional_info').valid ||
            !this.formGroup.get('shipper_addresses_neighborhood').valid ||
            !this.formGroup.get('shipper_addresses_city').valid ||
            !this.formGroup.get('shipper_addresses_zip_code').valid ||
            !this.formGroup.get('shipper_addresses_state').valid ||
            !this.formGroup.get('shipper_phones_number').valid ||
            !this.formGroup.get('shipper_email_address').valid ||
            !this.formGroup.get('bag_code').valid ||
            !this.formGroup.get('bag_origin').valid ||
            !this.formGroup.get('bag_destination').valid ||
            !this.formGroup.get('bag_date').valid
        ) {
            createValid = true;
        }

        if (!this.isCreate) {
            return updateValid;
        } else {
            return updateValid && createValid;
        }
    }

    /**
     * Lifecycle hook that is called after data-bound properties of a directive are
     * initialized.
     */
    public ngOnInit(): void {
        this.isCreate = this.router.url.includes('create');
        this.buildFormGroup();

        this.route.params.subscribe(params => {
            if (params.id) {
                this._update = true;
                this.currentId = params.id;
                this.fetchHouseInformation(`${this.endpoint}/${this.currentId}`);
            } else {
                this.arrayListSize++;
                this.addNewItemsGroup();
            }
        });

        this.companyService.fetchData('', 300).subscribe(companies => {
            this.companies = companies.data;
            this.filteredCompany.next(this.companies.slice());
            this.loadingCompany = false;
        });

        this.airportService.fetchData('', 300).subscribe(airports => {
            this.airportsOrigin = airports.data;
            this.airportsDestination = airports.data;
            this.filteredOriginAirport.next(this.airportsOrigin.slice());
            this.filteredOriginBag.next(this.airportsOrigin.slice());
            this.filteredDestinationAirport.next(this.airportsDestination.slice());
            this.filteredDestinationBag.next(this.airportsDestination.slice());
            this.loadingAirport = false;
        });

        this.contractService.fetchData('', 300).subscribe(contracts => {
            this.contracts = contracts.data;
            this.filteredContract.next(this.contracts.slice());
            this.loadingContract = false;
        });

        this.countryService.fetchData('', 300).subscribe(countries => {
            this.countries = countries.data;
            this.filteredShipperCountry.next(this.countries.slice());
            this.filteredImporterCountry.next(this.countries.slice());
            this.loadingCountry = false;
        });

        this.cityService.fetchData('', 5000).subscribe(cities => {
            this.cities = cities.data;
            this.filteredShipperCity.next(this.cities.slice());
            this.filteredImporterCity.next(this.cities.slice());
            this.loadingCity = false;
        });


        this.loading = false;

        this.initCompanyFilter();
        this.initOriginFilter();
        this.initDestinationFilter();
        this.initContractFilter();
        this.initCountryFilter();
        this.initCityFilter();
    }

    /**
     * Build reactive form
     * 
     */
    private buildFormGroup() {

        this.formGroup = this._formBuilder.group({
            company: [{ value: '', disabled: !this.isCreate }, Validators.required],
            external_id: [{ value: '', disabled: !this.isCreate }, Validators.required],
            origin: [{ value: '', disabled: !this.isCreate }, Validators.required],
            destination: [{ value: '', disabled: !this.isCreate }, Validators.required],
            contract: [{ value: '', disabled: !this.isCreate }, Validators.required],

            // SHIPPER
            shipper_name: [{ value: '', disabled: !this.isCreate }, Validators.required],
            shipper_document_number: [{ value: '', disabled: !this.isCreate }, Validators.required],
            shipper_document_type: [{ value: '', disabled: !this.isCreate }, Validators.required],
            shipper_addresses_country_name: [{ value: '', disabled: !this.isCreate }, Validators.required],
            shipper_addresses_address: [{ value: '', disabled: !this.isCreate }, Validators.required],
            shipper_addresses_number: [{ value: '', disabled: !this.isCreate }, Validators.required],
            shipper_addresses_additional_info: [{ value: '', disabled: !this.isCreate }, Validators.required],
            shipper_addresses_neighborhood: [{ value: '', disabled: !this.isCreate }, Validators.required],
            shipper_addresses_city: [{ value: '', disabled: !this.isCreate }, Validators.required],
            shipper_addresses_zip_code: [{ value: '', disabled: !this.isCreate }, Validators.required],
            shipper_addresses_state: [{ value: '', disabled: !this.isCreate }, Validators.required],
            shipper_phones_number: [{ value: '', disabled: !this.isCreate }, Validators.required],
            shipper_email_address: [{ value: '', disabled: !this.isCreate }, Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")],

            // IMPORTER
            importer_name: ['', Validators.required],
            importer_document_number: ['', Validators.required],
            importer_document_type: ['', Validators.required],
            importer_addresses_country_name: ['', Validators.required],
            importer_addresses_address: ['', Validators.required],
            importer_addresses_number: ['', Validators.required],
            importer_addresses_additional_info: ['', Validators.required],
            importer_addresses_neighborhood: ['', Validators.required],
            importer_addresses_city: ['', Validators.required],
            importer_addresses_zip_code: ['', Validators.required],
            importer_addresses_state: ['', Validators.required],
            importer_phones_number: ['', Validators.required],
            importer_email_address: ['', Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")],

            // HOUSE
            description: ['', Validators.required],
            amount_freight: ['', Validators.required],
            unit_type_name: [{ value: 'QUILOGRAMA LIQUIDO', disabled: true }, Validators.required],
            gross_weight: ['', Validators.required],
            service_type: ['', Validators.required],
            type_transport: ['', Validators.required],
            packages: ['', Validators.required],
            currency: ['', Validators.required],

            // CUSTOMS SETTINGS
            customs_settings_payment_type: ['', Validators.required],
            customs_settings_tax_class_description: ['', Validators.required],
            customs_settings_customs_adm_name: ['', Validators.required],
            customs_settings_currency_code: [{ value: 'BRL', disabled: true }, Validators.required],
            customs_settings_customs_incoterm_name: ['', Validators.required],
            customs_settings_customs_service: ['', Validators.required],
            customs_settings_destination: ['', Validators.required],

            // ITEMS TOTAL
            item_sum_commercial_dutiable_value: [{ value: '', disabled: true }, Validators.required],
            item_sum_commercial_value: [{ value: '', disabled: true }, Validators.required],
            item_sum_weight: [{ value: '', disabled: true }, Validators.required],
            item_sum_volu_weight: [{ value: '', disabled: true }, Validators.required],

            // ITEMS LIST
            items_list: this._formBuilder.array([]),

            // BAGS
            bag_code: [{ value: '', disabled: !this.isCreate }, Validators.required],
            bag_origin: [{ value: '', disabled: !this.isCreate }, Validators.required],
            bag_destination: [{ value: '', disabled: !this.isCreate }, Validators.required],
            bag_date: [{ value: '', disabled: !this.isCreate }, Validators.required],

            doubleDot: [{ value: '', disabled: !this.isCreate }, Validators.required],
            outlying: [{ value: '', disabled: !this.isCreate }, Validators.required],
            datein: [{ value: '', disabled: !this.isCreate }, Validators.required],
            pieces: [{ value: '', disabled: !this.isCreate }, Validators.required],
            wtLbs: [{ value: '', disabled: !this.isCreate }, Validators.required],
            terms: [{ value: '', disabled: !this.isCreate }, Validators.required],
            dispcode: [{ value: '', disabled: !this.isCreate }, Validators.required],
            dispdesc: [{ value: '', disabled: !this.isCreate }, Validators.required],
            dispdate: [{ value: '', disabled: !this.isCreate }, Validators.required],
            remarks: [{ value: '', disabled: !this.isCreate }, Validators.required],
            stattempt1: [{ value: '', disabled: !this.isCreate }, Validators.required],
            stattempt2: [{ value: '', disabled: !this.isCreate }, Validators.required],
            stattempt3: [{ value: '', disabled: !this.isCreate }, Validators.required],
            taxesvalue: [{ value: '', disabled: !this.isCreate }, Validators.required],
            taxduedeadline: [{ value: '', disabled: !this.isCreate }, Validators.required],
            observations: [{ value: '', disabled: !this.isCreate }, Validators.required],
            birthdate: [{ value: '', disabled: !this.isCreate }, Validators.required],
            statusHouse: [{ value: '', disabled: !this.isCreate }, Validators.required],
            finalstatus: [{ value: '', disabled: !this.isCreate }, Validators.required],
            finished: [{ value: '', disabled: !this.isCreate }, Validators.required],
            reminder: [{ value: '', disabled: !this.isCreate }, Validators.required],
            bluecolor: [{ value: '', disabled: !this.isCreate }, Validators.required],
            greycolor: [{ value: '', disabled: !this.isCreate }, Validators.required],

            companyLastFilter: [{ value: '', disabled: false }],
            airportOriginLastFilter: [{ value: '', disabled: false }],
            airportDestinationLastFilter: [{ value: '', disabled: false }],
            contractLastFilter: [{ value: '', disabled: false }],
            countryShipperFilter: [{ value: '', disabled: false }],
            countryImporterFilter: [{ value: '', disabled: false }],
            cityShipperFilter:[{ value: '', disabled: false }],
            cityImporterFilter: [{ value: '', disabled: false }],
            bagOriginLastFilter: [{ value: '', disabled: false }],
            bagDestinationLastFilter: [{ value: '', disabled: false }],
        });
    }

    public initCompanyFilter() {
        const companyLastFilter = this.formGroup.controls.companyLastFilter;

        companyLastFilter.valueChanges.subscribe(() => {
            let search = companyLastFilter.value.toLowerCase();

            this.filteredCompany.next(
                this.companies.filter(company => company['Nome da Empresa'].toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initOriginFilter() {
        const airportOriginLastFilter = this.formGroup.controls.airportOriginLastFilter;

        airportOriginLastFilter.valueChanges.subscribe(() => {
            let search = airportOriginLastFilter.value.toLowerCase();

            this.filteredOriginAirport.next(
                this.airportsOrigin.filter(airport => airport['Nome do Aeroporto'].toLowerCase().indexOf(search) > 1)
            );
        });

        const bagOriginLastFilter = this.formGroup.controls.bagOriginLastFilter;

        bagOriginLastFilter.valueChanges.subscribe(() => {
            let searchBagOrigin = bagOriginLastFilter.value.toLowerCase();

            this.filteredOriginBag.next(
                this.airportsOrigin.filter(airport => airport['Nome do Aeroporto'].toLowerCase().indexOf(searchBagOrigin) > 1)
            );
        });
    }

    public initDestinationFilter() {
        const airportDestinationLastFilter = this.formGroup.controls.airportDestinationLastFilter;

        airportDestinationLastFilter.valueChanges.subscribe(() => {
            let search = airportDestinationLastFilter.value.toLowerCase();

            this.filteredDestinationAirport.next(
                this.airportsDestination.filter(airport => airport['Nome do Aeroporto'].toLowerCase().indexOf(search) > 1)
            );
        });

        const bagDestinationLastFilter = this.formGroup.controls.bagDestinationLastFilter;

        bagDestinationLastFilter.valueChanges.subscribe(() => {
            let searchBagDestination = bagDestinationLastFilter.value.toLowerCase();

            this.filteredDestinationBag.next(
                this.airportsDestination.filter(airport => airport['Nome do Aeroporto'].toLowerCase().indexOf(searchBagDestination) > 1)
            );
        });
    }

    public initContractFilter() {
        const contractLastFilter = this.formGroup.controls.contractLastFilter;

        contractLastFilter.valueChanges.subscribe(() => {
            let search = contractLastFilter.value.toLowerCase();

            this.filteredContract.next(
                this.contracts.filter(contract => contract.description.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initCountryFilter() {
        const countryShipperFilter = this.formGroup.controls.countryShipperFilter;

        countryShipperFilter.valueChanges.subscribe(() => {
            let searchShipper = countryShipperFilter.value.toLowerCase();

            this.filteredShipperCountry.next(
                this.countries.filter(country => country.name.toLowerCase().indexOf(searchShipper) > 1)
            );
        });
        
        const countryImporterFilter = this.formGroup.controls.countryImporterFilter;

        countryImporterFilter.valueChanges.subscribe(() => {
            let searchImporter = countryImporterFilter.value.toLowerCase();

            this.filteredImporterCountry.next(
                this.countries.filter(country => country.name.toLowerCase().indexOf(searchImporter) > 1)
            );
        });
    }

    public initCityFilter() {
        const cityImporterFilter = this.formGroup.controls.cityImporterFilter;

        cityImporterFilter.valueChanges.subscribe(() => {
            let searchImporter = cityImporterFilter.value.toLowerCase();

            this.filteredImporterCity.next(
                this.cities.filter(city => city.name.toLowerCase().indexOf(searchImporter) > 1)
            );
        });

        const cityShipperFilter = this.formGroup.controls.cityShipperFilter;

        cityShipperFilter.valueChanges.subscribe(() => {
            let searchShipper = cityShipperFilter.value.toLowerCase();

            this.filteredShipperCity.next(
                this.cities.filter(city => city.name.toLowerCase().indexOf(searchShipper) > 1)
            );
        });
    }

    /**
     * Return external_ids FormArray
     * @returns FormArray
     */
    get externalIds(): FormArray {
        return <FormArray>this.formGroup.get('external_ids');
    }

    /**
     * Add new extrafield number field to form array
     * @param value 
     */
    public addNewExternalIdField(value: string = '') {
        this.externalIds.push(this.initExternalIdRow(value));
    }
    /**
     * Initiate new form Array control for external ids
     * @param value 
     */
    private initExternalIdRow(value: string = '') {
        return this._formBuilder.group({
            external_id: [value]
        });
    }

    get itemsGroup(): FormArray {
        return <FormArray>this.formGroup.get('items_list');
    }

    /**
     * 
     * @param item 
     */
    public addNewItemsGroup(item = null) {
        this.itemsGroup.push(this.initItemsRow(item));
    }

    /**
     * 
     * @param item 
     */
    private initItemsRow(item: any = null) {

        return this._formBuilder.group({
            item_quantity: [item ? item.quantity : '', Validators.required],
            item_modal_description: [item ? item.modal.description: '', Validators.required],
            item_description: [item ? item.description: '', Validators.required],
            commercial_value: [item ? item.commercial_value: '', Validators.required],
            item_length: [item ? item.length: '', Validators.required],
            item_width: [item ? item.width: '', Validators.required],
            item_height: [item ? item.height: '', Validators.required],
            item_weight: [item ? item.weight: '', Validators.required],
            item_currency: [item ? item.currency.code: '', Validators.required],
            item_volu_weight: [item ? item.volu_weight: '', Validators.required],
            item_unit_type: [{ value: item ? item.unitType.name : 'QLI', disabled: true }, Validators.required],
            item_commercial_value: [item ? item.commercial_value: '', Validators.required],
        });
    }

    /**
     * Fetch data to current form
     * 
     * @param url 
     */
    private fetchHouseInformation(url: string) {
        this.itemsSum = {
            commercial_value: 0,
            weight: 0,
            volu_weight: 0
        };

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(house => {
                this.data = house;
                this.houses = house.items;
                this.houses.forEach(item => {
                    this.itemsSum.commercial_value += parseFloat(item.commercial_value);
                    this.itemsSum.weight += parseFloat(item.weight);
                    this.itemsSum.volu_weight += parseFloat(item.volu_weight);
                });
                this.formGroup.patchValue({
                    company: house.company ? house.company.id : '',
                    origin: house.origin ? house.origin[0].id : !this.isCreate ? 'SEM INFORMAÇÃO' : '',
                    destination: house.destination ? house.destination[0].id : !this.isCreate ? 'SEM INFORMAÇÃO' : '',
                    external_id: house.external_identifiers[0].external_id,

                    // HOUSE
                    number: house.code ? house.code : '',
                    description: house.description ? house.description : '',
                    amount_freight: house.amount_freight ? house.amount_freight : '',
                    gross_weight: house.gross_weight ? house.gross_weight : '',
                    unit_type_name: house.unit_type ? house.unit_type.name : 'QUILOGRAMA LIQUIDO',
                    service_type: house.service_type ? house.service_type : '',
                    type_transport: house.type_transport ? house.type_transport : '',
                    packages: house.packages ? house.packages : '',
                    currency: house.currency ? house.currency.code : '',

                    // SHIPPER
                    shipper_name: house.shipper.name,
                    shipper_document_number: house.shipper.documents ? house.shipper.documents[0].number : '',
                    shipper_document_type: house.shipper.documents ? house.shipper.documents[0].type : '',

                    // SHIPPER - ADDRESS
                    shipper_addresses_address: house.shipper.addresses ? house.shipper.addresses[0].address : '',
                    shipper_addresses_number: house.shipper.addresses ? house.shipper.addresses[0].number : '',
                    shipper_addresses_additional_info: house.shipper.addresses ? house.shipper.addresses[0].additional_info : '',
                    shipper_addresses_neighborhood: house.shipper.addresses ? house.shipper.addresses[0].neighborhood : '',
                    shipper_addresses_zip_code: house.shipper.addresses ? house.shipper.addresses[0].zip_code : '',
                    shipper_addresses_city: house.shipper.addresses ? house.shipper.addresses[0].cities_id : '',
                    shipper_addresses_state: house.shipper.addresses ? house.shipper.addresses[0].state : '',
                    shipper_addresses_country_name: house.shipper.addresses ? house.shipper.addresses[0].country.id : '',
                    shipper_phones_number: house.shipper.phones ? house.shipper.phones[0].number : '',
                    shipper_email_address: house.shipper.email ? house.shipper.email : '',

                    // importer
                    importer_name: house.importer.name,
                    importer_document_number: house.importer.documents ? house.importer.documents[0].number : '',
                    importer_document_type: house.importer.documents ? house.importer.documents[0].type : '',

                    // importer - ADDRESS
                    importer_addresses_address: house.importer.addresses ? house.importer.addresses[0].address : '',
                    importer_addresses_number: house.importer.addresses ? house.importer.addresses[0].number : '',
                    importer_addresses_additional_info: house.importer.addresses ? house.importer.addresses[0].additional_info : '',
                    importer_addresses_neighborhood: house.importer.addresses ? house.importer.addresses[0].neighborhood : '',
                    importer_addresses_zip_code: house.importer.addresses ? house.importer.addresses[0].zip_code : '',
                    importer_addresses_city: house.importer.addresses ? house.importer.addresses[0].cities_id : '',
                    importer_addresses_state: house.importer.addresses ? house.importer.addresses[0].state : '',
                    importer_addresses_country_name: house.importer.addresses ? house.importer.addresses[0].country.id : '',
                    importer_phones_number: house.importer.phones ? house.importer.phones[0].number : '',
                    importer_email_address: house.importer.email ? house.importer.email : '',

                    // CUSTOMS SETTINGS
                    customs_settings_payment_type: house.customs_settings ? house.customs_settings.payment_type : '',
                    customs_settings_tax_class_description: house.customs_settings ? house.customs_settings.tax_class.description : '',
                    customs_settings_customs_adm_name: house.customs_settings ? house.customs_settings.customs_adm ? house.customs_settings.customs_adm.name : '' : '',
                    customs_settings_currency_code: house.customs_settings ? house.customs_settings.currency.code : 'USD',
                    customs_settings_customs_incoterm_name: house.customs_settings ? house.customs_settings.customs_incoterm.name : '',
                    customs_settings_customs_service: house.customs_settings ? house.customs_settings.customs_service : '',
                    customs_settings_destination: house.customs_settings ? house.customs_settings.destination : '',

                    // ITEMS
                    item_sum_commercial_dutiable_value: '0.00',
                    item_sum_commercial_value: this.itemsSum.commercial_value,
                    item_sum_weight: this.itemsSum.weight,
                    item_sum_volu_weight: this.itemsSum.volu_weight,

                    // BAGS
                    bag_code: house.bag ? house.bag.code : '',
                    bag_origin: house.bag ? house.bag.origin : '',
                    bag_destination: house.bag ? house.bag.destination : '',
                    bag_date: house.bag ? new Date(house.bag.date) : '',
                });

                if (house.items) {
                    this.items = house.items;
                    this.arrayListSize = house.items.length - 1;
                    house.items.forEach(item => {
                        this.addNewItemsGroup(item);
                    });
                } else {
                    this.arrayListSize++;
                    this.addNewItemsGroup();
                }

                this.loading = false;
            });
        this.loading = false;
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {

        let bag_date = new Date(controls.bag_date.value);
        let date_to_formated = '';
        date_to_formated = bag_date.getFullYear() + "-" + ("0" + (bag_date.getMonth() + 1)).slice(-2) + "-" + ("0" + (bag_date.getDate())).slice(-2);

        const parsedCreate = {
            "company_id": controls.company.value,
            "external_identifiers": [
                controls.external_id.value
            ],
            "origin_id": controls.origin.value,
            "destination_id": controls.destination.value,
            "contract": controls.contract.value,
            "shipper": {
                "name": controls.shipper_name.value,
                "addresses": [
                    {
                        "number": controls.shipper_addresses_number.value,
                        "additional_info": controls.shipper_addresses_additional_info.value,
                        "country_id": controls.shipper_addresses_country_name.value,
                        "zip_code": controls.shipper_addresses_zip_code.value,
                        "address": controls.shipper_addresses_address.value,
                        "cities_id": controls.shipper_addresses_city.value,
                        "city.uf": controls.shipper_addresses_state.value,
                        "city.name": controls.shipper_addresses_city.value,
                        "neighborhood": controls.shipper_addresses_neighborhood.value,
                    }
                ],
                "documents": [
                    {
                        "type": controls.shipper_document_type.value,
                        "number": controls.shipper_document_number.value,
                    }
                ],
                "phones": [
                    {
                        "type": "phone",
                        "number": controls.shipper_phones_number.value
                    }
                ],
                "emails": [
                    {'address': controls.shipper_email_address.value}
                ]
            },
            "bag_code": {
                "code": controls.bag_code.value,
                "origin": controls.bag_origin.value,
                "destination": controls.bag_destination.value,
                "date": date_to_formated,
            }
        }

        const parsed = {
            "description": controls.description.value,
            "amount_freight": controls.amount_freight.value,
            "gross_weight": controls.gross_weight.value,
            "service_type": controls.service_type.value,
            'unit_type': 'QLI',
            "type_transport": controls.type_transport.value,
            "packages": controls.packages.value,
            "currency": controls.currency.value,
            "customs_settings_payment_type": controls.customs_settings_payment_type.value,
            "tax_class_code_name": controls.customs_settings_tax_class_description.value,
            "customs_settings_destination": controls.customs_settings_destination.value,
            "customs_adm_name": controls.customs_settings_customs_adm_name.value,
            "customs_settings_currency": "BRL",
            "customs_incoterm_name": controls.customs_settings_customs_incoterm_name.value,
            "customs_service": controls.customs_settings_customs_service.value,
            "importer": {
                "name": controls.importer_name.value,
                "addresses": [
                    {
                        "number": controls.importer_addresses_number.value,
                        "additional_info": controls.importer_addresses_additional_info.value,
                        "country_id": controls.importer_addresses_country_name.value,
                        "zip_code": controls.importer_addresses_zip_code.value,
                        "address": controls.importer_addresses_address.value,
                        "cities_id": controls.importer_addresses_city.value,
                        // "city": {
                        //     "name": controls.importer_addresses_city.value,
                        //     "uf": controls.importer_addresses_state.value
                        // },
                        "neighborhood": controls.importer_addresses_neighborhood.value,
                    }
                ],
                "documents": [
                    {
                        "type": controls.importer_document_type.value,
                        "number": controls.importer_document_number.value,
                    }
                ],
                "phones": [
                    {
                        "type": "phone",
                        "number": controls.importer_phones_number.value
                    }
                ],
                "emails": [
                    {'address': controls.importer_email_address.value}
                ],
            },
            "items": this.itemsGroup.controls.map((control: FormGroup) => {
                return {
                    "description": control.controls.item_description.value,
                    "commercial_value": control.controls.item_commercial_value.value,
                    "quantity": control.controls.item_quantity.value,
                    "height": control.controls.item_height.value,
                    "length": control.controls.item_length.value,
                    "width": control.controls.item_width.value,
                    "weight": control.controls.item_weight.value,
                    "modal_id": control.controls.item_modal_description.value,
                    "currency": control.controls.item_currency.value,
                    "volu_weight": control.controls.item_volu_weight.value,
                    "unit_type": control.controls.item_unit_type.value,
                }
            }),
        }

        if (this.isCreate) {
            return { ...parsed, ...parsedCreate }
        }
        return parsed;
    }

    /**
     * Submit form group (update|create)
     * 
     */
    public submit(): void {
        const controls = this.parseControls(this.formGroup.controls);
        console.log('controls');
        console.log(controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {

        this.http.post(this.endpoint, controls)
            .subscribe(res => {
                this.router.navigate([this.endpoint]);
            })
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => {
                this.loading = false;
                this.notification.notify('Dados atualizados com sucesso!');
                this.router.navigate([this.endpoint]);
            })
    }

    /**
     * 
     * @param obj 
     */
    public changeShipper(selected: any) {

        this.shipperService
            .fetchData(`id=${selected.value}`).pipe(map(shipper => shipper.data[0]))
            .subscribe(shipper => this.fetchShipperInformation(shipper));
    }

    /**
     * 
     * @param shipper 
     */
    public fetchShipperInformation(shipper: any) {

        this.formGroup.patchValue({
            shipper_document: shipper.document,
            shipper_name: shipper.name,

            // ADDRESS
            shipper_address: shipper.address.address,
            shipper_number: shipper.address.number,
            shipper_additional_info: shipper.address.additional_info,
            shipper_neighborhood: shipper.address.neighborhood,
            shipper_zip_code: shipper.address.zip_code,
            shipper_city: shipper.address.city,
            shipper_state: shipper.address.state,
            shipper_country_id: shipper.address.country_id,
        });
    }

    /**
     * 
     * @param obj 
     */
    public changeImporter(selected: any) {

        this.importerService
            .fetchData(`id=${selected.value}`).pipe(map(importer => importer.data[0]))
            .subscribe(importer => this.fetchImporterInformation(importer));
    }

    /**
     * 
     * @param importer 
     */
    public fetchImporterInformation(importer: any) {

        this.formGroup.patchValue({
            importer_document: importer.document,
            importer_name: importer.name,

            // ADDRESS
            importer_address: importer.address.address,
            importer_number: importer.address.number,
            importer_additional_info: importer.address.additional_info,
            importer_neighborhood: importer.address.neighborhood,
            importer_zip_code: importer.address.zip_code,
            importer_city: importer.address.city,
            importer_state: importer.address.state,
            importer_country_id: importer.address.country_id,
        });
    }

    /**
     * 
     * @param selection 
     */
    public useCompanyInformation() {
        const companyShipper = this.formGroup.controls.company_shipper.value;

        if (!companyShipper) {
            this.formGroup.controls.shipper.enable();
        }
        if (companyShipper) {
            this.formGroup.controls.shipper.disable();

            const companyId = this.formGroup.controls.company.value;
            this.companyService.fetchData(`id=${companyId}`).pipe(
                map(company => company.data[0]))
                .subscribe(company => {

                    this.fetchShipperInformation({
                        document: company['document'],
                        name: company['name'],
                        address: company['addresses'][0],
                    })
                })
        }
    }

    public ufList: Array<any> = [
        'AC',
        'AL',
        'AM',
        'AP',
        'BA',
        'CE',
        'DF',
        'ES',
        'GO',
        'MA',
        'MG',
        'MS',
        'MT',
        'PA',
        'PB',
        'PE',
        'PI',
        'PR',
        'RJ',
        'RN',
        'RO',
        'RR',
        'RS',
        'SC',
        'SE',
        'SP',
        'TO',
        'EX'
    ];

    public addNewItem(item = null) {
        this.arrayListSize++;
        this.notification.notify('Item adicionado com sucesso !');
        this.addNewItemsGroup(item);
    }

    public removeItem(item) {
        this.arrayListSize > 0 ? this.arrayListSize-- : 0;
        this.notification.notify('Item excluído com sucesso ! ', 'error');
        // this.formGroup.controls.splice(this.items.indexOf(item), 1);
        this.itemsGroup.controls.splice(this.items.indexOf(item), 1);

        // var listOfServices = this.itemsGroup.value;
        // let lenghtArray = listOfServices.length - 1;
        // listOfServices.splice(lenghtArray, 1);
        // this.itemsGroup.controls.splice(this.items.indexOf(item), 1);
        // this.itemsGroup.updateValueAndValidity();
        // if (listOfServices.length == 0) {
        //     this.addNewItemsGroup();
        // }
    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {
        this.itemsSum = {
            commercial_value: 0,
            weight: 0,
            volu_weight: 0
        };

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(house => {
                this.houses = house.items;
                this.data = house;
                this.loading = false;
                let item: Item[] = this.houses;
                this.itens = new MatTableDataSource(item);
                this.houses.forEach(item => {
                    this.itemsSum.commercial_value += parseFloat(item.commercial_value);
                    this.itemsSum.weight += parseFloat(item.weight);
                    this.itemsSum.volu_weight += parseFloat(item.volu_weight);
                });
            });
    }
}