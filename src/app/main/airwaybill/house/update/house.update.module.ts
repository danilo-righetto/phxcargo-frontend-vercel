import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { HouseUpdateComponent } from './house.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { ImporterService } from 'app/_services/importer/importer.service';
import { ShipperService } from 'app/_services/shipper/shipper.service';
import { CompanyService } from 'app/_services/company/company.service';
import { HouseCheckpointModule } from '../checkpoint/checkpoint.module';
import { TaxClasseService } from 'app/_services/customs-settings/tax-class.service';
import { CustomsAdministratorsService } from 'app/_services/customs-settings/customs-administrators.service';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { ConsolidatorService } from 'app/_services/airwaybill/consolidator.service';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { DateTimeModule } from 'app/_default/datetime/datetime.default.module';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
    declarations: [
        HouseUpdateComponent
    ],
    imports: [
        NgxMaskModule.forRoot(options),
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        HouseCheckpointModule,
        MatDividerModule,
        MatTooltipModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule,
        NgxMatSelectSearchModule,
        DateTimeModule,
    ],
    exports: [
        HouseUpdateComponent,
        HouseCheckpointModule,
    ],
    providers: [
        ImporterService,
        ShipperService,
        CompanyService,
        TaxClasseService,
        CustomsAdministratorsService,
        ConsolidatorService,
    ]
})

export class HouseUpdateModule {
}
