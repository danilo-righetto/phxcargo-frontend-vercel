import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { DashobardComponent } from './dashboard.component';
import { MatOptionModule, MatIconModule,  } from '@angular/material';
import { DashboardWidgetModule } from './widgets/dashboard.widget.module';

@NgModule({
    declarations: [
        DashobardComponent
    ],
    imports: [
        TranslateModule,
        FuseSharedModule,
        MatOptionModule,
        MatIconModule,
        DashboardWidgetModule,
    ],
    exports: [
        DashobardComponent
    ]
})

export class DashboardModule {
}
