import { Component, OnInit } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { AirwaybillService } from 'app/_services/airwaybill/house.service';
import { AuthenticationService } from 'app/_services/_authentication/authentication.service';

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['dashboard.component.css']
})
export class DashobardComponent implements OnInit {
    
    public widgets: Array<{ description: string, service: any }>;
    public user: any;
    
    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private houseService: AirwaybillService,
        public authenticationService: AuthenticationService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(english, portuguese);
    }

    public ngOnInit(): void {
        this.user = this.authenticationService.getUser();
        
        this.widgets = [
            {
                description: "Remessas desembaraçadas",
                service: this.houseService.setQuery('&checkpoint_code=clr')
            },
        ];
    }
}
