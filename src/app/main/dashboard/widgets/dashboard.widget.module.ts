import { NgModule } from "@angular/core";
import { DashboardWidgetComponent } from "./dashboard.widget.component";
import { FuseWidgetModule } from "@fuse/components";
import { DeclarationsModule } from "app/declarations.module";

@NgModule({
    declarations: [
        DashboardWidgetComponent,
    ],
    imports:[
        DeclarationsModule,
        FuseWidgetModule,
    ],
    exports: [
        DashboardWidgetComponent,
    ]
})
export class DashboardWidgetModule {
}