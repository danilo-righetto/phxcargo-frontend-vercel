import { Component, Input, OnInit } from "@angular/core";
import { IndexService } from "app/_services/index.service";

@Component({
    selector: 'dashboard-widget',
    styleUrls: ['./dashboard.widget.component.css'],
    templateUrl: './dashboard.widget.component.html'
})
export class DashboardWidgetComponent implements OnInit {

    @Input() description: string;
    @Input() service: IndexService;

    public count: number; 
    public endpoint: string;

    public ngOnInit(): void {

        this.endpoint = this.service.getEndpoint();

        this.service.fetchData().subscribe(res => {
            this.count = res.meta['Meta-Total-Count'];
        });
    }
}