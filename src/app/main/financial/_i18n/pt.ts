export const locale = {
    lang: 'pt',
    data: {
        'FINANCIAL': {
            'TITLE': 'Financeiro',
            'BADGE': '15',
            'INVOICES': {
                'TITLE': 'Faturas',
                'BACK': 'Voltar',
                'EDIT': 'Editar',
                'SAVE': 'Salvar',
                'CREATE': 'Adicionar',
                'INFORMATIONS': 'Informações',
                'EXTERNAL_CODE': 'Código Externo',
                'ARDUOUS_PROCESSES': 'Processos aduaneiros',
                'COMPANY': 'Empresa',
                'HOUSE': 'Número House',
                'MASTER': 'Número Master',
                'MASTERS': 'Masters',
                'DATE': 'Data',
                'ADD_COLLETION': 'Adicionar cobranças pendentes',
                'REMOVE_COLLECTION': 'Remover cobranças pendentes',
            },
            'DEBIT_NOTE': {
                'TITLE': 'Notas de Débito',
                'ID': 'ID',
                'CODE': 'Codigo',
                'COMPANY': 'Empresa',
                'TOTAL': 'Total',
                'DIGITABLE_LINE': 'Linha digitável',
                'PAID_AT': 'Pago em',
                'BACK': 'Voltar',
                'EDIT': 'Editar',
                'INFORMATIONS': 'Informações',
                'SAVE': 'Salvar',
                'CREATE': 'Adicionar',
            },
            'FINES': {
                'TITLE': 'Multas e Encargos',
                'BACK': 'Voltar',
                'SAVE': 'Salvar',
                'CREATE': 'Adicionar',
                'EDIT': 'Editar',
                'INFORMATIONS': 'Informações',
            }
        },
    }
}