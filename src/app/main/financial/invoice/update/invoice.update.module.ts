import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { InvoiceService } from 'app/_services/financial/invoice.service';
import { DeclarationsModule } from 'app/declarations.module';
import { MatTableModule } from "@angular/material";
import { MatFormFieldModule } from "@angular/material";
//import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { OwlDateTimeModule, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE, OwlDateTimeIntl } from 'ng-pick-datetime';
import { OwlMomentDateTimeModule } from 'ng-pick-datetime-moment';
import { InvoiceUpdateComponent } from './invoice.update.component';
//import { InvoiceModule } from '../invoice.module';

/**
 * Define datetime picker lables
 * 
 */
export class DefaultIntl extends OwlDateTimeIntl {
    setBtnLabel = 'Selecionar';
    cancelBtnLabel = 'Cancelar';
};

export const MY_MOMENT_FORMATS = {
    parseInput: 'l LT',
    fullPickerInput: 'DD/MM/YYYY HH:mm',
    datePickerInput: 'l',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
};

@NgModule({
    declarations: [
        InvoiceUpdateComponent,
        //InvoiceModule,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
        MatFormFieldModule,
        HttpClientModule,
        OwlDateTimeModule,
        OwlMomentDateTimeModule,
    ],
    exports: [
        InvoiceUpdateComponent,
    ],
    providers: [
        InvoiceService,
        { provide: OwlDateTimeIntl, useClass: DefaultIntl },
        { provide: OWL_DATE_TIME_LOCALE, useValue: 'pt-BR' },
        { provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS },
    ]
})

export class InvoiceUpdateModule {
}
