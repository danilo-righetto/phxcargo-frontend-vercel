import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../_i18n/pt';
import { MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { InvoiceListingElement } from 'app/_interfaces/financial/invoice.interface';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InvoiceService } from 'app/_services/financial/invoice.service';
import { MasterLitingElement } from 'app/_interfaces/airwaybill/master.interface';
import { MasterAirwaybillService } from 'app/_services/airwaybill/master.service';
import { DefaultTableComponent } from 'app/_default/data-list/table.component';
//import { ManifestRequest, Manifest } from 'app/_interfaces/airwaybill/manifest';
import { ManifestEventsService } from 'app/_services/manifest/manifest.events.service';
import { ManifestFinesService } from 'app/_services/manifest/manifest.fines.service';
import { TaxPaymentOrderService } from 'app/_services/payment/tax-payment-order.service';
import { ManifestGnreService } from 'app/_services/manifest/manifest.gnre.service';
import { ManifestService } from 'app/_services/manifest/manifest.service';

@Component({
    selector: 'app-invoice-update',
    templateUrl: './invoice.update.component.html',
})
export class InvoiceUpdateComponent implements OnInit {

    public data: InvoiceListingElement;
    public loading: boolean = true;
    public _update: boolean = false;
    public currentId = null;
    public items = [];
    public code: any;
    showMe: boolean = false;
    public doNotShowMe: boolean = false;

    public formGroup: FormGroup;

    public endpointMasters: string = '/airwaybills/master';

    public endpointManifests: string = '/manifests'; 

    public endpoint: string = '/financial/invoices';

    public table: DefaultTableComponent;

    public displayedColumnsManifest: string[] = [
        'Número',
        'ID Externo',
        'Descrição',
    ];

    public displayedColumns: string[] = [
        'ID Externo',
        'Data de Saída',
        'Origem do Aeroporto',
        'Destino do Aeroporto',
    ];

    public filterColumns = [
        {
            column: 'company_code',
            display_name: 'Empresa',
            type: 'string'
        },
        {
            column: 'air_waybill_code',
            display_name: 'Número House',
            type: 'string'
        },
        {
            column: 'code',
            display_name: 'Número Master',
            type: 'string'
        },
        {
            column: 'created_at',
            type: 'date'
        }
    ];

    public filterColumnsManifest = [
        {
            column: 'company_code',
            display_name: 'Empresa',
            type: 'string'
        },
        {
            column: 'air_waybill_code',
            display_name: 'Número House',
            type: 'string'
        },
        {
            column: 'master_airwaybill_code',
            display_name: 'Número Master',
            type: 'string'
        },
        {
            column: 'created_at',
            type: 'date'
        }
    ];

    addCode() {
        const code = this.formGroup.controls.code.value;
        this.items.push(code);
    }

    removeCode(item) {
        this.items.splice(this.items.indexOf(item), 1);
    }

    onClickShowMe() {
        this.showMe = !this.showMe;
    }

    onClickDoNotShowMe() {
        this.doNotShowMe != this.doNotShowMe;
        this.showMe = !this.showMe;
    }



    public dataSource: MatTableDataSource<MasterLitingElement>;

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService
     * @param _formBuilder 
     * @param route 
     * @param http
     * @param invoiceService
     * @param router 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        public service: MasterAirwaybillService,
        private invoiceService: InvoiceService,
        public manifest: ManifestService,
        public eventService: ManifestEventsService,
        public fineService: ManifestFinesService,
        public gnreService: ManifestGnreService,
        public paymentOrderService: TaxPaymentOrderService,
        private router: Router
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    /**
     * Lifecycle hook that is called after data-bound properties of a directive are
     * initialized.
     */
    public ngOnInit(): void {

        this.buildFormGroup();

        this.route.params.subscribe(params => {

            if (params.ID) {

                this._update = true;
                this.currentId = params.ID;
                this.fetchData(`${this.endpoint}/${this.currentId}`);
            }
        });

    }

    /**
     * Build reactive form
     *
     */
    private buildFormGroup() {
        this.formGroup = this._formBuilder.group({
            code: ['', Validators.required],
            description: ['', Validators.required],
            paid: ['', Validators.required],  
            master: [''],
            external_id: [''],
            date: [''],
        });
    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(invoice => {
                this.data = invoice;
                this.formGroup.patchValue({
                    code: invoice.code,
                    desctiption: invoice.description,
                    paid: invoice.paid,
                });
                this.loading = false;
            });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {

        const controls = this.parseControls(this.formGroup.controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {
        const parsed = {
            "code": controls.code.value,
            "description": controls.description.value,
            "paid": controls.paid.value,
        }

        return parsed;
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]) && this.router.navigate([this.endpointManifests]) && this.router.navigate([this.endpointMasters]))
    } 
}
