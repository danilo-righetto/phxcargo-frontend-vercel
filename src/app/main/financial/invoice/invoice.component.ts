import { Component, OnInit } from '@angular/core';
import { InvoiceService } from 'app/_services/financial/invoice.service';
import { locale as portuguese } from '../_i18n/pt';
import { MatTableDataSource } from '@angular/material';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

@Component({
	selector: 'app-invoice',
	templateUrl: './invoice.component.html'
})
export class InvoiceComponent implements OnInit {

	public endpoint: string = '/financial/invoices';

	public displayedColumns: Array<string> = [
		'Código',
		'ID Externo',
		'Valor Total',
		'Pagamento',
		'Data de Criação',
	];

	public filterColumns = [
        {
            column: 'company_code',
            display_name: 'Empresa',
            type: 'string'
        },
        {
            column: 'created_at',
            type: 'date'
        }
    ];

	/**
	 * 
	 * @param service 
	 */
	constructor(
		public service: InvoiceService,
		private _fuseTranslationLoaderService: FuseTranslationLoaderService
	) { 
		this._fuseTranslationLoaderService.loadTranslations(portuguese);
	}

	public ngOnInit(): void {
		this.service.setQuery('');
	}

}
