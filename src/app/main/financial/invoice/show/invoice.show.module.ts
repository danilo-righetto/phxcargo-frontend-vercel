import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { InvoiceShowComponent } from './invoice.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatTableModule } from '@angular/material';

@NgModule({
    declarations: [
        InvoiceShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatTableModule,
    ],
    exports: [
        InvoiceShowComponent,
    ]
})

export class InvoiceShowModule {
}
