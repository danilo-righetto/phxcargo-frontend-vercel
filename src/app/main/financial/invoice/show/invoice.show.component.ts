
import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../_i18n/pt';
import { ActivatedRoute } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { InvoiceListingElement, InvoiceRequest, Invoice } from 'app/_interfaces/financial/invoice.interface';

@Component({
    selector: 'invoice-show',
    templateUrl: './invoice.show.component.html',
})
export class InvoiceShowComponent {

    private data: InvoiceListingElement;
    public loading: boolean = true;

    /**
     * Constructor
     * 
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    public constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.route.params.subscribe(params => {
            this.fetchData(`/financial/invoices/${params.id}`);
        })

    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe((invoice: InvoiceRequest) => {
                this.data = new Invoice(invoice);
                this.loading = false;
            });
    }
}
