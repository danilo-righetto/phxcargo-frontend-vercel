
//import { InvoiceUpdateComponent } from './update/invoice.update.component';
import { NgModule } from "@angular/core";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { InvoiceComponent } from './invoice.component';
import { InvoiceService } from "app/_services/financial/invoice.service";
import { MatInputModule, MatSelectModule } from "@angular/material";
import { DateTimeModule } from "app/_default/datetime/datetime.default.module";
import { OwlDateTimeIntl, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE, OwlDateTimeModule } from 'ng-pick-datetime';
import { OwlMomentDateTimeModule } from "ng-pick-datetime-moment";


/**
 * Define datetime invoices
 * 
 */
export class DefaultIntl extends OwlDateTimeIntl {
    setBtnLabel = 'Selecionar';
    cancelBtnLabel = 'Cancelar';
};

export const MY_MOMENT_FORMATS = {
    parseInput: '1 LT',
    fullPickerInput: 'DD/MM/YYYY HH:mm',
    datePickerInput: '1',
    timePickerInput: 'LT',
    monthYearLabel: 'LL',
    dateA11yLabel: 'MM YYYY',
    monthYearA11yLabel: 'MMMM YYYYY',
};

@NgModule({
    declarations: [
        InvoiceComponent,
        //InvoiceUpdateComponent,  
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
        MatInputModule,
        MatSelectModule,
        DateTimeModule,
        OwlDateTimeModule,
        OwlMomentDateTimeModule,
    ],
    exports: [
        InvoiceComponent,
    ],
    providers: [ 
        InvoiceService,
        { provide: OwlDateTimeIntl, useClass: DefaultIntl },
        { provide: OWL_DATE_TIME_LOCALE, useValue: 'pt-BR' },
        { provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS },
    ]
})
export class InvoiceModule {

}