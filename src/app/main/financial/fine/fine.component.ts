import { Component, OnInit } from '@angular/core';
import { FuseTranslationLoaderService } from '../../../../@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../airwaybill/_i18n/pt';
import { FineService } from '../../../_services/financial/FineService';

@Component({
    selector: 'app-fine',
    templateUrl: './fine.component.html'
})
export class FineComponent implements OnInit {

    public endpoint: string = '/financial/fines';

    public displayedColumns: string[] = [
        'Código',
        'Descrição da Multa',
        'Código da Multa',
        'Quantidade',
        'Data de Criação'
    ];

    public filterColumns = [
        {
            column: 'company_code',
            display_name: 'Empresa',
            type: 'string'
        },
        {
            column: 'code',
            display_name: 'Código House',
            type: 'string'
        },
        {
            column: 'created_at',
            type: 'date'
        }
    ];

    private data: any = [];
    public dataSource: any;

    constructor(
        public service: FineService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    ngOnInit() {
        this.service.setQuery('');
    }

}
