import { FineShowComponent } from './fine.show.component';
import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { DeclarationsModule } from 'app/declarations.module';
import { MatTableModule } from '@angular/material';

@NgModule({
    declarations: [
        FineShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatTableModule,
    ],
    exports: [
        FineShowComponent,
    ]
})

export class FineShowModule {
}
