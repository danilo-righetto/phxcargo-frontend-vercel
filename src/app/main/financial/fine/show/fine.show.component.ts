import { FineListingElement } from './../../../../_interfaces/financial/fine-listing';
import { map } from 'rxjs/operators';
import { HttpAuthenticateService } from '../../../../_services/_authentication/http.authenticate.service';
import { ActivatedRoute } from '@angular/router';
import { locale as portuguese } from '../../_i18n/pt';
import { FuseTranslationLoaderService } from '../../../../../@fuse/services/translation-loader.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-fine.show',
    templateUrl: './fine.show.component.html',
})
export class FineShowComponent {

    private data: FineListingElement;
    public loading: boolean = true;

    private endpoint: string = '/financial/fine/show/'
    /**
     * Constructor
     * 
     * @param _fuseTranslationLoaderService
     * @param route
     * @param http
     */
    public constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.fetchData(`${this.endpoint}/${params.id}`);
        })
    }

    /**
     * 
     * @param url
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(fine => {
                this.data = fine;
                this.loading = false;
            });
    }
}