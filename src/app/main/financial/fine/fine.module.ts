import { NgModule } from "@angular/core";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { FineComponent } from './fine.component';
import { FineService } from '../../../_services/financial/FineService';

@NgModule({
    declarations: [
        FineComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        FineComponent,
    ],
    providers: [
        FineService,
    ]
})
export class FineModule {

}