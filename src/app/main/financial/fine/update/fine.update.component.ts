import { FineListingElement } from './../../../../_interfaces/financial/fine-listing';
import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { FineService } from '../../../../_services/financial/FineService';

@Component({
    selector: 'fine.update',
    templateUrl: './fine.update.component.html',
})
export class FineUpdateComponent implements OnInit {

    public data: FineListingElement;
    public loading: boolean = true;
    public _update: boolean = false;
    public currentId = null;
    public loadingFines: boolean = true;

    public formGroup: FormGroup;

    private endpoint: string = '/financial/fines'

    /**
     * 
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     * @param _formBuilder
     * @param route
     * @param http
     * @param router
     * @param fineService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private fineService: FineService,
        private router: Router
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    /**
     * Lifecycle hook that is called after data-bound properties of a directive are
     * initialized.
     */
    public ngOnInit(): void {

        this.buildFormGroup();

        this.route.params.subscribe(params => {

            if (params.id) {
                this._update = true;
                this.currentId = params.id;
                this.fetchFineInformation(`${this.endpoint}/${this.currentId}`);
            } else {

            }
        });

        /* this.fineService.fetchData().subscribe((fine => {
            this.fine = fine.data;
            this.loadingFines = false;
        }) */
    }

    /**
     * Build reactive form
     * 
     */
    private buildFormGroup() {

        this.formGroup = this._formBuilder.group({

        })
    }

    /**
     * Fetch data to current form
     * 
     * @param url 
     */
    private fetchFineInformation(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(fine => {
                this.data = fine;
                this.formGroup.patchValue({
                    //ADD HERE --------------------->
                });

                this.loading = false;
            });
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {

        const parsed = {

        }
        return parsed;
    }

    /**
     * Submit form group (update|create)
     * 
     */
    public submit(): void {

        const controls = this.parseControls(this.formGroup.controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {

        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void{

        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))   
    }
}