import { FineUpdateComponent } from './fine.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { FineService } from './../../../../_services/financial/FineService';
import { NgModule } from '@angular/core';
import { MatFormFieldModule, MatInputModule, MatSelectModule } from '@angular/material';
import { MatTableModule } from "@angular/material";
import { FuseSharedModule } from '@fuse/shared.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        FineUpdateComponent
    ],
    imports: [
        FuseSharedModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTableModule,
        HttpClientModule,
        DeclarationsModule,
    ],
    exports: [
        FineUpdateComponent
    ],
    providers: [
        FineService,        
    ]
})

export class FineUpdateModule {
}