//import { DebitNoteUpdateModule } from './debit-note-update.module';
import { NgModule } from "@angular/core";
import { FuseSharedModule } from '@fuse/shared.module';
// import { DebitNoteUpdateModule } from "./debit-note-DebitNoteUpdateModule";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { DebitNoteService } from 'app/_services/debit-note/debi-note.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
        HttpClientModule,
    ],
    exports: [
        
    ],
    providers: [
        DebitNoteService,
    ]
})

export class DebitNoteUpdateModule {
}
