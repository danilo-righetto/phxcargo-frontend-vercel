
import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReplaySubject } from 'rxjs';
import { Checkpoint } from 'app/main/airwaybill/house/checkpoint/checkpoint.component';
import { DebitNoteListingElement } from 'app/_interfaces/financial/debit-note-listing';

@Component({
  selector: 'app-debit-note-update',
  templateUrl: './debit-note-update.component.html',
})
export class DebitNoteUpdateComponent {

  private data: DebitNoteListingElement;
  public loading: boolean = true;
  public _update: boolean = false;
  private endpoint: string = '/financial/debit-note';

  private currentId = null;

  //public currencies: any;
  public loadingCurrency: boolean = true;

  public loadingCheckpoints: boolean = true;
  public allCheckpoints: Array<any>;
  public filteredCheckpoint: ReplaySubject<Checkpoint[]> = new ReplaySubject<Checkpoint[]>(1);

  public formGroup: FormGroup;

  /**
   * 
   * Constructor
   * @param _fuseTranslationLoaderService
   * @param route
   * @param http 
   */
  constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private http: HttpAuthenticateService,
    //private currencyService: CurrencyService,
    //private checkpointService: CheckpointService,
    private router: Router
  ) {
    this._fuseTranslationLoaderService.loadTranslations(portuguese);
  }

  public ngOnInit(): void {

    this.buildFormGroup();

    this.route.params.subscribe(params => {

      if (params.id) {

        this._update = true;
        this.currentId = params.id;
        this.fetchData(`/financial/debit-note/${params.id}`);
        
      }
    });
  }


  //Verificar nescessidade de incluir checkpoint

  /**
   * Build reactive form
   * 
   */
  private buildFormGroup() {

    this.formGroup = this._formBuilder.group({
      code: ['', Validators.required],
      total_amount: [''],
      paid_at: [''],
      fine_code: [''],
      amount: [''],
      company: [''],
    });
  }

  /**
   * 
   * @param url 
   */
  private fetchData(url: string) {


    this.http.put(url).pipe(
      map((res: any) => res.data))
      .subscribe(debit => {
        this.data = debit;
        this.formGroup.patchValue({
          code: debit.code,
          total_amount: debit.total_amount,
          paid_at: debit.paid_at,
          fine_code: debit.fine_code,
          amount: debit.amount,
          company: debit.company,
        });
        this.loading = false;
      })
  }

  /**
   * Submit form group (update|create)
   * 
   */
  public submit(): void {

    const controls = this.parseControls(this.formGroup.controls);

    if (this._update) {
      return this.update(controls);
    }

    return this.create(controls);
  }

  /**
   * Parse controls to expected data structure for request
   * 
   * @param controls
   */
  public parseControls(controls: any) {

    const parsed = {
      'code': controls.code.value,
      'total_amount': controls.total_amount.value,
      'company': controls.company.value,
    }

    return parsed;
  }

  /**
   * 
   * @param controls 
   */
  private create(controls: any): void {
    this.http.post(this.endpoint, controls)
      .subscribe(res => this.router.navigate([this.endpoint]))
  }

  /**
   * 
   * @param controls
   */
  private update(controls: any): void {
    this.http.put(`${this.endpoint}/${this.currentId}`, controls)
      .subscribe(res => this.router.navigate([this.endpoint]))
  }


}




