import { DebitNoteUpdateComponent } from './update/debit-note-update.component';
import { NgModule } from "@angular/core";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { DebitNoteComponent } from './debit-note.component';
import { DebitNoteService } from '../../../_services/financial/DebitNoteService';
import { DebitNoteShowComponent } from './show/debit-note-show.component';

@NgModule({
    declarations: [
        DebitNoteComponent,
        DebitNoteShowComponent,
        DebitNoteUpdateComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        DebitNoteComponent,
    ],
    providers: [
        DebitNoteService,
    ]
})
export class DebitNoteModule {

}