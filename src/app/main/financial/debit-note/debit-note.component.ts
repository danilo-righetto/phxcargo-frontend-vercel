import { Component, OnInit } from '@angular/core';
import { FuseTranslationLoaderService } from '../../../../@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../financial/_i18n/pt';
import { DebitNoteService } from '../../../_services/financial/DebitNoteService';
 
@Component({
    selector: 'app-debit-note',
    templateUrl: './debit-note.component.html'
})
export class DebitNoteComponent implements OnInit {

    public endpoint: string = '/financial/debit-note';

    public displayedColumns: string[] = [
        'Empresa',
        'Código',
        'Valor Total',
        'Linha Digital',
        'Pago',
    ];

    public filterColumns = [
        {
            column: 'company_code',
            display_name: 'Empresa',
            type: 'string'
        },
        {
            column: 'code',
            display_name: 'Código House',
            type: 'string'
        }
    ];

    private data: any = [];
    public dataSource: any;

    constructor(
        public service: DebitNoteService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    ngOnInit() {
        this.service.setQuery('');
    }

}
