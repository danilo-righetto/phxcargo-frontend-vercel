import { Component, OnInit } from '@angular/core';
import { DebitNoteListingElement } from '../../../../_interfaces/financial/debit-note-listing';
import { FuseTranslationLoaderService } from '../../../../../@fuse/services/translation-loader.service';
import { locale as portuguese } from '../../_i18n/pt';
import { ActivatedRoute } from '@angular/router';
import { HttpAuthenticateService } from '../../../../_services/_authentication/http.authenticate.service';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-debit-note-show',
    templateUrl: './debit-note-show.component.html',
})
export class DebitNoteShowComponent implements OnInit {

    private data: DebitNoteListingElement;
    public loading: boolean = true;

    /**
     * Constructor
     *
     * @param _fuseTranslationLoaderService
     * @param route
     * @param http
     */
    public constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.fetchData(`/financial/debit-note/show/${params.id}`);
        })
    }

    /**
     *
     * @param url
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(debitNote => {
                this.data = debitNote;
                this.loading = false;
            });
    }

}
