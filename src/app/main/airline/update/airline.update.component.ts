
import {map} from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { AirlineListingElement } from 'app/_interfaces/airline/listing-element';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'airline-update',
    templateUrl: './airline.update.component.html',
    styleUrls: ['./airline.update.component.css'],
})
export class AirlineUpdateComponent {

    private data: AirlineListingElement;
    public loading: boolean = true;
    public _update: boolean = false;
    private endpoint: string = '/airlines';
    private currentId = null;

    public formGroup: FormGroup;

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private router: Router
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.buildFormGroup();

        this.route.params.subscribe(params => {

            if (params.id) {
                this.loading = true;
                this._update = true;
                this.currentId = params.id;
                this.fetchData(`/airlines/${params.id}`);
            }
        });

    }

    /**
     * Build reactive form
     *
     */
    private buildFormGroup() {
        this.formGroup = this._formBuilder.group({
            name: ['', Validators.required],
            code: ['', Validators.required]
        });
        this.loading = false;
    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(airline => {
                this.data = airline;
                this.formGroup.patchValue({
                    name: airline.name,
                    code: airline.code
                });
                this.loading = false;
            });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {

        const controls = this.parseControls(this.formGroup.controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {
        const parsed = {
            "name": controls.name.value,
            "code": controls.code.value
        }

        return parsed;
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }
}