import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { AirlineUpdateComponent } from './airline.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
    declarations: [
        AirlineUpdateComponent
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        BrowserAnimationsModule,
        MatProgressSpinnerModule,
    ],
    exports: [
        AirlineUpdateComponent
    ]
})

export class AirlineUpdateModule {
}
