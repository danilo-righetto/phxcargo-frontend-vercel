import { Component } from "@angular/core";
import { MatTableDataSource } from "@angular/material";
import { AirlineListingElement } from 'app/_interfaces/airline/listing-element';
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { locale as portuguese } from './_i18n/pt';
import { AirlineService } from "app/_services/airline/airline.service";

@Component({
    selector: 'airline',
    templateUrl: './airline.component.html'
})
export class AirlineComponent {

    public endpoint: string = '/airlines';

    public displayedColumns: string[] = [
        'Companhia Aérea'
    ];

    public filterColumns = [
        {
            column: 'name',
            display_name: 'Companhia Aérea',
            type: 'string',
        }
    ]

    private data: AirlineListingElement[] = [];
    public dataSource: MatTableDataSource<any>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        public airlineService: AirlineService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

}