import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { AirlineShowComponent } from './airline.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
    declarations: [
        AirlineShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        BrowserAnimationsModule,
        MatProgressSpinnerModule,
    ],
    exports: [
        AirlineShowComponent,
    ]
})

export class AirlineShowModule {
}
