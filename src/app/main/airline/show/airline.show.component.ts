
import {map} from 'rxjs/operators';
import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { AirlineListingElement } from 'app/_interfaces/airline/listing-element';

@Component({
    selector: 'airline-show',
    templateUrl: './airline.show.component.html',
    styleUrls: ['./airline.show.component.css'],
})
export class AirlineShowComponent {

    private data: AirlineListingElement;
    public loading: boolean = true;

    /**
     * Constructor
     * 
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    public constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.route.params.subscribe(params => {

            this.fetchData(`/airlines/${params.id}`);
        })

    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(airline => {
                this.data = airline;
                this.loading = false;
            });
    }
}
