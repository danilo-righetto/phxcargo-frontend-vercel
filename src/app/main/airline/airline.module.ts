import { NgModule } from "@angular/core";
import { AirlineComponent } from "./airline.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { AirlineService } from "app/_services/airline/airline.service";

@NgModule({
    declarations: [
        AirlineComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        AirlineComponent,
    ],
    providers: [
        AirlineService,
    ]
})
export class AirlineModule {

}