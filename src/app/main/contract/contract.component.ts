import { Component } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from './_i18n/pt';
import { ContractListingElement } from 'app/_interfaces/contract/contract.interface';
import { MatTableDataSource } from '@angular/material';
import { ContractService } from 'app/_services/company/contract.service';

@Component({
    selector: 'contract',
    templateUrl: './contract.component.html'
})
export class ContractComponent {

    public endpoint: string = '/contracts';

    public displayedColumns: string[] = [
        'Empresa',
        'WACC',
        'Taxa de Câmbio',
        'Último Checkpoint',
        'Padrão',
    ];

    public filterColumns = [
        {
            column: 'description',
            display_name: 'Empresa',
            type: 'string'
        },
        {
            column: 'last_checkpoint_id',
            display_name: 'Último checkpoint',
            type: 'string'
        }
    ];

    private data: ContractListingElement[] = [];
    public dataSource: MatTableDataSource<ContractListingElement>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        public service: ContractService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }
}