import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { ContractUpdateComponent } from './contract.update.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatFormFieldModule, MatInputModule, MatTableModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { CurrencyService } from 'app/_services/currency/currency.service';
import { CompanyService } from 'app/_services/company/company.service';
import { PaymentService } from 'app/_services/payment/payment.service';
import { OwlDateTimeModule, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE, OwlDateTimeIntl } from 'ng-pick-datetime';
import { OwlMomentDateTimeModule } from 'ng-pick-datetime-moment';
import { CommonModule } from '@angular/common';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { NotificationService } from 'app/_services/notification/notification.service';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { DateTimeModule } from "app/_default/datetime/datetime.default.module";
import { MatTooltipModule } from '@angular/material/tooltip';


export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

/**
 * Define datetime picker lables
 * 
 */
export class DefaultIntl extends OwlDateTimeIntl {
    setBtnLabel = 'Selecionar';
    cancelBtnLabel = 'Cancelar';
};

export const MY_MOMENT_FORMATS = {
    parseInput: 'l LT',
    fullPickerInput: 'DD/MM/YYYY HH:mm',
    datePickerInput: 'l',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
};

@NgModule({
    declarations: [
        ContractUpdateComponent
    ],
    imports: [
        NgxMaskModule.forRoot(options),
        FuseSharedModule,
        DeclarationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        OwlDateTimeModule,
        OwlMomentDateTimeModule,
        CommonModule,
        NgxMatSelectSearchModule,
        FormsModule,
        MatTableModule,
        MatDividerModule,
        MatProgressSpinnerModule,
        DateTimeModule,
        MatTooltipModule,
    ],
    exports: [
        ContractUpdateComponent,
    ],
    providers: [
        CurrencyService,
        CompanyService,
        PaymentService,
        NotificationService,
        { provide: OwlDateTimeIntl, useClass: DefaultIntl },
        { provide: OWL_DATE_TIME_LOCALE, useValue: 'pt-BR' },
        { provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS },

    ]
})

export class ContractUpdateModule {
}
