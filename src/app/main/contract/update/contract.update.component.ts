
import { map } from 'rxjs/operators';
import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { ContractListingElement, ContractRequest, Contract } from 'app/_interfaces/contract/contract.interface';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, FormGroupDirective, NgForm } from '@angular/forms';
import { CurrencyService } from 'app/_services/currency/currency.service';
import { CompanyService } from 'app/_services/company/company.service';
import { ReplaySubject } from 'rxjs';
import { Checkpoint } from 'app/main/airwaybill/house/checkpoint/checkpoint.component';
import { CheckpointService } from 'app/_services/checkpoints/checkpoint.service';
import { ServiceService } from 'app/_services/company/service.service';
import { NotificationService } from 'app/_services/notification/notification.service';
import { MatTableDataSource, MatSelectModule } from '@angular/material';

export interface Company {
    'ID': number;
    'Código': string;
    'Nome da Empresa': string;
}

@Component({
    selector: 'contract-update',
    templateUrl: './contract.update.component.html',
    styleUrls: ['contract.update.component.css']
})

export class ContractUpdateComponent {

    public data: ContractListingElement;
    public loading: boolean = true;
    public _update: boolean = false;
    private endpoint: string = '/contracts';
    private currentId = null;
    public currencies: any;
    public loadingCurrency: boolean = true;
    public checkpoints: any;
    public services: any;
    public loadingService: boolean = true;
    public companies: any;
    public loadingCompany: boolean = true;
    public loadingCheckpoints: boolean = true;
    public allCheckpoints: Array<any>;
    public filteredCheckpoints: ReplaySubject<Checkpoint[]> = new ReplaySubject<Checkpoint[]>(1);
    public items = [];
    public filteredLastCheckpoints: ReplaySubject<Checkpoint[]> = new ReplaySubject<Checkpoint[]>(1);
    public filteredCompany: ReplaySubject<Company[]> = new ReplaySubject<Company[]>(1);
    public all: Array<any>;
    public formCheckpoint: FormGroup;
    public checkpointFilter: FormGroup;
    public checkpointLastFilter: FormGroup;
    public formCompany: FormGroup;
    public companyLastFilter: FormGroup;
    public servicesListing: ContractListingElement;
    public release_code: string;
    public last_checkpoint: string;
    public company_id: string;
    public formGroup: FormGroup;

    public displayedColumnsService: string[] = [
        'Descrição',
        'Nível de Serviço',
        'Checkpoint',
        'Tipo de Carga',
        'Aproximação'
    ];

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private currencyService: CurrencyService,
        private companyService: CompanyService,
        private checkpointService: CheckpointService,
        private serviceService: ServiceService,
        private router: Router,
        private notificationService: NotificationService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public verifyFields() {
        if (
            !this.formGroup.get('wacc').valid ||
            !this.formGroup.get('description').valid ||
            !this.formGroup.get('exchange_spread_rate').valid ||
            !this.formGroup.get('reimbursement_modal_id').valid ||
            !this.formGroup.get('standard_duties_collection_id').valid ||
            !this.formGroup.get('duty_invoice').valid ||
            !this.formGroup.get('duty_kind_of_charge').valid ||
            !this.formGroup.get('duties_collect_percent_fee').valid ||
            !this.formGroup.get('duties_pre_payment_add').valid ||
            !this.formGroup.get('services_currency_id').valid ||
            !this.formGroup.get('duties_currency_id').valid ||
            !this.formGroup.get('release_code_id').valid ||
            !this.formGroup.get('charge_mawb').valid ||
            !this.formGroup.get('mawb_price').valid ||
            !this.formGroup.get('mawb_weight_fist_day').valid ||
            !this.formGroup.get('mawb_weight_next_days').valid ||
            !this.formGroup.get('hdl_price').valid ||
            !this.formGroup.get('min_charge').valid ||
            !this.formGroup.get('max_charge').valid ||
            !this.formGroup.get('billing_schedule').valid ||
            !this.formGroup.get('deadline_billing').valid ||
            !this.formGroup.get('deadline_due_date').valid ||
            !this.formGroup.get('type').valid ||
            !this.formGroup.get('type_invoice').valid ||
            !this.formGroup.get('seller_key_mandae').valid ||
            !this.formGroup.get('last_checkpoint').valid
        ) {
            // this.notificationService.notify('Preencha os campos obrigatórios ! ', 'error');
            return true;
        }
    }

    public ngOnInit(): void {
        this.buildFormGroup();

        this.release_code = '';
        this.last_checkpoint = '';
        this.company_id = '';

        this.formCheckpoint = this._formBuilder.group({
            checkpoint: [''],
            checkpointFilter: [''],
            checkpointLastFilter: [''],
            comments: [''],
            date: [''],
        });

        this.formCompany = this._formBuilder.group({
            companyLastFilter: [''],
        });

        this.route.params.subscribe(params => {
            if (params.id) {
                this._update = true;
                this.currentId = params.id;
                this.fetchData(`/contracts/${params.id}`);
            } else {
                this.addNewServiceGroups();
            }
        });

        this.currencyService.fetchData('').subscribe(currencies => {
            this.loading = true;
            this.currencies = currencies.data
            this.loadingCurrency = false;
            this.loading = false;
        });

        this.companyService.fetchData(this.company_id, 255, 1).subscribe(companies => {
            this.loading = true;
            this.companies = companies.data;
            this.loadingCompany = false;
            this.filteredCompany.next(this.companies.slice());
            this.loading = false;
        });

        this.checkpointService.fetchData(this.release_code, 180, 1).subscribe(all => {
            this.loading = true;
            this.all = all.data;
            this.filteredCheckpoints.next(this.all.slice());
            this.loading = false;
        });

        this.checkpointService.fetchData(this.last_checkpoint, 180, 1).subscribe(all => {
            this.loading = true;
            this.all = all.data;
            this.filteredLastCheckpoints.next(this.all.slice());
            this.loading = false;
        });

        this.serviceService.fetchData('', 255).subscribe(services => {
            this.loading = true;
            this.services = services.data;
            this.loadingService = false;
            this.loading = false;
        });

        this.initCheckpointFilter();

        this.initCompanyFilter();
    }

    removeServiceGroup(item) {
        var listOfServices = this.servicesGroup.value;
        let lenghtArray = listOfServices.length - 1;
        listOfServices.splice(lenghtArray, 1);
        this.servicesGroup.controls.splice(this.items.indexOf(item), 1);
        this.servicesGroup.updateValueAndValidity()
        this.notificationService.notify('Serviço excluído com sucesso ! ', 'error');
        if (listOfServices.length == 0) {
            this.addNewServiceGroup();
        }
    }


    /**
     * 
     * @param service
     */
    public addNewServiceGroup(service = null) {
        var listOfServices = this.servicesGroup.value;
        let check = 0;
        let checkServices = listOfServices.map(function (item, index, array) {
            let count = listOfServices.length - 1;
            if (count == index) {
                if (item.id == '' || item.amount == '') {
                    return check = 1;
                } else {
                    return check = 2;
                }
            }
        });

        if (check == 1) {
            this.notificationService.notify('Os campos descrição e valor são obrigatórios ! ', 'error');
            return null;
        } else {
            if (service == null || service == ''){
            this.notificationService.notify('Serviço adicionado com sucesso ! ');
            }
            this.servicesGroup.push(this.initServicesRow(service));
        }
    }

    /**
     * 
     * @param service
     */
    public addNewServiceGroups(service = null) {
        this.servicesGroup.push(this.initServicesRow(service));
    }


    get servicesGroup(): FormArray {
        return <FormArray>this.formGroup.get('services_list');
    }

    public initCheckpointFilter() {
        const checkpointFilter = this.formGroup.controls.checkpointFilter;

        checkpointFilter.valueChanges.subscribe(() => {
            let search = checkpointFilter.value.toLowerCase();

            this.filteredCheckpoints.next(
                this.allCheckpoints.filter(checkpoint => checkpoint.code.toLowerCase().indexOf(search) > -1)
            );
        });

        const checkpointLastFilter = this.formCheckpoint.controls.checkpointLastFilter;

        checkpointLastFilter.valueChanges.subscribe(() => {
            let search = checkpointLastFilter.value.toLowerCase();

            this.filteredLastCheckpoints.next(
                this.all.filter(checkpoint => checkpoint.code.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initCompanyFilter() {
        const companyLastFilter = this.formCompany.controls.companyLastFilter;

        companyLastFilter.valueChanges.subscribe(() => {
            let search = companyLastFilter.value.toLowerCase();

            this.filteredCompany.next(
                this.companies.filter(company => company.code.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    /**
     * Build reactive form
     *
     */
    private buildFormGroup() {

        this.formGroup = this._formBuilder.group({
            wacc: [{ value: '', disabled: false }, Validators.required],
            description: [{ value: '', disabled: false }, Validators.required],
            exchange_spread_rate: [{ value: '', disabled: false }, Validators.required],
            reimbursement_modal_id: [{ value: '', disabled: false }, Validators.required],
            standard_duties_collection_id: [{ value: '', disabled: false }, Validators.required],
            default: [{ value: '', disabled: false }, Validators.required],
            duty_invoice: [{ value: '', disabled: false }, Validators.required],
            duty_kind_of_charge: [{ value: '', disabled: false }, Validators.required],
            duties_collect_percent_fee: [{ value: '', disabled: false }, Validators.required],
            duties_pre_payment_add: [{ value: '', disabled: false }, Validators.required],
            services_currency_id: [{ value: '', disabled: false }, Validators.required],
            duties_currency_id: [{ value: '', disabled: false }, Validators.required],
            release_code_id: [{ value: '', disabled: false }, Validators.required],
            invoice_service_seperate_hold: [{ value: '', disabled: false }, Validators.required],
            invoice_duties_separate_hold: [{ value: '', disabled: false }, Validators.required],
            active: [{ value: '', disabled: false }, Validators.required],
            charge_mawb: [{ value: '', disabled: false }, Validators.required],
            mawb_price: [{ value: '', disabled: false }, Validators.required],
            charge_mawb_weight: [{ value: '', disabled: false }, Validators.required],
            mawb_weight_fist_day: [{ value: '', disabled: false }, Validators.required],
            mawb_weight_next_days: [{ value: '', disabled: false }, Validators.required],
            charge_hdl_to_mawb: [{ value: '', disabled: false }, Validators.required],
            hdl_price: [{ value: '', disabled: false }, Validators.required],
            charge_individual_storage: [{ value: '', disabled: false }, Validators.required],
            min_charge: [{ value: '', disabled: false }, Validators.required],
            max_charge: [{ value: '', disabled: false }, Validators.required],
            billing_schedule: [{ value: '', disabled: false }, Validators.required],
            deadline_billing: [{ value: '', disabled: false }, Validators.required],
            deadline_due_date: [{ value: '', disabled: false }, Validators.required],
            type: [{ value: '', disabled: false }, Validators.required],
            type_invoice: [{ value: '', disabled: false }, Validators.required],
            seller_key_mandae: [{ value: '', disabled: false }, Validators.required],
            duties_invoice_required: [{ value: '', disabled: false }, Validators.required],
            collect_tax_id: [{ value: '', disabled: false }, Validators.required],
            last_checkpoint: [{ value: '', disabled: false }, Validators.required],
            company: [{ value: '', disabled: false }, Validators.required],

            // Checkpoint
            checkpointFilter: [{ value: '', disabled: false }],
            checkpointLastFilter: [{ value: '', disabled: false }],
            services_list: this._formBuilder.array([]),
            companyLastFilter: [{ value: '', disabled: false }],
        });
    }

    /**
     * 
     * @param service
     */
    private initServicesRow(service: any = null) {

        return this._formBuilder.group({
            description: [service ? service.description : ''],
            amount: [service ? service.amount : '', Validators.required],
            created_at: [service ? service.created_at : ''],
            service_id: [service ? service.service_id : ''],
        });
    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(contract => {
                this.data = new Contract(contract);
                this.services = contract.services;
                this.loading = false;
                this.release_code = contract.release_code_id.code;
                this.last_checkpoint = contract.last_checkpoint.code;
                this.company_id = contract.company ? contract.company.id : 1;
                this.formGroup.patchValue({
                    wacc: contract.wacc ? contract.wacc : '',
                    description: contract.description ? contract.description : '',
                    exchange_spread_rate: contract.exchange_spread_rate ? contract.exchange_spread_rate : '',
                    reimbursement_modal_id: contract.reimbursement_modal_id ? contract.reimbursement_modal_id : '',
                    standard_duties_collection_id: contract.standard_duties_collection_id ? contract.standard_duties_collection_id : '',
                    default: contract.default ? contract.default : '',
                    duty_invoice: contract.duty_invoice ? contract.duty_invoice : '',
                    duty_kind_of_charge: contract.duty_kind_of_charge ? contract.duty_kind_of_charge : '',
                    duties_collect_percent_fee: contract.duties_collect_percent_fee ? contract.duties_collect_percent_fee : '',
                    duties_pre_payment_add: contract.duties_pre_payment_add ? contract.duties_pre_payment_add : '',
                    services_currency_id: contract.services_currency_id ? contract.services_currency_id.id : '',
                    duties_currency_id: contract.duties_currency_id ? contract.duties_currency_id.id : '',
                    release_code_id: contract.release_code_id ? contract.release_code_id.id : '',
                    invoice_service_seperate_hold: contract.invoice_service_seperate_hold ? contract.invoice_service_seperate_hold : 0,
                    invoice_duties_separate_hold: contract.invoice_duties_separate_hold ? contract.invoice_duties_separate_hold : '',
                    active: contract.active ? contract.active : 0,
                    charge_mawb: contract.charge_mawb ? contract.charge_mawb : 0,
                    mawb_price: contract.mawb_price ? contract.mawb_price : '',
                    charge_mawb_weight: contract.charge_mawb_weight ? contract.charge_mawb_weight : 0,
                    mawb_weight_fist_day: contract.mawb_weight_fist_day ? contract.mawb_weight_fist_day : '',
                    mawb_weight_next_days: contract.mawb_weight_next_days ? contract.mawb_weight_next_days : '',
                    charge_hdl_to_mawb: contract.charge_hdl_to_mawb ? contract.charge_hdl_to_mawb : 0,
                    hdl_price: contract.hdl_price ? contract.hdl_price : '',
                    charge_individual_storage: contract.charge_individual_storage ? contract.charge_individual_storage : 0,
                    min_charge: contract.min_charge ? contract.min_charge : '',
                    max_charge: contract.max_charge ? contract.max_charge : '',
                    billing_schedule: contract.billing_schedule ? contract.billing_schedule : '',
                    deadline_billing: contract.deadline_billing ? contract.deadline_billing : '',
                    deadline_due_date: contract.deadline_due_date ? contract.deadline_due_date : '',
                    type: contract.type ? contract.type : '',
                    type_invoice: contract.type_invoice ? contract.type_invoice : '',
                    seller_key_mandae: contract.seller_key_mandae ? contract.seller_key_mandae : '',
                    duties_invoice_required: contract.duties_invoice_required ? contract.duties_invoice_required : 0,
                    collect_tax_id: contract.collect_tax_id ? contract.collect_tax_id : 0,
                    last_checkpoint: contract.last_checkpoint ? contract.last_checkpoint.id : '',
                    company: contract.company ? contract.company.id : ''
                });
                if (contract.services) {
                    contract.services.forEach(service => {
                        this.addNewServiceGroup(service);
                    });
                } else {
                    this.addNewServiceGroup();
                }
                this.loading = false;
            });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {

        if (this.verifyFields() == false) {
            return;
        }

        const controls = this.parseControls(this.formGroup.controls);

        if (this._update) {
            return this.update(controls);
        }

        return this.create(controls);
    }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls(controls: any) {
        let services = this.servicesGroup.controls.map((service: FormGroup) => {
            if (service.controls.service_id.value) {
                return {
                    "description": service.controls.description.value,
                    "amount": service.controls.amount.value,
                    "cost_amount": service.controls.amount.value,
                    "currency_id": controls.services_currency_id.value ? 2 : 1,
                    "created_at": service.controls.created_at.value,
                    "service_id": service.controls.service_id.value,
                }
            }
        });

        let billing_schedule = new Date(controls.billing_schedule.value);
        let billing_schedule_formated = '';
        billing_schedule_formated = billing_schedule.getFullYear() + "-" + ("0" + (billing_schedule.getMonth() + 1)).slice(-2) + "-" + ((billing_schedule.getDate() + 1));

        let deadline_billing = new Date(controls.deadline_billing.value);
        let deadline_billing_formated = '';
        deadline_billing_formated = deadline_billing.getFullYear() + "-" + ("0" + (deadline_billing.getMonth() + 1)).slice(-2) + "-" + ((deadline_billing.getDate() + 1));

        let deadline_due_date = new Date(controls.deadline_due_date.value);
        let deadline_due_date_formated = '';
        deadline_due_date_formated = deadline_due_date.getFullYear() + "-" + ("0" + (deadline_due_date.getMonth() + 1)).slice(-2) + "-" + ((deadline_due_date.getDate() + 1));

        const parsed = {
            'company_id':  controls.company.value,
            'description': controls.description.value,
            'currency_id': controls.services_currency_id.value,
            'wacc': controls.wacc.value,
            'exchange_spread_rate': controls.exchange_spread_rate.value,
            'default': controls.default.value,
            'last_checkpoint_id': controls.last_checkpoint.value,
            'standard_duties_collection_id':controls.standard_duties_collection_id.value,
            'reimbursement_modal_id': controls.reimbursement_modal_id.value,
            'duty_invoice': controls.duty_invoice.value,
            'duty_kind_of_charge': controls.duty_kind_of_charge.value,
            'duties_collect_percent_fee': controls.duties_collect_percent_fee.value,
            'duties_pre_payment_add': controls.duties_pre_payment_add.value,
            'services_currency_id': controls.services_currency_id.value,
            'duties_currency_id': controls.duties_currency_id.value,
            'release_code_id': controls.release_code_id.value,
            'invoice_service_seperate_hold': controls.invoice_service_seperate_hold.value,
            'invoice_duties_separate_hold': controls.invoice_duties_separate_hold.value,
            'active': controls.active.value,
            'charge_mawb': controls.charge_mawb.value,
            'mawb_price': controls.mawb_price.value,
            'charge_mawb_weight': controls.charge_mawb_weight.value,
            'mawb_weight_fist_day': controls.mawb_weight_fist_day.value,
            'mawb_weight_next_days': controls.mawb_weight_next_days.value,
            'charge_hdl_to_mawb': controls.charge_hdl_to_mawb.value,
            'hdl_price': controls.hdl_price.value,
            'charge_individual_storage': controls.charge_individual_storage.value,
            'min_charge': controls.min_charge.value,
            'max_charge': controls.max_charge.value,
            'billing_schedule': billing_schedule_formated,
            'deadline_billing': deadline_billing_formated,
            'deadline_due_date': deadline_due_date_formated,
            'type': controls.type.value,
            'type_invoice': controls.type_invoice.value,
            'seller_key_mandae': controls.seller_key_mandae.value,
            'duties_invoice_required': controls.duties_invoice_required.value,
            'collect_tax_id': controls.collect_tax_id.value
        }

        if (services.length > 0 && services != null) {
            return { ...parsed, 'services': services };
        }

        return { ...parsed, 'services': [] };
    }

    /**
     * 
     * @param controls 
     */
    private create(controls: any): void {
        this.http.post(this.endpoint, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {
        this.http.put(`${this.endpoint}/${this.currentId}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }
}
