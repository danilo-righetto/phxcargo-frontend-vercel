import { NgModule } from "@angular/core";
import { FuseSharedModule } from '@fuse/shared.module';
import { ContractComponent } from "./contract.component";
import { MatTableModule } from "@angular/material";
import { DeclarationsModule } from "app/declarations.module";
import { ContractService } from 'app/_services/company/contract.service';

@NgModule({
    declarations: [
        ContractComponent
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        DeclarationsModule,
    ],
    exports: [
        ContractComponent,
    ],
    providers: [
        ContractService,
    ]
})

export class ContractModule {
}
