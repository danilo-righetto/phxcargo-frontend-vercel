import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { ContractShowComponent } from './contract.show.component';
import { DeclarationsModule } from 'app/declarations.module';
import { MatTableModule, MatSelectModule } from '@angular/material';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { DateTimeModule } from "app/_default/datetime/datetime.default.module";
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
    declarations: [
        ContractShowComponent,
    ],
    imports: [
        FuseSharedModule,
        DeclarationsModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatDividerModule,
        MatSelectModule,
        NgxMatSelectSearchModule,
        DateTimeModule,
        MatTooltipModule
    ],
    exports: [
        ContractShowComponent,
    ]
})

export class ContractShowModule {
}
