
import { map } from 'rxjs/operators';
import { ReplaySubject } from "rxjs";
import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as portuguese } from '../_i18n/pt';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, FormGroupDirective, NgForm} from '@angular/forms';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { ContractListingElement, ContractRequest, Contract } from 'app/_interfaces/contract/contract.interface';
import { MatTableDataSource, MatSelectModule } from '@angular/material';
import { NotificationService } from 'app/_services/notification/notification.service';
import { CheckpointService } from "app/_services/checkpoints/checkpoint.service";
import { CompanyService } from 'app/_services/company/company.service';

export interface Checkpoint {
    code: string;
    name: string;
}

export interface Company {
    'ID': number;
    'Código': string;
    'Nome da Empresa': string;
}

@Component({
    selector: 'contract-show',
    templateUrl: './contract.show.component.html',
    styleUrls: ['contract.show.component.css']
})
export class ContractShowComponent implements OnInit {

    public data: ContractListingElement;
    public loading: boolean = true;
    public formGroup: FormGroup;
    public filteredCheckpoints: ReplaySubject<Checkpoint[]> = new ReplaySubject<Checkpoint[]>(1);
    public filteredLastCheckpoints: ReplaySubject<Checkpoint[]> = new ReplaySubject<Checkpoint[]>(1);
    public filteredCompany: ReplaySubject<Company[]> = new ReplaySubject<Company[]>(1);
    public all: Array<any>;
    public formCheckpoint: FormGroup;
    public checkpointFilter: FormGroup;
    public checkpointLastFilter: FormGroup;
    public formCompany: FormGroup;
    public companyLastFilter: FormGroup;
    public services: ContractListingElement;
    public release_code: string;
    public last_checkpoint: string;
    public companies: Array<any>;

    public displayedColumnsService: string[] = [
        'Descrição',
        'Nível de Serviço',
        'Checkpoint',
        'Tipo de Carga',
        'Aproximação'
    ];

    /**
     * Constructor
     * 
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param http 
     * @param _formBuilder 
     */
    public constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private _formBuilder: FormBuilder,
        private checkpointService: CheckpointService,
        private companyService: CompanyService,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.buildFormGroup();

        this.release_code = '';
        this.last_checkpoint = '';

        this.formCheckpoint = this._formBuilder.group({
            checkpoint: ['', Validators.required],
            checkpointFilter: [''],
            checkpointLastFilter: [''],
            comments: [''],
            date: ['', Validators.required],
        });

        this.formCompany = this._formBuilder.group({
            companyLastFilter: [''],
        });

        this.route.params.subscribe(params => {
            this.fetchData(`/contracts/${params.id}`);
        });

        this.companyService.fetchData('', 255, 1).subscribe(all => {
            this.loading = false;
            this.companies = all.data;
            this.filteredCompany.next(this.companies.slice());
        });

        this.checkpointService.fetchData(this.release_code, 180, 1).subscribe(all => {
            this.loading = false;
            this.all = all.data;
            this.filteredCheckpoints.next(this.all.slice());
        });

        this.checkpointService.fetchData(this.last_checkpoint, 180, 1).subscribe(all => {
            this.loading = false;
            this.all = all.data;
            this.filteredLastCheckpoints.next(this.all.slice());
        });

        this.initCheckpointFilter();

        this.initCompanyFilter();
    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(contract => {
                this.data = new Contract(contract);
                this.services = contract.services;
                this.loading = false;
                this.release_code = contract.release_code_id.code;
                this.last_checkpoint = contract.last_checkpoint.code;
                this.formGroup.patchValue({
                    wacc: contract.wacc ? contract.wacc : '',
                    description: contract.description ? contract.description : '',
                    exchange_spread_rate: contract.exchange_spread_rate ? contract.exchange_spread_rate : '',
                    reimbursement_modal_id: contract.reimbursement_modal_id ? contract.reimbursement_modal_id : '',
                    standard_duties_collection_id: contract.standard_duties_collection_id ? contract.standard_duties_collection_id : '',
                    default: contract.default ? contract.default : '',
                    duty_invoice: contract.duty_invoice ? contract.duty_invoice : '',
                    duty_kind_of_charge: contract.duty_kind_of_charge ? contract.duty_kind_of_charge : '',
                    duties_collect_percent_fee: contract.duties_collect_percent_fee ? contract.duties_collect_percent_fee : '',
                    duties_pre_payment_add: contract.duties_pre_payment_add ? contract.duties_pre_payment_add : '',
                    services_currency_id: contract.services_currency_id ? contract.services_currency_id.id : '',
                    duties_currency_id: contract.duties_currency_id ? contract.duties_currency_id.id : '',
                    release_code_id: contract.release_code_id ? contract.release_code_id.id : '',
                    invoice_service_seperate_hold: contract.invoice_service_seperate_hold ? contract.invoice_service_seperate_hold : 0,
                    invoice_duties_separate_hold: contract.invoice_duties_separate_hold ? contract.invoice_duties_separate_hold : '',
                    active: contract.active ? contract.active : 0,
                    charge_mawb: contract.charge_mawb ? contract.charge_mawb : 0,
                    mawb_price: contract.mawb_price ? contract.mawb_price : '',
                    charge_mawb_weight: contract.charge_mawb_weight ? contract.charge_mawb_weight : 0,
                    mawb_weight_fist_day: contract.mawb_weight_fist_day ? contract.mawb_weight_fist_day : '',
                    mawb_weight_next_days: contract.mawb_weight_next_days ? contract.mawb_weight_next_days : '',
                    charge_hdl_to_mawb: contract.charge_hdl_to_mawb ? contract.charge_hdl_to_mawb : 0,
                    hdl_price: contract.hdl_price ? contract.hdl_price : '',
                    charge_individual_storage: contract.charge_individual_storage ? contract.charge_individual_storage : 0,
                    min_charge: contract.min_charge ? contract.min_charge : '',
                    max_charge: contract.max_charge ? contract.max_charge : '',
                    billing_schedule: contract.billing_schedule ? contract.billing_schedule : '',
                    deadline_billing: contract.deadline_billing ? contract.deadline_billing : '',
                    deadline_due_date: contract.deadline_due_date ? contract.deadline_due_date : '',
                    type: contract.type ? contract.type : '',
                    type_invoice: contract.type_invoice ? contract.type_invoice : '',
                    seller_key_mandae: contract.seller_key_mandae ? contract.seller_key_mandae : '',
                    duties_invoice_required: contract.duties_invoice_required ? contract.duties_invoice_required : 0,
                    collect_tax_id: contract.collect_tax_id ? contract.collect_tax_id : 0,
                    last_checkpoint: contract.last_checkpoint ? contract.last_checkpoint.id : '',
                    company: contract.company ? contract.company.id : ''
                });
            });
    }

    private buildFormGroup() {
        this.formGroup = this._formBuilder.group({
            wacc: [{ value: '', disabled: true }, Validators.required],
            description: [{ value: '', disabled: true }, Validators.required],
            exchange_spread_rate: [{ value: '', disabled: true }, Validators.required],
            reimbursement_modal_id: [{ value: '', disabled: true }, Validators.required],
            standard_duties_collection_id: [{ value: '', disabled: true }, Validators.required],
            default: [{ value: '', disabled: true }, Validators.required],
            duty_invoice: [{ value: '', disabled: true }, Validators.required],
            duty_kind_of_charge: [{ value: '', disabled: true }, Validators.required],
            duties_collect_percent_fee: [{ value: '', disabled: true }, Validators.required],
            duties_pre_payment_add: [{ value: '', disabled: true }, Validators.required],
            services_currency_id: [{ value: '', disabled: true }, Validators.required],
            duties_currency_id: [{ value: '', disabled: true }, Validators.required],
            release_code_id: [{ value: '', disabled: true }, Validators.required],
            invoice_service_seperate_hold: [{ value: '', disabled: true }, Validators.required],
            invoice_duties_separate_hold: [{ value: '', disabled: true }, Validators.required],
            active: [{ value: '', disabled: true }, Validators.required],
            charge_mawb: [{ value: '', disabled: true }, Validators.required],
            mawb_price: [{ value: '', disabled: true }, Validators.required],
            charge_mawb_weight: [{ value: '', disabled: true }, Validators.required],
            mawb_weight_fist_day: [{ value: '', disabled: true }, Validators.required],
            mawb_weight_next_days: [{ value: '', disabled: true }, Validators.required],
            charge_hdl_to_mawb: [{ value: '', disabled: true }, Validators.required],
            hdl_price: [{ value: '', disabled: true }, Validators.required],
            charge_individual_storage: [{ value: '', disabled: true }, Validators.required],
            min_charge: [{ value: '', disabled: true }, Validators.required],
            max_charge: [{ value: '', disabled: true }, Validators.required],
            billing_schedule: [{ value: '', disabled: true }, Validators.required],
            deadline_billing: [{ value: '', disabled: true }, Validators.required],
            deadline_due_date: [{ value: '', disabled: true }, Validators.required],
            type: [{ value: '', disabled: true }, Validators.required],
            type_invoice: [{ value: '', disabled: true }, Validators.required],
            seller_key_mandae: [{ value: '', disabled: true }, Validators.required],
            duties_invoice_required: [{ value: '', disabled: true }, Validators.required],
            collect_tax_id: [{ value: '', disabled: true }, Validators.required],
            last_checkpoint: [{ value: '', disabled: true }, Validators.required],
            company: [{ value: '', disabled: true }, Validators.required],

            // Checkpoint
            checkpointFilter: [{ value: '', disabled: false }, Validators.required],
            checkpointLastFilter: [{ value: '', disabled: false }, Validators.required],

            // Company
            companyLastFilter: [{ value: '', disabled: false }],
        });
    }

    public initCheckpointFilter() {
        const checkpointFilter = this.formCheckpoint.controls.checkpointFilter;

        checkpointFilter.valueChanges.subscribe(() => {
            let search = checkpointFilter.value.toLowerCase();

            this.filteredCheckpoints.next(
                this.all.filter(checkpoint => checkpoint.code.toLowerCase().indexOf(search) > 1)
            );
        });

        const checkpointLastFilter = this.formCheckpoint.controls.checkpointLastFilter;

        checkpointLastFilter.valueChanges.subscribe(() => {
            let search = checkpointLastFilter.value.toLowerCase();

            this.filteredLastCheckpoints.next(
                this.all.filter(checkpoint => checkpoint.code.toLowerCase().indexOf(search) > 1)
            );
        });
    }

    public initCompanyFilter() {
        const companyLastFilter = this.formCompany.controls.companyLastFilter;

        companyLastFilter.valueChanges.subscribe(() => {
            let search = companyLastFilter.value.toLowerCase();

            this.filteredCompany.next(
                this.companies.filter(company => company.code.toLowerCase().indexOf(search) > 1)
            );
        });
    }
}
