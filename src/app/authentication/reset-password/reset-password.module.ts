import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import { FuseSharedModule } from '@fuse/shared.module';
import { ResetPasswordComponent } from './reset-password.component';
import { ResetPasswordService } from 'app/_services/_authentication/reset-password.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        ResetPasswordComponent
    ],
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        HttpClientModule,
        FuseSharedModule,
        MatIconModule,
    ],
    providers: [
        ResetPasswordService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ResetPasswordModule {
}
