import { map } from 'rxjs/operators';
import { UserService } from './../../_services/user/user.service';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { ResponseInterface } from 'app/_interfaces/Http/response.interface';
import { ResetPasswordService } from 'app/_services/_authentication/reset-password.service';
import { NotificationService } from 'app/_services/notification/notification.service';
import { UserListingRequest } from 'app/_interfaces/user/listing-element';
import { MatTableDataSource } from '@angular/material';



@Component({
    selector: 'user-update',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss'],
    animations: fuseAnimations
})
export class ResetPasswordComponent implements OnInit {
    private endpoint: string = 'users/reset-password';
    private currentId = null;
    public _update: boolean = false;
    resetPasswordForm: FormGroup;
    public formGroup: FormGroup;
    form: FormGroup;
    private returnUrl = 'login';
    public hide = true;


    public dataSource: MatTableDataSource<UserListingRequest>;
    public data: UserListingRequest[] = [];

    // Private
    public _unsubscribeAll: Subject<any>;

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route
     * @param {FormBuilder} _formBuilder
     * @param http 
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private router: Router,
        public service: UserService,
        private http: HttpAuthenticateService,
        private resetPasswordService: ResetPasswordService,
        private route: ActivatedRoute,
        private notificationService: NotificationService,
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {


        this.route.params.subscribe(params => {

            if (params) {
                this.fetchData(`/users/authenticated-user`);
            }
        })
        var email = this.data['email'];
        this.resetPasswordForm = this._formBuilder.group({

            name: [''],
            email: email,
            password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(100), confirmPasswordValidatorLenght]],
            passwordConfirm: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(100), confirmPasswordValidator, confirmPasswordValidatorLenght]]
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.resetPasswordForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.resetPasswordForm.get('passwordConfirm').updateValueAndValidity();
            });

    }

    /**
     * 
     * @param url 
     */
    private fetchData(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(user => {
                this.data = user;
            });
    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {

        var emailUser = this.data['email'];
        this.resetPasswordForm.controls.email = emailUser = this.data['email'];
        const controls = this.resetPasswordForm.controls;
        return this.update(controls);

    }

    public requestRecoveryLink() {

        var email = this.data['email'];
        this.resetPasswordForm.controls.email = email = this.data['email'];
        const controls = this.resetPasswordForm.controls;

        if (controls.password.value == null ||  controls.password.value == ''){
            this.notificationService.notify('A senha é obrigatória ! ', 'error');
            return null;
        }

        if (controls.passwordConfirm.value != controls.password.value){
            this.notificationService.notify('As senhas precisam ser idênticas ! ', 'error');
            return null;
        }

        var emailDomainValidation = email.indexOf('@');
        this.resetPasswordService.requestRecoveryLink(
            email,
            controls.password.value,

        )
            .subscribe(
                res => {
                    this.notificationService.notify('Senha alterada com sucesso !');
                    this.router.navigate([this.returnUrl]),
                    error => {
                        if (error.status == ResponseInterface.HTTP_UNAUTHORIZED) {
                            if (emailDomainValidation != -1) {
                                this.router.navigate([this.returnUrl]);
                                this.notificationService.notify('Senha alterada com sucesso !');
                            } else {
                                this.notificationService.notify('Error', 'error');
                            }

                        }
                    }
                }
            );
            this.logout();
    }

    /**
     * Remove user from local storage to log user out
     */
    public logout(): void {

        localStorage.removeItem('currentUser');
        this.router.navigate(['login']);
    }


    /**
     * Easy access to form fields
     * 
     */
    get controls() { return this.formGroup.controls; }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls() {

        const parsed = {
            "email": this.controls.email.value,
            "password": this.controls.password.value,
        }

        return parsed;
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {

        this.http.post(`${this.endpoint}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    public request() {
        const controls = this.resetPasswordForm.controls;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}


/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { 'passwordsNotMatching': true };
};

/**
 * Confirm password validator lenght
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidatorLenght: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (password.validator.length < 8) {
        return null;
    } else if (password.validator.length > 8) {
        return null;
    }

    if (password.validator.length > 49 || passwordConfirm.validator.length > 49) {
        return null;
    } else if (password.validator.length > 49 || passwordConfirm.validator.length > 49) {
        return null;
    }

    return { 'passwordsNotMatchingLenght': false };
};

/**
 * Confirm password validator lenght
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidatorNull: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');

    if (password.value === '') {
        return { 'passwordNull': true }
    }

    return { 'passwordsNotMatching': true };
};

