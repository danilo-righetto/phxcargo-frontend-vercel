import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthenticationService } from 'app/_services/_authentication/authentication.service';
import { Router } from '@angular/router';
import { NotificationService } from 'app/_services/notification/notification.service';
import { ResponseInterface } from 'app/_interfaces/Http/response.interface';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { Navigation } from 'app/navigation/navigation';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: fuseAnimations
})
export class LoginComponent implements OnInit {

    public loginForm: FormGroup;
    private returnUrl = 'dashboard';
    public isLoadingResults: boolean = false;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        private router: Router,
        private notificationService: NotificationService,
        private fuseNavigation: FuseNavigationService,
        private navigationService: Navigation,
        private snackBar: MatSnackBar
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    /**
     * On init
     */
    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

        this.loginForm.setValue({email: '', password: ''});
    }

    public login() {
        this.isLoadingResults = true;

        const controls = this.loginForm.controls;
        this.authenticationService.login(controls.email.value, controls.password.value)
            .subscribe(
                data => {

                    this.authenticationService.currentUserInformation().subscribe(user => {
                        this.authenticationService.setUser(user);
                        this.notificationService.notify('Login efetuado com sucesso !');
                        this.fuseNavigation.unregister('main');
                        this.fuseNavigation.register('main', this.navigationService.generate())
                        this.router.navigate([this.returnUrl]);
                        this.isLoadingResults = false;
                        this.snackBar.open('Seja bem-vindo(a): ' + user.name, 'PHX', {
                            duration: 2000,
                            horizontalPosition: "start",
                            verticalPosition: "bottom"
                        });
                    });
                },
                error => {

                    if (error.status == ResponseInterface.HTTP_UNAUTHORIZED) {
                        this.notificationService.notify('Usuário ou senha incorretos !', 'error');
                        this.snackBar.open('Por gentileza tente novamente.', 'PHX', {
                            duration: 2000,
                            horizontalPosition: "start",
                            verticalPosition: "bottom"
                        });
                    }
                    this.isLoadingResults = false;
                }
            );

    }

}