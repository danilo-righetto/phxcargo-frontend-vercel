import { map } from 'rxjs/operators';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpAuthenticateService } from 'app/_services/_authentication/http.authenticate.service';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { NotificationService } from 'app/_services/notification/notification.service';
import { ResponseInterface } from 'app/_interfaces/Http/response.interface';
import { ResetPasswordForgotService } from 'app/_services/_authentication/reset-password-forgot.service';



@Component({
    selector: 'reset-password-forgot',
    templateUrl: './reset-password-forgot.component.html',
    styleUrls: ['./reset-password-forgot.component.scss'],
    animations: fuseAnimations
})
export class ResetPasswordForgotComponent implements OnInit {
    public requestToken(token) {
        this.resetPasswordForgotService.requestToken(
            token
        );

    }
    private endpoint: string = 'users/reset-password-forget';
    private currentToken = null;
    public _update: boolean = false;
    resetPasswordForgotForm: FormGroup;
    public formGroup: FormGroup;
    form: FormGroup;
    private returnUrl = 'login';
    private token: string;
    private email: string;
    

    

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * 
     * Constructor
     * @param _fuseTranslationLoaderService 
     * @param route 
     * @param {FormBuilder} _formBuilder
     * @param http 
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private http: HttpAuthenticateService,
        private notificationService: NotificationService,
        private resetPasswordForgotService: ResetPasswordForgotService

    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    public ngOnInit(): void {

        /**
     * Lifecycle hook that is called after data-bound properties of a directive are
     * initialized.
     */

        this.route.params.subscribe(params => {

            if (params.token) {
                this.currentToken = params.token;
                var teste = this.resetPasswordForgotService.requestToken(`${this.currentToken}`);
                
            }
            
        });

        

        this.resetPasswordForgotForm = this._formBuilder.group({
            token: [this.currentToken, Validators.required],
            name: [''],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(100), confirmPasswordValidatorLenght]],
            passwordConfirm: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(100), confirmPasswordValidator, confirmPasswordValidatorLenght]]
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.resetPasswordForgotForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.resetPasswordForgotForm.get('passwordConfirm').updateValueAndValidity();
                
            });

    }

    /**
     * Submit form group (update|create)
     *
     */
    public submit(): void {

        const controls = this.parseControls();

        if (this._update) {
            return this.update(controls);
        }

    }

    /**
     * 
     * @param url 
     */
    private fetchToken(url: string) {

        this.http.get(url).pipe(
            map((res: any) => res.data))
            .subscribe(token => {
                token;
            });
    }

    public requestRecoveryLink() {

        const controls = this.resetPasswordForgotForm.controls;
        var email = controls.email.value;

        var emailDomainValidation = email.indexOf('@');
        this.resetPasswordForgotService.requestRecoveryLink(
            controls.email.value,
            controls.token.value,
            controls.password.value,

        )
            .subscribe(
                res => {
                    this.notificationService.notify('Senha alterada com sucesso !');
                    this.router.navigate([this.returnUrl]);
                    
                    
                 
                    error => {
                        if (error.status == ResponseInterface.HTTP_UNAUTHORIZED) {
                            this.notificationService.notify('Token expirado !', 'error');
                            if (emailDomainValidation != -1) {
                                this.router.navigate([this.returnUrl]);
                                this.notificationService.notify('Senha alterada com sucesso !');
                            } else {
                                this.notificationService.notify('Error', 'error');
                            }

                        }
                    }
                }, 
                err =>{ 
                    this.notificationService.notify('O Token está expirado !', 'error');
                    return null;
                }                
            );
            

    }

    /**
     * Easy access to form fields
     * 
     */
    get controls() { return this.formGroup.controls; }

    /**
     * Parse controls to expected data structure for request
     * 
     * @param controls 
     */
    public parseControls() {

        const parsed = {
            "email": this.controls.email.value,
            "password": this.controls.password.value,
        }

        return parsed;
    }

    /**
     * 
     * @param controls 
     */
    private update(controls: any): void {

        this.http.put(`${this.endpoint}/${this.currentToken}`, controls)
            .subscribe(res => this.router.navigate([this.endpoint]))
    }

    public request() {
        const controls = this.resetPasswordForgotForm.controls;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}


/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { 'passwordsNotMatching': true };
};

/**
 * Confirm password validator lenght
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidatorLenght: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (password.validator.length < 8) {
        return null;
    } else if (password.validator.length > 8) {
        return null;
    }

    if (password.validator.length > 49 || passwordConfirm.validator.length > 49) {
        return null;
    } else if (password.validator.length > 49 || passwordConfirm.validator.length > 49) {
        return null;
    }

    return { 'passwordsNotMatchingLenght': false };
};

/**
 * Confirm password validator lenght
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidatorNull: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');

    if (password.value === '') {
        return { 'passwordNull': true }
    }

    return { 'passwordsNotMatching': true };
};

