import { NgModule } from '@angular/core';
import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { ResetPasswordForgotComponent } from './reset-password-forgot.component';
import { ResetPasswordForgotService } from 'app/_services/_authentication/reset-password-forgot.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        ResetPasswordForgotComponent
    ],
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        HttpClientModule,
        FuseSharedModule
    ],
    providers: [
        ResetPasswordForgotService,
    ]
})
export class ResetPasswordForgotModule {
}
