import { NotificationService } from 'app/_services/notification/notification.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ResponseInterface } from 'app/_interfaces/Http/response.interface';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { RecoverPasswordService } from 'app/_services/_authentication/recover-password.service';
import { Router } from '@angular/router';

@Component({
    selector: 'forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss'],
    animations: fuseAnimations
})
export class ForgotPasswordComponent implements OnInit {

    form: FormGroup;
    requestPasswordForgotForm: FormGroup;
    private returnUrl = 'login';

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private recoverPasswordService: RecoverPasswordService,
        private notificationService: NotificationService,
        private router: Router,
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.requestPasswordForgotForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }

    public getControls(){
        let controls;
        return controls = this.requestPasswordForgotForm.controls;
    }

    public emailVerification(){
       
        const controls = this.getControls();
   
        let array = ['@'];
        let email = controls.email.value;
        let checkEmail = false;

            var index = 0;
              array.forEach(element => {
                let teste = email.indexOf(element);
                index++;
                if(teste > -1){
                    return checkEmail = true;
                }
                
            });

            return checkEmail;
    }


    public requestRecoveryLink() {
        let checkEmail;
        checkEmail = this.emailVerification();

         const controls = this.getControls();    
        
        if (checkEmail == false){
            this.notificationService.notify('Email inválido !', 'error');
            return null;
        }

        var email = controls.email.value;

        if (!email.indexOf('@')){
            this.notificationService.notify('Email inválido !', 'error');
            return null;
        }
        
        this.recoverPasswordService.requestRecoveryLink(
            controls.email.value,

        )
            .subscribe(
                res => {
                    this.notificationService.notify('Email de recuperação enviado com sucesso !');
                    this.router.navigate([this.returnUrl]),
                    error => {
                        if (error.status == ResponseInterface.HTTP_UNAUTHORIZED) {
                            if (checkEmail == true) {
                                this.router.navigate([this.returnUrl]);
                                this.notificationService.notify('Email de recuperação enviado com sucesso !');
                            } else if (checkEmail == false){
                                this.notificationService.notify('Error', 'error');
                            }

                        }
                    }
                    
                },
                err => {
                    this.notificationService.notify('Email não cadastrado !', 'error');
                    return null;
                } 
                
            );

    }


}
