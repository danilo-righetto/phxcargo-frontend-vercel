import { NgModule } from "@angular/core";
import { DefaultHeaderComponent } from "./_default/header/default.header.component";
import { DefaultTableComponent } from "./_default/data-list/table.component";
import { MatIconModule, MatCardModule, MatPaginatorModule, MatButtonModule, MatCheckboxModule, MatInputModule, MatGridListModule } from "@angular/material";
import { DefaultTableModule } from "./_default/data-list/table.module";
import { TranslateModule } from "@ngx-translate/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";


@NgModule({
    declarations: [
        DefaultHeaderComponent,

    ],
    imports: [
        RouterModule,
        CommonModule,
        MatIconModule,
        DefaultTableModule,
        MatButtonModule,
        MatCheckboxModule,
        MatInputModule,
        MatGridListModule,
    ],
    exports: [
        CommonModule,
        DefaultHeaderComponent,
        DefaultTableComponent,
        MatCardModule,
        MatIconModule,
        TranslateModule,
        MatPaginatorModule,
        DefaultTableModule,
        MatButtonModule,
        MatCheckboxModule,
        MatInputModule,
        MatGridListModule,
        RouterModule
    ],
})
export class DeclarationsModule {

}