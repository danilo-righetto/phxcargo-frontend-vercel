
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Component, Input, ViewChild } from "@angular/core";
import { MatTableDataSource, MatPaginator, MatSort } from "@angular/material";
import { SelectionModel } from "@angular/cdk/collections";
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { locale as portuguese } from './_i18n/pt';
import { IndexService } from "app/_services/index.service";
import { merge } from 'rxjs';
import { NotificationService } from "app/_services/notification/notification.service";
import { HttpAuthenticateService } from "app/_services/_authentication/http.authenticate.service";
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { Router } from '@angular/router';
import { saveAs as importedSaveAs } from "file-saver";

export const DATE_FORMAT = {
    parse: {
        dateInput: 'YYYY-MM-DD',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'default-table',
    styleUrls: ['./table.component.css'],
    templateUrl: './table.component.html',
    providers: [
        { provide: MAT_DATE_FORMATS, useValue: DATE_FORMAT },
    ]
})
export class DefaultTableComponent {
    public endpointScreen: any;

    @Input() public dataSource: MatTableDataSource<any>;
    @Input() public source: Array<any>;

    @Input() public columns: Array<any>;
    @Input() public displayedColumns: string[];
    @Input() public tooltipColumns: string[];
    @Input() public endpoint: string = '';
    @Input() public pagination: Array<any>;
    @Input() public service: IndexService;

    @Input() public actions: boolean = true;
    @Input() public allowRowSelection = true;
    @Input() public displayFilter: boolean = true;
    @Input() public disableControls: boolean = false;

    @Input() public itemsPerPageLabel: string = 'Itens por página';
    @Input() public nextPageLabel: string = 'Próxima página';
    @Input() public previousPageLabel: string = 'Página anterior';

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;


    public selection = new SelectionModel<any>(true, []);
    public resultsLength = 0;
    public pageSize = 10;
    public isLoadingResults = true;

    /**
     * Filtering
     *
     */
    @Input() public advancedFilter: boolean = false;
    @Input() public filterColumns;
    public filters = [];

    public date_from;
    public date_to;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private notification: NotificationService,
        private http: HttpAuthenticateService,
        private router: Router,
    ) {
        this._fuseTranslationLoaderService.loadTranslations(portuguese);
    }

    public ngOnInit(): void {

        this.endpointScreen = this.router.url;
        this.checkControls();
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        if (this.service) {
            this.fetchDataSource();
        } else if (this.source) {
            this.fetchRawSource(this.source);
        }

        if (this.allowRowSelection) {

            /**
             * Add selector coll to the begining of array
             */
            this.displayedColumns.unshift('select');
        }
        this.isLoadingResults = false;
    }

    /**
     *
     * @param filter
     */
    public fetchDataSource(filter?: string): void {

        this.selection.clear();

        this.paginator.page.observers = [];
        this.sort.sortChange.observers = [];

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    this.isLoadingResults = true;
                    return this.service!.fetchData(
                        filter,
                        this.paginator.pageSize,
                        this.paginator.pageSize * this.paginator.pageIndex,
                    );
                }),
                map(res => {
                    this.paginator._intl.nextPageLabel = this.nextPageLabel,
                        this.paginator._intl.itemsPerPageLabel = this.itemsPerPageLabel,
                        this.paginator._intl.previousPageLabel = this.previousPageLabel,
                        this.isLoadingResults = false;
                    this.resultsLength = res.meta['Meta-Filter-Count'];
                    return res.data;
                })
            ).subscribe(data => this.dataSource = new MatTableDataSource(data));
    }

    /**
     * Allow to add and fetch an raw source
     *
     * @param source
     */
    public fetchRawSource(source: Array<any>): void {

        this.dataSource = new MatTableDataSource(source);
        this.displayedColumns = this.columns;
        this.isLoadingResults = false;
    }

    /**
     *
     * @param filterValue
     */
    public applyFilter(filterValue: string) {

        if (filterValue == '') {
            this.service
            this.fetchDataSource();
            return;
        }

        if (this.service) {
            this.fetchDataSource(`id=${filterValue}`)
            return;
        }

        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    public applyAdvancedFilter() {

        if (!this.service || !this.filterColumns) {
            return;
        }
        let filterValue: string = '';

        this.filterColumns.forEach(filter => {

            if ("created_at" == filter.column && this.filters['date_from']) {
                filterValue = this.appendAdvancedFilter(filterValue, `created_at-min=${this.filters['date_from'].format('YYYY-MM-DD')}`);
            }
            if ("created_at" == filter.column && this.filters['date_to']) {
                filterValue = this.appendAdvancedFilter(filterValue, `created_at-max=${this.filters['date_to'].format('YYYY-MM-DD')}`);
            }

            if (this.filters[filter.column]) {
                filterValue = this.appendAdvancedFilter(filterValue, `${filter.column}=${this.filters[filter.column]}`);
            }
        });

        this.fetchDataSource(filterValue)
    }

    /**
     *
     * @param currentFilter
     * @param value
     */
    private appendAdvancedFilter(currentFilter: string, value: string): string {

        if (currentFilter) {
            value = `&${value}`;
        }

        return currentFilter + value;
    }

    /**
     * Whether the number of selected elements matches the total number of rows.
     *
     */
    public isAllSelected() {

        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /**
     * Selects all rows if they are not all selected; otherwise clear selection.
     *
     */
    public masterToggle() {

        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    /**
     * Return first selected row
     *
     */
    public getSelected() {

        return this.selection.selected[0];
    }

    /**
     * Bulk destroy
     *
     */
    public destroy() {

        this.isLoadingResults = true;
        const selectedRows = this.selection.selected;

        if (selectedRows.length == 0) {

            this.notification.notify('Nenhum registro foi selecionado !', 'error');
            this.isLoadingResults = false;
            return;
        }

        this.http.delete(
            `${this.endpoint}`, { ids: selectedRows.map(row => row['ID']) }
        ).pipe(catchError(err => this.isLoadingResults = false || err))
            .subscribe(res => {
                this.fetchDataSource();
                this.notification.notify('Registro deletado com sucesso !');
                this.isLoadingResults = false;
            })
    }

    /**
     * Generate Debit Note
     */
    public generateDebitNote() {
        this.isLoadingResults = true;
        const selectedRows = this.selection.selected;

        if (selectedRows.length == 0) {

            this.notification.notify('Nenhum registro foi selecionado', 'error');
            this.isLoadingResults = false;
            return;
        }

        this.http.post('/financial/debit-note', selectedRows)
            .subscribe(res => this.router.navigate(['/financial/debit-note']));
    }

    /**
     *
     * @param id
     */
    public downloadDebitNotePdf() {
        // this.isLoadingResults = true;
        const selectedRows = this.selection.selected;

        let url: string = this.endpoint + '/download/' + selectedRows.map(row => row.id)[0];

        this.http.get(url)
            .map((res: any) => res.data)
            .subscribe(res => {
                this.isLoadingResults = false;
                window.open(res.file)
            });
    }

    /**
     *
     * @param id
     */
    public generateDebitNoteBankSlip() {
        // this.isLoadingResults = true;
        const selectedRows = this.selection.selected;

        let url: string = this.endpoint + '/request-bank-slip/' + selectedRows.map(row => row.id)[0];

        this.http.get(url)
            .map((res: any) => res.data)
            .subscribe(res => {
                this.isLoadingResults = false;
                window.open(res.file)
            });
    }

    /**
     *
     * @param id
     * @param type
     * @param anvisa
     */
    public download(type: string, anvisa: boolean = false) {
        // this.isLoadingResults = true;
        const selectedRows = this.selection.selected;

        let url: string = this.endpoint + '/export';

        if (anvisa) {
            url = this.endpoint + '/anvisa/export';
        }

        this.http.post(url, { ids: selectedRows.map(row => row.id) })
            .map((res: any) => res.data)
            .subscribe(res => {
                this.isLoadingResults = false;
                res.files.forEach((url: string) => {
                    window.open(url)
                });
            });
    }

    public downloadManifest() {
        let idManifest = this.endpointScreen.split("/");
        this.downloadFile(
            this.http.endpoint(`/manifests/xml-download/${idManifest[3]}`),
            ''
        );
    }

    public downloadDeclaration() {
        let idManifest = this.endpointScreen.split("/");
        let ids = this.selection.selected.map(row => row['ID']);
        let body = `{\"airwaybills\":[${ids}]}`;
        this.downloadFile(
            this.http.endpoint(`/manifests/download-declaration-xml-house/${idManifest[3]}`),
            body
        );
    }

    public downloadPresence() {
        let idManifest = this.endpointScreen.split("/");
        let ids = this.selection.selected.map(row => row.id);
        let body = `{\"airwaybills\":[${ids}]}`;
        this.downloadFile(
            this.http.endpoint(`/manifests/download-presence-house/${idManifest[3]}`),
            body
        );
    }

    public downloadFile(url: any, body: any) {
        let idManifest = this.endpointScreen.split("/");
        fetch(url, {
            "method": "POST",
            "headers": {
                "Content-Type": "application/json",
                "Authorization": this.http.getToken()
            },
            "body": body
        })
            .then(response => {
                const contentDisposition = response.headers.get('content-disposition') == null ? 'erro-download' : response.headers.get('content-disposition');
                if (contentDisposition != 'erro-download') {
                    const rota = this.router;
                    response.blob().then(function (myBlob) {
                        const blob = new Blob([myBlob], { type: 'application/xml' });
                        importedSaveAs(blob, contentDisposition);
                    });
                } else {
                    if (url.includes('xml-download')) { alert('Não é possível fazer o download do Manifesto!') }
                    if (url.includes('download-declaration-xml-house') ||
                        url.includes('download-presence-house')) {
                        alert('Por gentileza envie o retorno do Manifesto.')
                    }
                }
            })
            .catch(err => {
                console.error(err);
            });
    }

    /**
     * Disable actions, selection and filter controls
     *
     */
    private checkControls(): void {

        if (!this.disableControls) {
            return;
        }

        this.actions = false;
        this.allowRowSelection = false;
        this.displayFilter = false;
    }
}