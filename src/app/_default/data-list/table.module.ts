import { NgModule } from "@angular/core";
import { DefaultTableComponent } from "./table.component";
import { MatTableModule, MatPaginatorModule, MatButtonModule, MatFormField, MatFormFieldModule, MatInputModule, MatCheckboxModule, MatProgressSpinnerModule, MatSortModule, MatSelectModule, MatDatepickerModule } from "@angular/material";
import { FuseSharedModule } from "@fuse/shared.module";
import { RouterModule } from "@angular/router";
import { MatPaginatorIntl } from '@angular/material/paginator';


@NgModule({
    declarations: [
        DefaultTableComponent,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        MatPaginatorModule,
        MatButtonModule,
        RouterModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatSortModule,
        MatSelectModule,
        MatDatepickerModule,
    ],
    exports: [
        DefaultTableComponent,
        MatPaginatorModule,
    ]
})
export class DefaultTableModule {
}