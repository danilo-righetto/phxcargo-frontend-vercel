import { NgModule } from "@angular/core";
import { OwlDateTimeModule, OwlDateTimeIntl, OWL_DATE_TIME_LOCALE, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { OwlMomentDateTimeModule } from 'ng-pick-datetime-moment';

/**
 * Define datetime picker lables
 * 
 */
export class DefaultIntl extends OwlDateTimeIntl {
    setBtnLabel = 'Selecionar';
    cancelBtnLabel = 'Cancelar';
};

export const MY_MOMENT_FORMATS = {
    parseInput: 'l LT',
    fullPickerInput: 'DD/MM/YYYY HH:mm',
    datePickerInput: 'l',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
};

@NgModule({
    exports: [
        OwlDateTimeModule,
        OwlMomentDateTimeModule,
    ],
    providers: [
        { provide: OwlDateTimeIntl, useClass: DefaultIntl },
        { provide: OWL_DATE_TIME_LOCALE, useValue: 'pt-BR' },
        { provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS },
    ],
})
export class DateTimeModule {}