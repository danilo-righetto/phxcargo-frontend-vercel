import { Component, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';


export interface DialogData {
  data: any;
}

/**
 * @title Injecting data when opening a dialog
 */
@Component({
  selector: 'dialog-company-create',
  templateUrl: './dialog-company-create.html',
})
export class DialogDataExample {
    public data: any;
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<DialogCompany>
  ) { }

  openDialogCreate() {
    this.dialog.open(DialogCompany, {
      data: {
        code: '',
      }
    });
  }
}

@Component({
  selector: 'dialog-company-create',
  templateUrl: './dialog-company-create.html',
})
export class DialogCompany {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) { }
}