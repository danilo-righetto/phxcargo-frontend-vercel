import { NgModule } from "@angular/core";
import { DialogCompany } from "./dialog-company-create.component";
import { DialogDataExample } from "./dialog-company-create.component";
import { MatTableModule, MatPaginatorModule, MatButtonModule, MatFormField, MatFormFieldModule, MatInputModule, MatCheckboxModule, MatProgressSpinnerModule, MatSortModule, MatSelectModule, MatDatepickerModule } from "@angular/material";
import { FuseSharedModule } from "@fuse/shared.module";
import { RouterModule } from "@angular/router";
import { MatPaginatorIntl } from '@angular/material/paginator';


@NgModule({
    declarations: [
        DialogCompany,
        DialogDataExample,
    ],
    imports: [
        FuseSharedModule,
        MatTableModule,
        MatPaginatorModule,
        MatButtonModule,
        RouterModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatSortModule,
        MatSelectModule,
        MatDatepickerModule,
    ],
    exports: [
        DialogCompany,
        DialogDataExample,
        MatPaginatorModule,
    ],
    entryComponents: [
        DialogCompany,
        DialogDataExample,
      ],
    
})
export class DialogCompanyModule {
}