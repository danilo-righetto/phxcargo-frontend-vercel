import { Component, Input } from "@angular/core";

@Component({
    selector: 'default-header',
    templateUrl: './default.header.component.html'
})
export class DefaultHeaderComponent {
    @Input() public title: string;
    @Input() public endpoint: string;
    @Input() public import: boolean = false;
    @Input() public first: string;
    @Input() public firstUrl: string;
    @Input() public second: string;
    @Input() public secondUrl: string;
}