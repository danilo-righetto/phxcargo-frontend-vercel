interface CityListingElement {
    uf: string
    code: string
    name: string
    ibge_code: string
}

export class CityRequest {
    uf: string
    code: string
    name: string
    ibge_code: string
}