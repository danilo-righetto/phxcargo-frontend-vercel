export interface CompanyListingElement {
    'ID': number,
    'Nome da Empresa': string,
    'Status': string,
    'Código': string,
    document: any,
    addresses: any,
    billing_addresses: any,
    fantasy_name: string;
    type_of_supplier_relationship: any,
    type_of_carrier_relationship: any,
    type_of_customer_relationship: any,
    
}

export interface CompanyListingRequest {
    id: number;
    account_number: string;
    name: string;
    kind_of_person: string;
    fantasy_name: string;
    status: string;
    code: string;
    document: any;
    documents: any;
    addresses: any;
    billing_addresses: any,
    type_of_supplier_relationship: any,
    type_of_carrier_relationship: any,
    type_of_customer_relationship: any,
}

export class Company implements CompanyListingElement { 

    constructor(private _company: CompanyListingRequest) { }

        get fantasy_name(){ return this._company.fantasy_name }

        get 'Nome da Empresa'() { return this._company.name }

        get 'Status'() { if (this._company.status == "ACTIVE"){
            return this._company.status = "Ativa"} else {
                return this._company.status = "Desativa";
            }
        }

        get 'ID'() {
            return this._company.id;
        }
        
        get id() {
          return this._company.id;
        }

        get 'Código'(){
            return this._company.code;
        }

        get document(){
            return this._company.documents ? this._company.documents[0].number: '';
        }

        get addresses(){
            return this._company.addresses[0];
        }

        get billing_addresses(){
            return this._company.addresses[1];
        }

        get type_of_customer_relationship(){
            return this._company.type_of_customer_relationship;
        }

        get type_of_supplier_relationship(){
            return this._company.type_of_supplier_relationship;
        }

        get type_of_carrier_relationship(){
            return this._company.type_of_carrier_relationship;
        }
}
