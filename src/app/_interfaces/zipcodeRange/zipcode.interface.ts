import { Items } from "./zipcode_items.interface";

export interface ZipCodeRangeListingElement {
    id: number;
    name: string;
    active: boolean;
    costs_id: number;
    companies_id: number;
    contract_id: number;
    tax_class_id: number;
    date_to: string;
    date_from: string;
    items: Array<any>;
}

export class ZipCodeRangeListingRequest {
    id: number;
    name: string;
    active: boolean;
    costs_id: number;
    companies_id: any;
    contract_id: number;
    tax_class_id: number;
    date_to: string;
    date_from: string;
    items: Array<any>;
}

export class ZipCodeRange implements ZipCodeRangeListingElement {

    public constructor(private _zipCode: ZipCodeRangeListingRequest) { }

    get id() {
        return this._zipCode.id;
    };

    get 'ID'() {
        return this._zipCode.id;
    };

    get name() {
        return this._zipCode.name;
    };

    get 'Nome'() {
        return this._zipCode.name;
    };

    get active() {
        return this._zipCode.active;
    };

    get 'Ativo'() {
        if (this._zipCode.active == true) {
            return 'Ativo';
        } else {
            return 'Inativo';
        }
    };

    get costs_id() {
        return this._zipCode.costs_id;
    };

    get companies_id() {
        return this._zipCode.companies_id;
    };

    get 'Empresa'() {
        if (this._zipCode.companies_id) {
            return this._zipCode.companies_id.code;
        }
        return 'Sem Empresa';
    };

    get contract_id() {
        return this._zipCode.contract_id;
    };

    get tax_class_id() {
        return this._zipCode.tax_class_id;
    };

    get date_to() {
        return this._zipCode.date_to;
    };

    get date_from() {
        return this._zipCode.date_from;
    };

    get 'Data De'() {
        var data = this._zipCode.date_from;

        data = FormataStringData(data)

        function FormataStringData(data) {
            var ano = data.split("-")[0];
            var mes = data.split("-")[1];
            var dia = data.split("-")[2];

            return ("0" + dia).slice(-2) + '/' + ("0" + mes).slice(-2) + '/' + ano;
        }

        return data;
    };

    get 'Data Para'() {
        var data = this._zipCode.date_to;
        data = FormataStringData(data)

        function FormataStringData(data) {
            var ano = data.split("-")[0];
            var mes = data.split("-")[1];
            var dia = data.split("-")[2];

            return ("0" + dia).slice(-2) + '/' + ("0" + mes).slice(-2) + '/' + ano;
        }
        return data;
    };

    get items() {
        
        if (!this._zipCode.items) {
            return [];
        }

        return this._zipCode.items.map(items => {
            return new Items(items)
        });
    }
}