export interface ItemsListingElement {
    id: number;
    item: number;
    type_service: string;
    uf: string;
    from_weigth: any;
    to_weigth: any;
    rate: any;
    add_rate: number;
    type_zone: number;
    modal: number;
    description: string;
    cep_from: string;
    cep_to: string;
    cep_ranges_id: any;
}

export class ItemsListingRequest {
    id: number;
    item: number;
    type_service: string;
    uf: string;
    from_weigth: any;
    to_weigth: any;
    rate: any;
    add_rate: number;
    type_zone: number;
    modal: number;
    description: string;
    cep_from: string;
    cep_to: string;
    cep_ranges_id: any;
}

export class Items implements ItemsListingElement {

    constructor(private _item: ItemsListingRequest) { }

    get id() {
        return this._item.id;
    }

    get item() {
        return this._item.item;
    }

    get 'Item'() {
        return this._item.item;
    }

    get type_service() {
        return this._item.type_service;
    }

    get 'Tipo de Serviço'() {
        return this._item.type_service;
    }

    get uf() {
        return this._item.uf;
    }

    get 'UFs'() {
        return this._item.uf;
    }

    get from_weigth() {
        return this._item.from_weigth;
    }

    get 'From Weight'() {
        return this._item.from_weigth;
    }

    get to_weigth() {
        return this._item.to_weigth;
    }

    get 'To Weight'() {
        return this._item.to_weigth;
    }

    get rate() {
        return this._item.rate;
    }

    get 'Taxa'() {
        return this._item.rate;
    }

    get add_rate() {
        return this._item.add_rate;
    }

    get 'Taxa Adicional'() {
        return this._item.add_rate;
    }

    get type_zone() {
        return this._item.type_zone;
    }

    get 'Tipo de Zona'() {
        return this._item.type_zone;
    }

    get modal() {
        return this._item.item;
    }

    get 'Modal'() {
        return this._item.item;
    }

    get description() {
        return this._item.description;
    }

    get 'Descrição'() {
        return this._item.description;
    }

    get cep_from() {
        return this._item.cep_from;
    }

    get 'CEP DE'() {
        return this._item.cep_from;
    }

    get cep_to() {
        return this._item.cep_to;
    }

    get 'CEP PARA'() {
        return this._item.cep_to;
    }

    get cep_ranges_id() {
        return this._item.cep_ranges_id;
    }
}