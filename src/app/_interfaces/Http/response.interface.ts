/**
 * Represents an HTTP response.
 * 
 */
export class ResponseInterface {

    static HTTP_OK = 200;
    static HTTP_CREATED = 201;
    static HTTP_NO_CONTENT = 204;
    static HTTP_UNAUTHORIZED = 401;
    static HTTP_NOT_FOUND = 404;
    static HTTP_METHOD_NOT_ALLOWED = 405;
    static HTTP_UNPROCESSABLE_ENTITY = 422;

    static statusTexts = {
        // 2XX
        '200': 'Ok',
        '201': 'Criado com sucesso',
        '204': 'Registro deletado',

        // 4XX
        '401': 'Ação não autorizada',
        '404': 'Página ou registro não encontrados',
        '405': 'Impossível realizar esta ação',
        '422': 'Algum dos campos não está de acordo com a requisição',
    }
}