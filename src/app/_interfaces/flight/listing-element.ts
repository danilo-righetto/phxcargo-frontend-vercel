export interface FlightListingRequest {
    id: number;
    number: string;
    alternative_number: any;
    airline: any; 
    origin: any;
    destination: any;
}


export interface FlightListingElement {
    'ID': number;
    'Número do Voo': string;
    'Número Alternativo': string;
    'Destino e Origem': any;
    'Companhia Aérea': any;


}

export class Flight implements FlightListingElement {

    constructor(private _flight: FlightListingRequest) { }

    get 'ID'() { return this._flight.id }

    get 'Nome do Aeroporto'() { return this._flight.airline.name }

    get 'Número Alternativo'() { return this._flight.alternative_number }

    get 'Número do Voo'() { return this._flight.number }

    get 'Destino'() { return (typeof this._flight.destination.airportId !== null && typeof this._flight.destination.airportId !== undefined ? this._flight.destination.airportId : 's/n')}

    get 'Destino e Origem'() { return (typeof this._flight.destination.airportCode !== null && typeof this._flight.destination.airportCode !== undefined ? this._flight.destination.airportCode : 's/n') + " - " + (this._flight.origin.airportCode ? this._flight.origin.airportCode : 's/n')}

    get 'Sigla do Aeroporto'() { return this._flight.origin.airportCode }

    get 'Companhia Aérea'() { return this._flight.airline.name }
}