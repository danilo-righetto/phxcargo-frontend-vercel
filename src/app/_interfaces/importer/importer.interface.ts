export interface ImporterListingRequest {
    id: string;
    name: string;
    documents: any;
    address: any;
}

export interface ImporterListingElement {
    'ID': string,
    'Nome do Importador': string,
    'Documento do Importador': string,
}

export class Importer implements ImporterListingElement {

    constructor(private _importer: ImporterListingRequest) { }

    get 'ID'() { return this._importer.id }

    get 'Nome do Importador'() { return this._importer.name }

    get 'Documento do Importador'() { return this._importer.documents ? this._importer.documents[0].number : ''; }


}