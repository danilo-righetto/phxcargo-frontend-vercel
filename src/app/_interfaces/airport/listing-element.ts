export interface AirportListingElement {
    'ID': string;
    'Nome do Aeroporto': string;
    'Sigla do Aeroporto': string;
}

export interface AirportListingRequest {
    id: string;
    name: string;
    code: string;
}

export class Airport implements AirportListingElement {

    constructor(private _airport: AirportListingRequest) { }

    get 'ID'() { return this._airport.id }

    get 'Nome do Aeroporto'() { return this._airport.name }

    get 'Sigla do Aeroporto'() { return this._airport.code }

}