export interface CurrencyListingElement {
    'ID': number;
    'Nome da Moeda': string;
    'Código da Moeda': string;
    'Símbolo da Moeda': string;
}

export class CurrencyRequest {
    id: number;
    name: string;
    code: string;
    symbol: string;
}

export class Currency implements CurrencyListingElement {

    constructor(private _currency: CurrencyRequest) { }

    get 'ID'(): number { return this._currency.id; }

    get 'Nome da Moeda'(): string { return this._currency.name; }

    get 'Código da Moeda'(): string { return this._currency.code }

    get 'Símbolo da Moeda'(): string { return this._currency.symbol }

}