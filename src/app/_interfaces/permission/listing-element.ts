export interface PermissionListingElement {
    id: number;
    name: string;
    display_name: string;
    description: string;
}