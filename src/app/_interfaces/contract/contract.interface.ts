import { Service } from "./contract_service.interface";

export interface ContractListingElement {
    'ID': any;
    'Empresa': any;
    //code: string;
    'WACC': number;
    'Taxa de Câmbio': number;
    'Último Checkpoint': any;
    'Padrão': any;
    services: Array<any>;
}

export class ContractRequest {
    id: any;
    description: string;
    code: string;
    wacc: number;
    exchange_spread_rate: number;
    last_checkpoint: string;
    default: boolean;
    services: Array<any>;
}

export class Contract implements ContractListingElement {

    constructor(private _contract: ContractRequest) { }

    get 'ID'() {
        return this._contract.id;
    }
    get 'Empresa'() {
        return this._contract.description;
    }
    get 'WACC'() {
        return this._contract.wacc;
    }
    get 'Taxa de Câmbio'() {
        return this._contract.exchange_spread_rate;
    }
    get 'Último Checkpoint'() {
        if (!this._contract.last_checkpoint) {
            return this._contract.last_checkpoint['code'];
        }
        return this._contract.last_checkpoint['code'];
    }
    get 'Padrão'() {
        if (this._contract.default == false){
            return "Não";
        } else {
            return "Sim";
        }
    } 
    get services() {

        if (!this._contract.services) {
            return [];
        }

        return this._contract.services.map(service => {
            return new Service(service)
        });
    }
}