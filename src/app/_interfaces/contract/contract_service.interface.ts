export interface ServiceListingElement {
    'ID': number;
    'Descrição': string;
    'Nível de Serviço': string;
    'Checkpoint': any;
    'Tipo de Carga': string;
    'Aproximação': number;
    'Quantidade': number;
    checkpoint: any;
    description: any;
    service_level_agreement: any;
    charge_type: any;
    approach: any;
    amount: any;
}

export class ServiceListingRequest {
    id: number;
    description: string;
    service_level_agreement: string;
    last_checkpoint: any;
    charge_type: string;
    approach: number;
    amount: number;
    service_id: number;
    checkpoint: any;
}

export class Service implements ServiceListingElement {

    constructor(private _service: ServiceListingRequest) { }

    get 'ID'() {
        return this._service.id;
    }
    get 'Descrição'() {
        return this._service.description;
    }
    get 'Nível de Serviço'() {
        return this._service.service_level_agreement;
    }
    get 'Checkpoint'() {
        if (!this._service.checkpoint) {
            return this._service.checkpoint.code;
            
        }
        return this._service.checkpoint.code;
    }
    get 'Tipo de Carga'() {
        return this._service.charge_type;
    }
    get 'Aproximação'() {
        return this._service.approach;
    }
    get 'Quantidade'() {
        return this._service.amount;
    }

    get description() {
        return this._service.description;
    }
    get service_level_agreement() {
        return this._service.service_level_agreement;
    }
    get charge_type() {
        return this._service.charge_type;
    }
    get approach() {
        return this._service.approach;
    }
    get amount() {
        return this._service.amount;
    }

    get checkpoint() {
        if (!this._service.checkpoint) {
            return this._service.checkpoint.code;
            
        }
        return this._service.checkpoint.code;
    }
    
}