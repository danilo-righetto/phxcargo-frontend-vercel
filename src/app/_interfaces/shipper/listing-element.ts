export interface ShipperListingRequest {
    id: string;
    name: string;
    documents: any;
}

export interface ShipperListingElement {
    'ID': string;
    'Nome do Embarcador': string,
    'Documento do Embarcador': any,
}

export class Shipper implements ShipperListingElement {

    constructor(private _shipper: ShipperListingRequest) { }

    get 'ID'() {
        return this._shipper.id;
    }

    get 'Nome do Embarcador'() { return this._shipper.name }

    get 'Documento do Embarcador'() {
        return this._shipper.documents ? this._shipper.documents[0].number : '';
    }

}