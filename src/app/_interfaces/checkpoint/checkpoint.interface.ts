export interface CheckpointListingElement {
    'Código do Checkpoint': string,
    'Nome do Checkpoint': string,
    'ID': any,
    name: string,
    code: string,
    id: string,
}

export interface CheckpointListingRequest {
    id: any,
    code: string,
    name: string,
}

export class Checkpoint implements CheckpointListingElement {

    constructor(private _checkpoint: CheckpointListingRequest) {}

    get code () { return this._checkpoint.code }

    get name () { return this._checkpoint.name }

    get id () { return this._checkpoint.id }

    get 'Código do Checkpoint'() { return this._checkpoint.code }

    get 'Nome do Checkpoint'() { return this._checkpoint.name }

    get 'ID'() { return this._checkpoint.id }
}