export class Phone {
    id: number;
    number: string;
    type: string;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}