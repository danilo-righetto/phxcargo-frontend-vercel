/**
 * House AirWaybill
 */
export interface HouseListingElement {

    'ID': number;
    'Número': string;
    'ID Externo': string;
    'Peso': any;
    'Quantidade': any;
    'Endereço': string;
    'Cidade': string;
    address_cities_id: number;
    'Código Postal': string;
    'Estado': string;
    importer: any;
    shipper: any;
}

export class AirwaybillRequest {
    id: number;
    code: string;
    external_identifiers: Array<{ external_id: string }>;
    gross_weight: number;
    total_amount: number;
    address: any;
    statuses: any;
    events: any;
    master: { code: string, external_identifiers: Array<{ external_id: string }> };

    checkpoints: Array<any>;

    customs_settings: {
        customs_adm: { code: number, name: string },
        tax_class: { code: number, description: string }
    };

    fines: Array<{
        code: string
        amount: number
    }>;

    importer: any;
    shipper: any;
}

export class Airwaybill implements HouseListingElement {

    constructor(private _airwaybill: AirwaybillRequest) { }

    get 'ID'(): number { return this._airwaybill.id; }

    get 'Número'(): string { return this._airwaybill.code; }

    get 'ID Externo'(): string {
        if (this._airwaybill.external_identifiers) {
            return this._airwaybill.external_identifiers[0].external_id;
        }
    }

    get Cliente(): string {
        if (this._airwaybill.external_identifiers) {
            return this._airwaybill.external_identifiers[0].external_id;
        }
    }

    get Master(): string {
        if (!this._airwaybill.master || !this._airwaybill.master.external_identifiers) {
            return;
        }

        return this._airwaybill.master.external_identifiers[0].external_id;
    }

    get 'Peso'(): string {
        return this._airwaybill.gross_weight.toLocaleString('pt-Br');
    }

    get 'Quantidade'(): string {
        return this._airwaybill.total_amount.toLocaleString('pt-Br');
    }

    get Configuracao(): any {
        return this._airwaybill.customs_settings;
    }

    get 'Endereço'(): string {
        if (!this.hasAddress()) {
            return;
        }

        return this._airwaybill.address.address;
    }
    get 'Cidade'(): string {
        if (!this.hasAddress()) {
            return;
        }

        return this._airwaybill.address.city;
    }
    get address_cities_id(): number {
        if (!this.hasAddress()) {
            return;
        }

        return this._airwaybill.address.cities_id;
    }
    get 'Código Postal'(): string {
        if (!this.hasAddress()) {
            return;
        }

        return this._airwaybill.address.zip_code;
    }
    get 'Estado'() {
        if (!this.hasAddress()) {
            return;
        }

        return this._airwaybill.address.state;
    }

    get Status() {

        if (!this._airwaybill.statuses) {
            return '';
        }

        const statuses: Array<any> = this._airwaybill.statuses;

        return statuses[statuses.length - 1].name;
    }

    get 'Último Checkpoint'() {
        if (!this._airwaybill.checkpoints) {
            return '';
        }

        const checkpoints: Array<any> = this._airwaybill.checkpoints;

        return checkpoints[checkpoints.length -1].code;
    }

    get fines() {
        if (!this._airwaybill.fines) {
            return;
        }

        return this._airwaybill.fines
    }
    
    get events() {
        if (!this._airwaybill.events) {
            return;
        }

        return this._airwaybill.events;
    }

    get importer() {
        if (!this._airwaybill.importer) {
            return;
        }

        return this._airwaybill.importer;
    }

    get shipper() {
        if (!this._airwaybill.shipper) {
            return;
        }

        return this._airwaybill.shipper;
    }

    private hasAddress(): boolean {
        return this._airwaybill.address;
    }
}