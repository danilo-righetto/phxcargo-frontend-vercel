export interface ConsolidatorListingElement {
    'ID': number,
    'Código da Bag': string,
    'Origem': string;
    'Nome da Empresa': string;
    'Destino da Bag': string;
    'Data': any;
    "Empresa": any;
}

export class ConsolidatorListingRequest {
    id: number;
    code: string;
    origin: string;
    company: any;
    destination: string;
    date: any;
}
