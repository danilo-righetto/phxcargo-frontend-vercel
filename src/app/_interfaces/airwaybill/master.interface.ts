import { CompanyListingElement } from "../company/listing-element";

/**
 * Structure representation of master request
 * 
 */
export interface MasterAirwaybillRequest {
  id: number;
  code: string;
  description: any;
  gross_weight: number;
  external_identifiers: Array<{ external_id: string }>;
  company_branch: any;
  company: any;
  flight: any;
  bag: any;
  contract: any;
  airline: any;
}

/**
 * Master AirWaybill
 */
export interface MasterLitingElement {

    'ID': number;
    'Número': string;
    Descricao: string;
    Bruto: number;
    'Data de Saída': string;
    'Origem do Aeroporto': string;
    'Destino do Aeroporto': string;
    'ID Externo': string;
    'Empresa': any;
    'Filial': any;
    'Contrato': any;
    'Companhia Aerea': any;
}

export class MasterAirwaybill implements MasterLitingElement {

    constructor(private _master: MasterAirwaybillRequest) { }

    get 'ID'() { return this._master.id }

    get company_id() { return this._master.company['ID'] }

    get 'Número'() { return this._master.code }

    get Descricao() { return this._master.description }

    get Bruto() { return this._master.gross_weight }

    get 'ID Externo'() {
        if (this._master.external_identifiers) {
            return this._master.external_identifiers[0].external_id
        }
    }

    get 'Data de Saída'() {
        if (this._master.flight && this._master.flight.origin) {
            return this._master.flight.origin.date;
        }
    }

    get 'Origem do Aeroporto'() {
        if (this._master.flight && this._master.flight.origin) {
            return this._master.flight.origin.airportCode
        }
    }

    get 'Destino do Aeroporto'() {
        if (this._master.flight && this._master.flight.destination) {
            return this._master.flight.destination.airportCode
        }
    }
  
    get 'Empresa'() {
      if (this._master.company) {
        return this._master.company;
      }
    }
  
    get 'Filial'() {
      if (this._master.company_branch) {
        return this._master.company_branch;
      }
    }
  
    get 'Contrato'() {
       if (this._master.contract) {
         return this._master.contract;
       }
    }
  
  get 'Companhia Aerea'() {
    if (this._master.airline) {
      return this._master.airline;
    }
  }
}