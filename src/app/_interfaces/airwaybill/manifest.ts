import { MasterAirwaybillRequest, MasterAirwaybill } from "./master.interface";

export interface ManifestListingElement {
    'ID': number;
    'Número': string;
    'Descrição': string;
    'ID Externo': string;
    masters: Array<MasterAirwaybill>;
    files: Array<any>
}

export class ManifestRequest {
    id: number;
    code: string;
    description: string;
    files: Array<any>
    external_identifiers: Array<{ external_id: string }>;
    masters: Array<MasterAirwaybillRequest>;
}

export class Manifest implements ManifestListingElement {

    constructor(private _manifest: ManifestRequest) { }

    get 'ID'(): number { return this._manifest.id; }

    get 'Número'(): string { return this._manifest.code; }

    get 'Descrição'(): string { return this._manifest.description }

    get 'ID Externo'(): string {
        if (this._manifest.external_identifiers) {
            return this._manifest.external_identifiers[0].external_id;
        }
    }

    get files() {
        return this._manifest.files;
    }

    get masters() {

        return this._manifest.masters.map(master => {
            return new MasterAirwaybill(master)
        })
    }
}