import { RoleListingRequest as Role } from "../role/listing-element";

export interface UserListingRequest {
    id: number;
    name: string;
    email: string;
    role: Role;
    company: any;
}

export interface UserListingElement {
    'ID': number,
    'Nome do Usuario': string,
    'Email': string,
    'Permissão': string,
    'Empresa Vinculada': string,
}

export class User implements  UserListingElement {

    constructor(private _user: UserListingRequest) { }

    get 'ID'() { return this._user.id }

    get 'Nome do Usuario'() { return this._user.name }

    get 'Email'() { return this._user.email }

    get 'Permissão'() { return this._user.role.name }
    
    get 'Empresa Vinculada'() {
        if (this._user.company) {
            return this._user.company.name
        }
        return 'Sem empresa';
    }
    
}