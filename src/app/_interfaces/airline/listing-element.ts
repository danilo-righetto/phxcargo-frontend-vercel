export interface AirlineListingElement {
    'ID': number;
    'Companhia Aérea': string;
}

export class AirlineListingRequest {
    id: number;
    name: string;
}

export class Airline implements AirlineListingElement {

    constructor(private _airline: AirlineListingRequest) { }

    get 'ID'() { return this._airline.id }

    get 'Companhia Aérea'() { return this._airline.name }


}