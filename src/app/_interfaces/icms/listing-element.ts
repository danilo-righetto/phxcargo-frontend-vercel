export interface IcmsListingElement {
    'Estado do ICMS': string,
    'Alíquota do ICMS': string,
}

export interface IcmsListingRequest {
    id: number;
    state: string;
    aliquot: string;
}

export class Icms implements IcmsListingElement {

    constructor(private _icms: IcmsListingRequest) { }

    get 'Estado do ICMS'() { return this._icms.state }

    get 'Alíquota do ICMS'() { return this._icms.aliquot + ' %' }
}