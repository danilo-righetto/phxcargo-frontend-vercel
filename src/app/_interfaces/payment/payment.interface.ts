
export interface PaymentListingElement {
    'Nome': string,
}

export interface PaymentListingRequest {
    id: number
    name: string,
}

export class Payment {

    constructor(private _payment: PaymentListingRequest) { } 

    get 'Nome'() { return this._payment.name }
}