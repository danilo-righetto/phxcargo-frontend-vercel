import { PermissionListingElement as Permission } from "../permission/listing-element";

export interface RoleListingRequest {
    id: number;
    name: string;
    description: string;
    admin: boolean;
    permissions: Array<Permission>;
}

export interface RoleListingElement {
    'ID': number,
    'Nome da Permissão': string,
    'Descrição da Permissão': string,
    'Administrador': string,
}

export class Role implements RoleListingElement {

    constructor(private _role: RoleListingRequest) { }

    get 'ID'() { return this._role.id }

    get 'Nome da Permissão'() { return this._role.name }

    get 'Descrição da Permissão'() { return this._role.description }

    get 'Administrador'() { return this._role.admin ? 'Sim' : 'Não' }

}