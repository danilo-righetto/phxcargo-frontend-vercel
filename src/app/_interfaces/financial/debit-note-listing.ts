export interface DebitNoteListingElement {
    'ID': number;
    'Código': string;
    'Valor Total': string;
    Empresa: string;
    'Linha Digital': string;
    Pago: any;
}

export class DebitNoteListingRequest {
    id: number;
    code: string;
    total_amount: string;
    company: string;
    digitable_line: string;
    paid_at: any;
}