export interface FineListingElement {
    'ID': number;
    'Código': string;
    'Descrição da Multa': string;
    'Código da Multa': string;
    'Quantidade': number;
    'Data de Criação': string;
}

export class FineListingRequest {
    id: number;
    code: string;
    fine_description: string;
    fine_code: string;
    amount: number;
    created_at: string;
}