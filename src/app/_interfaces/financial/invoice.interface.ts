import { MasterAirwaybillRequest, MasterAirwaybill } from "../airwaybill/master.interface";

export interface InvoiceListingElement {
    'ID': number;
    'Código': string;
    'Descrição': string;
    'Valor Total': number;
    'Data de Criação': string;
    'Pagamento': number;
    'ID Externo': Array<any>;
    masters: Array<any>;
    charges: Array<any>;
}

export class InvoiceRequest {
    id: number;
    code: string;
    description: string;
    total_amount: number;
    created_at: string;
    paid: number;
    masters: Array<MasterAirwaybillRequest>;
    charges: Array<any>;
}

export class Invoice implements InvoiceListingElement {

    constructor(private _invoice: InvoiceRequest) {

    }

    get 'ID'(): number {
        return this._invoice.id;
    }
    get 'Código'(): string {
        return this._invoice.code;
    }
    get 'Descrição'(): string {
        return this._invoice.description;
    }
    get 'Valor Total'(): any {
        return this._invoice.total_amount;
    }
    get 'Data de Criação'(): string {
        return this._invoice.created_at;
    }
    get 'Pagamento'(): any {
        if (this._invoice.paid == 0) {
            return "Pendente";
        } else {
            return "Pago";
        }
    }
    get masters() {

        return this._invoice.masters.map(master => {
            return new MasterAirwaybill(master)

        });
    }

    get 'ID Externo'(): any {

        return this._invoice.masters[0].external_identifiers[0].external_id;

    }

    get charges() {

        if (!this._invoice.charges) {
            return []
        }

        return this._invoice.charges.map(charge => {
            return {
                'ID': charge.id,
                'Serviço': charge.service,
                'House': charge.house,
                'Quantidade': charge.amount,
                'Data de criação': charge.created_at,
            }
        });
    }
}