# Phxcargo - Frontend

Aplicação [**phxcargo 4.0**](https://hml.phxcargo.net/) em `Angular 8.0.2`.

## Ambiente de desenvolvimento

No nosso ambiente de desenvolvimento estamos usando **Docker** para facilitar as configurações e instalações de pacotes necessário para bom funcionamento do sistema.

### Versão do Angular

É necessário ter o **Angular CLI** na versão `8.0.3`, o **Node JS** na versão `14.16.0` ou superior.
Além do **angular-devkit** na versão `0.800.3` para que a aplicação funcione corretamente.

### Baixando o projeto:

```bash
$ git clone git@gitlab.com:phxcargo/phxcargo-frontend.git
$ cd phxcargo-frontend/
```

### Iniciando o projeto com Docker

```bash
$ docker-compose build
$ docker-compose up -d
```

### Iniciando o projeto sem Docker

```bash
$ npm install
$ ng serve
```

**Agora basta acessar a URL**: `http://localhost:4200`

### Complilando para Produção

```bash
$ npm run build-prod
```

Após isso veja que a pasta `dist` é criada com todo o projeto compilado para produção.

## Utilizando o Gitflow

`Gitflow Workflow` é um design de fluxo de trabalho `Git` que foi publicado e popularizado pela primeira vez por **Vincent Driessen** no nvie. O Gitflow Workflow define um modelo de ramificação rigoroso projetado com base no lançamento do projeto. Isto oferece uma estrutura robusta para gerenciar projetos maiores.

O **Gitflow** é ideal para projetos que têm um ciclo de lançamento agendado. Este fluxo de trabalho não adiciona novos conceitos ou comandos além do necessário para o Fluxo de trabalho de ramificação de recurso. Em vez disso, ele atribui **funções bem específicas para diferentes ramificações** e define quando elas devem interagir.

**Para mais informações acesse**: [como funciona o gitflow](https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow)

**Para entender como trabalhar com o gitflow acesse**: [cheatsheet do git-flow](https://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html)

### Instalando o Gitflow

**Para linux:**

```bash
$ apt-get install git-flow
```

**Para windows:**

```bash
$ wget -q -O - --no-check-certificate https://raw.github.com/petervanderdoes/gitflow-avh/develop/contrib/gitflow-installer.sh install stable | bash
```

### Inicializando o Gitflow

Execute o comando abaixo:

```bash
$ git flow init
```

**Importante**: sempre que uma **tarefa for finalizada**, **uma versão for gerada** ou **um bugfix ou hotfix for resolvido**, o arquivo `CHANGELOG.md` deve ser atualizado!

### Trabalhando com o Gitflow

Iniciando uma nova `feature` (**funcionalidade**) baseada em uma **tarefa** ou **chamado**: 

```bash
$ git flow feature start nome-da-tarefa
```

Gerando uma **nova versão** da aplicação (com o número da versão) :

```bash
$ git flow release start v1.0.0
```

## IDE

Visual Studio Code - [Download](https://code.visualstudio.com/Download)

Configurações

```json
{
  "editor.minimap.enabled": false,
  "javascript.updateImportsOnFileMove.enabled": "always",
  "editor.tabSize": 2,
  "editor.fontSize": 14,
  "editor.lineHeight": 24,
  "editor.fontFamily": "Fira Code",
  "editor.fontLigatures": true,
  "files.autoSave": "off",
  "workbench.colorTheme": "Dracula"
}
```

`Extensões` do visual studio code:

- [Theme Dracula](https://marketplace.visualstudio.com/items?itemName=dracula-theme.theme-dracula)
- [Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)
- [Rocketseat ReactJS](https://marketplace.visualstudio.com/items?itemName=rocketseat.RocketseatReactJS)
- [Rocketseat React Native](https://marketplace.visualstudio.com/items?itemName=rocketseat.RocketseatReactNative)
- [FiraCode](https://github.com/tonsky/FiraCode)

## License
Todos os direitos desse projeto são reservados da empresa **Parcel Handling Express**.