const path = require('path');

  module.exports = {
    mode: 'development',
    entry: {
      app: './src/main.ts',
    },
    devtool: 'cheap-module-source-map',
    devServer: {
        port: '3000',
        host: 'localhost',
        historyApiFallback: true,
        watchOptions: {
          ignored: /node_modules/
        }
      },
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist')
    }
  };