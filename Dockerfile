# base image
FROM node:12.2.0

# install chrome for protractor tests
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
# RUN apt-get update && apt-get install -yq google-chrome-stable

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular-devkit/build-angular@0.800.3 @angular/cli@8.0.3 --unsafe

# add app
COPY . /app

# build app
RUN node --max_old_space_size=6144
# CMD export NODE_OPTIONS="--max-old-space-size=8192"
# RUN ng build

# start app
# CMD ng serve --host 0.0.0.0 --aot
CMD ng serve --max_old_space_size=6144 --aot
